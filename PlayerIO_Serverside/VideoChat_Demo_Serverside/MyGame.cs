﻿using PlayerIO.GameLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

public class MyPlayer : BasePlayer 
{
    public bool isStarted;
}

[RoomType("RoomAdminMobil")]
public class MyGame : Game<MyPlayer>
{
    private List<MyPlayer> players;

    #region Message Types
    private const string FirstConnectionMessageType = "FirstConnection";
    private const string GetFirstAdminDataMessageType = "GetFirstAdminData";
    private const string CheckingConnectionMessageType = "CheckingConnection";

    private const string GetGarageAndAppraiserDatasMessageType = "GetGarageAndAppraiserDatas";
    private const string GetGarageDatasMessageType = "GetGarageDatas";
    private const string GetAppraiserDatasMessageType = "GetAppraiserDatas";
    private const string GetUpdateCardInfoMessageType = "UpdateCardInfo";

    private const string AddNewEmailMessageType = "AddNewEmail";
    private const string RemoveFromEmailsMessageType = "RemoveFromEmails";
    private const string AddDocumentToAppraiserMessageType = "AddDocumentToAppraiser";
    private const string SendSetPlayFabIDMessageType = "SetPlayFabID";
    private const string SendGetPlayFabIDMessageType = "GetPlayFabID";

    private const string SearchCardsMessageType = "SearchCards";
    private const string AddCardsToAdminBlockMessageType = "AddCardsToAdminBlock";
    private const string GetFullCardDataMessageType = "GetFullCardData";
    private const string ChangeCardDataMessageType = "ChangeCardData";

    private const string CreateAppraiserMessageType = "CreateAppraiser";
    private const string CreateGarageMessageType = "CreateGarage";
    private const string DeleteAppraiserMessageType = "DeleteAppraiser";
    private const string DeleteGarageMessageType = "DeleteGarage";
    private const string ChangeAppraiserDataMessageType = "ChangeAppraiserData";
    private const string ChangeGarageDataMessageType = "ChangeGarageData";

    private const string GetBlocksDataMessageType = "GetBlocksData";
    private const string ConnectAppraiserToCardMessageType = "ConnectAppraiserToCard";
    private const string ChangeAppraiserInCardMessageType = "ChangeAppraiserInCard";
    private const string ChangeGarageInCardMessageType = "ChangeGarageInCard";
    private const string ChangeCardSignalMessageType = "ChangeCardSignal";
    private const string SkipFirstCardCheckMessageType = "SkipFirstCardCheck";
    private const string CloseCardMessageType = "CloseCard";
    private const string CreateCardMessageType = "CreateCard";

    private const string ManualRemoveUnderCardFromBlockMessageType = "ManualRemoveUnderCardFromBlock";
    private const string ManualBusyUnderCardInBlockMessageType = "ManualBusyUnderCardInBlock";

    private const string DebugMessageType = "Debug";
    #endregion

    private Timer currentScheldueTask;

    private const int inactiveCardTime = 3600;

    public override void GameStarted()
    {
        players = new List<MyPlayer>();
        currentScheldueTask = ScheduleCallback(SendCheckConnection, 7000); // после первого подключения через 7секунд будет разослана проверка коннекта
    }

    #region Checking Stable Connection
    private void SendCheckConnection()
    {
        foreach (MyPlayer pl in players)
        {
            if(pl.isStarted)
                pl.Send(CheckingConnectionMessageType, true);
        }
        currentScheldueTask.Stop();
        currentScheldueTask = ScheduleCallback(SendCheckConnection, 7000); // после того как разослали проверку, повторная будет через 7 сек
    }
    #endregion

    public override void UserJoined(MyPlayer player)
    {
        if (players.Count > 0)
        {
            //PlayerIO.ErrorLog.WriteError("WOW, cannot be more than 1 admin mobil in a room " + players.Count);

            //player.Disconnect();
            //return;
        }
        players.Add(player);

        SendFirstData(player);
    }

    #region Send First Connect Data
    private void SendFirstData(MyPlayer player)
    {
        if (!player.JoinData.ContainsKey("password"))
        {
            string errorMessage = "AdminMobil: does not contains 'password' in JoinData";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(FirstConnectionMessageType, false,
         errorMessage);
            return;
        }
        string password = player.JoinData["password"];

        if (!player.JoinData.ContainsKey("accountType"))
        {
            string errorMessage = "AdminMobil: does not contains 'accountType' in JoinData";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(FirstConnectionMessageType, false,
         errorMessage);
            return;
        }
        string accountType = player.JoinData["accountType"];

        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
                  (adminBigData) =>
                  {
                      if (adminBigData == null)
                      {
                          string errorMessage = "SendFirstData 'Load' Admin success, but 'adminBigData' is null!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }

                      string rightPassword = adminBigData.GetString("password");
                      if (rightPassword != password)
                      {
                          string errorMessage = "SendFirstData 'Load' Admin success, but 'password' " + password + " is wrong!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }

                      string rightAccountType = adminBigData.GetString("accountType");
                      if (rightAccountType != accountType)
                      {
                          string errorMessage = "SendFirstData 'Load' Admin success, but 'accountType' " + accountType + " is wrong!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }

                      object temp;
                      if (!adminBigData.TryGetValue("emails", out temp))
                      {
                          string errorMessage = "SendFirstData property 'emails' in Admin key does not exist!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }
                      string emails = adminBigData.GetString("emails");

                      if (!adminBigData.TryGetValue("displayName", out temp))
                      {
                          string errorMessage = "SendFirstData property 'displayName' in Admin key does not exist!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }
                      string displayName = adminBigData.GetString("displayName");

                      player.Send(GetFirstAdminDataMessageType, true, emails, displayName);
                      SendAllGarageAndAppraiserDatas(GetGarageAndAppraiserDatasMessageType, player);

                      if (!adminBigData.TryGetValue("allCards", out temp))
                      {
                          string errorMessage = "SendFirstData property 'allCards' in Admin key does not exist!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(FirstConnectionMessageType, false,
                              errorMessage);
                          return;
                      }
                      string rawAllCards = adminBigData.GetString("allCards");

                      if (!string.IsNullOrEmpty(rawAllCards))
                          LoadAllAppraisersCards(rawAllCards.Split('_'), FirstConnectionMessageType, player);
                      else
                      {
                          player.isStarted = true;
                          player.Send(FirstConnectionMessageType, true, string.Empty);
                      }
                  },
                  (error) =>
                  {
                      string errorMessage = "SendFirstData can't Load data for Admin, cuz:\n" +
                      error.Message;
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(FirstConnectionMessageType, false,
                          errorMessage);
                  });
    }
    #endregion

    public override void GotMessage(MyPlayer player, Message message)
    {
        switch (message.Type)
        {
            #region Check Connection


            #endregion

            #region Update Garages/Appraisers Lists
            case GetGarageDatasMessageType:
                {
                    //string[] garageNames = message.GetString(0).Split('_');
                    //if (garageNames.Length == 0)
                    //{
                    //    string errorMessage = "'garageNames' is has no parameters";
                    //    PlayerIO.ErrorLog.WriteError(errorMessage);

                    //    player.Send(GetGarageDatasMessageType, false,
                    //        errorMessage);
                    //    return;
                    //}

                    //SendAllGarageAndAppraiserDatas(GetGarageAndAppraiserDatasMessageType, player);
                }
                break;
            #endregion

            #region Add/Remove Email
            case AddNewEmailMessageType:
                {
                    if (message.Count != 1)
                    {
                        string errorMessage = "AddNewEmail incoming message has Count != 1 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(AddNewEmailMessageType, false,
                            errorMessage);
                        return;
                    }

                    string rawEmailData = message.GetString(0);
                    if (string.IsNullOrEmpty(rawEmailData))
                    {
                        string errorMessage = "AddNewEmail variable 'rawEmailData' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(AddNewEmailMessageType, false,
                            errorMessage);
                        return;
                    }
                    AddNewEmail(rawEmailData, AddNewEmailMessageType, player);
                }
                break;

            case RemoveFromEmailsMessageType:
                {
                    string rawEmailData = message.GetString(0);
                    RemoveFromEmailsList(rawEmailData, RemoveFromEmailsMessageType, player);
                }
                break;
            #endregion

            #region Add Document To Appraiser
            case AddDocumentToAppraiserMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "AddDocumentToAppraiser incoming message has Count != 1 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(AddDocumentToAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    string appraiserName = message.GetString(0);
                    if (string.IsNullOrEmpty(appraiserName))
                    {
                        string errorMessage = "AddDocumentToAppraiser variable 'appraiserName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(AddDocumentToAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    string rawDocumentsData = message.GetString(1);
                    if (string.IsNullOrEmpty(rawDocumentsData))
                    {
                        string errorMessage = "AddDocumentToAppraiser variable 'rawDocumentsData' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(AddDocumentToAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    AddDocumentToAppraiser(appraiserName, rawDocumentsData,
                        AddDocumentToAppraiserMessageType, player);
                }
                break;
            #endregion

            #region Get Blocks Data
            case GetBlocksDataMessageType:
                {
                    UpdateBlocksData(GetBlocksDataMessageType, player);
                }
                break;
            #endregion

            #region Search Cards
            case SearchCardsMessageType:
                {
                    string rawFilters = message.GetString(0);
                    if (string.IsNullOrEmpty(rawFilters))
                    {
                        string errorMessage = "SearchCards 'rawFilters' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(SearchCardsMessageType, false,
                            errorMessage);
                        return;
                    }
                    SearchCardsByFilter(rawFilters.Split('_'), SearchCardsMessageType, player);
                }
                break;
            #endregion

            #region Garage&Appraiser Control

            #region Create Appraiser
            case CreateAppraiserMessageType:
                {
                    string rawAppraiserCreationData = message.GetString(0);
                    if (string.IsNullOrEmpty(rawAppraiserCreationData))
                    {
                        string errorMessage = "CreateAppraiser 'rawAppraiserCreationData' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(SearchCardsMessageType, false,
                            errorMessage);
                        return;
                    }

                    string[] appraiserCreationData = rawAppraiserCreationData.Split('_');
                    if (appraiserCreationData.Length != 11)
                    {
                        string errorMessage = "CreateAppraiser incoming 'appraiserCreationData' has Count != 11 variables, its " +
                            appraiserCreationData.Length;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CreateAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    PlayerIO.ErrorLog.WriteError("CreateAppraiser");
                    CheckAvailableAppraiserDisplayName(rawAppraiserCreationData, appraiserCreationData,
                        CreateAppraiserMessageType, player);
                }
                break;
            #endregion

            #region Create Garage
            case CreateGarageMessageType:
                {
                    string rawGarageCreationData = message.GetString(0);
                    if (string.IsNullOrEmpty(rawGarageCreationData))
                    {
                        string errorMessage = "CreateGarage 'rawGarageCreationData' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CreateGarageMessageType, false,
                            errorMessage);
                        return;
                    }

                    string[] garageCreationData = rawGarageCreationData.Split('_');
                    if (garageCreationData.Length != 13)
                    {
                        string errorMessage = "CreateGarage incoming 'garageCreationData' has Count != 13 variables, its " +
                            garageCreationData.Length;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CreateGarageMessageType, false,
                            errorMessage);
                        return;
                    }

                    CheckAvailableGarageDisplayName(rawGarageCreationData, garageCreationData,
                        CreateGarageMessageType, player);
                }
                break;
            #endregion

            #region Delete Appraiser
            case DeleteAppraiserMessageType:
                {
                    string appraiserName = message.GetString(0);
                    if (string.IsNullOrEmpty(appraiserName))
                    {
                        string errorMessage = "DeleteAppraiser 'appraiserName' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DeleteAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    CheckDeleteAppraiser(appraiserName, DeleteAppraiserMessageType, player);
                }
                break;
            #endregion

            #region Delete Garage
            case DeleteGarageMessageType:
                {
                    string garageName = message.GetString(0);
                    if (string.IsNullOrEmpty(garageName))
                    {
                        string errorMessage = "DeleteGarage 'garageName' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DeleteAppraiserMessageType, false,
                            errorMessage);
                        return;
                    }

                    CheckDeleteGarage(garageName, DeleteGarageMessageType, player);
                }
                break;
            #endregion

            #region Change Appraiser Data
            case ChangeAppraiserDataMessageType:
                {
                    string rawAppraiserData = message.GetString(0);
                    if (string.IsNullOrEmpty(rawAppraiserData))
                    {
                        string errorMessage = "ChangeAppraiserData 'rawAppraiserData' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    string[] appraiserCreationData = rawAppraiserData.Split('_');
                    if (appraiserCreationData.Length != 10)
                    {
                        string errorMessage = "ChangeAppraiserData incoming 'appraiserCreationData' has Count != 10 variables, its " +
                            appraiserCreationData.Length;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    CheckChangeAppraiserData(appraiserCreationData, rawAppraiserData,
                        ChangeAppraiserDataMessageType, player);
                }
                break;
            #endregion

            #region Change Garage Data
            case ChangeGarageDataMessageType:
                {
                    string rawGarageData = message.GetString(0);
                    if (string.IsNullOrEmpty(rawGarageData))
                    {
                        string errorMessage = "ChangeGarageData'rawGarageData' is empty";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    string[] garageCreationData = rawGarageData.Split('_');
                    if (garageCreationData.Length != 12)
                    {
                        string errorMessage = "ChangeGarageData incoming 'garageCreationData' has Count != 12 variables, its " +
                            garageCreationData.Length;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    CheckChangeGarageData(garageCreationData, rawGarageData,
                        ChangeGarageDataMessageType, player);
                }
                break;
            #endregion

            #endregion

            #region Card Control

            #region Change Card Signal
            case ChangeCardSignalMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "ChangeCardSignal incoming message has Count != 2 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardSignalMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "ChangeCardSignal variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardSignalMessageType, false,
                            errorMessage);
                        return;
                    }

                    string newSignal = message.GetInt(1).ToString();
                    if (string.IsNullOrEmpty(newSignal))
                    {
                        string errorMessage = "ChangeCardSignal variable 'newSignal' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardSignalMessageType, false,
                            errorMessage);
                        return;
                    }

                    ChangeCardSignal(cardNumber, newSignal, ChangeCardSignalMessageType, player);
                }
                break;
            #endregion

            #region Connect Appraiser To Card
            case ConnectAppraiserToCardMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "ConnectAppraiserToCardMessageType incoming message has Count != 2 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ConnectAppraiserToCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "ConnectAppraiserToCardMessageType variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ConnectAppraiserToCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string appraiserName = message.GetString(1);
                    if (string.IsNullOrEmpty(appraiserName))
                    {
                        string errorMessage = "ConnectAppraiserToCardMessageType variable 'appraiserName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ConnectAppraiserToCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    ConnectAppraiserToCard(cardNumber, appraiserName,
                        ConnectAppraiserToCardMessageType, player);
                }
                break;
            #endregion

            #region Change Appraiser/Garage In Card
            case ChangeAppraiserInCardMessageType:
                {
                    if (message.Count != 4)
                    {
                        string errorMessage = "ChangeAppraiserInCardMessageType incoming message has Count != 4 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "ChangeAppraiserInCardMessageType variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string oldAppraiserName = message.GetString(1);
                    if (string.IsNullOrEmpty(oldAppraiserName))
                    {
                        string errorMessage = "ChangeAppraiserInCardMessageType variable 'oldAppraiserName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string newAppraiserName = message.GetString(2);
                    if (string.IsNullOrEmpty(newAppraiserName))
                    {
                        string errorMessage = "ChangeAppraiserInCardMessageType variable 'newAppraiserName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeAppraiserInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    bool isConfirmed = message.GetBoolean(3);

                    ChangeAppraiserInCard(cardNumber, oldAppraiserName, newAppraiserName, isConfirmed,
                        ChangeAppraiserInCardMessageType, player);
                }
                break;
            case ChangeGarageInCardMessageType:
                {
                    if (message.Count != 3)
                    {
                        string errorMessage = "ChangeGarageInCard incoming message has Count != 3 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "ChangeGarageInCard variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string oldGarageName = message.GetString(1);
                    if (string.IsNullOrEmpty(oldGarageName))
                    {
                        string errorMessage = "ChangeGarageInCard variable 'oldGarageName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string newGarageName = message.GetString(2);
                    if (string.IsNullOrEmpty(newGarageName))
                    {
                        string errorMessage = "ChangeAppraiserInCardMessageType variable 'newGarageName' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeGarageInCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    ChangeGarageInCard(cardNumber, oldGarageName, newGarageName,
                        ChangeGarageInCardMessageType, player);
                }
                break;
            #endregion

            #region Skip First Card Check
            case SkipFirstCardCheckMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "SkipFirstCardCheck incoming message has Count != 2 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(SkipFirstCardCheckMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "SkipFirstCardCheck variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(SkipFirstCardCheckMessageType, false,
                            errorMessage);
                        return;
                    }

                    string closeDate = message.GetString(1);
                    if (string.IsNullOrEmpty(closeDate))
                    {
                        string errorMessage = "SkipFirstCardCheck variable 'closeData' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(SkipFirstCardCheckMessageType, false,
                            errorMessage);
                        return;
                    }

                    TryToSkipFirstCardCheck(cardNumber, closeDate, SkipFirstCardCheckMessageType, player);
                }
                break;
            #endregion

            #region Close Card
            case CloseCardMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "CloseCard incoming message has Count != 2 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CloseCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "CloseCardMessageType variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CloseCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    string closeData = message.GetString(1);
                    if (string.IsNullOrEmpty(closeData))
                    {
                        string errorMessage = "CloseCardMessageType variable 'closeData' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CloseCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    TryToCloseCard(cardNumber, closeData, CloseCardMessageType, player);
                }
                break;
            #endregion

            #region Create Card
            case CreateCardMessageType:
                {
                    string[] cardCreationData = message.GetString(0).Split('\\');
                    if (cardCreationData.Length != 9)
                    {
                        string errorMessage = "CreateCard incoming message has Count != 9 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(CreateCardMessageType, false,
                            errorMessage);
                        return;
                    }

                    PrepareToCreateCard(cardCreationData, true,
                        CreateCardMessageType, player);
                }
                break;
            #endregion

            #region  Add Cards To Admin Block
            case AddCardsToAdminBlockMessageType:
                {
                    string rawCardNumbers = message.GetString(0);
                    if (string.IsNullOrEmpty(rawCardNumbers))
                    {
                        string errorMessage = "AddCardsToAdminBlockMessageType variable 'rawCardNumbers' is empty";
                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }

                    string[] cardNumbers = rawCardNumbers.Split('_');
                    CreateCardsFromAdminBlock(cardNumbers,
                        AddCardsToAdminBlockMessageType, player);
                }
                break;
            #endregion

            #region Get Full Card Data
            case GetFullCardDataMessageType:
                {
                    string cardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(cardNumber))
                    {
                        string errorMessage = "GetCardData variable 'cardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(GetFullCardDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    GetFullCardData(cardNumber, GetFullCardDataMessageType, player);
                }
                break;
            #endregion

            #region Change Card Data
            case ChangeCardDataMessageType:
                {
                    if (message.Count != 2)
                    {
                        string errorMessage = "ChangeCardData incoming message has Count != 2 variables, its " + message.Count;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    string oldCardNumber = message.GetString(0);
                    if (string.IsNullOrEmpty(oldCardNumber))
                    {
                        string errorMessage = "ChangeCardData variable 'oldCardNumber' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    string rawCardDate = message.GetString(1);
                    if (string.IsNullOrEmpty(rawCardDate))
                    {
                        string errorMessage = "ChangeCardData variable 'rawCardDate' is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(ChangeCardDataMessageType, false,
                            errorMessage);
                        return;
                    }

                    ChangeCardData(oldCardNumber, rawCardDate.Split('_'), ChangeCardDataMessageType, player);
                }
                break;
            #endregion
            #endregion

            #region Manual Card Commands
            case ManualRemoveUnderCardFromBlockMessageType:
                {
                    foreach (MyPlayer pl in players)
                    {
                        if (pl.ConnectUserId != player.ConnectUserId)
                            pl.Send(message);
                    }
                }
                break;

            case ManualBusyUnderCardInBlockMessageType:
                {
                    foreach (MyPlayer pl in players)
                    {
                        if (pl.ConnectUserId != player.ConnectUserId)
                            pl.Send(message);
                    }
                }
                break;
            #endregion
        }
    }

    #region SinglePlayer Messages

    #region Get Blocks Data
    private void UpdateBlocksData(string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
          (adminBigData) =>
          {
              if (adminBigData == null)
              {
                  string errorMessage = "UpdateBlocksData 'Load' Admin success, but 'adminBigData' is null!";
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
                  return;
              }

              object temp;
              if (!adminBigData.TryGetValue("allCards", out temp))
              {
                  string errorMessage = "UpdateBlocksData property 'allCards' in Admin key does not exist!";
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false, errorMessage);
                  return;
              }
              string rawAllCards = adminBigData.GetString("allCards");

              if (!string.IsNullOrEmpty(rawAllCards))
                  LoadAllAppraisersCards(rawAllCards.Split('_'), MessageType, player);
          },
          (error) =>
          {
              string errorMessage = "UpdateBlocksData can't Load data for Admin, cuz:\n" +
              error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    private void LoadAllAppraisersCards(string[] allCardNumbers, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allCardNumbers,
                 (cardsBigData) =>
                 {
                     if (cardsBigData == null)
                     {
                         string errorMessage = "LoadAllAppraisersCards 'LoadKeys' allCardNumbers: succes, but 'cardsBigData' is null!";
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     List<string> blockAppraisers = new List<string>();
                     Dictionary<string, List<string[]>> blocksData = new Dictionary<string, List<string[]>>();
                     string rawAllAppraisersCards = string.Empty;
                     for (int i = 0; i < cardsBigData.Length; i++)
                     {
                         DatabaseObject cardBigData = cardsBigData[i];
                         if (cardBigData == null)
                         {
                             string errorMessage = "LoadAllAppraisersCards 'LoadKeys' success, but: 'cardBigData' " +
                             allCardNumbers[i] + " is null";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }

                         string cardNumber = allCardNumbers[i];
                         object temp;
                         if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
                         {
                             string errorMessage = "LoadAllAppraisersCards property 'connectedAppraiser' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string connectedAppraiser = cardBigData.GetString("connectedAppraiser");
                         if (string.IsNullOrEmpty(connectedAppraiser))
                             continue;

                         if (!cardBigData.TryGetValue("carNumber", out temp))
                         {
                             string errorMessage = "LoadAllAppraisersCards property 'carNumber' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string carNumber = cardBigData.GetString("carNumber");

                         if (!cardBigData.TryGetValue("status", out temp))
                         {
                             string errorMessage = "LoadAllAppraisersCards property 'status' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string status = cardBigData.GetString("status");

                         if (!cardBigData.TryGetValue("signal", out temp))
                         {
                             string errorMessage = "LoadAllAppraisersCards property 'signal' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string signal = cardBigData.GetString("signal");

                         if (!cardBigData.TryGetValue("confirmed", out temp))
                         {
                             string errorMessage = "LoadAllAppraisersCards property 'confirmed' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         bool confirmed = cardBigData.GetBool("confirmed");
                         string isConfirmed = confirmed.ToString();

                         string[] underCardData = new string[5];
                         underCardData[0] = cardNumber;
                         underCardData[1] = carNumber;
                         underCardData[2] = status;
                         underCardData[3] = signal;
                         underCardData[4] = isConfirmed;

                         if (!blockAppraisers.Contains(connectedAppraiser))
                             blockAppraisers.Add(connectedAppraiser);

                         if (!blocksData.ContainsKey(connectedAppraiser))
                             blocksData.Add(connectedAppraiser, new List<string[]>());
                         blocksData[connectedAppraiser].Add(underCardData);
                     }

                     LoadAllAppraisers(allCardNumbers, blockAppraisers.ToArray(), blocksData, MessageType, player);
                 },
                 (error) =>
                 {
                     string errorMessage = "LoadAllAppraisersCards can't 'LoadKeys' cards for Admin, cuz:\n" +
                     error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void LoadAllAppraisers(string[] allCardNumbers, string[] allAppraiserNames, Dictionary<string, List<string[]>> blocksData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allAppraiserNames,
                 (appraisersBigData) =>
                 {
                     if (appraisersBigData == null)
                     {
                         string errorMessage = "LoadAllAppraisers 'LoadKeys' allAppraiserNames succes, but 'appraisersBigData' is null!";
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     string rawAppraiserBlocksData = string.Empty;
                     for (int i = 0; i < allAppraiserNames.Length; ++i)
                     {
                         DatabaseObject appraiserBigData = appraisersBigData[i];
                         if (appraiserBigData == null)
                         {
                             string errorMessage = string.Format("LoadAllAppraisers 'LoadKeys' 'allAppraiserNames' success, but 'appraiserBigData' {0} is null!",
                             allAppraiserNames[i]);
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(MessageType, false,
                                 errorMessage);
                             continue;
                         }

                         string appraiserName = allAppraiserNames[i];
                         object temp;
                         if (!appraiserBigData.TryGetValue("displayName", out temp))
                         {
                             string errorMessage = string.Format("LoadAllAppraisers property 'displayName' in Appraiser {0} key does not exist!",
                             allAppraiserNames[i]);
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(MessageType, false, errorMessage);
                             return;
                         }
                         string displayName = appraiserBigData.GetString("displayName");

                         rawAppraiserBlocksData += appraiserName + "\\" + displayName + "|";
                         List<string[]> underCardList = blocksData[appraiserName];
                         for (int j = 0; j < underCardList.Count; ++j)
                         {
                             string[] underCardData = underCardList[j];
                             for (int k = 0; k < underCardData.Length; ++k)
                             {
                                 rawAppraiserBlocksData += underCardData[k] + "_";
                             }
                             rawAppraiserBlocksData = rawAppraiserBlocksData.Trim('_');
                             rawAppraiserBlocksData += "|";
                         }
                         rawAppraiserBlocksData = rawAppraiserBlocksData.Trim('|');
                         rawAppraiserBlocksData += "~";
                     }
                     rawAppraiserBlocksData = rawAppraiserBlocksData.Trim('~');

                     LoadAllGaragesCards(allCardNumbers, rawAppraiserBlocksData, MessageType, player);
                 },
                 (error) =>
                 {
                     string errorMessage = "LoadAllAppraisers can't 'LoadKeys' cards for Admin, cuz:\n" +
                     error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void LoadAllGaragesCards(string[] allCardNumbers, string rawAppraiserBlocksData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allCardNumbers,
                 (cardsBigData) =>
                 {
                     if (cardsBigData == null)
                     {
                         string errorMessage = "LoadAllAppraisersCards 'LoadKeys' allCardNumbers: succes, but 'cardsBigData' is null!";
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     List<string> garageBlocks = new List<string>();
                     Dictionary<string, List<string[]>> garageBlocksData = new Dictionary<string, List<string[]>>();
                     string rawAllGaragesCards = string.Empty;
                     for (int i = 0; i < cardsBigData.Length; i++)
                     {
                         DatabaseObject cardBigData = cardsBigData[i];
                         if (cardBigData == null)
                         {
                             string errorMessage = "LoadAllGaragesCards 'LoadKeys' success, but: 'cardBigData' " +
                             allCardNumbers[i] + " is null";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }

                         string cardNumber = allCardNumbers[i];
                         object temp;
                         if (!cardBigData.TryGetValue("connectedGarage", out temp))
                         {
                             string errorMessage = "LoadAllGaragesCards property 'connectedGarage' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string connectedGarage = cardBigData.GetString("connectedGarage");
                         if (string.IsNullOrEmpty(connectedGarage))
                             continue;

                         if (!cardBigData.TryGetValue("carNumber", out temp))
                         {
                             string errorMessage = "LoadAllGaragesCards property 'carNumber' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string carNumber = cardBigData.GetString("carNumber");

                         if (!cardBigData.TryGetValue("status", out temp))
                         {
                             string errorMessage = "LoadAllGaragesCards property 'status' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         string status = cardBigData.GetString("status");

                         if (!cardBigData.TryGetValue("confirmed", out temp))
                         {
                             string errorMessage = "LoadAllGaragesCards property 'confirmed' in " +
                             allCardNumbers[i] + " key does not exist!";
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(DebugMessageType, errorMessage);
                             continue;
                         }
                         bool confirmed = cardBigData.GetBool("confirmed");
                         string isConfirmed = confirmed.ToString();

                         string[] underCardData = new string[4];
                         underCardData[0] = cardNumber;
                         underCardData[1] = carNumber;
                         underCardData[2] = status;
                         underCardData[3] = isConfirmed;

                         if (!garageBlocks.Contains(connectedGarage))
                             garageBlocks.Add(connectedGarage);

                         if (!garageBlocksData.ContainsKey(connectedGarage))
                             garageBlocksData.Add(connectedGarage, new List<string[]>());
                         garageBlocksData[connectedGarage].Add(underCardData);
                     }

                     LoadAllGarages(garageBlocks.ToArray(), garageBlocksData, rawAppraiserBlocksData, MessageType, player);
                 },
                 (error) =>
                 {
                     string errorMessage = "LoadAllGaragesCards can't 'LoadKeys' cards for Admin, cuz:\n" +
                     error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void LoadAllGarages(string[] allGarageNames, Dictionary<string, List<string[]>> garageBlocksData,
        string rawAppraiserBlocksData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allGarageNames,
                 (garagesBigData) =>
                 {
                     if (garagesBigData == null)
                     {
                         string errorMessage = "LoadAllGarages 'LoadKeys' 'allGarageNames' succes, but 'garagesBigData' is null!";
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     string rawGarageBlocksData = string.Empty;
                     for (int i = 0; i < allGarageNames.Length; ++i)
                     {
                         DatabaseObject garageBigData = garagesBigData[i];
                         if (garageBigData == null)
                         {
                             string errorMessage = string.Format("LoadAllAppraisers 'LoadKeys' 'allGarageNames' success, but 'garageBigData' {0} is null!",
                             allGarageNames[i]);
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(MessageType, false,
                                 errorMessage);
                             continue;
                         }

                         string garageName = allGarageNames[i];
                         object temp;
                         if (!garageBigData.TryGetValue("displayName", out temp))
                         {
                             string errorMessage = string.Format("LoadAllGarages property 'displayName' in Garage {0} key does not exist!",
                             allGarageNames[i]);
                             PlayerIO.ErrorLog.WriteError(errorMessage);

                             player.Send(MessageType, false, errorMessage);
                             return;
                         }
                         string displayName = garageBigData.GetString("displayName");

                         rawGarageBlocksData += garageName + "\\" + displayName + "|";
                         List<string[]> underCardList = garageBlocksData[garageName];
                         for (int j = 0; j < underCardList.Count; ++j)
                         {
                             string[] underCardData = underCardList[j];
                             for (int k = 0; k < underCardData.Length; ++k)
                             {
                                 rawGarageBlocksData += underCardData[k] + "_";
                             }
                             rawGarageBlocksData = rawGarageBlocksData.Trim('_');
                             rawGarageBlocksData += "|";
                         }
                         rawGarageBlocksData = rawGarageBlocksData.Trim('|');
                         rawGarageBlocksData += "~";
                     }
                     rawGarageBlocksData = rawGarageBlocksData.Trim('~');

                     SendAllBlocksData(rawAppraiserBlocksData, rawGarageBlocksData, MessageType, player);
                 },
                 (error) =>
                 {
                     string errorMessage = "LoadAllGarages can't 'LoadKeys' cards for Admin, cuz:\n" +
                     error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void SendAllBlocksData(string rawAppraiserBlocksData, string rawGarageBlocksData, string MessageType, MyPlayer player)
    {
        player.isStarted = true;
        player.Send(MessageType, true, rawAppraiserBlocksData, rawGarageBlocksData);
    }
    #endregion

    #region Search Cards
    private void SearchCardsByFilter(string[] filters, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
            (adminBigData) =>
            {
                object temp;
                if (!adminBigData.TryGetValue("allallCards", out temp))
                {
                    string errorMessage = "SearchCardsByFilter property 'allallCards' in Admin key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                string allCards = adminBigData.GetString("allallCards");
                if (!string.IsNullOrEmpty(allCards))
                    FilterAllCardsAndVersions(allCards.Split('_'), filters,
                        MessageType, player);
                else
                    player.Send(MessageType, true, string.Empty);
            },
            (error) =>
            {
                string errorMessage = "SearchCardsByFilter can't 'Load' Admin, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void FilterAllCardsAndVersions(string[] allCards, string[] filters, string MessageType, MyPlayer player)
    {
        string garageNameFilter = filters[0];
        string appraiserNameFilter = filters[1];
        string carNumberFilter = filters[2];
        string cardNumberFilter = filters[3];
        string firstCreatedDateFilter = filters[4];
        DateTime firstDateTime = DateTime.MinValue;
        if (!string.IsNullOrEmpty(firstCreatedDateFilter))
        {
            string[] ymd = firstCreatedDateFilter.Split('.');
            int year = int.Parse(ymd[2]);
            int month = int.Parse(ymd[1]);
            int day = int.Parse(ymd[0]);
            firstDateTime = new DateTime(year, month, day, 0, 0, 0);
        }
        string lastCreatedDateFilter = filters[5];
        DateTime lastDateTime = DateTime.MinValue;
        if (!string.IsNullOrEmpty(lastCreatedDateFilter))
        {
            string[] ymd = lastCreatedDateFilter.Split('.');
            int year = int.Parse(ymd[2]);
            int month = int.Parse(ymd[1]);
            int day = int.Parse(ymd[0]);
            lastDateTime = new DateTime(year, month, day, 23, 59, 59);
        }

        string[] statusFitler = filters[6].Split('\\');
        if (string.IsNullOrEmpty(statusFitler[0]))
            statusFitler = new string[0];

        string secondNumberFilter = filters[7];
        string secondDateFilter = filters[8];
        string cardTypeFilter = filters[9];
        string insurerNameFilter = filters[10];

        //if(string.IsNullOrEmpty(carNumberFilter) && string.IsNullOrEmpty(cardNumberFilter))
        //{
        //    string errorMessage = "carNumberParameter or cardNumberParameter has a low number of characters";
        //    PlayerIO.ErrorLog.WriteError(errorMessage);

        //    player.Send(MessageType, false,
        //        errorMessage);
        //    return;
        //}

        PlayerIO.BigDB.LoadKeys("PlayerObjects", allCards,
        (cardsBigData) =>
        {
            if (cardsBigData.Length == 0)
            {
                player.Send(MessageType, true, string.Empty);
                return;
            }

            for (int i = 0; i < cardsBigData.Length; ++i)
            {
                DatabaseObject cardBigData = cardsBigData[i];
                if (cardBigData == null)
                {
                    string errorMessage = "'LoadKeys' success, but: 'cardBigData' " +
                    allCards[i] + " is null";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(DebugMessageType, errorMessage);
                    continue;
                }
            }

            List<DatabaseObject> searchResult = new List<DatabaseObject>(cardsBigData);
            List<DatabaseObject> filteredCards = new List<DatabaseObject>(searchResult);

            object temp;
            if (!string.IsNullOrEmpty(cardNumberFilter))
            {
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    string cardNumber = filteredCards[i].Key;
                    if (cardNumber != cardNumberFilter)
                    {
                        if (!cardNumber.Contains(cardNumberFilter))
                        {
                            filteredCards.RemoveAt(i--);
                        }
                    }
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(carNumberFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];
                    if (!cardBigData.TryGetValue("carNumber", out temp))
                    {
                        string errorMessage = "Property 'carNumber' in " +
                        allCards[i] + " key does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        continue;
                    }
                    string carNumber = cardBigData.GetString("carNumber");
                    if (carNumber != carNumberFilter)
                    {
                        if (!carNumber.Contains(carNumberFilter))
                        {
                            filteredCards.RemoveAt(i--);
                        }
                    }
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(garageNameFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];
                    if (!cardBigData.TryGetValue("connectedGarage", out temp))
                    {
                        string errorMessage = "Property 'connectedGarage' in " +
                        allCards[i] + " key does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        continue;
                    }
                    string garageName = cardBigData.GetString("connectedGarage");
                    if (garageName != garageNameFilter)
                    {
                        filteredCards.RemoveAt(i--);
                    }
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(appraiserNameFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];
                    if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
                    {
                        string errorMessage = "Property 'connectedAppraiser' in " +
                        allCards[i] + " key does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        continue;
                    }
                    string appraiserName = cardBigData.GetString("connectedAppraiser");
                    if (appraiserName != appraiserNameFilter)
                    {
                        filteredCards.RemoveAt(i--);
                    }
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(firstCreatedDateFilter) ||
                !string.IsNullOrEmpty(lastCreatedDateFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("createdDate", out temp))
                    {
                        string errorMessage = "Property 'createdDate' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string createdDate = cardBigData.GetString("createdDate");
                    if (string.IsNullOrEmpty(createdDate))
                    {
                        string errorMessage = "Property 'createdDate' in "
                      + cardBigData.Key + " is empty!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false, errorMessage);
                        return;
                    }

                    string[] allDate = createdDate.Split('-');
                    string[] ymd = allDate[0].Split('/');
                    string[] time = allDate[1].Split(':');

                    int year = int.Parse(ymd[2]);
                    int month = int.Parse(ymd[1]);
                    int day = int.Parse(ymd[0]);

                    int hour = int.Parse(time[0]);
                    int minute = int.Parse(time[1]);

                    DateTime createdDateTime = new DateTime(year, month, day, hour, minute, 0);
                    if (firstDateTime != DateTime.MinValue)
                    {
                        if ((createdDateTime - firstDateTime).TotalMilliseconds < 0)
                        {
                            filteredCards.RemoveAt(i--);
                            continue;
                        }
                    }
                    if (lastDateTime != DateTime.MinValue)
                    {
                        if ((lastDateTime - createdDateTime).TotalMilliseconds < 0)
                        {
                            filteredCards.RemoveAt(i--);
                            continue;
                        }
                    }
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (statusFitler.Length > 0 && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("status", out temp))
                    {
                        string errorMessage = "Property 'status' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string status = cardBigData.GetString("status");
                    if (status == "-1")
                        status = "0";
                    if (!statusFitler.Contains(status))
                        filteredCards.RemoveAt(i--);
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(secondNumberFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("secondNumber", out temp))
                    {
                        string errorMessage = "Property 'secondNumber' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string secondNumber = cardBigData.GetString("secondNumber");
                    if (secondNumber != secondNumberFilter)
                        filteredCards.RemoveAt(i--);
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(secondDateFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("secondDate", out temp))
                    {
                        string errorMessage = "Property 'secondDate' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string secondDate = cardBigData.GetString("secondDate");
                    if (secondDate != secondDateFilter)
                        filteredCards.RemoveAt(i--);
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(cardTypeFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("cardType", out temp))
                    {
                        string errorMessage = "Property 'cardType' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string cardType = cardBigData.GetString("cardType");
                    if (cardType != cardTypeFilter)
                        filteredCards.RemoveAt(i--);
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (!string.IsNullOrEmpty(insurerNameFilter) && filteredCards.Count > 0)
            {
                filteredCards = new List<DatabaseObject>(searchResult);
                for (int i = 0; i < filteredCards.Count; ++i)
                {
                    DatabaseObject cardBigData = filteredCards[i];

                    if (!cardBigData.TryGetValue("insurerName", out temp))
                    {
                        string errorMessage = "Property 'insurerName' in "
                        + cardBigData.Key + " does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        return;
                    }
                    string insurerName = cardBigData.GetString("insurerName");
                    if (insurerName != insurerNameFilter)
                        filteredCards.RemoveAt(i--);
                    else
                        PlayerIO.ErrorLog.WriteError(insurerName);
                }

                if (filteredCards.Count > 0)
                    searchResult = filteredCards;
            }

            if (searchResult.Count == 0)
                player.Send(MessageType, true, string.Empty);
            else
                EndSearch(searchResult, MessageType, player);
        },
        (error) =>
        {
            string errorMessage = "Can't 'LoadKeys' Cards " +
            allCards.Length + " count, cuz:\n" + error.Message;
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
        });
    }
    private void EndSearch(List<DatabaseObject> searchResult, string MessageType, MyPlayer player)
    {
        string rawSearchResultData = string.Empty;
        for (int i = 0; i < searchResult.Count; ++i)
        {
            DatabaseObject cardBigData = searchResult[i];
            string appraiserName = cardBigData.GetString("connectedAppraiser");
            string garageName = cardBigData.GetString("connectedGarage");
            string carNumber = cardBigData.GetString("carNumber");
            string createdDate = cardBigData.GetString("createdDate");
            string cardNumber = cardBigData.Key;

            rawSearchResultData += appraiserName + "|" +
                garageName + "|" +
                carNumber + "|" +
                createdDate + "|" +
                cardNumber + "~";
        }

        if (rawSearchResultData.EndsWith("~"))
            rawSearchResultData = rawSearchResultData.Remove(rawSearchResultData.Length - 1, 1);

        player.Send(MessageType, true, rawSearchResultData);
    }
    #endregion

    #region  Add Cards To Admin Block 
    private void CreateCardsFromAdminBlock(string[] cardNumbers, string MessageType, MyPlayer player)
    {
        string[] cardCreationDataNames = new string[]
        {
         "carNumber", "createdDate", "connectedGarage", "connectedAppraiser",
         "secondNumber", "secondDate", "cardType", "insurerName"
        };

        PlayerIO.BigDB.LoadKeys("PlayerObjects", cardNumbers,
        (cardsBigData) =>
        {
            if (cardsBigData == null)
            {
                string errorMessage = "AddCardsToAdminBlock 'LoadKeys' succes, but 'cardsBigData' is null!";
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
                return;
            }

            List<string> unknownCardNames = new List<string>();
            for (int i = 0; i < cardNumbers.Length; ++i)
            {
                DatabaseObject cardBigData = cardsBigData[i];
                if (cardBigData == null)
                {
                    string errorMessage = "AddCardsToAdminBlock 'LoadKeys' succes, but 'cardBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(DebugMessageType, errorMessage);
                    continue;
                }

                string[] cardCreationData = new string[9];

                string newCardNumber = cardNumbers[i].Split('-')[0] + "-0";
                if (unknownCardNames.Contains(newCardNumber))
                    continue;

                cardCreationData[0] = newCardNumber;

                object temp = null;
                bool hasEmptyParameter = false;
                for (int j = 0; j < cardCreationDataNames.Length; ++j)
                {
                    if (!cardBigData.TryGetValue(cardCreationDataNames[j], out temp))
                    {
                        hasEmptyParameter = true;

                        string errorMessage = string.Format("CreateCardsFromAdminBlock property '{0}' in card {1} key does not exist!",
                        cardCreationDataNames[j], cardNumbers[i]);
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        break;
                    }
                    cardCreationData[j + 1] = cardBigData.GetString(cardCreationDataNames[j]);
                }

                cardCreationData[4] = "Admin";

                if (hasEmptyParameter)
                    continue;

                unknownCardNames.Add(newCardNumber);
                PrepareToCreateCard(cardCreationData, false, MessageType, player);
            }

            if (unknownCardNames.Count != 0)
            {
                ScheduleCallback(() =>
                AddCardsToAdminBlock(unknownCardNames.ToArray(), MessageType, player),
                1000);
            }
            else
            {
                string errorMessage = "CreateCardsFromAdminBlock list 'unknownCardNames' is empty!";
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(DebugMessageType, errorMessage);
            }
        },
        (error) =>
        {
            string errorMessage = "CreateCardsFromAdminBlock can't 'LoadKeys', cuz:\n" + error.Message;
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
        });
    }
    private void AddCardsToAdminBlock(string[] cardNumbers, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
      (adminBigData) =>
      {
          if (adminBigData == null)
          {
              string errorMessage = "AddCardsToAdminBlock 'Load' Admin succes, but 'adminBigData' is null!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          object temp;
          if (!adminBigData.TryGetValue("allCards", out temp))
          {
              string errorMessage = "AddCardsToAdminBlock property 'actualCards' in Admin key does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string allCards = adminBigData.GetString("allCards");

          for (int i = 0; i < cardNumbers.Length; ++i)
          {
              if (string.IsNullOrEmpty(cardNumbers[i]))
                  allCards = cardNumbers[i];
              else
                  allCards += "_" + cardNumbers[i];
          }

          adminBigData.Set("allCards", allCards);
          adminBigData.Save(() =>
          {
              player.Send(MessageType, true);
              UpdateBlocksData(GetBlocksDataMessageType, player);
          },
          (error) =>
          {
              string errorMessage = "AddCardsToAdminBlock can't 'Save' Admin data, cuz:\n" + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
      },
      (error) =>
      {
          string errorMessage = "AddCardsToAdminBlock can't 'Load' Admin, cuz:\n" + error.Message;
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
      });
    }
    #endregion

    #region Get Full Card Data
    private void GetFullCardData(string cardNumber, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
  (cardBigData) =>
  {
      if (cardBigData == null)
      {
          string errorMessage = "GetFullCardData 'Load' Card " + cardNumber + " key success, but 'cardBigData' is null!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
          return;
      }

      string rawCardData = string.Empty;

      rawCardData += cardNumber + "_";
      object temp;
      if (!cardBigData.TryGetValue("carNumber", out temp))
      {
          string errorMessage = "GetFullCardData property 'carNumber' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("carNumber") + "_";

      if (!cardBigData.TryGetValue("connectedGarage", out temp))
      {
          string errorMessage = "GetFullCardData property 'connectedGarage' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("connectedGarage") + "_";

      if (!cardBigData.TryGetValue("secondNumber", out temp))
      {
          string errorMessage = "GetFullCardData property 'secondNumber' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("secondNumber") + "_";

      if (!cardBigData.TryGetValue("secondDate", out temp))
      {
          string errorMessage = "GetFullCardData property 'secondDate' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("secondDate") + "_";

      if (!cardBigData.TryGetValue("cardType", out temp))
      {
          string errorMessage = "GetFullCardData property 'cardType' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("cardType") + "_";

      if (!cardBigData.TryGetValue("insurerName", out temp))
      {
          string errorMessage = "GetFullCardData property 'insurerName' Card " + cardNumber + " key does not exist!";
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false, errorMessage);
          return;
      }
      rawCardData += cardBigData.GetString("insurerName");

      player.Send(MessageType, true, rawCardData);
  },
  (error) =>
  {
      string errorMessage = "GetFullCardData can't 'Load' data for " + cardNumber + " key, cuz:\n" +
      error.Message;
      PlayerIO.ErrorLog.WriteError(errorMessage);

      player.Send(MessageType, false,
          errorMessage);
  });
    }
    #endregion

    #region Add Document To Appraiser
    public void AddDocumentToAppraiser(string appraiserName, string rawDocumentsData,
        string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
         (appraiserBigData) =>
         {
             if (appraiserBigData == null)
             {
                 string errorMessage = "AddDocumentToAppraiser 'LoadKeys' success, but 'appraiserBigData' " +
                 appraiserName + " is null";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false,
                     errorMessage);
                 return;
             }

             object temp;
             if (!appraiserBigData.TryGetValue("photos", out temp))
             {
                 string errorMessage = "AddDocumentToAppraiser property 'photos' in Appraiser " +
                 appraiserName + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false,
                     errorMessage);
                 return;
             }

             string documents = appraiserBigData.GetString("photos");
             if (string.IsNullOrEmpty(documents))
                 documents += rawDocumentsData;
             else
                 documents += "_" + rawDocumentsData;

             appraiserBigData.Set("photos", documents);
             appraiserBigData.Save(() =>
             {
                 player.Send(MessageType, true);
             },
             (error) =>
             {
                 string errorMessage = "AddDocumentToAppraiser can't 'Save' Appraiser " +
                 appraiserName + " data, cuz:\n" + error.Message;
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false,
                     errorMessage);
             });
         },
         (error) =>
         {
             string errorMessage = "AddDocumentToAppraiser сan't 'Load' Appraiser " + appraiserName +
             ", cuz:\n" + error.Message;
             PlayerIO.ErrorLog.WriteError(errorMessage);

             player.Send(MessageType, false,
                 errorMessage);
         });
    }
    #endregion

    #region Update Garages/Appraisers Lists
    private void SendAllGarageAndAppraiserDatas(string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "SendGaragesData 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allGarages", out temp))
                  {
                      string errorMessage = "SendGaragesData property 'allGarages' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string[] allGarages = adminBigData.GetString("allGarages").Split('_');

                  if (!adminBigData.TryGetValue("allAppraisers", out temp))
                  {
                      string errorMessage = "SendGaragesData property 'allAppraisers' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string[] allAppraisers = adminBigData.GetString("allAppraisers").Split('_');

                  if (!string.IsNullOrEmpty(allGarages[0]))
                  {
                      GetGarageDatas(allGarages, MessageType, player);
                  }

                  if (!string.IsNullOrEmpty(allAppraisers[0]))
                  {
                      GetAppraiserDatas(allAppraisers, MessageType, player);
                  }
              },
              (error) =>
              {
                  string errorMessage = "SendGaragesData can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
    }

    private void GetGarageDatas(string[] allGarages, string MessageType, MyPlayer player)
    {
        string[] garageDataKeys = new string[]
        {
             "password", "displayName", "ID", "officePhone", "mobilePhone", "fax", "email", "area", "adress", "insurerName", "isActive", "checkType"
        };
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allGarages,
                          (garagesBigData) =>
                          {
                              object temp;
                              string rawGarageDatas = string.Empty;
                              for (int i = 0; i < garagesBigData.Length; ++i)
                              {
                                  DatabaseObject garageBigData = garagesBigData[i];
                                  if (garageBigData == null)
                                  {
                                      string errorMessage = "GetGarageDatas 'LoadKeys' success, but: 'garageBigData' " +
                                      allGarages[i] + " is null";
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(DebugMessageType, errorMessage);
                                      continue;
                                  }

                                  rawGarageDatas += garageBigData.Key + "\\";

                                  for (int j = 0; j < garageDataKeys.Length; ++j)
                                  {
                                      if (!garageBigData.TryGetValue(garageDataKeys[j], out temp))
                                      {
                                          string errorMessage = "GetGarageDatas 'LoadKeys' success in " +
                                          garageBigData.Key + " key, but protepty " +
                                          garageDataKeys[j] + " does not exist!";
                                          PlayerIO.ErrorLog.WriteError(errorMessage);

                                          player.Send(MessageType, false,
                                              errorMessage);
                                          return;
                                      }

                                      string cardDataProperty = garageBigData.GetString(garageDataKeys[j]);
                                      if (string.IsNullOrEmpty(cardDataProperty))
                                      {
                                          string errorMessage = "GetGarageDatas 'LoadKeys' success, but " +
                                          garageDataKeys[j] + " property' is empty";
                                          PlayerIO.ErrorLog.WriteError(errorMessage);

                                          player.Send(MessageType, false,
                                              errorMessage);
                                          return;
                                      }
                                      rawGarageDatas += cardDataProperty + "\\";
                                  }
                                  rawGarageDatas += "_";
                              }

                              if (rawGarageDatas.EndsWith("_"))
                                  rawGarageDatas = rawGarageDatas.Remove(rawGarageDatas.Length - 1, 1);

                              if (string.IsNullOrEmpty(rawGarageDatas))
                                  return;

                              player.Send(GetGarageDatasMessageType, true,
                                  rawGarageDatas);
                          },
                          (error) =>
                          {
                              string errorMessage = "GetGarageDatas сan't Load Garage keys for Admin, cuz:\n" +
                              error.Message;
                              PlayerIO.ErrorLog.WriteError(errorMessage);

                              player.Send(MessageType, false,
                                  errorMessage);
                          });
    }

    private void GetAppraiserDatas(string[] allAppraisers, string MessageType, MyPlayer player)
    {
        string[] appraiserDataKeys = new string[]
        {
            "password", "displayName", "ID", "officePhone", "carTax", "role", "area", "adress", "email", "isActive" 
        };
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allAppraisers,
                      (appraisersBigData) =>
                      {
                          object temp;
                          string rawAppraiserDatas = string.Empty;
                          for (int i = 0; i < appraisersBigData.Length; ++i)
                          {
                              DatabaseObject appraiserBigData = appraisersBigData[i];
                              if (appraiserBigData == null)
                              {
                                  string errorMessage = "GetAppraiserDatas 'LoadKeys' success, but: 'garageBigData' " +
                                  allAppraisers[i] + " is null";
                                  PlayerIO.ErrorLog.WriteError(errorMessage);

                                  player.Send(DebugMessageType, errorMessage);
                                  continue;
                              }

                              rawAppraiserDatas += appraiserBigData.Key + "\\";

                              for (int j = 0; j < appraiserDataKeys.Length; ++j)
                              {
                                  if (!appraiserBigData.TryGetValue(appraiserDataKeys[j], out temp))
                                  {
                                      string errorMessage = "GetAppraiserDatas 'LoadKeys' success in " +
                                      appraiserBigData.Key + " key, but property " +
                                      appraiserDataKeys[j] + " does not exist!";
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false, errorMessage);
                                      return;
                                  }

                                  string cardDataProperty = appraiserBigData.GetString(appraiserDataKeys[j]);
                                  if (string.IsNullOrEmpty(cardDataProperty))
                                  {
                                      string errorMessage = "'LoadKeys' success, but " +
                                      appraiserDataKeys[j] + " property' is empty";
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false, errorMessage);
                                      return;
                                  }
                                  rawAppraiserDatas += cardDataProperty + "\\";
                              }
                              rawAppraiserDatas += "_";
                          }

                          if (rawAppraiserDatas.EndsWith("_"))
                              rawAppraiserDatas = rawAppraiserDatas.Remove(rawAppraiserDatas.Length - 1, 1);

                          if (string.IsNullOrEmpty(rawAppraiserDatas))
                              return;

                          player.Send(GetAppraiserDatasMessageType, true,
                              rawAppraiserDatas);
                      },
                      (error) =>
                      {
                          string errorMessage = "GetAppraiserDatas сan't Load Appraiser keys for Admin, cuz:\n" +
                          error.Message;
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                      });
    }
    #endregion

    #endregion

    #region MultiPlayer Messages

    #region Add/Remove Email
    #region Add New Email
    private void AddNewEmail(string rawEmailData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
      (adminBigData) =>
      {
          if (adminBigData == null)
          {
              string errorMessage = "AddNewEmail 'Load' Admin succes, but 'adminBigData' is null!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          object temp;
          if (!adminBigData.TryGetValue("emails", out temp))
          {
              string errorMessage = "AddNewEmail property 'emails' in Admin key does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string emails = adminBigData.GetString("emails");

          if (string.IsNullOrEmpty(emails))
              emails = rawEmailData;
          else
              emails += "_" + rawEmailData;

          adminBigData.Set("emails", emails);
          adminBigData.Save(() =>
          {
              foreach (MyPlayer pl in players)
              {
                  pl.Send(MessageType, true, rawEmailData);
              }
          },
          (error) =>
          {
              string errorMessage = "AddNewEmail can't 'Save' Admin data, cuz:\n" + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
      },
      (error) =>
      {
          string errorMessage = "AddNewEmail can't 'Load' Admin, cuz:\n" + error.Message;
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
      });
    }
    #endregion

    #region Remove Email
    private void RemoveFromEmailsList(string rawEmailData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
      (garageBigData) =>
      {
          if (garageBigData == null)
          {
              string errorMessage = "RemoveFromEmailsList 'Load' Admin " + player.ConnectUserId
              + " succes, but 'garageBigData' is null!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          object temp;
          if (!garageBigData.TryGetValue("emails", out temp))
          {
              string errorMessage = "RemoveFromEmailsList property 'emails' in Admin " +
              player.ConnectUserId + " key does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string emails = garageBigData.GetString("emails");

          if (string.IsNullOrEmpty(emails))
          {
              string errorMessage = "RemoveFromEmailsList property 'emails' in Admin " +
              player.ConnectUserId + " is empty!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          int startIndex = StringFind(emails, rawEmailData);
          if (startIndex == -1)
          {
              player.Send(MessageType, false,
                  "RemoveFromEmailsList Admin " + player.ConnectUserId + " does not contains email:\n" +
                  rawEmailData);
              return;
          }

          if (startIndex > 0)
              emails = emails.Remove(startIndex - 1, rawEmailData.Length + 1);
          else
          {
              if (emails.Split('_').Length > 1)
                  emails = emails.Remove(startIndex, rawEmailData.Length + 1);
              else
                  emails = emails.Remove(startIndex, rawEmailData.Length);
          }

          garageBigData.Set("emails", emails);
          garageBigData.Save(() =>
          {
              foreach (MyPlayer pl in players)
              {
                  if (pl.ConnectUserId != player.ConnectUserId)
                      pl.Send(MessageType, true, rawEmailData);
              }
          },
          (error) =>
          {
              string errorMessage = "RemoveFromEmailsList can't 'Save' Admin " +
               player.ConnectUserId + " data, cuz:\n" + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
      },
      (error) =>
      {
          string errorMessage = "RemoveFromEmailsList can't 'Load' Admin: " +
          player.ConnectUserId + ", cuz:\n" + error.Message;
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
      });
    }
    #endregion
    #endregion

    #region Garage&Appraiser Control

    #region Change Garage Data
    private void CheckChangeGarageData(string[] garageCreationData, string rawGarageData, string MessageType, MyPlayer player)
    {
        string garageName = garageCreationData[0];
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CheckChangeGarageData 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allGarages", out temp))
                  {
                      string errorMessage = "CheckChangeGarageData property 'allGarages' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllGarages = adminBigData.GetString("allGarages");

                  string[] allGarages = rawAllGarages.Split('_');
                  PlayerIO.BigDB.LoadKeys("PlayerObjects", allGarages,
                      (garagesBigData) =>
                      {
                          if (garagesBigData == null)
                          {
                              string errorMessage = "CheckChangeGarageData 'LoadKeys' succes, but 'garagesBigData' is null!";
                              PlayerIO.ErrorLog.WriteError(errorMessage);

                              player.Send(MessageType, false,
                                  errorMessage);
                              return;
                          }

                          for (int i = 0; i < allGarages.Length; ++i)
                          {
                              if (allGarages[i] == garageName)
                                  continue;

                              DatabaseObject garageBigData = garagesBigData[i];
                              if (garageBigData == null)
                              {
                                  string errorMessage = string.Format("CheckChangeGarageData 'LoadKeys' succes, but 'garageBigData' {0} is null!",
                                      allGarages[i]);
                                  PlayerIO.ErrorLog.WriteError(errorMessage);

                                  player.Send(DebugMessageType, errorMessage);
                                  continue;
                              }
                          }

                          ChangeGarageData(garageCreationData, rawGarageData, MessageType, player);
                      },
                      (error) =>
                      {
                          string errorMessage = "CheckChangeGarageData can't 'LoadKeys' 'allGarages', cuz:\n" + error.Message;
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                      });
              },
              (error) =>
              {
                  string errorMessage = "CheckChangeGarageData can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
    }
    private void ChangeGarageData(string[] garageCreationData, string rawGarageData, string MessageType, MyPlayer player)
    {
        string garageName = garageCreationData[0];
        string password = garageCreationData[1];
        string ID = garageCreationData[2];
        string officePhone = garageCreationData[3];
        string mobilePhone = garageCreationData[4];
        string fax = garageCreationData[5];
        string email = garageCreationData[6];
        string area = garageCreationData[7];
        string adress = garageCreationData[8];
        string insurerName = garageCreationData[9];
        string isActive = garageCreationData[10];
        string checkType = garageCreationData[11];

        PlayerIO.BigDB.Load("PlayerObjects", garageName,
                 (garageBigData) =>
                 {
                     if (garageBigData == null)
                     {
                         string errorMessage = string.Format("ChangeGarageData 'Load' Garage {0} success, but 'garageBigData' is null!",
                             garageName);
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     garageBigData.Set("password", password);
                     garageBigData.Set("ID", ID);
                     garageBigData.Set("officePhone", officePhone);
                     garageBigData.Set("mobilePhone", mobilePhone);
                     garageBigData.Set("fax", fax);
                     garageBigData.Set("email", email);
                     garageBigData.Set("area", area);
                     garageBigData.Set("adress", adress);
                     garageBigData.Set("insurerName", insurerName);
                     garageBigData.Set("isActive", isActive);
                     garageBigData.Set("checkType", checkType);

                     garageBigData.Save(() =>
                     {
                         foreach (MyPlayer pl in players)
                         {
                             pl.Send(MessageType, true, rawGarageData);
                         }
                     },
                     (error) =>
                     {
                         string errorMessage = string.Format("ChangeGarageData can't 'Save' Garage {0} data, cuz:\n",
                             garageName) + error.Message;
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                     });
                 },
                 (error) =>
                 {
                     string errorMessage = string.Format("ChangeGarageData can't 'Load' data for Garage {0}, cuz:\n",
                         garageName) + error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    #endregion

    #region Change Appraiser Data
    private void CheckChangeAppraiserData(string[] appraiserCreationData, string rawAppraiserData, string MessageType, MyPlayer player)
    {
        string appraiserName = appraiserCreationData[0];
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CheckChangeAppraiserData 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allAppraisers", out temp))
                  {
                      string errorMessage = "CheckChangeAppraiserData property 'allAppraisers' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllAppraisers = adminBigData.GetString("allAppraisers");

                  string[] allAppraisers = rawAllAppraisers.Split('_');
                  PlayerIO.BigDB.LoadKeys("PlayerObjects", allAppraisers,
                      (appraisersBigData) =>
                      {
                          if (appraisersBigData == null)
                          {
                              string errorMessage = "CheckChangeAppraiserData 'LoadKeys' succes, but 'appraisersBigData' is null!";
                              PlayerIO.ErrorLog.WriteError(errorMessage);

                              player.Send(MessageType, false,
                                  errorMessage);
                              return;
                          }

                          for (int i = 0; i < allAppraisers.Length; ++i)
                          {
                              if (allAppraisers[i] == appraiserName)
                                  continue;

                              DatabaseObject appraiserBigData = appraisersBigData[i];
                              if (appraiserBigData == null)
                              {
                                  string errorMessage = string.Format("CheckChangeAppraiserData 'LoadKeys' succes, but 'appraiserBigData' {0} is null!",
                                      allAppraisers[i]);
                                  PlayerIO.ErrorLog.WriteError(errorMessage);

                                  player.Send(DebugMessageType, errorMessage);
                                  continue;
                              }

                              //if (!appraiserBigData.TryGetValue("displayName", out temp))
                              //{
                              //    string errorMessage = string.Format("CheckChangeAppraiserData property 'displayName' in garage {0} key does not exist!",
                              //    allAppraisers[i]);
                              //    PlayerIO.ErrorLog.WriteError(errorMessage);

                              //    player.Send(MessageType, false,
                              //        errorMessage);
                              //    continue;
                              //}
                              //string currentDisplayName = appraiserBigData.GetString("displayName");

                              //if (currentDisplayName == displayName)
                              //{
                              //    string errorMessage = string.Format("CheckChangeAppraiserData Appraiser {0} is already have 'displayName' {1}!",
                              //       allAppraisers[i], displayName);
                              //    PlayerIO.ErrorLog.WriteError(errorMessage);

                              //    player.Send(MessageType, false,
                              //        errorMessage, false);
                              //    return;
                              //}
                          }

                          ChangeAppraiserData(appraiserCreationData, rawAppraiserData, MessageType, player);
                      },
                      (error) =>
                      {
                          string errorMessage = "CheckChangeAppraiserData can't 'LoadKeys' 'allAppraisers', cuz:\n" + error.Message;
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                      });
              },
              (error) =>
              {
                  string errorMessage = "CheckChangeAppraiserData can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
    }
    private void ChangeAppraiserData(string[] appraiserCreationData, string rawAppraiserData, string MessageType, MyPlayer player)
    {
        string appraiserName = appraiserCreationData[0];
        string password = appraiserCreationData[1];
        string ID = appraiserCreationData[2];
        string officePhone = appraiserCreationData[3];
        string carTax = appraiserCreationData[4];
        string role = appraiserCreationData[5];
        string area = appraiserCreationData[6];
        string adress = appraiserCreationData[7];
        string email = appraiserCreationData[8];
        string isActive = appraiserCreationData[9];

        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
                 (appraiserBigData) =>
                 {
                     if (appraiserBigData == null)
                     {
                         string errorMessage = string.Format("ChangeAppraiserData 'Load' Appraiser {0} success, but 'appraiserBigData' is null!",
                             appraiserName);
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     appraiserBigData.Set("password", password);
                     appraiserBigData.Set("ID", ID);
                     appraiserBigData.Set("officePhone", officePhone);
                     appraiserBigData.Set("carTax", carTax);
                     appraiserBigData.Set("role", role);
                     appraiserBigData.Set("area", area);
                     appraiserBigData.Set("adress", adress);
                     appraiserBigData.Set("email", email);
                     appraiserBigData.Set("isActive", isActive);

                     appraiserBigData.Save(() =>
                     {
                         foreach (MyPlayer pl in players)
                         {
                             pl.Send(MessageType, true, rawAppraiserData);
                         }
                     },
                     (error) =>
                     {
                         string errorMessage = string.Format("ChangeAppraiserData can't 'Save' Appraiser {0} data, cuz:\n",
                             appraiserName) + error.Message;
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                     });
                 },
                 (error) =>
                 {
                     string errorMessage = string.Format("ChangeAppraiserData can't 'Load' data for Appraiser {0}, cuz:\n",
                         appraiserName) + error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    #endregion

    #region Delete Garage
    private void CheckDeleteGarage(string garageName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", garageName,
                 (garageBigData) =>
                 {
                     if (garageBigData == null)
                     {
                         string errorMessage = string.Format("CheckDeleteGarage 'Load' Garage {0} success, but 'garageBigData' is null!",
                             garageName);
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     PlayerIO.BigDB.DeleteKeys("PlayerObjects", new string[] { garageName },
                           () =>
                           {
                               DeleteGarageFromAdmin(garageName, MessageType, player);
                           },
                           (error) =>
                           {
                               string errorMessage = "CheckDeleteGarage can't 'DeleteKeys' Garage " + garageName + " key, cuz:\n" + error.Message;
                               PlayerIO.ErrorLog.WriteError(errorMessage);

                               player.Send(MessageType, false,
                                   errorMessage);
                           });
                 },
                 (error) =>
                 {
                     string errorMessage = string.Format("CheckDeleteGarage can't 'Load' data for Garage {0}, cuz:\n",
                         garageName) + error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void DeleteGarageFromAdmin(string garageName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
      (adminBigData) =>
      {
          if (adminBigData == null)
          {
              string errorMessage = "DeleteGarageFromAdmin 'Load' Admin succes, but 'adminBigData' is null!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          object temp;
          if (!adminBigData.TryGetValue("allGarages", out temp))
          {
              string errorMessage = "DeleteGarageFromAdmin property 'allGarages' in Admin key does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string rawAllGarages = adminBigData.GetString("allGarages");
          string[] allGarages = rawAllGarages.Split('_');

          if (!allGarages.Contains(garageName))
          {
              string errorMessage = "DeleteGarageFromAdmin 'allGarages' not contains Garage " + garageName;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          string rawNewGarages = string.Empty;
          for (int i = 0; i < allGarages.Length; i++)
          {
              if (allGarages[i] != garageName)
              {
                  rawNewGarages += allGarages[i] + "_";
              }
          }

          if (rawNewGarages.EndsWith("_"))
              rawNewGarages = rawNewGarages.Remove(rawNewGarages.Length - 1, 1);

          adminBigData.Set("allGarages", rawNewGarages);
          adminBigData.Save(() =>
          {
              foreach (MyPlayer pl in players)
              {
                  pl.Send(MessageType, true, garageName);
              }
          },
          (error) =>
          {
              string errorMessage = "DeleteGarageFromAdmin can't 'Save' Admin data, cuz:\n" + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false, errorMessage);
          });
      },
      (error) =>
      {
          string errorMessage = "DeleteGarageFromAdmin can't 'Load' Admin, cuz:\n" + error.Message;
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
      });
    }
    #endregion

    #region Delete Appraiser
    private void CheckDeleteAppraiser(string appraiserName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
                 (appraiserBigData) =>
                 {
                     if (appraiserBigData == null)
                     {
                         string errorMessage = string.Format("CheckDeleteAppraiser 'Load' Appraiser {0} success, but 'appraiserBigData' is null!",
                             appraiserName);
                         PlayerIO.ErrorLog.WriteError(errorMessage);

                         player.Send(MessageType, false,
                             errorMessage);
                         return;
                     }

                     PlayerIO.BigDB.DeleteKeys("PlayerObjects", new string[] { appraiserName },
                           () =>
                           {
                               DeleteAppraiserFromAdmin(appraiserName, MessageType, player);
                           },
                           (error) =>
                           {
                               string errorMessage = "CheckDeleteAppraiser can't 'DeleteKeys' Appraiser " + appraiserName + " key, cuz:\n" + error.Message;
                               PlayerIO.ErrorLog.WriteError(errorMessage);

                               player.Send(MessageType, false,
                                   errorMessage);
                           });
                 },
                 (error) =>
                 {
                     string errorMessage = string.Format("CheckDeleteAppraiser can't 'Load' data for Appraiser {0}, cuz:\n",
                         appraiserName) + error.Message;
                     PlayerIO.ErrorLog.WriteError(errorMessage);

                     player.Send(MessageType, false,
                         errorMessage);
                 });
    }
    private void DeleteAppraiserFromAdmin(string appraiserName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
      (adminBigData) =>
      {
          if (adminBigData == null)
          {
              string errorMessage = "DeleteAppraiserFromAdmin 'Load' Admin succes, but 'adminBigData' is null!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          object temp;
          if (!adminBigData.TryGetValue("allAppraisers", out temp))
          {
              string errorMessage = "DeleteAppraiserFromAdmin property 'allAppraisers' in Admin key does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string rawAllAppraisers = adminBigData.GetString("allAppraisers");
          string[] allAppraisers = rawAllAppraisers.Split('_');

          if (!allAppraisers.Contains(appraiserName))
          {
              string errorMessage = "DeleteAppraiserFromAdmin 'allAppraisers' not contains Appraiser " + appraiserName;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }

          string rawNewAppraisers = string.Empty;
          for (int i = 0; i < allAppraisers.Length; i++)
          {
              if (allAppraisers[i] != appraiserName)
              {
                  rawNewAppraisers += allAppraisers[i] + "_";
              }
          }

          if (rawNewAppraisers.EndsWith("_"))
              rawNewAppraisers = rawNewAppraisers.Remove(rawNewAppraisers.Length - 1, 1);

          if (!adminBigData.TryGetValue("allCards", out temp))
          {
              string errorMessage = "DeleteAppraiserFromAdmin property 'allCards' in Admin does not exist!";
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
              return;
          }
          string rawAllCards = adminBigData.GetString("allCards");

          adminBigData.Set("allAppraisers", rawNewAppraisers);
          adminBigData.Save(() =>
          {
              if (string.IsNullOrEmpty(rawAllCards))
              {
                  player.Send(MessageType, true, appraiserName);
              }
              else
              {
                  string[] allCards = rawAllCards.Split('_');
                  ResetAppraiserActualCards(appraiserName, allCards, MessageType, player);
              }
          },
          (error) =>
          {
              string errorMessage = "DeleteAppraiserFromAdmin can't 'Save' Admin data, cuz:\n" + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false, errorMessage);
          });
      },
      (error) =>
      {
          string errorMessage = "DeleteAppraiserFromAdmin can't 'Load' Admin, cuz:\n" + error.Message;
          PlayerIO.ErrorLog.WriteError(errorMessage);

          player.Send(MessageType, false,
              errorMessage);
      });
    }
    private void ResetAppraiserActualCards(string appraiserName, string[] allCards, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.LoadKeys("PlayerObjects", allCards,
            (cardsBigData) =>
            {
                if (cardsBigData == null)
                {
                    string errorMessage = "ResetAppraiserActualCards 'LoadKeys' succes, but 'cardsBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                for (int i = 0; i < allCards.Length; ++i)
                {
                    DatabaseObject cardBigData = cardsBigData[i];
                    if (cardBigData == null)
                    {
                        string errorMessage = "ResetAppraiserActualCards 'LoadKeys' succes, but 'cardBigData' is null!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                        continue;
                    }

                    if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
                    {
                        string errorMessage = string.Format("ResetAppraiserActualCards property 'connectedAppraiser' in card {0} key does not exist!",
                        allCards[i]);
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        continue;
                    }
                    string connectedAppraiser = cardBigData.GetString("connectedAppraiser");
                    if (connectedAppraiser == appraiserName)
                    {
                        cardBigData.Set("connectedAppraiser", "Admin");
                        cardBigData.Save(null,
                            (error) =>
                            {
                                string errorMessage = "ResetAppraiserActualCards can't 'Save' " + allCards[i] + " key data, cuz:\n" +
                                error.Message;
                                PlayerIO.ErrorLog.WriteError(errorMessage);

                                player.Send(MessageType, false,
                                    errorMessage);
                            });
                    }
                }

                foreach (MyPlayer pl in players)
                {
                    pl.Send(MessageType, true, appraiserName);
                }
            },
            (error) =>
            {
                string errorMessage = "ResetAppraiserActualCards can't 'LoadKeys' 'actualCards', cuz:\n" + error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #region Create Garage
    private void CheckAvailableGarageDisplayName(string rawGarageData, string[] garageCreationData, string MessageType, MyPlayer player)
    {
        string displayName = garageCreationData[2];
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CheckAvailableGarageDisplayName 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allGarages", out temp))
                  {
                      string errorMessage = "CheckAvailableGarageDisplayName property 'allGarages' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllGarages = adminBigData.GetString("allGarages");

                  if (string.IsNullOrEmpty(rawAllGarages))
                  {
                      PrepareToCreateGarage(rawGarageData, garageCreationData, MessageType, player);
                  }
                  else
                  {
                      string[] allGarages = rawAllGarages.Split('_');
                      PlayerIO.BigDB.LoadKeys("PlayerObjects", allGarages,
                          (garagesBigData) =>
                          {
                              if (garagesBigData == null)
                              {
                                  string errorMessage = "CheckAvailableGarageDisplayName 'LoadKeys' succes, but 'garagesBigData' is null!";
                                  PlayerIO.ErrorLog.WriteError(errorMessage);

                                  player.Send(MessageType, false,
                                      errorMessage);
                                  return;
                              }

                              for (int i = 0; i < allGarages.Length; ++i)
                              {
                                  DatabaseObject garageBigData = garagesBigData[i];
                                  if (garageBigData == null)
                                  {
                                      string errorMessage = string.Format("CheckAvailableGarageDisplayName 'LoadKeys' succes, but 'garageBigData' {0} is null!",
                                          allGarages[i]);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(DebugMessageType, errorMessage);
                                      continue;
                                  }

                                  if (!garageBigData.TryGetValue("displayName", out temp))
                                  {
                                      string errorMessage = string.Format("CheckAvailableGarageDisplayName property 'displayName' in garage {0} key does not exist!",
                                      allGarages[i]);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false,
                                          errorMessage);
                                      continue;
                                  }
                                  string currentDisplayName = garageBigData.GetString("displayName");

                                  if (currentDisplayName == displayName)
                                  {
                                      string errorMessage = string.Format("CheckAvailableGarageDisplayName Garage {0} is already have 'displayName' {1}!",
                                         allGarages[i], displayName);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false,
                                          errorMessage, false);
                                      return;
                                  }
                              }

                              PrepareToCreateGarage(rawGarageData, garageCreationData, MessageType, player);
                          },
                          (error) =>
                          {
                              string errorMessage = "CheckAvailableGarageDisplayName can't 'LoadKeys' 'allGarages', cuz:\n" + error.Message;
                              PlayerIO.ErrorLog.WriteError(errorMessage);

                              player.Send(MessageType, false,
                                  errorMessage);
                          });
                  }
              },
              (error) =>
              {
                  string errorMessage = "CheckAvailableGarageDisplayName can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
    }
    private void PrepareToCreateGarage(string rawGarageData, string[] garageCreationData, string MessageType, MyPlayer player)
    {
        string garageName = garageCreationData[0];
        string password = garageCreationData[1];

        string displayName = garageCreationData[2];
        string ID = garageCreationData[3];
        string officePhone = garageCreationData[4];
        string mobilePhone = garageCreationData[5];
        string fax = garageCreationData[6];
        string email = garageCreationData[7];
        string area = garageCreationData[8];
        string adress = garageCreationData[9];
        string insurerName = garageCreationData[10];
        string isActive = garageCreationData[11];
        string checkType = garageCreationData[12];

        PlayerIO.BigDB.Load("PlayerObjects", garageName,
          (garageBigData) =>
          {
              if (garageBigData != null)
              {
                  string errorMessage = string.Format("PrepareToCreateGarage Garage {0} is already in Database!",
                      garageName);
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage, true);
                  return;
              }

              DatabaseObject newGarageBigData = new DatabaseObject();
              newGarageBigData.Set("photos", string.Empty);
              newGarageBigData.Set("missedCalls", string.Empty);
              newGarageBigData.Set("actualCards", string.Empty);
              newGarageBigData.Set("PlayFabID", string.Empty);
              newGarageBigData.Set("emails", string.Empty);
              newGarageBigData.Set("accountType", "garage");

              newGarageBigData.Set("separ", string.Empty);

              newGarageBigData.Set("password", password);
              newGarageBigData.Set("displayName", displayName);
              newGarageBigData.Set("ID", ID);
              newGarageBigData.Set("officePhone", officePhone);
              newGarageBigData.Set("mobilePhone", mobilePhone);
              newGarageBigData.Set("fax", fax);
              newGarageBigData.Set("email", email);
              newGarageBigData.Set("area", area);
              newGarageBigData.Set("adress", adress);
              newGarageBigData.Set("insurerName", insurerName);
              newGarageBigData.Set("isActive", isActive);
              newGarageBigData.Set("checkType", checkType);

              CreateGarage(garageName, rawGarageData, newGarageBigData, MessageType, player);
          },
          (error) =>
          {
              string errorMessage = string.Format("PrepareToCreateGarage can't 'Load' data for Garage {0}, cuz:\n",
                  garageName) + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    private void CreateGarage(string garageName, string rawGarageData, DatabaseObject newGarageBigData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.CreateObject("PlayerObjects", garageName, newGarageBigData,
          (garageBigData) =>
          {
              if (garageBigData == null)
              {
                  string errorMessage = string.Format("CreateGarage 'Load' Garage {0} success, but 'garageBigData' is null!",
                      garageName);
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
                  return;
              }

              PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CreateGarage 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allGarages", out temp))
                  {
                      string errorMessage = "CreateGarage property 'allGarages' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllGarages = adminBigData.GetString("allGarages");

                  if (rawAllGarages == string.Empty)
                      rawAllGarages = garageName;
                  else
                      rawAllGarages += "_" + garageName;

                  adminBigData.Set("allGarages", rawAllGarages);
                  adminBigData.Save(() =>
                  {
                      foreach (MyPlayer pl in players)
                      {
                          pl.Send(MessageType, true, rawGarageData);
                      }
                  },
                  (error) =>
                  {
                      string errorMessage = "CreateGarage can't 'Save' Admin data, cuz:\n" + error.Message;
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                  });
              },
              (error) =>
              {
                  string errorMessage = "CreateGarage can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
          },
          (error) =>
          {
              string errorMessage = string.Format("CreateGarage can't 'Load' data for Garage {0}, cuz:\n",
                  garageName) + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    #endregion

    #region Create Appraiser
    private void CheckAvailableAppraiserDisplayName(string rawAppraiserData, string[] appraiserCreationData, string MessageType, MyPlayer player)
    {
        PlayerIO.ErrorLog.WriteError("CheckAvailableAppraiserDisplayName");
        string displayName = appraiserCreationData[2];
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CheckAvailableAppraiserDisplayName 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allAppraisers", out temp))
                  {
                      string errorMessage = "CheckAvailableAppraiserDisplayName property 'allAppraisers' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllAppraisers = adminBigData.GetString("allAppraisers");

                  if (string.IsNullOrEmpty(rawAllAppraisers))
                  {
                      PrepareToCreateAppraiser(rawAppraiserData, appraiserCreationData, MessageType, player);
                  }
                  else
                  {
                      string[] allAppraisers = rawAllAppraisers.Split('_');
                      PlayerIO.BigDB.LoadKeys("PlayerObjects", allAppraisers,
                          (appraisersBigData) =>
                          {
                              if (appraisersBigData == null)
                              {
                                  string errorMessage = "CheckAvailableAppraiserDisplayName 'LoadKeys' succes, but 'appraisersBigData' is null!";
                                  PlayerIO.ErrorLog.WriteError(errorMessage);

                                  player.Send(MessageType, false,
                                      errorMessage);
                                  return;
                              }

                              for (int i = 0; i < allAppraisers.Length; ++i)
                              {
                                  DatabaseObject appraiserBigData = appraisersBigData[i];
                                  if (appraiserBigData == null)
                                  {
                                      string errorMessage = string.Format("CheckAvailableAppraiserDisplayName 'LoadKeys' succes, but 'appraiserBigData' {0} is null!",
                                          allAppraisers[i]);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(DebugMessageType, errorMessage);
                                      continue;
                                  }

                                  if (!appraiserBigData.TryGetValue("displayName", out temp))
                                  {
                                      string errorMessage = string.Format("CheckAvailableAppraiserDisplayName property 'displayName' in appraiser {0} key does not exist!",
                                      allAppraisers[i]);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false,
                                          errorMessage);
                                      continue;
                                  }
                                  string currentDisplayName = appraiserBigData.GetString("displayName");

                                  if (currentDisplayName == displayName)
                                  {
                                      string errorMessage = string.Format("CheckAvailableAppraiserDisplayName Appraiser {0} already have 'displayName' {1}!",
                                         allAppraisers[i], displayName);
                                      PlayerIO.ErrorLog.WriteError(errorMessage);

                                      player.Send(MessageType, false,
                                          errorMessage, false);
                                      return;
                                  }
                              }

                              PrepareToCreateAppraiser(rawAppraiserData, appraiserCreationData, MessageType, player);
                          },
                          (error) =>
                          {
                              string errorMessage = "CheckAvailableAppraiserDisplayName can't 'LoadKeys' 'allAppraisers', cuz:\n" + error.Message;
                              PlayerIO.ErrorLog.WriteError(errorMessage);

                              player.Send(MessageType, false,
                                  errorMessage);
                          });
                  }
              },
              (error) =>
              {
                  string errorMessage = "CheckAvailableAppraiserDisplayName can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
    }
    private void PrepareToCreateAppraiser(string rawAppraiserData, string[] appraiserCreationData, string MessageType, MyPlayer player)
    {
        string appraiserName = appraiserCreationData[0];
        string password = appraiserCreationData[1];

        string displayName = appraiserCreationData[2];
        string ID = appraiserCreationData[3];
        string officePhone = appraiserCreationData[4];
        string carTax = appraiserCreationData[5];
        string role = appraiserCreationData[6];
        string area = appraiserCreationData[7];
        string adress = appraiserCreationData[8];
        string email = appraiserCreationData[9];
        string isActive = appraiserCreationData[10];

        PlayerIO.ErrorLog.WriteError("PrepareToCreateAppraiser: " + appraiserName + " displayName: " + displayName);
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
          (appraiserBigData) =>
          {
              if (appraiserBigData != null)
              {
                  string errorMessage = string.Format("PrepareToCreateAppraiser Appraiser {0} already in Database!",
                      appraiserName);
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage, true);
                  return;
              }

              DatabaseObject newAppraiserBigData = new DatabaseObject();
              newAppraiserBigData.Set("photos", string.Empty);
              newAppraiserBigData.Set("missedCalls", string.Empty);
              newAppraiserBigData.Set("actualCards", string.Empty);
              newAppraiserBigData.Set("PlayFabID", string.Empty);
              newAppraiserBigData.Set("emails", string.Empty);
              newAppraiserBigData.Set("accountType", "appraiser");

              newAppraiserBigData.Set("separ", string.Empty);

              newAppraiserBigData.Set("password", password);
              newAppraiserBigData.Set("displayName", displayName);
              newAppraiserBigData.Set("ID", ID);
              newAppraiserBigData.Set("officePhone", officePhone);
              newAppraiserBigData.Set("carTax", carTax);
              newAppraiserBigData.Set("role", role);
              newAppraiserBigData.Set("area", area);
              newAppraiserBigData.Set("adress", adress);
              newAppraiserBigData.Set("email", email);
              newAppraiserBigData.Set("isActive", isActive);

              CreateAppraiser(appraiserName, rawAppraiserData, newAppraiserBigData, MessageType, player);
          },
          (error) =>
          {
              string errorMessage = string.Format("PrepareToCreateAppraiser can't 'Load' data for Appraiser {0}, cuz:\n",
                  appraiserName) + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    private void CreateAppraiser(string appraiserName, string rawAppraiserData, DatabaseObject newAppraiserBigData, string MessageType, MyPlayer player)
    {
        PlayerIO.ErrorLog.WriteError("CreateAppraiser: " + appraiserName + " displayName: " + rawAppraiserData);
        PlayerIO.BigDB.CreateObject("PlayerObjects", appraiserName, newAppraiserBigData,
          (appraiserBigData) =>
          {
              if (appraiserBigData == null)
              {
                  string errorMessage = string.Format("CreateAppraiser 'CreateObject' Appraiser {0} success, but 'appraiserBigData' is null!",
                      appraiserName);
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
                  return;
              }

              PlayerIO.BigDB.Load("PlayerObjects", "Admin",
              (adminBigData) =>
              {
                  if (adminBigData == null)
                  {
                      string errorMessage = "CreateAppraiser 'Load' Admin succes, but 'adminBigData' is null!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }

                  object temp;
                  if (!adminBigData.TryGetValue("allAppraisers", out temp))
                  {
                      string errorMessage = "CheckAvailableDisplayName property 'allAppraisers' in Admin key does not exist!";
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                      return;
                  }
                  string rawAllAppraisers = adminBigData.GetString("allAppraisers");

                  if (rawAllAppraisers == string.Empty)
                      rawAllAppraisers = appraiserName;
                  else
                      rawAllAppraisers += "_" + appraiserName;

                  PlayerIO.ErrorLog.WriteError("CreateAppraiser Send");
                  adminBigData.Set("allAppraisers", rawAllAppraisers);
                  adminBigData.Save(() =>
                  {
                      foreach (MyPlayer pl in players)
                      {
                          pl.Send(MessageType, true, rawAppraiserData);
                      }
                  },
                  (error) =>
                  {
                      string errorMessage = "CreateAppraiser can't 'Save' Admin data, cuz:\n" + error.Message;
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                  });
              },
              (error) =>
              {
                  string errorMessage = "CreateAppraiser can't 'Load' Admin, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
          },
          (error) =>
          {
              string errorMessage = string.Format("CreateAppraiser can't 'Load' data for Appraiser {0}, cuz:\n",
                  appraiserName) + error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    #endregion

    #endregion

    #region Card Control

    #region Change Card Signal
    private void ChangeCardSignal(string cardNumber, string newSignal, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
          (cardBigData) =>
          {
              if (cardBigData == null)
              {
                  string errorMessage = "ChangeCardSignal 'Load' Card " + cardNumber + " key success, but 'adminBigData' is null!";
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
                  return;
              }

              object temp;
              if (!cardBigData.TryGetValue("signal", out temp))
              {
                  string errorMessage = "ChangeCardSignal property 'signal' Card " + cardNumber + " key does not exist!";
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false, errorMessage);
                  return;
              }

              cardBigData.Set("signal", newSignal);
              cardBigData.Save(() =>
              {
                  UpdateAppraiserCardInfo(cardNumber, cardBigData, MessageType, player);
                  //player.Send(MessageType, true); // Multi Admin
              },
              (error) =>
              {
                  string errorMessage = "ChangeCardSignal can't 'Save' Card " + cardNumber + " key, cuz:\n" + error.Message;
                  PlayerIO.ErrorLog.WriteError(errorMessage);

                  player.Send(MessageType, false,
                      errorMessage);
              });
          },
          (error) =>
          {
              string errorMessage = "ChangeCardSignal can't 'Load' data for " + cardNumber + " key, cuz:\n" +
              error.Message;
              PlayerIO.ErrorLog.WriteError(errorMessage);

              player.Send(MessageType, false,
                  errorMessage);
          });
    }
    #endregion

    #region Connect Appraiser To Card Part
    private void ConnectAppraiserToCard(string cardNumber, string appraiserName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
                   (cardBigData) =>
                   {
                       if (cardBigData == null)
                       {
                           string errorMessage = "ConnectAppraiserToCard can't 'Load' Card: " + cardNumber + ", cuz this key does not exists!";
                           PlayerIO.ErrorLog.WriteError(errorMessage);

                           player.Send(MessageType, false,
                               errorMessage);
                           return;
                       }

                       object temp;
                       if (!cardBigData.TryGetValue("confirmed", out temp))
                       {
                           string errorMessage = "ConnectAppraiserToCard property 'confirmed' in " + cardNumber + " key does not exist!";
                           PlayerIO.ErrorLog.WriteError(errorMessage);

                           player.Send(MessageType, false,
                               errorMessage);
                           return;
                       }
                       bool isConfirmed = cardBigData.GetBool("confirmed");
                       if (!isConfirmed)
                           cardBigData.Set("confirmed", true);

                       cardBigData.Save(() =>
                       {
                           AddCardToAppraiserList(cardNumber, appraiserName, cardBigData, MessageType, player);
                       },
                       (error) =>
                       {
                           string errorMessage = "ConnectAppraiserToCard can't 'Save' " + cardNumber + " key, cuz:\n" +
                           error.Message;
                           PlayerIO.ErrorLog.WriteError(errorMessage);

                           player.Send(MessageType, false,
                               errorMessage);
                       });
                   },
                   (error) =>
                   {
                       string errorMessage = "ConnectAppraiserToCard can't 'Load' " + cardNumber + " key, cuz:\n" +
                       error.Message;
                       PlayerIO.ErrorLog.WriteError(errorMessage);

                       player.Send(MessageType, false,
                           errorMessage);
                   });
    }
    private void AddCardToAppraiserList(string cardNumber, string appraiserName, DatabaseObject cardBigData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
                  (appraiserBigData) =>
                  {
                      if (appraiserBigData == null)
                      {
                          string errorMessage = "AddCardToAppraiserList 'Load' Appraiser:" + appraiserName + " key success, but 'cardBigData' is null!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      object temp;
                      if (!appraiserBigData.TryGetValue("actualCards", out temp))
                      {
                          string errorMessage = "AddCardToAppraiserList property 'actualCards' in " + appraiserName + " key does not exist!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      string rawCardNames = appraiserBigData.GetString("actualCards");
                      string[] cardNames = rawCardNames.Split('_');
                      if (cardNames.Contains(cardNumber))
                      {
                          string errorMessage = "AddCardToAppraiserList property 'actualCards' already contains " + cardNumber + "!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      if (string.IsNullOrEmpty(rawCardNames))
                          rawCardNames += cardNumber;
                      else
                          rawCardNames += "_" + cardNumber;

                      appraiserBigData.Set("actualCards", rawCardNames);
                      appraiserBigData.Save(() =>
                      {
                          UpdateAppraiserCardInfo(cardNumber, cardBigData, MessageType, player);
                          //player.Send(MessageType, true); // Multi Admin
                      },
                      (error) =>
                      {
                          string errorMessage = "AddCardToAppraiserList AdminMobil can't 'Save' " + appraiserName + " key, cuz:\n" +
                          error.Message;
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                      });
                  },
                  (error) =>
                  {
                      string errorMessage = "AddCardToAppraiserList can't 'Load' Appraiser: " + appraiserName + " key, cuz:\n" +
                      error.Message;
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                  });
    }
    private void AddCardToGarageList(string cardNumber, string garageName, DatabaseObject cardBigData, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", garageName,
                  (garagerBigData) =>
                  {
                      if (garagerBigData == null)
                      {
                          string errorMessage = "AddCardToGarageList 'Load' Appraiser:" + garageName + " key success, but 'cardBigData' is null!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      object temp;
                      if (!garagerBigData.TryGetValue("actualCards", out temp))
                      {
                          string errorMessage = "AddCardToAppraiserList property 'actualCards' in " + garageName + " key does not exist!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      string rawCardNames = garagerBigData.GetString("actualCards");
                      string[] cardNames = rawCardNames.Split('_');
                      if (cardNames.Contains(cardNumber))
                      {
                          string errorMessage = "AddCardToGarageList property 'actualCards' already contains " + cardNumber + "!";
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                          return;
                      }

                      if (string.IsNullOrEmpty(rawCardNames))
                          rawCardNames += cardNumber;
                      else
                          rawCardNames += "_" + cardNumber;

                      garagerBigData.Set("actualCards", rawCardNames);
                      garagerBigData.Save(() =>
                      {
                          UpdateGarageCardInfo(cardNumber, cardBigData, MessageType, player);
                          //player.Send(MessageType, true); // Multi Admin
                      },
                      (error) =>
                      {
                          string errorMessage = "AddCardToAppraiserList AdminMobil can't 'Save' " + garageName + " key, cuz:\n" +
                          error.Message;
                          PlayerIO.ErrorLog.WriteError(errorMessage);

                          player.Send(MessageType, false,
                              errorMessage);
                      });
                  },
                  (error) =>
                  {
                      string errorMessage = "AddCardToGarageList can't 'Load' Appraiser: " + garageName + " key, cuz:\n" +
                      error.Message;
                      PlayerIO.ErrorLog.WriteError(errorMessage);

                      player.Send(MessageType, false,
                          errorMessage);
                  });
    }
    private void DeleteCardFromAppraiserList(string cardNumber, string oldAppraiser, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", oldAppraiser,
                (appraiserBigData) =>
                {
                    if (appraiserBigData == null)
                    {
                        string errorMessage = "DeleteCardFromAppraiserList 'Load' Appraiser:" + oldAppraiser + " key success, but 'cardBigData' is null!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        return;
                    }

                    object temp;
                    if (!appraiserBigData.TryGetValue("actualCards", out temp))
                    {
                        string errorMessage = "DeleteCardFromAppraiserList property 'actualCards' in " + oldAppraiser + " key does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        return;
                    }

                    string rawCardNames = appraiserBigData.GetString("actualCards");

                    if (!string.IsNullOrEmpty(rawCardNames))
                    {
                        string[] createdCardNames = rawCardNames.Split('_');
                        for (int i = 0; i < createdCardNames.Length; ++i)
                        {
                            if (createdCardNames[i] == cardNumber)
                            {
                                if (i == createdCardNames.Length - 1)
                                {
                                    if (createdCardNames.Length > 1)
                                        rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf("_" + createdCardNames[i]));
                                    else
                                        rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf(createdCardNames[i]));
                                }
                                else
                                    rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf(createdCardNames[i] + "_"));

                                isAppraiserInCardWasDeleted = true;
                                appraiserBigData.Set("actualCards", rawCardNames);
                                appraiserBigData.Save();
                                break;
                            }
                        }
                    }
                },
                (error) =>
                {
                    string errorMessage = "DeleteCardFromAppraiserList can't 'Load' Appraiser: " + oldAppraiser + " key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
    }
    private void DeleteCardFromGaragerList(string cardNumber, string oldGarage, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", oldGarage,
                (garageBigData) =>
                {
                    if (garageBigData == null)
                    {
                        string errorMessage = "DeleteCardFromGaragerList 'Load' Garage:" + oldGarage + " key success, but 'cardBigData' is null!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        return;
                    }

                    object temp;
                    if (!garageBigData.TryGetValue("actualCards", out temp))
                    {
                        string errorMessage = "DeleteCardFromGaragerList property 'actualCards' in " + oldGarage + " key does not exist!";
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(MessageType, false,
                            errorMessage);
                        return;
                    }

                    string rawCardNames = garageBigData.GetString("actualCards");

                    if (!string.IsNullOrEmpty(rawCardNames))
                    {
                        string[] createdCardNames = rawCardNames.Split('_');
                        for (int i = 0; i < createdCardNames.Length; ++i)
                        {
                            if (createdCardNames[i] == cardNumber)
                            {
                                if (i == createdCardNames.Length - 1)
                                {
                                    if (createdCardNames.Length > 1)
                                        rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf("_" + createdCardNames[i]));
                                    else
                                        rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf(createdCardNames[i]));
                                }
                                else
                                    rawCardNames = rawCardNames.Remove(rawCardNames.IndexOf(createdCardNames[i] + "_"));

                                isGarageInCardWasDeleted = true;
                                garageBigData.Set("actualCards", rawCardNames);
                                garageBigData.Save();
                                break;
                            }
                        }
                    }
                },
                (error) =>
                {
                    string errorMessage = "DeleteCardFromGaragerList can't 'Load' Garage: " + oldGarage + " key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
    }
    #endregion

    #region Change Appraiser/Garage In Card
    private bool isAppraiserInCardWasDeleted;
    private bool isGarageInCardWasDeleted;
    private void ChangeAppraiserInCard(string cardNumber, string oldAppraiserName, string newAppraiserName, bool isConfirmed, string MessageType, MyPlayer player)
    {
        isAppraiserInCardWasDeleted = false;
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
            (cardBigData) =>
            {
                if (cardBigData == null)
                {
                    string errorMessage = "ChangeAppraiserInCard 'Load' in card " + cardNumber + " key success, but 'cardBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
                {
                    string errorMessage = "ChangeAppraiserInCard property 'connectedAppraiser' in card " + cardNumber + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);
                    player.Send(MessageType, false, errorMessage);
                    return;
                }
                cardBigData.Set("connectedAppraiser", newAppraiserName);

                cardBigData.Save(() =>
                {
                    if (isConfirmed)
                        DeleteCardFromAppraiserList(cardNumber, oldAppraiserName, MessageType, player);
                    ScheduleCallback(() =>
                    {
                        if (isConfirmed)
                        {
                            if (isAppraiserInCardWasDeleted)
                                AddCardToAppraiserList(cardNumber, newAppraiserName, cardBigData, MessageType, player);
                            else
                            {
                                string errorMessage = "ChangeAppraiserInCard isAppraiserInCardWasDeleted not yet, so can't AddCardToAppraiserList!";
                                PlayerIO.ErrorLog.WriteError(errorMessage);

                                player.Send(MessageType, false,
                                    errorMessage);
                            }
                        }
                    },
                    100);
                },
                (error) =>
                {
                    string errorMessage = "ChangeAppraiserInCard can't 'Save' card " + cardNumber + " key, cuz:\n" + error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "ChangeAppraiserInCard can't 'Load' data for " + cardNumber + " key, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void ChangeGarageInCard(string cardNumber, string oldGarageName, string newGarageName, string MessageType, MyPlayer player)
    {
        isGarageInCardWasDeleted = false;
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
            (cardBigData) =>
            {
                if (cardBigData == null)
                {
                    string errorMessage = "ChangeGarageInCard 'Load' in card " + cardNumber + " key success, but 'cardBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!cardBigData.TryGetValue("connectedGarage", out temp))
                {
                    string errorMessage = "ChangeGarageInCard property 'connectedGarage' in card " + cardNumber + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);
                    player.Send(MessageType, false, errorMessage);
                    return;
                }
                cardBigData.Set("connectedGarage", newGarageName);

                cardBigData.Save(() =>
                {
                    DeleteCardFromGaragerList(cardNumber, oldGarageName, MessageType, player);
                    ScheduleCallback(() =>
                    {
                        if (isGarageInCardWasDeleted)
                            AddCardToGarageList(cardNumber, newGarageName, cardBigData, MessageType, player);
                        else
                        {
                            string errorMessage = "ChangeGarageInCard isGarageInCardWasDeleted not yet, so can't AddCardToAppraiserList!";
                            PlayerIO.ErrorLog.WriteError(errorMessage);

                            player.Send(MessageType, false,
                                errorMessage);
                        }
                    },
                    100);
                },
                (error) =>
                {
                    string errorMessage = "ChangeGarageInCard can't 'Save' card " + cardNumber + " key, cuz:\n" + error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "ChangeGarageInCard can't 'Load' data for " + cardNumber + " key, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #region Skip First Card Check
    private void TryToSkipFirstCardCheck(string cardNumber, string closedDate, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
           (cardBigData) =>
           {
               if (cardBigData == null)
               {
                   player.Send(MessageType, false,
                       "TryToAdditional card: " + cardNumber + " not found!");
                   return;
               }

               object temp;
               if (!cardBigData.TryGetValue("status", out temp))
               {
                   string errorMessage = "ConnectAppraiserToCard property 'status' in Card " +
                   cardNumber + " key does not exist!";
                   PlayerIO.ErrorLog.WriteError(errorMessage);

                   player.Send(MessageType, false,
                       errorMessage);
                   return;
               }
               string oldStatus = cardBigData.GetString("status");

               if (oldStatus == "-1")
               {
                   cardBigData.Set("closedDate", closedDate);
                   cardBigData.Set("status", "0");
                   cardBigData.Save(() =>
                   {
                       UpdateAppraiserCardInfo(cardNumber, cardBigData, MessageType, player); // Multi Admin
                       UpdateGarageCardInfo(cardNumber, cardBigData, MessageType, player); // Multi Admin
                   },
                   (error) =>
                   {
                       string errorMessage = "TryToSkipFirstCardCheck can't 'Save' " + cardNumber + " key, cuz:\n" +
                       error.Message;
                       PlayerIO.ErrorLog.WriteError(errorMessage);

                       player.Send(MessageType, false,
                           errorMessage);
                   });
               }
               else
               {
                   string errorMessage = "TryToAdditional property 'status' in Card " +
                   cardNumber + " key does not exist!";
                   PlayerIO.ErrorLog.WriteError(errorMessage);

                   player.Send(MessageType, false,
                       errorMessage);
               }
           },
           (error) =>
           {
               string errorMessage = "Can't 'Load' Card: " +
               cardNumber + ", cuz:\n" +
               error.Message;
               PlayerIO.ErrorLog.WriteError(errorMessage);

               player.Send(MessageType, false,
                   errorMessage);
           });
    }
    #endregion

    #region Close Card
    private void TryToCloseCard(string cardNumber, string closeDate, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
           (cardBigData) =>
           {
               if (cardBigData == null)
               {
                   string errorMessage = "TryToCloseCard card: " + cardNumber + " not found!";
                   PlayerIO.ErrorLog.WriteError(errorMessage);

                   player.Send(MessageType, false, errorMessage);
                   return;
               }

               object temp;
               if (!cardBigData.TryGetValue("status", out temp))
               {
                   string errorMessage = "TryToCloseCard property 'status' in " +
                   player.ConnectUserId + " key does not exist!";
                   PlayerIO.ErrorLog.WriteError(errorMessage);

                   player.Send(MessageType, false,
                       errorMessage);
                   return;
               }
               string oldStatus = cardBigData.GetString("status");

               if (oldStatus == "-1")
               {
                   CloseFirstCard(cardNumber, closeDate,
                       MessageType, cardBigData, player);
               }
               else
               {
                   if (oldStatus == "0")
                       LoadLastCardVersion(cardNumber, closeDate,
                           MessageType, player);
                   else
                       FindLastCardVersion(cardNumber, closeDate,
                           MessageType, player);
               }
           },
           (error) =>
           {
               string errorMessage = "TryToCloseCard can't 'Load' Card: " +
               cardNumber + ", cuz:\n" +
               error.Message;
               PlayerIO.ErrorLog.WriteError(errorMessage);

               player.Send(MessageType, false,
                   errorMessage);
           });
    }

    private void FindLastCardVersion(string cardNumber, string closeDate,
        string MessageType, MyPlayer player)
    {
        string parentCardNumber = cardNumber.Split('-')[0];

        PlayerIO.BigDB.Load("PlayerObjects", parentCardNumber,
            (cardBigData) =>
            {
                if (cardBigData == null)
                {
                    string errorMessage = "FindLastCardVersion card: " + cardNumber + " not found!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false, errorMessage);
                    return;
                }

                object temp;
                if (!cardBigData.TryGetValue("childs", out temp))
                {
                    string errorMessage = "FindLastCardVersion property 'childs' in " +
                    cardNumber + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string[] childs = cardBigData.GetString("childs").Split('_');
                string lastChildCardNumber = childs[childs.Length - 1];

                LoadLastCardVersion(lastChildCardNumber, closeDate,
                    MessageType, player);
            },
            (error) =>
            {
                string errorMessage = "FindLastCardVersion can't 'Load' Card: " +
                parentCardNumber + ", cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }

    private void LoadLastCardVersion(string cardNumber, string closeDate,
        string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
    (cardBigData) =>
    {
        if (cardBigData == null)
        {
            string errorMessage = "LoadLastCardVersion card: " + cardNumber + " not found!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false, errorMessage);
            return;
        }

        CloseAdditionalCard(cardNumber, closeDate,
           MessageType, cardBigData, player);
    },
    (error) =>
    {
        string errorMessage = "Can't 'Load' Card: " +
        cardNumber + ", cuz:\n" +
        error.Message;
        PlayerIO.ErrorLog.WriteError(errorMessage);

        player.Send(MessageType, false,
            errorMessage);
    });
    }

    private void CloseAdditionalCard(string originalCardNumber, string closeDate, string MessageType,
        DatabaseObject originalCardBigData, MyPlayer player)
    {
        string[] cardPropertyNames = new string[]
        {
            "carNumber", "secondNumber", "secondDate", "cardType", "insurerName", "signal"
        };
        string[] cardProperties = new string[cardPropertyNames.Length];

        string[] firstCardName = originalCardNumber.Split('-');
        int cardIndex = 0;
        if (firstCardName.Length > 1)
            cardIndex = int.Parse(firstCardName[1]);
        string newCardNumber = firstCardName[0] + "-" + (++cardIndex);

        DatabaseObject newCardData = new DatabaseObject();

        object temp;
        if (!originalCardBigData.TryGetValue("photos", out temp))
        {
            string errorMessage = "CloseAdditionalCard property 'photos' in Card: " + originalCardNumber +
            " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
            return;
        }
        string documents = originalCardBigData.GetString("photos");
        newCardData.Set("photos", documents);

        for (int i = 0; i < cardPropertyNames.Length; ++i)
        {
            cardProperties[i] = originalCardBigData.GetString(cardPropertyNames[i]);
            if (!originalCardBigData.TryGetValue(cardPropertyNames[i], out temp))
            {
                string errorMessage = "CloseAdditionalCard property '" + cardPropertyNames[i] +
                "' in Card: " + cardPropertyNames[i] + " key does not exist!";
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                 errorMessage);
                return;
            }
            newCardData.Set(cardPropertyNames[i], cardProperties[i]);
        }

        if (!originalCardBigData.TryGetValue("connectedGarage", out temp))
        {
            string errorMessage = "CloseAdditionalCard property 'connectedGarage' in Card: " + originalCardNumber +
            " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
            return;
        }
        string connectedGarage = originalCardBigData.GetString("connectedGarage");
        newCardData.Set("connectedGarage", connectedGarage);

        if (!originalCardBigData.TryGetValue("connectedAppraiser", out temp))
        {
            string errorMessage = "CloseAdditionalCard property 'connectedAppraiser' in Card: " + originalCardNumber +
            " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
            return;
        }
        string connectedAppraiser = originalCardBigData.GetString("connectedAppraiser");
        newCardData.Set("connectedAppraiser", connectedAppraiser);

        newCardData.Set("createdDate", closeDate);

        newCardData.Set("closedDate", closeDate);

        if (!originalCardBigData.TryGetValue("confirmed", out temp))
        {
            string errorMessage = "Property 'confirmed' in Card: " + originalCardNumber +
            " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
            return;
        }
        bool isConfirmed = originalCardBigData.GetBool("confirmed");
        newCardData.Set("confirmed", isConfirmed);

        newCardData.Set("status", "4");

        AddChildToCard(firstCardName[0], newCardNumber, DebugMessageType, player);
        CreateCard(newCardNumber, MessageType, true, isConfirmed, newCardData, player);
        ScheduleCallback(() =>
        {
            if (isCardWasCreated)
            {
                UpdateAppraiserCardInfo(newCardNumber, newCardData, MessageType, player);
                UpdateGarageCardInfo(newCardNumber, newCardData, MessageType, player);
                RemoveCardFromGarage(originalCardNumber, connectedGarage, connectedAppraiser, isConfirmed, MessageType, player);
            }
            else
            {
                string errorMessage = "CloseAdditionalCard card " + newCardNumber + " is not created yet, so can't RemoveFromGarage/Admin/Appraiser card " + originalCardNumber;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(DebugMessageType, errorMessage);
            }
        },
        1000);
    }
    private void AddChildToCard(string parentCardNumber, string childCardNumber, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", parentCardNumber,
        (parentCardBigData) =>
        {
            if (parentCardBigData == null)
            {
                player.Send(MessageType, "Parent card: " + parentCardNumber + " not found!");
                return;
            }

            string rawChilds = string.Empty;
            object temp;
            if (parentCardBigData.TryGetValue("childs", out temp))
            {
                rawChilds = parentCardBigData.GetString("childs");
            }

            if (string.IsNullOrEmpty(rawChilds))
                rawChilds = childCardNumber;
            else
                rawChilds += "_" + childCardNumber;

            parentCardBigData.Set("childs", rawChilds);
            parentCardBigData.Save(null,
            (error) =>
            {
                string errorMessage = "Can't 'Save' Parent card: " +
                parentCardNumber + " data, cuz:\n" + error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, errorMessage);
            });
        },
        (error) =>
        {
            string errorMessage = "Can't 'Load' Parent card: " +
            parentCardNumber + ", cuz:\n" +
            error.Message;
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, errorMessage);
        });
    }
    private void CloseFirstCard(string cardNumber, string closedDate, string MessageType, DatabaseObject cardBigData, MyPlayer player)
    {
        object temp;
        if (!cardBigData.TryGetValue("closedDate", out temp))
        {
            string errorMessage = "CloseFirstCard property 'closedDate' in " +
            cardNumber + " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
            return;
        }
        cardBigData.Set("closedDate", closedDate);

        cardBigData.Set("status", "4");
        cardBigData.Save(() =>
        {
            UpdateAppraiserCardInfo(cardNumber, cardBigData, MessageType, player);
            UpdateGarageCardInfo(cardNumber, cardBigData, MessageType, player);
            //player.Send(MessageType, true); // Multi Admin
        },
        (error) =>
        {
            string errorMessage = "CloseFirstCard can't 'Save' Card: " +
            cardNumber + " data, cuz:\n" +
            error.Message;
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false,
                errorMessage);
        });
    }
    #endregion

    #region Create Card
    private void PrepareToCreateCard(string[] cardCreationData, bool isNeedConnect, string MessageType, MyPlayer player)
    {
        string cardNumber = cardCreationData[0];
        string carNumber = cardCreationData[1];
        string createdDate = cardCreationData[2];
        string connectedGarage = cardCreationData[3];
        string connectedAppraiser = cardCreationData[4];

        string secondNumber = cardCreationData[5];
        string secondDate = cardCreationData[6];
        string cardType = cardCreationData[7];
        string insurerName = cardCreationData[8];

        PlayerIO.BigDB.Load("PlayerObjects", cardNumber,
            (searchedCardBigData) =>
            {
                if (searchedCardBigData != null)
                {
                    string errorMessage = "PrepareToCreateCard Card: " + cardNumber + " already in Database!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                DatabaseObject newCardData = new DatabaseObject();
                newCardData.Set("photos", string.Empty);

                newCardData.Set("carNumber", carNumber);
                newCardData.Set("createdDate", createdDate);

                newCardData.Set("closedDate", string.Empty);

                newCardData.Set("connectedGarage", connectedGarage);
                newCardData.Set("connectedAppraiser", connectedAppraiser);
                newCardData.Set("secondNumber", secondNumber);
                newCardData.Set("secondDate", secondDate);
                newCardData.Set("cardType", cardType);
                newCardData.Set("insurerName", insurerName);

                newCardData.Set("confirmed", false);
                newCardData.Set("status", "-1");
                newCardData.Set("signal", "-1");

                CreateCard(cardNumber, MessageType, isNeedConnect, false, newCardData, player);
            },
            (error) =>
            {
                string errorMessage = "PrepareToCreateCard can't 'Load': " +
                cardNumber + ", cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }

    private void CreateCard(string cardNumber, string MessageType, bool isNeedConnect,
        bool isConfirmed, DatabaseObject newCardData, MyPlayer player)
    {
        isCardWasCreated = false;
        PlayerIO.BigDB.CreateObject("PlayerObjects", cardNumber, newCardData,
            (cardBigData) =>
            {
                if (cardBigData == null)
                {
                    string errorMessage = "CreateCard 'CreateObject' success, but 'cardBigData' is null cuz some reason...";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                isCardWasCreated = true;

                object temp;
                if (!cardBigData.TryGetValue("connectedGarage", out temp))
                {
                    string errorMessage = "CreateCard property 'connectedGarage' in " + cardNumber
                    + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string garageName = cardBigData.GetString("connectedGarage");

                if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
                {
                    string errorMessage = "CreateCard property 'connectedAppraiser' in " + cardNumber
                    + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string appraiserName = cardBigData.GetString("connectedAppraiser");

                if (isNeedConnect)
                    AddCardToGarage(cardNumber, garageName, appraiserName, MessageType, isConfirmed, player);
            },
            (error) =>
            {
                string errorMessage = "CreateCard can't 'CreateObject': " +
                cardNumber + ", cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #region Change Card Data
    private bool isCardWasCreated;
    private void ChangeCardData(string oldCardNumber, string[] cardDate, string MessageType, MyPlayer player)
    {
        string newCardNumber = cardDate[0];
        string newCarNumber = cardDate[1];
        string createdDate = cardDate[2];
        string newConnectedGarage = cardDate[3];
        string newSecondNumber = cardDate[4];
        string newSecondDate = cardDate[5];
        string newCardType = cardDate[6];
        string newInsurerName = cardDate[7];

        PlayerIO.BigDB.Load("PlayerObjects", oldCardNumber,
            (cardBigData) =>
            {
                if (cardBigData == null)
                {
                    string errorMessage = "ChangeCardData 'Load' Card " + oldCardNumber + " key success, but 'cardBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!cardBigData.TryGetValue("photos", out temp))
                {
                    string errorMessage = "GetCardData property 'photos' Card " + oldCardNumber + " key does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false, errorMessage);
                    return;
                }
                string oldPhotos = cardBigData.GetString("photos");

                DatabaseObject newCardBigData = new DatabaseObject();
                newCardBigData.Set("photos", oldPhotos);

                newCardBigData.Set("carNumber", newCarNumber);
                newCardBigData.Set("createdDate", createdDate);

                newCardBigData.Set("closedDate", string.Empty);

                newCardBigData.Set("connectedGarage", newConnectedGarage);
                newCardBigData.Set("connectedAppraiser", "Admin");
                newCardBigData.Set("secondNumber", newSecondNumber);
                newCardBigData.Set("secondDate", newSecondDate);
                newCardBigData.Set("cardType", newCardType);
                newCardBigData.Set("insurerName", newInsurerName);

                newCardBigData.Set("confirmed", false);
                newCardBigData.Set("status", "-1");
                newCardBigData.Set("signal", "-1");

                CreateCard(newCardNumber, MessageType, true, false, newCardBigData, player);
                ScheduleCallback(() =>
                {
                    if (isCardWasCreated)
                    {
                        PlayerIO.BigDB.DeleteKeys("PlayerObjects", new string[] { oldCardNumber },
                           () =>
                           {
                               RemoveCardFromAdmin(oldCardNumber, "Admin", false, MessageType, player);
                           },
                           (error) =>
                           {
                               string errorMessage = "ChangeCardData can't 'DeleteKeys' Card: " + oldCardNumber + " key, cuz:\n" +
                               error.Message;
                               PlayerIO.ErrorLog.WriteError(errorMessage);

                               player.Send(MessageType, false,
                                   errorMessage);
                           });
                    }
                    else
                    {
                        string errorMessage = "ChangeCardData card " + newCardNumber + " is not created yet, so can't delete card " + oldCardNumber;
                        PlayerIO.ErrorLog.WriteError(errorMessage);

                        player.Send(DebugMessageType, errorMessage);
                    }
                },
                100);
            },
            (error) =>
            {
                string errorMessage = "ChangeCardData can't 'Load' Card: " + oldCardNumber + " key in " +
                MessageType + ", cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #region Add Created Card To Garage/Appraiser/Admin
    private void AddCardToGarage(string newCardNumber, string garageName, string appraiserName, string MessageType,
        bool isConfirmed, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", garageName,
            (garageBigData) =>
            {
                if (garageBigData == null)
                {
                    string errorMessage = "AddCardToGarage 'Load' Garage: " + garageName
                    + " succes, but 'garageBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!garageBigData.TryGetValue("actualCards", out temp))
                {
                    string errorMessage = "AddCardToGarage property 'actualCards' in Garage: " + garageName
                    + " does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string actualCards = garageBigData.GetString("actualCards");

                if (string.IsNullOrEmpty(actualCards))
                    actualCards = newCardNumber;
                else
                    actualCards += "_" + newCardNumber;

                garageBigData.Set("actualCards", actualCards);
                garageBigData.Save(() =>
                {
                    AddCardToAdmin(newCardNumber, appraiserName, isConfirmed, MessageType, player);
                },
                (error) =>
                {
                    string errorMessage = "AddCardToGarage can't 'Save' 'cardNumber' in Garage: " +
                    garageName + " key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "AddCardToGarage can't 'Load' Garage: " +
                garageName + " key, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void AddCardToAdmin(string newCardNumber, string appraiserName, bool isConfirmed, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
            (adminBigData) =>
            {
                object temp;
                if (!adminBigData.TryGetValue("allCards", out temp))
                {
                    string errorMessage = "AddCardToAdmin property 'allCards' in Admin does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                string allCards = adminBigData.GetString("allCards");
                if (string.IsNullOrEmpty(allCards))
                    allCards = newCardNumber;
                else
                    allCards += "_" + newCardNumber;

                string allallCards = adminBigData.GetString("allallCards");
                if (string.IsNullOrEmpty(allallCards))
                    allallCards = newCardNumber;
                else
                    allallCards += "_" + newCardNumber;


                adminBigData.Set("allCards", allCards);
                adminBigData.Set("allallCards", allallCards);
                adminBigData.Save(() =>
                {
                    if (isConfirmed)
                    {
                        AddCardToAppraiser(newCardNumber, appraiserName, MessageType, player);
                    }
                    else
                    {
                        player.Send(MessageType, true, newCardNumber);
                    }
                },
                (error) =>
                {
                    string errorMessage = "AddCardToAdmin can't 'Save' 'cardNumber' in Admin key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "AddCardToAdmin can't 'Load' Admin, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void AddCardToAppraiser(string cardNumber, string appraiserName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
            (appraiserBigData) =>
            {
                if (appraiserBigData == null)
                {
                    string errorMessage = "AddCardToAppraiser 'Load' Appraiser: " +
                    appraiserName + " succes, but 'appraiserBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!appraiserBigData.TryGetValue("actualCards", out temp))
                {
                    string errorMessage = "AddCardToAppraiser property 'actualCards' in Appraiser: " +
                    appraiserName + " does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                string actualCards = appraiserBigData.GetString("actualCards");
                if (string.IsNullOrEmpty(actualCards))
                    actualCards = cardNumber;
                else
                    actualCards += "_" + cardNumber;

                appraiserBigData.Set("actualCards", actualCards);
                appraiserBigData.Save(() =>
                {
                    player.Send(MessageType, true, cardNumber);
                },
                (error) =>
                {
                    string errorMessage = "AddCardToAppraiser can't 'Save' 'cardNumber' in Appraiser: " +
                    appraiserName + " key, cuz:\n" + error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "AddCardToAppraiser Can't 'Load' Appraiser: " +
                appraiserName + " key, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #region Remove Card From Garage/Appraiser/Admin
    private bool isCardWasRemoved;
    private void RemoveCardFromGarage(string oldCardNumber, string garageName, string appraiserName,
        bool isConfirmed, string MessageType, MyPlayer player)
    {
        isCardWasRemoved = true;
        PlayerIO.BigDB.Load("PlayerObjects", garageName,
            (garageBigData) =>
            {
                if (garageBigData == null)
                {
                    string errorMessage = "RemoveCardFromGarage 'Load' Garage: " + garageName
                    + " succes, but 'garageBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!garageBigData.TryGetValue("actualCards", out temp))
                {
                    string errorMessage = "RemoveCardFromGarage property 'actualCards' in Garage: " + garageName
                    + " does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string rawActualCards = garageBigData.GetString("actualCards");

                string[] actualCards = rawActualCards.Split('_');
                if (!actualCards.Contains(oldCardNumber))
                {
                    string errorMessage = "RemoveCardFromGarage parage: " + garageName + " does not contains card " +
                        oldCardNumber + " in 'actualCards'";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                rawActualCards = string.Empty;
                for (int i = 0; i < actualCards.Length; ++i)
                {
                    if (oldCardNumber != actualCards[i])
                        rawActualCards += actualCards[i] + "_";
                }

                if (rawActualCards.EndsWith("_"))
                    rawActualCards = rawActualCards.Remove(rawActualCards.Length - 1, 1);

                garageBigData.Set("actualCards", rawActualCards);
                garageBigData.Save(() =>
                {
                    RemoveCardFromAdmin(oldCardNumber, appraiserName, isConfirmed, MessageType, player);
                },
                (error) =>
                {
                    string errorMessage = "RemoveCardFromGarage can't 'Save' 'actualCards' in Garage: " +
                    garageName + " key, cuz:\n" + error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "RemoveCardFromGarage can't 'Load' Garage: " +
                garageName + " key, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void RemoveCardFromAdmin(string oldCardNumber, string appraiserName, bool isConfirmed, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", "Admin",
            (adminBigData) =>
            {
                if (adminBigData == null)
                {
                    string errorMessage = "RemoveCardFromAdmin 'Load' Admin success, but 'adminBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!adminBigData.TryGetValue("allCards", out temp))
                {
                    string errorMessage = "RemoveCardFromGarage property 'allCards' in Admin does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string rawAllCards = adminBigData.GetString("allCards");

                string[] allCards = rawAllCards.Split('_');
                if (!allCards.Contains(oldCardNumber))
                {
                    string errorMessage = "RemoveCardFromAdmin Admin does not contains card " +
                        oldCardNumber + " in 'allCards'";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                else
                {
                    rawAllCards = string.Empty;
                    for (int i = 0; i < allCards.Length; ++i)
                    {
                        if (oldCardNumber != allCards[i])
                            rawAllCards += allCards[i] + "_";
                    }
                    if (rawAllCards.EndsWith("_"))
                        rawAllCards = rawAllCards.Remove(rawAllCards.Length - 1, 1);
                }

                adminBigData.Set("allCards", rawAllCards);
                adminBigData.Save(() =>
                {
                    if (isConfirmed)
                        RemoveCardFromAppraiser(oldCardNumber, appraiserName, MessageType, player);
                    else
                        isCardWasRemoved = true;
                },
                (error) =>
                {
                    string errorMessage = "RemoveCardFromAdmin can't 'Save' 'cardNumber' in Admin key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "RemoveCardFromAdmin can't 'Load' Admin, cuz:\n" +
                error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    private void RemoveCardFromAppraiser(string oldCardNumber, string appraiserName, string MessageType, MyPlayer player)
    {
        PlayerIO.BigDB.Load("PlayerObjects", appraiserName,
            (appraiserBigData) =>
            {
                if (appraiserBigData == null)
                {
                    string errorMessage = "RemoveCardFromAppraiser 'Load' Appraiser: " + player.ConnectUserId
                    + " succes, but 'appraiserBigData' is null!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                object temp;
                if (!appraiserBigData.TryGetValue("actualCards", out temp))
                {
                    string errorMessage = "RemoveCardFromAppraiser property 'actualCards' in Appraiser: " + player.ConnectUserId
                    + " does not exist!";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }
                string rawActualCards = appraiserBigData.GetString("actualCards");

                string[] actualCards = rawActualCards.Split('_');
                if (!actualCards.Contains(oldCardNumber))
                {
                    string errorMessage = "RemoveCardFromAppraiser Appraiser: " + player.ConnectUserId + " does not contains card " +
                        oldCardNumber + " in 'actualCards'";
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                    return;
                }

                rawActualCards = string.Empty;
                for (int i = 0; i < actualCards.Length; ++i)
                {
                    if (oldCardNumber != actualCards[i])
                        rawActualCards += actualCards[i] + "_";
                }

                if (rawActualCards.EndsWith("_"))
                    rawActualCards = rawActualCards.Remove(rawActualCards.Length - 1, 1);

                appraiserBigData.Set("actualCards", rawActualCards);
                appraiserBigData.Save(() => { isCardWasRemoved = true; },
                (error) =>
                {
                    string errorMessage = "RemoveCardFromAppraiser can't 'Save' 'cardNumber' in Appraiser: " +
                    player.ConnectUserId + " key, cuz:\n" +
                    error.Message;
                    PlayerIO.ErrorLog.WriteError(errorMessage);

                    player.Send(MessageType, false,
                        errorMessage);
                });
            },
            (error) =>
            {
                string errorMessage = "RemoveCardFromAppraiser can't 'Load' Appraiser: " +
                player.ConnectUserId + " key, cuz:\n" + error.Message;
                PlayerIO.ErrorLog.WriteError(errorMessage);

                player.Send(MessageType, false,
                    errorMessage);
            });
    }
    #endregion

    #endregion

    #region Update Card Info
    private void UpdateAppraiserCardInfo(string cardNumber, DatabaseObject cardBigData, string MessageType, MyPlayer player)
    {
        object temp;
        if (!cardBigData.TryGetValue("connectedAppraiser", out temp))
        {
            string errorMessage = "UpdateAppraiserCardInfo property 'connectedAppraiser' Card " + cardNumber + " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false, errorMessage);
            return;
        }
        string connectedAppraiser = cardBigData.GetString("connectedAppraiser");

        PlayerIO.BigDB.Load("PlayerObjects", connectedAppraiser,
         (appraisersBigData) =>
         {
             if (appraisersBigData == null)
             {
                 string errorMessage = string.Format("UpdateCardInfo 'Load' Appraiser {0} succes, but 'appraisersBigData' is null!",
                     connectedAppraiser);
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false,
                     errorMessage);
                 return;
             }

             if (!appraisersBigData.TryGetValue("displayName", out temp))
             {
                 string errorMessage = "UpdateAppraiserCardInfo property 'displayName' in Appraiser " + connectedAppraiser + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string displayName = appraisersBigData.GetString("displayName");

             string appraiserBlockName = connectedAppraiser + "\\" + displayName;

             if (!cardBigData.TryGetValue("carNumber", out temp))
             {
                 string errorMessage = "UpdateAppraiserCardInfo property 'carNumber' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string carNumber = cardBigData.GetString("carNumber");

             if (!cardBigData.TryGetValue("status", out temp))
             {
                 string errorMessage = "UpdateAppraiserCardInfo property 'status' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string status = cardBigData.GetString("status");

             if (!cardBigData.TryGetValue("signal", out temp))
             {
                 string errorMessage = "UpdateAppraiserCardInfo property 'signal' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string signal = cardBigData.GetString("signal");

             if (!cardBigData.TryGetValue("confirmed", out temp))
             {
                 string errorMessage = "UpdateAppraiserCardInfo property 'confirmed' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             bool confirmed = cardBigData.GetBool("confirmed");

             string rawCardData = string.Format("{0}_{1}_{2}_{3}_{4}",
                 cardNumber, carNumber, status, signal, confirmed);

             foreach (MyPlayer pl in players)
             {
                 pl.Send(GetUpdateCardInfoMessageType, true, appraiserBlockName, rawCardData);
             }
         },
         (error) =>
         {
             string errorMessage = string.Format("UpdateAppraiserCardsInfo can't 'Load' card {0} for Appraiser {1}, cuz:\n",
                 cardNumber, connectedAppraiser) + error.Message;
             PlayerIO.ErrorLog.WriteError(errorMessage);

             player.Send(MessageType, false,
                 errorMessage);
         });
    }
    private void UpdateGarageCardInfo(string cardNumber, DatabaseObject cardBigData, string MessageType, MyPlayer player)
    {
        object temp;
        if (!cardBigData.TryGetValue("connectedGarage", out temp))
        {
            string errorMessage = "UpdateGarageCardsInfo property 'connectedGarage' Card " + cardNumber + " key does not exist!";
            PlayerIO.ErrorLog.WriteError(errorMessage);

            player.Send(MessageType, false, errorMessage);
            return;
        }
        string connectedGarage = cardBigData.GetString("connectedGarage");

        PlayerIO.BigDB.Load("PlayerObjects", connectedGarage,
         (garageBigData) =>
         {
             if (garageBigData == null)
             {
                 string errorMessage = string.Format("UpdateGarageCardsInfo 'Load' Garage {0} succes, but 'appraisersBigData' is null!",
                     connectedGarage);
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false,
                     errorMessage);
                 return;
             }

             if (!garageBigData.TryGetValue("displayName", out temp))
             {
                 string errorMessage = "UpdateGarageCardsInfo property 'displayName' in Garage " + connectedGarage + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string displayName = garageBigData.GetString("displayName");

             string garageBlockName = connectedGarage + "\\" + displayName;

             if (!cardBigData.TryGetValue("carNumber", out temp))
             {
                 string errorMessage = "ChangeCardSignal property 'carNumber' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string carNumber = cardBigData.GetString("carNumber");

             if (!cardBigData.TryGetValue("status", out temp))
             {
                 string errorMessage = "ChangeCardSignal property 'status' in Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             string status = cardBigData.GetString("status");

             if (!cardBigData.TryGetValue("confirmed", out temp))
             {
                 string errorMessage = "ChangeCardSignal property 'confirmed' Card " + cardNumber + " key does not exist!";
                 PlayerIO.ErrorLog.WriteError(errorMessage);

                 player.Send(MessageType, false, errorMessage);
                 return;
             }
             bool confirmed = cardBigData.GetBool("confirmed");

             string rawCardData = string.Format("{0}_{1}_{2}_{3}",
                 cardNumber, carNumber, status, confirmed);

             foreach (MyPlayer pl in players)
             {
                 pl.Send(GetUpdateCardInfoMessageType, false, garageBlockName, rawCardData);
             }
         },
         (error) =>
         {
             string errorMessage = string.Format("UpdateGarageCardsInfo can't 'Load' card {0} for Garage {1}, cuz:\n",
                 cardNumber, connectedGarage) + error.Message;
             PlayerIO.ErrorLog.WriteError(errorMessage);

             player.Send(MessageType, false,
                 errorMessage);
         });
    }
    #endregion

    #endregion

    #region Utilities
    private int StringFind(string targetStr, string searchStr)
    {
        for (int i = 0; i < targetStr.Length; ++i)
        {
            if (targetStr[i] == searchStr[0])
            {
                bool isWrong = false;
                for (int j = 0, k = i; j < searchStr.Length; ++j, ++k)
                {
                    if (targetStr[k] != searchStr[j])
                    {
                        isWrong = true;
                        break;
                    }
                }
                if (!isWrong)
                    return i;
            }
        }
        return -1;
    }
    #endregion

    public override void GameClosed()
    {
        if (currentScheldueTask != null)
            currentScheldueTask.Stop();

        base.GameClosed();
    }
}