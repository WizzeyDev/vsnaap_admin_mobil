﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class EMenu : Editor {

	[MenuItem("EasyPP/Feedback")]
	static void FeedbackForm(){
		Application.OpenURL(@"https://docs.google.com/forms/d/1PGQGMRniAr69FgcohDa548DlZbdTPUgnNVDAUjXcLiM/viewform");
	}

	[MenuItem("EasyPP/Add Printer Object")]
	static void AddPrinterInstance(){
		if (GameObject.FindObjectOfType (typeof(Prnt)) != null) {
			Debug.LogError("There is already Prnt object in scene");
			return;
		}

		GameObject _prntObj = new GameObject();
		_prntObj.name = "PrinterObj";
		_prntObj.AddComponent<Prnt> ();

		Debug.Log ("Select PrinterObj and assign texture for printing and settings UI elements");
	}

}
