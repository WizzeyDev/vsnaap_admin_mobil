﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
public class Prnt : MonoBehaviour {

	public Texture2D texture;

	public ToggleGroup m_ColorToggleGroup;
	public ToggleGroup m_OrientationToggleGroup;
	public ToggleGroup m_ScaleToggleGroup;

	public void Print(){
		Toggle color = m_ColorToggleGroup.ActiveToggles().ElementAt(0);
		Toggle orient = m_OrientationToggleGroup.ActiveToggles().ElementAt(0);
		Toggle scale = m_ScaleToggleGroup.ActiveToggles().ElementAt(0);
		//Debug.Log(color.name + orient.name + scale.name);

		PrntPlatformClass.ColorModeEnum c = 0;
		PrntPlatformClass.OrientationEnum o = 0;
		PrntPlatformClass.ScaleModeEnum s = 0;
		switch (color.name){
		case "Color":
			c = PrntPlatformClass.ColorModeEnum.COLOR_MODE_COLOR;
			break;
		case "BN":
			c = PrntPlatformClass.ColorModeEnum.COLOR_MODE_MONOCHROME;
			break;
		}

		switch(orient.name){
		case "Portrait":
			o = PrntPlatformClass.OrientationEnum.ORIENTATION_PORTRAIT;
			break;
		case "Landscape":
			o = PrntPlatformClass.OrientationEnum.ORIENTATION_LANDSCAPE;
			break;
		}

		switch(scale.name){
		case "Fit":
			s = PrntPlatformClass.ScaleModeEnum.SCALE_MODE_FIT;
			break;
		case "Fill":
			s = PrntPlatformClass.ScaleModeEnum.SCALE_MODE_FILL;
			break;
		}
		Debug.Log(((int)c).ToString() + ((int)o).ToString() + ((int)s).ToString());
		PrntPlatformClass.Print("UNITY TEST",texture,c,o,s);	
	}
}
