﻿using UnityEngine;
using System.Collections;

public static class PrntPlatformClass
{
	public enum ColorModeEnum
	{
		COLOR_MODE_COLOR = 2,
		COLOR_MODE_MONOCHROME = 1
	};
	public enum OrientationEnum
	{
		ORIENTATION_LANDSCAPE = 1,
		ORIENTATION_PORTRAIT = 2
	};
	public enum ScaleModeEnum
	{
		SCALE_MODE_FIT = 1,
		SCALE_MODE_FILL = 2
	};


#if UNITY_EDITOR
	public static void Print(string jobName, Texture2D imageTexture,ColorModeEnum colorMode = ColorModeEnum.COLOR_MODE_COLOR,
	                  OrientationEnum orientation = OrientationEnum.ORIENTATION_LANDSCAPE,
	                  ScaleModeEnum scaleMode = ScaleModeEnum.SCALE_MODE_FIT )
	{
		Debug.Log("PRINT ONLY ON ANDROID DEVICE");
	}
#endif

#if !UNITY_EDITOR
#if UNITY_ANDROID
	public static void Print(string jobName, Texture2D imageTexture,ColorModeEnum colorMode = ColorModeEnum.COLOR_MODE_COLOR,
	                  OrientationEnum orientation = OrientationEnum.ORIENTATION_LANDSCAPE,
	                  ScaleModeEnum scaleMode = ScaleModeEnum.SCALE_MODE_FIT )
	{

		AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
		AndroidJavaObject activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaClass PrinterClass = new AndroidJavaClass ("com.umair.printerplugin.PrinterProxyClass");
		AndroidJavaObject PrinterInstance = PrinterClass.CallStatic<AndroidJavaObject>("Instance");
		byte[] bytes = imageTexture.EncodeToJPG();
		PrinterInstance.Call("Print",activityContext,jobName, bytes,(int)colorMode,(int)orientation,(int)scaleMode);

	}
#endif
#endif
}
