﻿using UnityEngine;
using System.Collections;

public class QuitExample : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }


	}

}
