﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MenuPanelManager : MonoBehaviour
{
    #region Singleton
    private static MenuPanelManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("MenuPanelManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private RectTransform rectTransform;

    [SerializeField]
    private GameObject menuPanel;
    [SerializeField]
    private GameObject menuButtonObj;

    [Space(10)]
    [SerializeField]
    private GameObject homeScreenObj;

    [Space(10)]
    [SerializeField]
    private TextMeshProUGUI appraiserDisplayNameText;
    #endregion

    #region Private Fields
    private System.Action ExitFromSystemEvent;

    private List<TextMeshProUGUI> allTextElements = new List<TextMeshProUGUI>();

    private Vector2 showSize;
    private Vector2 hideSize;

    private const float movingSpeed = 5000f;
    private const float colorSpeed = 100f;

    private const int siblingIndex = 11;

    private bool isMoving;
    #endregion

    #region Setup
    private void Start()
    {
        showSize = new Vector2(GameResolution.GetCurrentResolution().x * 0.7f, rectTransform.sizeDelta.y);
        hideSize = Vector2.zero; 
        rectTransform.sizeDelta = hideSize;
        appraiserDisplayNameText.text = "Admin";

        GetAllTextChildElements(rectTransform);
    }

    private void GetAllTextChildElements(Transform targetTransform)
    {
        foreach(Transform child in targetTransform)
        {
            TextMeshProUGUI textElement = targetTransform.GetComponent<TextMeshProUGUI>();
            if (textElement != null)
                allTextElements.Add(textElement);

            if (child.childCount == 0)
            {
                TextMeshProUGUI childTextElement = child.GetComponent<TextMeshProUGUI>();
                if(childTextElement != null)
                    allTextElements.Add(childTextElement);
            }
            else
                GetAllTextChildElements(child);
        }
    }
    #endregion

    #region Add Event Listiners
    public static void AddExitFromSystemEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.ExitFromSystemEvent += eventListiner;
    }
    #endregion

    #region Show Panel Part
    public void ShowMenu()
    {
        if (isMoving)
            return;

        isMoving = true;
        StartCoroutine(ShowMenuCoroutine());
    }
    private IEnumerator ShowMenuCoroutine()
    {
        menuPanel.SetActive(true);
        while (rectTransform.sizeDelta.x != showSize.x)
        {
            rectTransform.sizeDelta = Vector2.MoveTowards(rectTransform.sizeDelta,
                showSize, movingSpeed * Time.deltaTime);
            yield return null;
        }
        yield return ShowMenuTextElements();
    }

    private IEnumerator ShowMenuTextElements()
    {
        while (allTextElements[0].color.a != 1f)
        {
            foreach(TextMeshProUGUI textElement in allTextElements)
            {
                textElement.color = new Color(0f, 0f, 0f,
                    Mathf.MoveTowards(textElement.color.a, 1f, colorSpeed * Time.deltaTime));
            }
            yield return null;
        }

        isMoving = false;
    }
    #endregion

    #region Hide Panel Part
    public void HideMenu()
    {
        if (isMoving)
            return;

        isMoving = true;
        StartCoroutine(HideMenuCoroutine());
    }
    private IEnumerator HideMenuCoroutine()
    {
        StartCoroutine(HideMenuTextElements());
        while (rectTransform.sizeDelta.x != hideSize.x)
        {
            rectTransform.sizeDelta = Vector2.MoveTowards(rectTransform.sizeDelta,
                hideSize, movingSpeed * Time.deltaTime);
            yield return null;
        }
        isMoving = false;

        menuPanel.SetActive(false);
    }

    private IEnumerator HideMenuTextElements()
    {
        while (allTextElements[0].color.a != 0f)
        {
            foreach (TextMeshProUGUI textElement in allTextElements)
            {
                textElement.color = new Color(0f, 0f, 0f,
                    Mathf.MoveTowards(textElement.color.a, 0f, colorSpeed * Time.deltaTime));
            }
            yield return null;
        }
    }
    #endregion

    #region On Click
    public void ShowHomeScreen()
    {
        homeScreenObj.SetActive(true);
        SetCurrentScreenFirst(homeScreenObj.transform);

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowAppraiserBlocksScreen()
    {
        AdminBlockListScreen.ShowBlocksListScreen();

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowSearchScreen()
    {
        CardSearchScreen.ShowCardsSearchScreen();

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowGaragesAndAppraisersScreen()
    {
        GaragesAndAppraisersScreen.ShowGaragesAndAppraisersScreen();

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowCardCreationScreen()
    {
        CardCreationScreen.ShowCardsCreationScreen();

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowAdminDocumentsScreen()
    {
        AdminDocumentsScreen.ShowAdminDocumentsScreen();

        StartCoroutine(HideMenuCoroutine());
    }

    public void ShowEmailsListScreen()
    {
        EmailsListScreen.ShowEmailsListScreen();

        StartCoroutine(HideMenuCoroutine());
    }
    #endregion

    #region Show/Hide Menu Button
    public static void ShowMenuButton()
    {
        if (isNullInstance)
            return;

        instance.menuButtonObj.SetActive(true);
    }
    public static void HideMenuButton()
    {
        if (isNullInstance)
            return;

        instance.menuButtonObj.SetActive(false);
    }
    #endregion

    #region Setters
    public static void SetAppraiserDisplayName(string garageDisplayName)
    {
        if (isNullInstance)
            return;

        instance.appraiserDisplayNameText.text = garageDisplayName;
    }
    #endregion

    #region Change Order
    public static void SetCurrentScreenFirst(Transform screenTransform)
    {
        if (isNullInstance)
            return;

        screenTransform.SetSiblingIndex(siblingIndex);
    }

    public static void SetCurrentScreenZero(Transform screenTransform)
    {
        if (isNullInstance)
            return;

        screenTransform.SetSiblingIndex(siblingIndex + 2);
    }
    #endregion

    #region Exit From System
    public void ExitFromSystem()
    {
        ExitFromSystemEvent?.Invoke();

        rectTransform.sizeDelta = hideSize;
        menuPanel.SetActive(false);
    }
    #endregion
}
