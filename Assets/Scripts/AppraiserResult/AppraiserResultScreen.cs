﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AppraiserResultScreen : MonoBehaviour
{
    #region Singleton
    private static AppraiserResultScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("AppraiserResultScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject appraisersResultScreen;
    [SerializeField]
    private GameObject changeAppraiserWindowObj;

    [Space(10)]
    [SerializeField]
    private Transform appraisersListParent;
    [SerializeField]
    private GameObject appraiserElementPrefab;

    [Space(10)]
    [SerializeField]
    private ScrollRect scrollRect;

    [Space(10)]
    [SerializeField]
    private InputField appraiserName;
    [SerializeField]
    private InputField displayName;
    [SerializeField]
    private InputField passwordInputField;
    [SerializeField]
    private InputField idInputField;
    [SerializeField]
    private InputField officePhoneInputField;
    [SerializeField]
    private InputField carTaxInputField;
    [SerializeField]
    private InputField roleInputField;
    [SerializeField]
    private InputField areaInputField;
    [SerializeField]
    private InputField adressInputField;
    [SerializeField]
    private InputField emailInputField;
    [SerializeField]
    private InputField isActiveInputField;
    #endregion

    #region Private Fields
    private List<AppraiserResultElement> createdAppraiserElements;

    private bool isBusy;
    #endregion

    #region Show/Hide Appraisers Result Screen
    public static void ShowAppraisersResultScreen(List<AppraiserElement> allAppraisers)
    {
        if (isNullInstance)
            return;

        instance.ShowAppraiserResultScreen(allAppraisers);
    }
    public void ShowAppraiserResultScreen(List<AppraiserElement> allAppraisers)
    {
        appraisersResultScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(appraisersResultScreen.transform);

        if (instance.createdAppraiserElements != null)
        {
            CustomLogger.LogErrorMessage("ShowAppraiserResultScreen 'createdAppraiserElements' list is already exist!");
            return;
        }

        createdAppraiserElements = new List<AppraiserResultElement>();

        for (int i = 0; i < allAppraisers.Count; ++i)
        {
            AddAppraiserResultElementToList(allAppraisers[i]);
        }
    }

    public static void HideAppraisersResultScreen()
    {
        if (isNullInstance)
            return;

        instance.HideAppraiserResultScreen();
    }
    public void HideAppraiserResultScreen()
    {
        DestroyAllAppraisersList();
        appraisersResultScreen.SetActive(false);
        HideChangeAppraiserDataWindow();
    }
    public static void DestroyAllAppraisersList()
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements == null)
            return;

        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            Destroy(instance.createdAppraiserElements[i].gameObject);
        }
        instance.createdAppraiserElements.Clear();
        instance.createdAppraiserElements = null;
    }

    public static void RemoveAppraiserResultFromList(string appraiserName)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements == null)
            return;

        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetAppraiserName() == appraiserName)
            {
                Destroy(instance.createdAppraiserElements[i].gameObject);
                instance.createdAppraiserElements.RemoveAt(i--);
                return;
            }
        }
    }
    #endregion

    #region Appraiser Result Elements List
    private void AddAppraiserResultElementToList(AppraiserElement refElement)
    {
        AppraiserResultElement appraiserElement =
            Instantiate(instance.appraiserElementPrefab, instance.appraisersListParent).
            GetComponent<AppraiserResultElement>();
        appraiserElement.Setup(refElement);

        instance.createdAppraiserElements.Add(appraiserElement);
    }
    #endregion

    #region Open/Close Under Panel
    public static void SlideDownUnderPanel()
    {
        if (isNullInstance)
            return;

        instance.scrollRect.velocity = new Vector2(0f, 1000f);
    }
    #endregion

    #region Change Appraiser Data
    public static void ShowChangeAppraiserDataWindow(AppraiserResultElement refElement)
    {
        instance.changeAppraiserWindowObj.SetActive(true);

        instance.appraiserName.text = refElement.GetAppraiserName();
        instance.displayName.text = refElement.GetDisplayName();
        instance.passwordInputField.text = refElement.GetPassword();
        instance.idInputField.text = refElement.GetID();
        instance.officePhoneInputField.text = refElement.GetOfficePhone();
        instance.carTaxInputField.text = refElement.GetCarTax();
        instance.roleInputField.text = refElement.GetRole();
        instance.areaInputField.text = refElement.GetArea();
        instance.adressInputField.text = refElement.GetAdress();
        instance.emailInputField.text = refElement.GetEmail();
        instance.isActiveInputField.text = refElement.GetIsActive();
    }
    public void HideChangeAppraiserDataWindow()
    {
        instance.changeAppraiserWindowObj.SetActive(false);
    }

    public void ConfirmChangeAppraiser()
    {
        if (isBusy)
            return;

        string[] joinStrings = new string[]
        {
            appraiserName.text,
            passwordInputField.text,
            idInputField.text,
            officePhoneInputField.text,
            carTaxInputField.text,
            roleInputField.text,
            areaInputField.text,
            adressInputField.text,
            emailInputField.text,
            isActiveInputField.text
        };
        string rawAppraiserData = string.Join("_", joinStrings);

        isBusy = true;

        PlayerIOServerManager.ChangeAppraiserData(rawAppraiserData);
    }

    public static void ChangeAppraiserDataSuccess(string[] appraiserData)
    {
        if (isNullInstance)
            return;

        instance.HideChangeAppraiserDataWindow();

        if (instance.createdAppraiserElements == null)
        {
            CustomLogger.LogErrorMessage("ChangeAppraiserDataSuccess 'createdAppraiserElements' list is null!");
            return;
        }

        string appraiserName = appraiserData[0];
        string password = appraiserData[1];
        string ID = appraiserData[2];
        string officePhone = appraiserData[3];
        string carTax = appraiserData[4];
        string role = appraiserData[5];
        string area = appraiserData[6];
        string adress = appraiserData[7];
        string email = appraiserData[8];
        string isActive = appraiserData[9];
        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetAppraiserName() == appraiserName)
            {
                instance.createdAppraiserElements[i].UpdateData(password, ID, officePhone, carTax, role, adress,
                    area, isActive, email);
                return;
            }
        }
    }
    //public static void ChangeAppraiserDataFailed()
    //{
    //    if (isNullInstance)
    //        return;

    //    instance.StartCoroutine(instance.HighlighInputField(instance.displayNameInputField.image));
    //}
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
