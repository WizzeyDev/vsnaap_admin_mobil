﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmailElement : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI appraiserName;
    [SerializeField]
    private TMPro.TextMeshProUGUI appraiserEmail;
    [SerializeField]
    private Image selectButtonImg;

    private Color selectedColor = new Color32(232, 88, 88, 255);
    private Color unselectedColor = Color.white;

    private string rawEmailData;

    private bool isSelected;

    public void Setup(string rawEmailData)
    {
        this.rawEmailData = rawEmailData;
        string[] emailData = rawEmailData.Split('\\');
        this.appraiserName.text = emailData[0];
        this.appraiserEmail.text = emailData[1];
    }

    public void RemoveEmailElement()
    {
        EmailsListScreen.RemoveEmailFromList(this);
        AudioManager.PlayButtonSound();
    }

    public void SelectElement()
    {
        if(!isSelected)
            selectButtonImg.color = selectedColor;
        else
            selectButtonImg.color = unselectedColor;

        isSelected = !isSelected;
        AudioManager.PlayButtonSound();
    }

    public string GetRawEmailData()
    {
        return rawEmailData;
    }

    public string GetEmail()
    {
        return appraiserEmail.text;
    }

    public bool IsSelected()
    {
        return isSelected;
    }
}
