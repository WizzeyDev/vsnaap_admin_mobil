﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmailsListScreen : MonoBehaviour
{
    #region Singleton
    private static EmailsListScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("EmailsListScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject emailElementPrefab;
    [SerializeField]
    private RectTransform emailsListParent;

    [Space(10)]
    [SerializeField]
    private GameObject emailsScreenObj;
    [SerializeField]
    private GameObject addingWindowObj;

    [Space(10)]
    [SerializeField]
    private InputField appraiserNameInputField;
    [SerializeField]
    private InputField appraiserEmailInputField;

    [Space(10)]
    [SerializeField]
    private GameObject sendButton;
    #endregion

    #region Private Fields
    private List<EmailElement> emailElementsList = new List<EmailElement>();
    private List<GameObject> emailObjectsList = new List<GameObject>();

    private System.Action<string[]> OnEmailsSelected;
    #endregion

    #region Setup
    public static void CreateEmailsList(string rawEmailsData)
    {
        if (isNullInstance)
            return;

        instance.emailElementsList.Clear();
        instance.emailObjectsList.Clear();

        string[] emailsData = rawEmailsData.Split('_');
        foreach (string rawEmailData in emailsData)
        {
            instance.CreateEmailElement(rawEmailData);
        }
    }
    #endregion

    #region Show/Hide Screen
    public static void ShowEmailsListScreen()
    {
        if (isNullInstance)
            return;

        instance.emailsScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.emailsScreenObj.transform);
        instance.sendButton.SetActive(false);
    }
    public static void ShowEmailsListScreen(System.Action<string[]> OnEmailsSelected)
    {
        if (isNullInstance)
            return;

        instance.emailsScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.emailsScreenObj.transform);
        instance.sendButton.SetActive(true);
        instance.OnEmailsSelected = OnEmailsSelected;
    }

    public static void HideEmailsListScreen()
    {
        if (isNullInstance)
            return;

        instance.HideEmailListScreen();
    }

    public void HideEmailListScreen()
    {
        if (emailElementsList == null)
            return;

        foreach (EmailElement emailElement in emailElementsList)
        {
            if (emailElement.IsSelected())
                emailElement.SelectElement();
        }
        emailsScreenObj.SetActive(false);
    }
    #endregion

    #region Create/Destroy Email List
    private void CreateEmailElement(string rawEmailData)
    {
        GameObject cardStatusObj =
           Instantiate(emailElementPrefab, emailsListParent);
        emailObjectsList.Add(cardStatusObj);

        EmailElement cardStatusElement =
            cardStatusObj.GetComponent<EmailElement>();
        cardStatusElement.Setup(rawEmailData);
        emailElementsList.Add(cardStatusElement);
    }
    public static void DestroyAllEmailsList()
    {
        if (isNullInstance)
            return;

        if (instance.emailElementsList == null)
            return;

        for (int i = 0; i < instance.emailElementsList.Count; ++i)
        {
            Destroy(instance.emailElementsList[i].gameObject);
        }
        instance.emailElementsList.Clear();
    }

    public static void RemoveEmailFromList(EmailElement emailElement)
    {
        if (isNullInstance)
            return;

        PlayerIOServerManager.SendRemoveFromEmailMessage(emailElement.GetRawEmailData());
        Destroy(emailElement.gameObject);
        instance.emailElementsList.Remove(emailElement);
    }

    public static void RemoveEmailFromList(string rawEmailData)
    {
        if (isNullInstance)
            return;

        for (int i = 0; i < instance.emailElementsList.Count; i++)
        {
            if (instance.emailElementsList[i].GetRawEmailData() == rawEmailData)
            {
                Destroy(instance.emailElementsList[i].gameObject);
                instance.emailElementsList.RemoveAt(i--);
                return;
            }
        }
    }
    #endregion

    #region Add New Email
    private bool isAddingWindowOpen;
    public void ShowHideAddingEmailWindow()
    {
        appraiserNameInputField.text = string.Empty;
        appraiserEmailInputField.text = string.Empty;

        addingWindowObj.SetActive(!isAddingWindowOpen);

        isAddingWindowOpen = !isAddingWindowOpen;
    }
    public void AddNewEmail()
    {
        if (appraiserNameInputField.text == string.Empty ||
            appraiserEmailInputField.text == string.Empty)
            return;

        string rawEmailData = appraiserNameInputField.text + '\\' +
            appraiserEmailInputField.text;
        PlayerIOServerManager.SendAddNewEmailMessage(rawEmailData);

        ShowHideAddingEmailWindow();
    }
    public static void AddNewEmailSuccess(string rawEmailData)
    {
        if (isNullInstance)
            return;

        instance.CreateEmailElement(rawEmailData);
    }
    #endregion

    #region Send Documents
    public void SendDocumentsToSelectedEmails()
    {
        List<string> selectedEmails = new List<string>();
        for (int i = 0; i < emailElementsList.Count; ++i)
        {
            EmailElement emailElement = emailElementsList[i];
            if (emailElement.IsSelected())
            {
                selectedEmails.Add(emailElementsList[i].GetEmail());
            }
        }
        OnEmailsSelected(selectedEmails.ToArray());

        HideEmailsListScreen();
    }
    #endregion
}
