﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullViewImageScreen : MonoBehaviour
{
    #region Singleton
    private static FullViewImageScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("FullViewImageScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
	private GameObject fullViewScreen;
    [SerializeField]
    private Image photoImage;
    #endregion

    #region Show/Hide Card Creation Screen
    public static void ShowFullViewImageScreen(Sprite img)
    {
        if (isNullInstance)
            return;

        instance.fullViewScreen.SetActive(true);
        instance.photoImage.sprite = img;
        AudioManager.PlayButtonSound();
    }
    public void HideFullViewImageScreen()
    {
        instance.photoImage.sprite = null;
        fullViewScreen.SetActive(false);
        AudioManager.PlayButtonSound();
    }
    #endregion
}
