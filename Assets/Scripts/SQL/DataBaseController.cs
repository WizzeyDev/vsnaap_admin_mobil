﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Linq;
//using System.Data;

public class DataBaseController : MonoBehaviour
{
    public SqliteConnection[] mDBConns; // mOrdersDBCon, mClientDBCon, mAdminDBCon;
    //string mOrderDbPath, mAdminDbPath, mClientDbPath;
    public string[] blockOfData;
    public static string[] mDbPaths;
    public static readonly string[] fileName = new string[]{"OrdersDB", "AdminDB" ,"ClientDB"},TableName = new string[] { "Orders","Admins", "Garages" }; //OrderDBNames = "/OrdersDB", AdminDbNames = "/AdminDb", ClientDbNames = "/ClientDb";
    public List<Orders> mMenuMealCards;

    System.Data.IDbCommand dbcmd;
    int[] tablesValueAmount = new int[] { 7, 11, 13 }; 
    int mDBFileCount = 3  , extra;
    bool isOrdersUpdated;
    string ordersData;

    static DataBaseController instance;
    private void Awake()
    {
        instance = this;
    }
    #region ------------------ Setup---------------------- 
    // Start is called before the first frame update
    void Start()
    {
        mDBConns = new SqliteConnection[mDBFileCount];
            mDbPaths = new string[mDBFileCount];

        for (int i = 0; i < mDBFileCount; i++) //Check if File exsits and create if not setup filepath to 
        {
            mDbPaths[i] = "URI=file:" + Utility.CreateFilePath('/' + fileName[i]);
            Debug.Log("Init DB path" + mDbPaths[i]);
            mDBConns[i] = DataStorage.NewSQL.InitSQLConnectsion(mDbPaths[i]);
            mDBConns[i].Open();
            Debug.Log("Opened Conn " + mDBConns[i].State + " " + i);

            CreateModulareDB(i, TableName[i], tablesValueAmount[i]); //Create Tables

            dbcmd = mDBConns[i].CreateCommand(); //Create Command

            //dbcmd = null; //Realse
        }
        for (int i = 0; i < 7; i++) //Create Orders Data Structure
        {
            ordersData += "data" + i.ToString() + ",";
        }
        ordersData = ordersData.Remove(ordersData.Length - 1);
    }

    public void CreateModulareDB(int sqConn, string tableDBName, int columnsAmount)
    {

        string commandText = "CREATE TABLE IF NOT EXISTS " + tableDBName + " (";//+ 0 + " " + "TEXT";
        for (var i = 0; i < columnsAmount; i++)
        {
            commandText += "data"+ i.ToString() + " TEXT, ";
        }
        commandText = commandText.Remove(commandText.Length -2);
        commandText += " );";
        Debug.Log("CreateModulareDB " + commandText );
        using (var command = mDBConns[sqConn].CreateCommand())
        {  // Create an sql command that creates a new table.

            command.CommandText = commandText;

            command.ExecuteNonQuery();
        }
    }



    #endregion ------------------ Setup---------------------- 

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            //ToggleAllDBConn(false);
            for (int i = 0; i < mDbPaths.Length; i++)
            {
                //StartCoroutine(UploadToDropBoxDelayed(mDbPaths[i].Remove(0, 9)));
                //Debug.Log("Uploading "  + i + " P: " + mDbPaths[i].Remove(0, 9));


                //DropBoxTest.UploadCapturedToDropbox(mDbPaths[i].Remove(0,9));
            }
            //GenrateInsert(TableName[0],tablesValueAmount[0]);

        }

             if (Input.GetKeyDown(KeyCode.D)) //Handle OrderList
        {
            if (!isOrdersUpdated)
            {
                //GetAllDataFromTable(0, TableName[0]); 
                mMenuMealCards = new List<Orders>(); 
                mMenuMealCards = GetOrdersData("!= ", 0, "Orders", mDbPaths[0]);//Populate the order list
            }
            for (int i = 0; i < mMenuMealCards.Count; i++)
            {             
                CardChangerScreen.CreateOrderCard(mMenuMealCards[i],i);
            }
            //for (int i = 0; i < ordersData.Length; i++)  //Create Order Cards
            //{
            //    CardChangerScreen.CreateOrderCard(ordersData[i]);
            //}  
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            ToggleAllDBConn(false);
        }
    
    }

    public static void UpdateOrders(string[] values,int id)
    {
        instance.dbcmd = instance.mDBConns[0].CreateCommand();
        string value = " SET ";
        for (int i = 0; i < values.Length; i++)
        {
            value += "data" + i + " = '" + values[i] + "',";
        }
        //value.TrimEnd();
      
        value += "WHERE ROWID = '" + id + "'";
        //string colums = "UPDATE " + tableName.ToString() + " SET data1 = 'DIMACHANGE',data4 = 'DIMACHANGE',data5 = 'DIMACHANGE' WHERE ROWID = '1'";
        //Debug.Log("<color=red> Update: </color>" + colums);
        instance.dbcmd.CommandText = "UPDATE " + TableName[0];
        instance.dbcmd.CommandText += value;
        Debug.Log(id+"<color=red> UpdateOrders </color>" + instance.dbcmd.CommandText);
        //instance.dbcmd.CommandText = value;
        instance.dbcmd.ExecuteNonQuery();
    }

    void GenrateInsert(string tableName, int columnsAmount)
    {
        dbcmd = mDBConns[0].CreateCommand();
        
        string colums = "INSERT INTO " + tableName.ToString() + " (";
        string values = " ) VALUES ('";
        for (int i = 0; i < columnsAmount; i++)
        {
            colums += "data" + i.ToString() + ",";
            values += "dima" + extra + i.ToString() + "', '";
        }
        colums = colums.Remove(colums.Length - 1);
        values = values.Remove(values.Length - 4);
        values += "' )";
        dbcmd.CommandText = colums + values;
        Debug.Log("GenrateInsert " + dbcmd.CommandText + " "  + tableName);
        extra++;
        dbcmd.ExecuteNonQuery();
    }

    public void GetAllDataFromTable(int sqConn, string tableDBName)
    {
        dbcmd = mDBConns[sqConn].CreateCommand();
        dbcmd.CommandText = "SELECT * FROM " + tableDBName;

        Debug.Log("GetAllDataFromTable: " + dbcmd.CommandText + " sq " + sqConn);
        System.Data.IDataReader reader = dbcmd.ExecuteReader();
        while (reader.Read())
        {
            Debug.Log("reader: " + reader.FieldCount );
            for (int i = 0; i < reader.FieldCount; i++)
            {            
                blockOfData[i] = reader[i].ToString();
            }
        }
        //reader.Close();
    }

    /// <summary>
    /// Menu DB for save by Admin & show on Client 
    ///  <param name="sqlComparisonOperator"> oPerastion like = != and ETC... dont forget the space " " </param>
    /// <param name="comparisonExprastion"> int for which resturant the DB belongs to</param>
    /// </summary>
    public List<Orders> GetOrdersData(string sqlComparisonOperator, int comparisonExprastion, string dbName, string dbPath)
    {
        List<Orders> menuDB = new List<Orders>();
        try
        {
            using (SqliteConnection conn = new SqliteConnection(dbPath))
            {
                conn.Open();
                string sql = "SELECT * FROM " + dbName; //+ " WHERE tab " + sqlComparisonOperator + comparisonExprastion;
                Debug.Log("SQL Commaned is: " + sql);
                using (SqliteCommand cmd = new SqliteCommand(sql, conn))
                {
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            //menuDB.Add(new MenuDB(reader["id"], reader["tabName"], reader["tabName"], reader["tabName"], reader["tabName"], reader["tabName"], reader["tabName"]))
                            menuDB.Add(new Orders(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(),reader[3].ToString()
                                , reader[4].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString()));
                        }
                    }
                }
                conn.Close();
            }
        }
        catch (System.Exception)
        {

            throw;
        }

        return menuDB;
    }

    public void GetAllDataFromOrders(int sqConn, string tableDBName)
    {
        dbcmd = mDBConns[sqConn].CreateCommand();
        //dbcmd.CommandText = "PRAGMA table_info(" + tableDBName + ");";
        //dbcmd.ExecuteNonQuery();
        dbcmd.CommandText = "SELECT * FROM " + tableDBName;
        //System.Data.DataTable table = mDBConns[sqConn].GetSchema("tableDBName");
        //print("Scheme: " + table);
        System.Data.IDataReader reader = dbcmd.ExecuteReader();

        //ordersData = new string[reader.FieldCount]; //Set array Size
        Debug.Log("GetAllDataFromTable: " + dbcmd.CommandText + " sq " + sqConn);
        while (reader.Read())
        {     
            for (int i = 0; i < reader.FieldCount; i++)
            {
                //ordersData[i] = reader[i].ToString();
            }
        }
        reader.Close();
    }
    #region  --------- Inserting To SQL ---------------
    void InsertModularSQL(int sqlConn, string tableName, int columnsAmount)
    {
        string values = ") VALUES (";
        dbcmd = mDBConns[sqlConn].CreateCommand();
        dbcmd.CommandText = "INSERT INTO " + tableName.ToString() + " (";//appraiser,carID,eventDate,eventID,garageName,insuranceCompany,typeEvent) VALUES (?,?,?,?,?,?,?)", sqlCon);
        for (int i = 0; i < columnsAmount; i++)
        {
            dbcmd.CommandText += "data" + i.ToString() + ",";
            values += "?,";
        }
        dbcmd.CommandText = dbcmd.CommandText.Remove(dbcmd.CommandText.Length - 1);
        values = values.Remove(values.Length - 1);
        values += ")";
        dbcmd.CommandText += values;
        dbcmd.ExecuteNonQuery();
    }

    public static void InsertNewOrder(int sqlConn, string tableValues) {
        instance.InsertNewOrderSQL(sqlConn, tableValues);
    }

    void InsertNewOrderSQL(int sqlConn, string tableValues)
    {
        string values = ") VALUES (";
        dbcmd = mDBConns[sqlConn].CreateCommand();
        dbcmd.CommandText = "INSERT INTO " + TableName[0] +" ("+ ordersData +") VALUES"+ tableValues +")"; //,?,?,?,?" +")";  //appraiser,carID,eventDate,eventID,garageName,insuranceCompany,typeEvent) VALUES (?,?,?,?,?,?,?)", sqlCon);
          Debug.Log("Insert Order TO SQL " + instance.dbcmd.CommandText );
        dbcmd.ExecuteNonQuery();
    }

    public static void InsertIntoOrder(string tableName, int columnsAmount)
    {
        instance.dbcmd = instance.mDBConns[0].CreateCommand();
        int extra = 0;
        string colums = "INSERT INTO " + tableName.ToString() + " (";
        string values = " ) VALUES ('";
        for (int i = 0; i < columnsAmount; i++)
        {
            colums += "data" + i.ToString() + ",";
            values += "dima" + extra + i.ToString() + "', '";
        }
        colums = colums.Remove(colums.Length - 1);
        values = values.Remove(values.Length - 4);
        values += "' )";
        instance.dbcmd.CommandText = colums + values;
        Debug.Log("GenrateInsert " + instance.dbcmd.CommandText + " " + tableName);
        extra++;
        instance.dbcmd.ExecuteNonQuery();
    }
    #endregion --------- Inserting To SQL ---------------




    public static void ToggleAllDBConn(bool isOn)
    {
        Debug.Log("toggleAllDBConn: "  + isOn + " L: " + instance.mDBConns.Length);
        for (int i = 0; i < instance.mDBConns.Length; i++)
        {
            if (isOn)
            {
                instance.mDBConns[i].Open();
            }
            else
            {

            instance.mDBConns[i].Close();
            }
        }
    }

    private void OnApplicationQuit()
    {
        Debug.Log("Exiting");
        ToggleAllDBConn(false);
    }

    IEnumerator UploadToDropBoxDelayed(string filePath)
    {
        print("Starting DropBox Upload " + filePath);
        var request = DropBoxTest.UploadCapturedToDropbox(filePath);
        //var request = UploadManger.DropBoxTest("/1.txt");
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {

            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:

                print("Uploaded to DropBox Yay");
                break;
            default:

                Debug.LogError("DropBox Error " + request.downloadHandler.text);


                break;
        }
        print(request.responseCode + " text " + request.downloadHandler.text);

        //UploadManger.UploadCapturedToDropbox(m_VideoPath);


    }  
}

[System.Serializable]
public class Orders
{
    public string carID, eventID,orderID, eventDate, typeEvent;
    public string garageName, appraiser, insuranceCompany;


    public Orders(string eventID, string carID, string garage,string order, string eventDate, string appraiser, string typeEvent, string insuranceCompany)
    {
        this.carID = carID;
        this.eventID = eventID;
        this.eventDate = eventDate;
        orderID = order;
        garageName = garage;
        this.appraiser = appraiser;
        this.typeEvent = typeEvent;
        this.insuranceCompany = insuranceCompany;
    }
}