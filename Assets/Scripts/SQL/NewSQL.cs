﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;

namespace DataStorage
{
    public class NewSQL 
    {
      public static SqliteConnection InitSQLConnectsion(string mDbPath)
        {
            if (System.IO.File.Exists(mDbPath))
            {
                Debug.Log("No File");
                Utility.CreateFilePath(mDbPath);
            }

            if (!mDbPath.StartsWith("URI=file:"))
                mDbPath = "URI=file:" + mDbPath;

            SqliteConnection dbSQConn = new SqliteConnection(mDbPath);
            return dbSQConn;
        }

    }
}