﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
public enum SQLiteValue { INTEGER, Real, TEXT, BLOB }
public class SQL : MonoBehaviour
{

    public ModularDataContainer[] dima2;
    public ModularDataContainer appraisersDB;
    public SqliteConnection mConnection, mOrderDBCon;
    string mOrderDbPath, mAdminDbPath, mClientDbPath, morderDBNames = "/OrdersDB", mAdminDbNames = "/AdminDb", mClientDbNames = "/ClientDb";
    string[] dbpaths , dbNames = { "OrdersDB" , "AdminDb" , "ClientDb" }; 
   
    #region Setup

    public static SQL instance;
    private void Awake()
    {
        instance = this;
#if UNITY_EDITOR || UNITY_STANDALONE
        //mOrderDbPath = "URI=file:" + Application.dataPath;
        //if (!System.IO.File.Exists(Application.dataPath + mDBNames))
        //{
        //    System.IO.File.Create(Application.dataPath + mDBNames);
        //    mDbPath = Application.dataPath + mDBNames;
        //}


        mOrderDbPath = "URI=file:" + Utility.CreateFilePath(morderDBNames); //Create File for DB
        var sq = OpenNewSQLCon(mOrderDbPath);
        if (sq != null)
        {
            print("CreateOrderDB: " + sq.State + " Path: " + mOrderDbPath);

            CreateOrderDB(sq, "Orders");
            CreateOrderDB(sq, "Garages");
            CreateOrderDB(sq, "Appraisels");
        }
        //sq.Close();

        //for (int i = 0; i < dbpaths.Length; i++)
        //{
        //    dbpaths[i] = "URI=file:" + Utility.CreateFilePath('/' + dbNames[i]);

        //    CreateDB(dbpaths[i], dbNames[i],)

        //}


#endif
    }
 
    void Start()
    {
     
        //print("path " + "URI=file:" + Application.persistentDataPath + "/");
        //InsertToAnOrder(new Orders(0, 1, 2, 3, 4, 5, 6));
        //GetDataFilterTab("Orders", "URI=file:" + Application.dataPath + morderDBNames);


    }

    public static void CreateOrderDB(SqliteConnection sqlConn, string tableDBName)
    {
        sqlConn.Open();
        using (var command = sqlConn.CreateCommand())
        {  // Create an sql command that creates a new table.
            if (tableDBName == "Orders")
            {
            command.CommandText = "CREATE TABLE IF NOT EXISTS " + tableDBName + " (garageName TEXT,appraiser TEXT,carID TEXT,orderID TEXT,eventDate TEXT,eventID TEXT,insuranceCompany TEXT,typeEvent TEXT,statusBool TEXT,signalBool TEXT );";

            }
            if (tableDBName == "Appraisels") //user, passwored, showName,iD, phone,carID, role, zone, address, email ,  active;
            {
                command.CommandText = "CREATE TABLE IF NOT EXISTS " + tableDBName + " (user TEXT,pass TEXT,showName TEXT,ID TEXT,phone TEXT,carID TEXT,role TEXT,zone TEXT,address TEXT,email TEXT,activeBool TEXT );";
            }
            if (tableDBName == "Garages")// user, passwored, showName,iD, phone,cellPhon,fax,email, zone, address, meiazag ,video,active;
            {
                command.CommandText = "CREATE TABLE IF NOT EXISTS " + tableDBName + " (user TEXT,pass TEXT,showName TEXT,ID TEXT,phone TEXT,cellPhon TEXT,fax TEXT,email TEXT,zone TEXT,address TEXT,meiazag TEXT,videoBool TEXT,activeBool TEXT );";
            }
            //print(command.CommandText);
            command.ExecuteNonQuery();
        }
        sqlConn.Close();
    }

    #endregion 
    // Update is called once per frame
    void Update()
    {
        
    }

    public static SqliteConnection OpenNewSQLCon(string mDbPath)
    {
        //if (!Utility.CheckIfFileExist(mDbPath)) {
        //    mDbPath= Utility.CreateFilePath(mDbPath);
        //    Debug.Log("Created File cuz didnt find file" + mDbPath);
        //}
        if (!mDbPath.StartsWith("URI=file:"))
            mDbPath = "URI=file:" + mDbPath;

        print("2:" + mDbPath);
        instance.mOrderDBCon = new SqliteConnection(mDbPath);
        return instance.mOrderDBCon;
    }



    public void SendingOrder()
    { //Called By a button in Scene
        ////Check if list is empty
        //if (mShopingDB != null)
        //{
        //    string fileName = "Client" + mUserID + "oID" + mOrderID;
        //    string oDBPath = Utility.AssingFilePath("/" + fileName);
        //    //string oDBPath = Utility.CreateFilePath("/" + fileName);
        //    Debug.Log("1; " + fileName);

        //    SqliteConnection mSqlCon = SQLTabale.OpenNewSQLCon(oDBPath); //Open SQLCOnectsion
        //    SQLTabale.CreateOrderDB(mSqlCon, fileName);
        //    mSqlCon = SQLTabale.OpenNewSQLCon(oDBPath); //Open SQLCOnectsion
        //    for (int i = 0; i < mShopingDB.Count; i++)
        //    {
        //        Debug.Log("Makeing SQL N: " + i + " Count" + mShopingDB.Count);
        //        SQLTabale.InsertOrder(mShopingDB[i], mSqlCon, fileName);
        //        if (i == mShopingDB.Count - 1)
        //            NetworkController.UploadToFireBase("/" + fileName);
        //    }
        //    mSqlCon.Close(); //Close SQLCOnectsion
        //    mSqlCon.Dispose();
        //    mOrderID++; //Increase Order ID  TODO: Make Real OrderID
        //}
        //else
        //{
        //    Debug.Log("Empty Shoping");
        //    //}
        //}
    }
    #region  --------- Inserting To SQL ---------------

    public static void InsertToAnOrder(string orderDB)
    {
            print("orderDB.ToString(): " + orderDB.ToString());

        SqliteConnection sqlCon = DataStorage.NewSQL.InitSQLConnectsion(instance.mOrderDbPath);
        sqlCon.Open();
        SqliteCommand insertSQL = new SqliteCommand("INSERT INTO " + orderDB.ToString() + " (appraiser,carID,eventDate,eventID,garageName,insuranceCompany,typeEvent) VALUES (?,?,?,?,?,?,?)", sqlCon);
        
        //insertSQL.Parameters.Add(new SqliteParameter("appraiser", orderDB.appraiser));
        //insertSQL.Parameters.Add(new SqliteParameter("carID", orderDB.carID));
        //insertSQL.Parameters.Add(new SqliteParameter("eventDate", orderDB.eventDate));
        //insertSQL.Parameters.Add(new SqliteParameter("eventID", orderDB.eventID));
        //insertSQL.Parameters.Add(new SqliteParameter("garageName", orderDB.garageName));
        //insertSQL.Parameters.Add(new SqliteParameter("insuranceCompany", orderDB.insuranceCompany));
        //insertSQL.Parameters.Add(new SqliteParameter("typeEvent", orderDB.typeEvent));
        try
        {
            Debug.Log("<color=green> Inserted</color>");
            insertSQL.ExecuteNonQuery();
        }
        catch (System.Exception ex)
        {
            Debug.LogError("Something wrong with SQL " );
            throw new System.Exception(ex.Message);
        }
        sqlCon.Close(); //Close SQLCOnectsion
        //sqlCon.Dispose();
    }

    #endregion

    #region -------------- Read From SQL -------------------


    /// <summary>
    /// Menu DB for save by Admin & show on Client 
    ///  <param name="sqlComparisonOperator"> oPerastion like = != and ETC... dont forget the space " " </param>
    /// <param name="tableDBName"> int for which resturant the DB belongs to</param>
    /// </summary>
    public static void GetDataFilterTab(string tableDBName, string dbPath)
    {
        
        try
        {
            using (SqliteConnection conn = new SqliteConnection(dbPath))
            {
                conn.Open();
                //instance.dima2 = new List<Orders>();
                string sql = "SELECT * FROM " + tableDBName;
                Debug.Log("SQL Commaned is: " + sql);
                using (SqliteCommand cmd = new SqliteCommand(sql, conn))
                {
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        print("DB Depth:" + reader.Depth + " C: " + reader.FieldCount);
                    
                        while (reader.Read())
                        {
                            print("DB " + reader[0]);
                        
                            //Debug.LogError("Pringting DB Data: "+reader);
                            
                        }
                            reader.Close();
                    }
                    cmd.Dispose();
                }                
                conn.Close();
                Debug.Break();
            }
        }
        catch (System.Exception)
        {

            throw;
        }
 
    }
        #endregion

        #region ----------- Testing -------------

        void TestMe()
    {
        Utility.CreateFilePath("/" + "My_Database");
        //    string connection = "URI=file:" + Application.dataPath + "/" + "My_Database";
        //    // Open connection
        //    SqliteConnection dbcon = new SqliteConnection(connection);
        //    dbcon.Open();

        //    // Create table
        //    SqliteCommand dbcmd;
        //    dbcmd = dbcon.CreateCommand();
        //    string q_createTable = "CREATE TABLE IF NOT EXISTS my_table (id INTEGER PRIMARY KEY, val INTEGER )";

        //    dbcmd.CommandText = q_createTable;
        //    dbcmd.ExecuteReader();

        //    // Insert values in table
        //    SqliteCommand cmnd = dbcon.CreateCommand();
        //    cmnd.CommandText = "INSERT INTO my_table (id, val) VALUES (1, 5)";
        //    cmnd.ExecuteNonQuery();

        //    // Read and print all values in table
        //    SqliteCommand cmnd_read = dbcon.CreateCommand();
        //    SqliteDataReader reader;
        //    string query = "SELECT * FROM my_table";
        //    cmnd_read.CommandText = query;
        //    reader = cmnd_read.ExecuteReader();

        //    while (reader.Read())
        //    {
        //        Debug.Log("id: " + reader[0].ToString());
        //        Debug.Log("val: " + reader[1].ToString());
        //    }

        //    // Close connection
        //    dbcmd.Dispose();
        //    dbcon.Close();
        //    cmnd.Dispose();
        //    cmnd_read.Dispose();
        //    dbcon.Dispose();
        //    reader.Close();
   
    }
    public static void InsertModularDB() { 
    //IDbCommand dbcmd = getDbCommand();
    //dbcmd.CommandText =
    //            "INSERT INTO " + TABLE_NAME
    //            + " ( "
    //            + KEY_ID + ", "
    //            + KEY_TYPE + ", "
    //            + KEY_LAT + ", "
    //            + KEY_LNG + " ) "

    //            + "VALUES ( '"
    //            + location._id + "', '"
    //            + location._type + "', '"
    //            + location._Lat + "', '"
    //            + location._Lng + "' )";
    //        dbcmd.ExecuteNonQuery();
    }

    public static void CreateModulareDB(string dbPath, string tableDBName, string[] calName, int[] colType)
    {
        SqliteConnection sqlCon = DataStorage.NewSQL.InitSQLConnectsion(dbPath);
        sqlCon.Open();
        string commandText = "CREATE TABLE IF NOT EXISTS " + tableDBName + "(" + calName[0] + " " + (SQLiteValue)colType[0];
        for (var i = 1; i < calName.Length; i++)
        {
            commandText += ", " + calName[i] + " " + (SQLiteValue)colType[i];
        }
        commandText += ")";
        using (var command = sqlCon.CreateCommand())
        {  // Create an sql command that creates a new table.
       
            command.CommandText = commandText;
            
            command.ExecuteNonQuery();
        }
        sqlCon.Close();
    }

    #endregion
}



[System.Serializable]
public class ModularDataContainer
{
    public string[] stringData;
}

//[System.Serializable]
//public class Orders : ModularDataContainer //Table Name
//{
//    public new string[] stringData = new string[11];// user, passwored, showName, role, zone, address, email ,mID, phone, carID, active;
//}

//[System.Serializable]
//public class Appraisels : ModularDataContainer //Table Name
//{
//    public new string[] stringData = new string[2];// user, passwored, showName,iD, phone,carID, role, zone, address, email ,  active;
//}

//[System.Serializable]
//public class Garages : ModularDataContainer //Table Name
//{
//    public new string[] stringData = new string[2];// user, passwored, showName,iD, phone,cellPhon,fax,email, zone, address, meiazag ,video,active;
//}

//[System.Serializable]
//public class Admin : ModularDataContainer //Table Name
//{
//    public new string[] stringData = new string[5];// user, passwored, showName, role, zone, address, email ,mID, phone, carID, active;
//}