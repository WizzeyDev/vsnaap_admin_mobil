﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UnityRequestManager : MonoBehaviour
{
    #region Singleton
    private static UnityRequestManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("InviewUICanvasManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    public static void DownloadPhotos(string[] URLs, System.Action<Sprite[]> requestHandler)
    {
        instance.StartCoroutine(instance.DownloadPhotosCoroutine(URLs, requestHandler));
    }

    private IEnumerator DownloadPhotosCoroutine(string[] URLs, System.Action<Sprite[]> requestHandler)
    {
        bool atleastOneDownloaded = false;
        Sprite[] downloadedSprites = new Sprite[URLs.Length];
        for (int i = 0; i < URLs.Length; ++i)
        {
            if (string.IsNullOrEmpty(URLs[i]))
                continue;

            UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(URLs[i]);
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                if (uwr.isNetworkError)
                    Debug.LogError("DownloadPhotos Network Error:\n" + uwr.error);
                else
                    Debug.LogError("DownloadPhotos Http Error:\n" + uwr.error);
            }
            else
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(uwr);
                if (texture == null)
                {
                    Debug.LogError("Downloaded texture is null!!!");
                    yield break;
                }
                atleastOneDownloaded = true;
                Sprite sprite = Sprite.Create(texture,
                    new Rect(0, 0, texture.width, texture.height),
                    new Vector2(0.5f, 0.5f),
                    100);
                downloadedSprites[i] = sprite;
            }
            uwr.Dispose();
        }

        if (atleastOneDownloaded)
            requestHandler(downloadedSprites);
    }

    public static void DownloadPhoto(string URL, System.Action<Sprite> requestHandler)
    {
        instance.StartCoroutine(instance.DownloadPhotoCoroutine(URL, requestHandler));
    }

    private IEnumerator DownloadPhotoCoroutine(string URL, System.Action<Sprite> requestHandler)
    {
        if (string.IsNullOrEmpty(URL))
        {
            Debug.LogError("DownloadPhoto error:\n" +
                URL + " is null");
            yield break;
        }

        UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(URL);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            if (uwr.isNetworkError)
                Debug.LogError("DownloadPhotos Network Error:\n" + uwr.error);
            else
                Debug.LogError("DownloadPhotos Http Error:\n" + uwr.error);
        }
        else
        {
            Texture2D texture = DownloadHandlerTexture.GetContent(uwr);
            if (texture == null)
            {
                Debug.LogError("Downloaded texture is null!!!");
                yield break;
            }
            TextureScaler.Bilinear(texture, 256, 256);
            Sprite downloadedSprite = Sprite.Create(texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                100);

            uwr.Dispose();
            requestHandler(downloadedSprite);
        }
    }
}
