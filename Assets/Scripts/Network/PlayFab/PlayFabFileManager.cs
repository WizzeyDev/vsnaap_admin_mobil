﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.DataModels;
using PlayFab.Internal;
using System.Text;
using System.IO;

public partial class PlayFabManager : MonoBehaviour
{
    private int GlobalFileLock = 0;
    private class PlayFabFileManager
    {
        #region Private Fields
        private System.Action<string> FileDownloadedEvent;

        private string actualDownloadFile;

        private const string PLAYFABDIR = "PlayFabFiles";
        private readonly string playFabFilespath;
        #endregion

        public PlayFabFileManager()
        {
            playFabFilespath = Path.Combine(Application.persistentDataPath, PLAYFABDIR);
            if (!Directory.Exists(playFabFilespath))
                Directory.CreateDirectory(playFabFilespath);
        }

        #region Get File Part
        private IEnumerator WaitGlobalUnlockDownload(string fileName, System.Action<string> OnDownloadFileSuccess)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalUnlockDownload complete...");
            DownloadEntityFile(fileName, OnDownloadFileSuccess);
        }
        public void DownloadEntityFile(string fileName, System.Action<string> eventListiner)
        {
            if (!isConnected)
            {
                Debug.Log("DownloadEntityFile warning: client is not Connected to PlayFab!");
                return;
            }

            if (currentEntity == null)
            {
                Debug.Log("DownloadEntityFile: currentEntity is not initialized!");
                return;
            }

            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't DownloadEntityFile: {0}, cuz instance.GlobalFileLock is {1}!", fileName, instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalUnlockDownload(fileName, eventListiner));
                return;
            }

            instance.GlobalFileLock += 1; // Start GetFiles, now its 1
            Debug.Log("DownloadEntityFile set GlobalFileLock to " + instance.GlobalFileLock);

            Debug.Log("DownloadEntityFile(0): " + fileName);
            actualDownloadFile = fileName;
            FileDownloadedEvent = eventListiner;

            GetFilesRequest request = new GetFilesRequest()
            {
                Entity = currentEntity
            };
            PlayFabDataAPI.GetFiles(request, OnGetFileInfoSuccess, 
                (error) =>
                {
                    if (instance.GlobalFileLock != 0)
                        instance.GlobalFileLock -= 1; // Failed GetFiles, now its 0
                    Debug.Log("DownloadEntityFile(1-E):PlayFabDataAPI.GetFiles failed reset GlobalFileLock to " + instance.GlobalFileLock);

                    Debug.Log("PlayFabFileManager:DownloadEntityFile error:\n" +
                        error.ErrorMessage);

                    if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                        OnDisconnected("PlayFabFileManager");
                    else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
                    error.Error == PlayFabErrorCode.EntityTokenExpired ||
                    error.Error == PlayFabErrorCode.AuthTokenExpired ||
                    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
                    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                        TokenExpired("PlayFabFileManager");

                    FileDownloadedEvent(string.Empty);
                    FileDownloadedEvent = null;
                });
        }

        private void OnGetFileInfoSuccess(GetFilesResponse result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1; // Finish GetFiles, now its 0
            Debug.Log("DownloadEntityFile(1): OnGetFileInfoSuccess reset GlobalFileLock to " + instance.GlobalFileLock);

            bool isFileFinded = false;
            foreach (var eachFilePair in result.Metadata)
            {
                if (actualDownloadFile == eachFilePair.Key)
                {
                    isFileFinded = true;
                    GetActualFile(eachFilePair.Value);
                    break;
                }
            }

            if (!isFileFinded)
            {
                CustomLogger.LogMessage("DownloadEntityFile(2-E):Can't find actualDownloadFile in result.Metadata");
                FileDownloadedEvent(string.Empty);
                FileDownloadedEvent = null;
            }
        }

        void GetActualFile(GetFileMetadata fileData)
        {
            if (!isConnected)
            {
                Debug.Log("DownloadEntityFile(2-E):OnInitFileUploadFailed can't, cuz is not connected to PlayFab!");
                return;
            }

            instance.GlobalFileLock += 1; // Start SimpleGetCall, now its n+1
            Debug.Log("DownloadEntityFile(2):GetActualFile set GlobalFileLock to " + instance.GlobalFileLock);
            PlayFabHttp.SimpleGetCall(fileData.DownloadUrl,
                (result) => 
                {
                    if (instance.GlobalFileLock != 0)
                        instance.GlobalFileLock -= 1;  // Finish SimpleGetCall, now its n-1, maybe 0
                    Debug.Log("DownloadEntityFile(3-E):PlayFabHttp.SimpleGetCall success reset GlobalFileLock to " + instance.GlobalFileLock);

                    string filePath = Path.Combine(playFabFilespath, fileData.FileName);
                    File.WriteAllBytes(filePath, result);
                    FileDownloadedEvent(filePath);
                    FileDownloadedEvent = null;
                },
                (errorMessage) => 
                {
                    if (instance.GlobalFileLock != 0)
                        instance.GlobalFileLock -= 1;  // Failed SimpleGetCall, now its n-1, maybe 0
                    Debug.Log("DownloadEntityFile(3-E):PlayFabHttp.SimpleGetCall failed reset GlobalFileLock to " + instance.GlobalFileLock);

                    CustomLogger.LogErrorMessage("PlayFabFileManager:GetActualFile error:\n" +
                        errorMessage);
                    FileDownloadedEvent(string.Empty);
                    FileDownloadedEvent = null;
                }
            );
        }
        #endregion

        #region Upload File
        private System.Action<bool> UploadFileSuccessEvent;

        private string ActiveUploadFileName;
        private string ActiveUploadFilePath;
        private IEnumerator WaitGlobalUnlockUpload(string fileName, string filePath, System.Action<bool> OnUploadFileSuccess)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalUnlockUpload complete...");
            UploadEntityFile(fileName, filePath, OnUploadFileSuccess);
        }
        public void UploadEntityFile(string fileName, string filePath, System.Action<bool> OnUploadFileSuccess)
        {
            if (!isConnected)
            {
                Debug.Log("UploadEntityFile warning: client is not Connected to PlayFab!");
                return;
            }

            if(currentEntity == null)
            {
                Debug.Log("UploadEntityFile warning: currentEntity is not initialized!");
                return;
            }

            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't upload file: {0}, cuz instance.GlobalFileLock is {1}!", fileName, instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalUnlockUpload(fileName, filePath, OnUploadFileSuccess));
                return;
            }

            instance.GlobalFileLock += 1; // Start InitiateFileUploads, now its 1
            Debug.Log("UploadEntityFile set GlobalFileLock to " + instance.GlobalFileLock);

            Debug.Log(string.Format("UploadEntityFile(0): {0} at path: {1}", fileName, filePath));

            ActiveUploadFileName = fileName;
            ActiveUploadFilePath = filePath;
            UploadFileSuccessEvent = OnUploadFileSuccess;

            var request = new InitiateFileUploadsRequest
            {
                Entity = currentEntity,
                FileNames = new List<string> { ActiveUploadFileName }
            };
            PlayFabDataAPI.InitiateFileUploads(request, 
                OnInitFileUploadSuccess, OnInitFileUploadFailed);
        }

        private void OnInitFileUploadFailed(PlayFabError error)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1; // Failed InitiateFileUploads, now its 0
            Debug.Log("UploadEntityFile(1):OnInitFileUploadFailed reset GlobalFileLock to " + instance.GlobalFileLock);

            if (error.Error == PlayFabErrorCode.EntityFileOperationPending)
            {

                if (!isConnected)
                {
                    Debug.Log("OnInitFileUploadFailed can't, cuz is not connected to PlayFab!");
                    return;
                }

                instance.GlobalFileLock += 1; // Start AbortFileUploads, now its 1
                Debug.Log("UploadEntityFile(2):OnInitFileUploadFailed:AbortFileUploadsRequest set GlobalFileLock to " + instance.GlobalFileLock);
                AbortFileUploadsRequest request = new AbortFileUploadsRequest
                {
                    Entity = currentEntity,
                    FileNames = new List<string> { ActiveUploadFileName }
                };
                PlayFabDataAPI.AbortFileUploads(request,
                    (result) =>
                    {
                        if (instance.GlobalFileLock != 0)
                            instance.GlobalFileLock -= 1; // Finish AbortFileUploads, now its 0
                        Debug.Log("UploadEntityFile(3):OnInitFileUploadFailed:AbortFileUploads success reset GlobalFileLock to " + instance.GlobalFileLock);
                        UploadEntityFile(ActiveUploadFileName, ActiveUploadFilePath, UploadFileSuccessEvent);
                    },
                    (abordFileUploadsError) =>
                    {
                        if (instance.GlobalFileLock != 0)
                            instance.GlobalFileLock -= 1; // Finish AbortFileUploads, now its 0
                        Debug.Log("UploadEntityFile(3-E):OnInitFileUploadFailed:AbortFileUploads failed reset GlobalFileLock to " + instance.GlobalFileLock);

                        Debug.Log("PlayFabFileManager:AbortFileUploads error:\n" +
                            abordFileUploadsError.ErrorMessage);

                        if (abordFileUploadsError.Error == PlayFabErrorCode.ServiceUnavailable)
                            OnDisconnected("PlayFabFileManager");
                        else if (abordFileUploadsError.Error == PlayFabErrorCode.ExpiredAuthToken ||
                        abordFileUploadsError.Error == PlayFabErrorCode.EntityTokenExpired ||
                        abordFileUploadsError.Error == PlayFabErrorCode.AuthTokenExpired ||
                        abordFileUploadsError.Error == PlayFabErrorCode.ExpiredContinuationToken ||
                        abordFileUploadsError.Error == PlayFabErrorCode.ExpiredGameTicket)
                            TokenExpired("PlayFabFileManager");

                        UploadFileSuccessEvent(false);
                    });
            }
            else
            {
                Debug.Log("UploadEntityFile(2-E):PlayFabFileManager:OnInitFileUploadFailed error:\n" +
                    error);

                if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                    OnDisconnected("PlayFabFileManager");
                else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
                error.Error == PlayFabErrorCode.EntityTokenExpired ||
                error.Error == PlayFabErrorCode.AuthTokenExpired ||
                error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
                error.Error == PlayFabErrorCode.ExpiredGameTicket)
                    TokenExpired("PlayFabFileManager");

                UploadFileSuccessEvent(false);
            }
        }

        private void OnInitFileUploadSuccess (InitiateFileUploadsResponse response)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1; // Finish InitiateFileUploads, now its 0
            Debug.Log("UploadEntityFile(1):OnInitFileUploadSuccess reset GlobalFileLock to " + instance.GlobalFileLock + " : " + ActiveUploadFilePath);

            if (!isConnected)
            {
                Debug.Log("UploadEntityFile(1-E):OnInitFileUploadSuccess can't, cuz is not connected to PlayFab!");
                return;
            }

            string payloadStr = File.ReadAllText(ActiveUploadFilePath);
            byte[] payload = Encoding.UTF8.GetBytes(payloadStr);

            instance.GlobalFileLock += 1; // Start SimplePutCall, now its 1
            Debug.Log("UploadEntityFile(2):OnInitFileUploadSuccess:SimplePutCall set GlobalFileLock to " + instance.GlobalFileLock + " : " + payloadStr);
            PlayFabHttp.SimplePutCall(response.UploadDetails[0].UploadUrl,
                payload,
                FinalizeUploadFile,
                (errorMessage) =>
                {
                    if (instance.GlobalFileLock != 0)
                        instance.GlobalFileLock -= 1; // Failed SimplePutCall, now its 0
                    Debug.Log("UploadEntityFile(3-E):OnInitFileUploadSuccess:SimplePutCall failed reset GlobalFileLock to " + instance.GlobalFileLock);

                    Debug.Log("PlayFabFileManager:OnInitFileUploadSuccess error:\n" +
                        errorMessage);
                    UploadFileSuccessEvent(false);
                }
            );
        }

        private void FinalizeUploadFile(byte[] uploadBytes)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1; // Finish SimplePutCall, now its 0
            Debug.Log("UploadEntityFile(3):FinalizeUploadFile reset GlobalFileLock to " + instance.GlobalFileLock);

            if (!isConnected)
            {
                Debug.Log("UploadEntityFile(3-E):FinalizeUploadFile can't, cuz is not connected to PlayFab!");
                return;
            }

            instance.GlobalFileLock += 1; // Start FinalizeFileUploads, now its 1
            Debug.Log("UploadEntityFile(4):FinalizeUploadFile:PlayFabDataAPI.FinalizeFileUploads set GlobalFileLock to " + instance.GlobalFileLock);
            var request = new FinalizeFileUploadsRequest
            {
                Entity = currentEntity,
                FileNames = new List<string> { ActiveUploadFileName },
            };
            PlayFabDataAPI.FinalizeFileUploads(request, OnUploadFileSuccess,(finalizeFileUploadsError) =>
                {
                    if (instance.GlobalFileLock != 0)
                        instance.GlobalFileLock -= 1; // Failed FinalizeFileUploads, now its 0
                    Debug.Log("UploadEntityFile(5-E):FinalizeUploadFile:PlayFabDataAPI.FinalizeFileUploads failed reset GlobalFileLock to " + instance.GlobalFileLock);

                    Debug.Log("PlayFabFileManager:FinalizeFileUploads request error:\n" +
                        finalizeFileUploadsError.ErrorMessage);

                    if (finalizeFileUploadsError.Error == PlayFabErrorCode.ServiceUnavailable)
                        OnDisconnected("PlayFabFileManager");
                    else if (finalizeFileUploadsError.Error == PlayFabErrorCode.ExpiredAuthToken ||
                    finalizeFileUploadsError.Error == PlayFabErrorCode.EntityTokenExpired ||
                    finalizeFileUploadsError.Error == PlayFabErrorCode.AuthTokenExpired ||
                    finalizeFileUploadsError.Error == PlayFabErrorCode.ExpiredContinuationToken ||
                    finalizeFileUploadsError.Error == PlayFabErrorCode.ExpiredGameTicket)
                        TokenExpired("PlayFabFileManager");

                    UploadFileSuccessEvent(false);
                });
        }

        private void OnUploadFileSuccess(FinalizeFileUploadsResponse result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1; // Finish FinalizeFileUploads, now its 0
            Debug.Log("UploadEntityFile(5-E):OnUploadFileSuccess reset GlobalFileLock to " + instance.GlobalFileLock);

            CustomLogger.LogMessage("OnUploadFileSuccess:\n" + ActiveUploadFilePath);
            UploadFileSuccessEvent(true);
        }
        #endregion

        #region Getters
        public string GetFilesPath()
        {
            return playFabFilespath;
        }
        #endregion
    }
}
