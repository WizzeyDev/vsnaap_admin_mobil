﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using UnityEngine;

public partial class PlayFabManager : MonoBehaviour
{
    private class PlayFabAnalytics
    {
        public PlayFabAnalytics()
        {
            SendStartUpEvent();
            StartTrackingMenuTime();
        }

        public void SendStartUpEvent()
        {
            //Firebase.Analytics.FirebaseAnalytics.LogEvent("start_up");
        }

        public void StartTrackingMenuTime()
        {
            //Firebase.Analytics.FirebaseAnalytics.SetCurrentScreen("mainMenu", null);
        }

        public void StartTrackingGameTime()
        {
            //Firebase.Analytics.FirebaseAnalytics.SetCurrentScreen("game", null);
        }
    }
}
