﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;

public partial class PlayFabManager : MonoBehaviour
{
    private class PlayFabDatabase
    {
        private System.Action<GetPlayerStatisticsResult> GetDataSuccess;
        private System.Action UpdateDataSuccess;
        private System.Action UpdateDataFailed;

        private List<StatisticUpdate> actualPlayerStatistics;
        private List<string> actualPlayerStatisticNames = new List<string>()
        {
            "Coins", "Episode2", "Episode3", "Episode4", "Episode5"
        };

        public PlayFabDatabase()
        {
            CreateClearStatistic();
        }

        public void CreateClearStatistic()
        {
            if (actualPlayerStatistics == null)
                actualPlayerStatistics = new List<StatisticUpdate>();
            else
                actualPlayerStatistics.Clear();
            for (int i = 0; i < actualPlayerStatisticNames.Count; ++i)
            {
                actualPlayerStatistics.Add(new StatisticUpdate()
                {
                    StatisticName = actualPlayerStatisticNames[i],
                    Value = 0
                });
            }
        }

        #region Add Event Listiners
        public void AddGetDataSuccessEventListiner(System.Action<GetPlayerStatisticsResult> eventListiner)
        {
            GetDataSuccess += eventListiner;
        }
        public void AddUpdateDataSuccessEventListiner(System.Action eventListiner)
        {
            UpdateDataSuccess += eventListiner;
        }

        public void AddUpdateDataFailedEventListiner(System.Action eventListiner)
        {
            UpdateDataFailed += eventListiner;
        }
        #endregion

        public void ChangePlayerStatistic(string key, int value)
        {
            for (int i = 0; i < actualPlayerStatistics.Count; ++i)
            {
                if (actualPlayerStatistics[i].StatisticName == key)
                {
                    actualPlayerStatistics[i].Value = value;
                }
            }

            UpdatePlayerStatistics();
        }

        #region Player Statistics
        public void UpdatePlayerStatistics()
        {
            PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest()
            {
                Statistics = actualPlayerStatistics
            },
            UpdatePlayerStatisticsSuccess,
            UpdatePlayerStatisticsFailed);
        }

        private void UpdatePlayerStatisticsSuccess(UpdatePlayerStatisticsResult result)
        {
            Debug.Log("Update PlayerStatistics success!\n");

            UpdateDataSuccess();
        }

        private void UpdatePlayerStatisticsFailed(PlayFabError error)
        {
            Debug.Log("PlayFabDatabase:UpdatePlayerStatisticsFailed error:\n" +
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabDatabase");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabDatabase");

            UpdateDataFailed();
        }

        public void GetPlayerStatistics()
        {
            PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest()
            {
                StatisticNames = actualPlayerStatisticNames
            },
            GetStatisticsSuccess,
            GetStatisticsFailed);
        }

        private void GetStatisticsSuccess(GetPlayerStatisticsResult result)
        {
            Debug.Log("Getting PlayerStatistics success!\n");

            GetDataSuccess(result);
        }

        private void GetStatisticsFailed(PlayFabError error)
        {
            Debug.Log("PlayFabDatabase:GetStatisticsFailed error:\n" +
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabDatabase");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabDatabase");
        }
        #endregion

        #region User Data
        public void ChangeUsername(string newUserName)
        {
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
            {
                DisplayName = newUserName
            },
            ChangeUsernameSuccess,
            ChangeUsernameFailed);
        }

        private void ChangeUsernameSuccess(UpdateUserTitleDisplayNameResult result)
        {
            Debug.Log("Change Username success!\n");
        }

        private void ChangeUsernameFailed(PlayFabError error)
        {
            Debug.Log("ChangeUsernameFailed error: \n" + 
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabDatabase");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabDatabase");
        }
        #endregion
    }
}

