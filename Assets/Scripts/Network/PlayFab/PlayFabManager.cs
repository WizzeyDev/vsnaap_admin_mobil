﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.DataModels;
using UnityEngine.UI;

public partial class PlayFabManager : MonoBehaviour
{
    #region Singleton
    private static PlayFabManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("PlayFabManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Field
    #endregion

    #region Private Fields
    private PlayFabAuthorization playFabAuth;
    private PlayFabDatabase playFabData;
    private PlayFabNotifications playFabNotify;
    private PlayFabAnalytics playFabAnalytics;
    private PlayFabCloudMessenger playFabCloudMessenger;
    private PlayFabFileManager playFabFileManager;
    private PlayFabFileLogger playFabFileLogger;

    private System.Action LostConnectionEvent;

    private const string titleID = "408E2";

    private static EntityKey currentEntity;
    private static string playFabID;
    private static string playFabDisplayName;

    private static bool isConnected;
    #endregion

    #region Setup
    private void Start()
    {
        InitializePlayFab();
    }

    private void InitializePlayFab()
    {
        PlayFabSettings.TitleId = titleID;
        playFabAuth = new PlayFabAuthorization();
        playFabData = new PlayFabDatabase();
        playFabNotify = new PlayFabNotifications();
        playFabAnalytics = new PlayFabAnalytics();
        playFabCloudMessenger = new PlayFabCloudMessenger();
        playFabFileManager = new PlayFabFileManager();
        playFabFileLogger = new PlayFabFileLogger();
    }
    #endregion

    #region Add Event Listiners
    public static void AddDownloadLogFileSuccess(System.Action<string> eventHandler)
    {
        if (isNullInstance)
            return;

        instance.playFabFileLogger.AddDownloadRemoteLogFileSuccessEventListiner(eventHandler);
    }
    public static void AddAuthenticateEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.AddAuthenticateEventListiner(eventListiner);    
    }
    public static void AddRegistrationSuccessEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.AddRegisterSuccessEventListiner(eventListiner);
    }
    public static void AddRegistrationFailedEventListiner(System.Action<string> eventListiner)
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.AddRegisterFailedEventListiner(eventListiner);
    }
    public static void AddLostConnectionEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.LostConnectionEvent += eventListiner;
    }
    public static void AddConnectionFailedEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.AddConnectionFailedEventListiner(eventListiner);
    }
    #endregion

    #region Authorization
    public static void PlayFabLogin(string username)
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.PlayFabLoginWithCustomID(username);
    }

    public static void RegistrationPlayFabUsername(string customUserID, string displayName) //Fix 1 Called by Aprraiser Create User name
    {
        if (isNullInstance)
            return;

        instance.playFabAuth.PlayFabRegistration(customUserID, displayName);
    }
    #endregion

    #region Cloud Message
    public static void SendCallNotificationMessage(string targetId)
    {
        if (isNullInstance)
            return;

        instance.playFabCloudMessenger.SendCallNotificationRequest(targetId);
    }

    public static void ChangeDisplayName(string newUserName)
    {
        instance.playFabCloudMessenger.ChangeDisplayName(newUserName);
    }
    #endregion

    #region FileManager
    public static void DownloadPlayFabFile(string fileName, System.Action<string> OnActualFileDownloaded)
    {
        if (isNullInstance || instance.playFabFileManager == null)
            return;

        instance.playFabFileManager.DownloadEntityFile(fileName, OnActualFileDownloaded);
    }

    public static void UploadPlayFabFile(string filePath, System.Action<bool> OnUploadFileSuccess)
    {
        if (isNullInstance || instance.playFabFileManager == null)
            return;

        string[] splitFilePath = filePath.Split('\\');
        string fileName = splitFilePath[splitFilePath.Length - 1];
        string[] alias = fileName.Split('/');
        if (alias.Length > 1)
            fileName = alias[alias.Length - 1];
        instance.playFabFileManager.UploadEntityFile(fileName, filePath, OnUploadFileSuccess);
    }
    #endregion

    #region Getters
    public static string GetPlayFabID()
    {
        return playFabID;
    }

    public static string GetPlayFabDisplayName()
    {
        return playFabDisplayName;
    }

    public static string GetPlayFabFilespath()
    {
        if (isNullInstance || instance.playFabFileManager == null)
            return string.Empty;

        return instance.playFabFileManager.GetFilesPath();
    }

    public static int IsInitialize()
    {
        if (isNullInstance)
            return 0;

        if (instance.playFabFileManager == null)
            return -1;

        return instance.playFabFileManager != null ? 1 : -1;
    }

    public static bool IsConnectedToPlayFab()
    {
        return isConnected;
    }
    #endregion

    #region Disconnect
    public static void ManualDisconnect()
    {
        if (isNullInstance)
            return;

        if (!isConnected)
            return;

        CustomLogger.LogMessage("PlayFab ManualDisconnect");

        currentEntity = null;
        playFabID = string.Empty;
        playFabDisplayName = string.Empty;

        PlayFabClientAPI.ForgetAllCredentials();

        isConnected = false;
        instance.GlobalFileLock = 0;
    }
    public static void OnDisconnected(string sender)
    {
        if (isNullInstance)
            return;

        if (!isConnected)
            return;

        CustomLogger.LogMessage("PlayFab OnDisconnected by: " + sender);

        currentEntity = null;
        playFabID = string.Empty;
        playFabDisplayName = string.Empty;

        PlayFabClientAPI.ForgetAllCredentials();

        isConnected = false;
        instance.GlobalFileLock = 0;

        instance.LostConnectionEvent?.Invoke();
    }

    private static void TokenExpired(string sender)
    {
        CustomLogger.LogMessage(string.Format("PlayFabManager:TokenExpired by {0}",
            sender));

        OnDisconnected("PlayFabManager:TokenExpired");
    }
    #endregion
}
