﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;

public partial class PlayFabManager : MonoBehaviour
{
    private class PlayFabAuthorization
    {
        #region Private Fields
        private System.Action RegisterationSuccessEvent;
        private System.Action<string> RegisterationFailedEvent;
        private System.Action AuthenticateSuccessEvent;
        private System.Action<string> AuthenticateWithNameSuccessEvent;
        private System.Action ConnectionFailedEvent;

        private string lastCustomID;
        #endregion

        #region Add Event Listiners
        public void AddRegisterSuccessEventListiner(System.Action eventListiner)
        {
            RegisterationSuccessEvent += eventListiner;
        }
        public void AddRegisterFailedEventListiner(System.Action<string> eventListiner)
        {
            RegisterationFailedEvent += eventListiner;
        }
        public void AddAuthenticateEventListiner(System.Action eventListiner)
        {
            AuthenticateSuccessEvent += eventListiner;
        }
        public void AddConnectionFailedEventListiner(System.Action eventListiner)
        {
            ConnectionFailedEvent += eventListiner;
        }
        #endregion

        #region Authorization Types
        private IEnumerator WaitGlobalLockAuthorization(string customID)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalLockAuthorization complete...");
            PlayFabLoginWithCustomID(customID);
        }
        public void PlayFabLoginWithCustomID(string customID)
        {
            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't PlayFabLogin, cuz instance.GlobalFileLock is {0}!", instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalLockAuthorization(customID));
                return;
            }
            CustomLogger.LogMessage("PlayFabAuthorization:PlayFabLoginWithCustomID " + customID);

#if UNITY_ANDROID
            instance.GlobalFileLock += 1;
            Debug.Log("PlayFabLogin set GlobalFileLock to " + instance.GlobalFileLock);
            LoginWithCustomIDRequest playFabAuthRequest = new LoginWithCustomIDRequest()
            {
                TitleId = titleID,
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
                {
                    GetPlayerProfile = true
                }, 
                CustomId = customID
            };
            PlayFabClientAPI.LoginWithCustomID(playFabAuthRequest, AuthorizationSuccess, AuthorizationFailed);
#endif
        }

        private IEnumerator WaitGlobalLockRegistration(string customID, string displayName)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalLockRegistration complete...");
            PlayFabRegistration(customID, displayName);
        }
        public void PlayFabRegistration(string customID, string displayName)
        {
            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't PlayFabRegistration, cuz instance.GlobalFileLock is {0}!", instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalLockRegistration(customID, displayName));
                return;
            }
            CustomLogger.LogMessage("PlayFabAuthorization:PlayFabRegistration " + customID);

#if UNITY_ANDROID
            instance.GlobalFileLock += 1;
            Debug.Log("PlayFabRegistration set GlobalFileLock to " + instance.GlobalFileLock);
            RegisterPlayFabUserRequest registerRequest = new RegisterPlayFabUserRequest()
            {
                TitleId = titleID,
                Username = customID,
                Password = "123456",
                DisplayName = displayName,
                RequireBothUsernameAndEmail = false
            };
            PlayFabClientAPI.RegisterPlayFabUser(registerRequest, RegistrationSuccess, RegistrationFailed);
            lastCustomID = customID;
#endif
        }
        #endregion

        #region Registration Results
        private void RegistrationSuccess(RegisterPlayFabUserResult result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("RegistrationSuccess reset GlobalFileLock to " + instance.GlobalFileLock);

            CustomLogger.LogMessage(string.Format("Registration Username {0} success!", lastCustomID)); //Fix 1
            RegisterationSuccessEvent?.Invoke();
            RegisterationSuccessEvent = null;
        }

        private void RegistrationFailed(PlayFabError error)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("RegistrationFailed reset GlobalFileLock to " + instance.GlobalFileLock);
            CustomLogger.LogErrorMessage("RegistrationFailed error: \n" +
                error.GenerateErrorReport());

            RegisterationFailedEvent?.Invoke(error.ErrorMessage);
        }
        #endregion

        #region Authorization Results
        private void AuthorizationSuccess(LoginResult result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("AuthorizationSuccess reset GlobalFileLock to " + instance.GlobalFileLock);
            if (result.NewlyCreated)
            {
                CustomLogger.LogErrorMessage("Can't find Username in PlayFab, so it was created!");
            }
            else
            {
                CustomLogger.LogMessage("Login to PlayFab success!");
            }

            currentEntity = new PlayFab.DataModels.EntityKey();
            currentEntity.Id = result.EntityToken.Entity.Id;
            currentEntity.Type = result.EntityToken.Entity.Type;

            playFabID = result.PlayFabId;
            if (result.InfoResultPayload != null && result.InfoResultPayload.PlayerProfile != null)
            {
                playFabDisplayName = result.InfoResultPayload.PlayerProfile.DisplayName;
            }

            isConnected = true;

            AuthenticateSuccessEvent?.Invoke();
        }

        private void AuthorizationFailed(PlayFabError error)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("AuthorizationFailed reset GlobalFileLock to " + instance.GlobalFileLock);
            CustomLogger.LogErrorMessage("AuthorizationFailed error: \n" + 
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabAuthorization");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabAuthorization");

            ConnectionFailedEvent?.Invoke();
        }
        #endregion
    }
}

