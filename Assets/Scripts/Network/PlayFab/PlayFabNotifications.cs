﻿using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using Firebase;

public partial class PlayFabManager : MonoBehaviour
{
    private class PlayFabNotifications
    {
        #region Private Fields
        private string pushToken;
        private string lastMsg;
        #endregion

        #region Setup
        public PlayFabNotifications()
        {
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
            AddAuthenticateEventListiner(PushNotificationsRegistration);
        }
        #endregion

        #region FCN Part
        private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            Debug.Log("OnTokenReceived token: " + token.Token);
            pushToken = token.Token;
        }

        private void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            Debug.Log("Received a new message from: " + e.Message.From);
          
            if (e.Message.Notification != null)
            {
                Debug.Log("PlayFab: Received a notification:\n" +
                     e.Message.Notification.Body);
            }
        }
        #endregion

        #region PlayFab Notifications Register
        private IEnumerator WaitGlobalLockRegistration()
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalLockRegistration complete...");
            PushNotificationsRegistration();
        }
        private void PushNotificationsRegistration()
        {
            if (!isConnected)
            {
                Debug.Log("PushNotificationsRegistration warning: client is not Connected to PlayFab!");
                return;
            }

            if (string.IsNullOrEmpty(pushToken))
            {
                Debug.Log("Can't PushNotificationsRegistration, cuz 'pushToken' is empty!");
                return;
            }

            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't PushNotificationsRegistration, cuz instance.GlobalFileLock is {0}!", instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalLockRegistration());
                return;
            }
            Debug.Log("Waiting for PushNotifications registration...");

#if UNITY_ANDROID && !UNITY_EDITOR
            instance.GlobalFileLock += 1;
            Debug.Log("PushNotificationsRegistration set GlobalFileLock to " + instance.GlobalFileLock);
            var request = new AndroidDevicePushNotificationRegistrationRequest
            {
                DeviceToken = pushToken,
                SendPushNotificationConfirmation = true,
                ConfirmationMessage = "Push notifications registered successfully"
            };

            PlayFabClientAPI.AndroidDevicePushNotificationRegistration(request,
                NotificationsRegistrationSuccess,
                NotificationsRegistrationFailed);
#endif
        }

        private void NotificationsRegistrationSuccess(AndroidDevicePushNotificationRegistrationResult result)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            if (instance.GlobalFileLock != 0)
            instance.GlobalFileLock -= 1;
            Debug.Log("PushNotificationsRegistration reset GlobalFileLock to " + instance.GlobalFileLock);
#endif
            Debug.Log("PlayFabNotifications registration Success!");
        }

        private void NotificationsRegistrationFailed(PlayFabError error)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
             if (instance.GlobalFileLock != 0)
            instance.GlobalFileLock -= 1;
            Debug.Log("NotificationsRegistrationFailed reset GlobalFileLock to " + instance.GlobalFileLock);
#endif

            Debug.Log("PlayFabNotifications:NotificationsRegistrationFailed with error:\n" + 
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabNotifications");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
                error.Error == PlayFabErrorCode.EntityTokenExpired ||
                error.Error == PlayFabErrorCode.AuthTokenExpired ||
                error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
                error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabNotifications");
        }
#endregion
    }
}
