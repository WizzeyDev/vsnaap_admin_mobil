﻿using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class PlayFabManager : MonoBehaviour
{
    private class PlayFabCloudMessenger
    {
        #region Send Call Notification
        private IEnumerator WaitGlobalLockCallNotification(string targetId)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalLockCallNotification complete...");
            SendCallNotificationRequest(targetId);
        }
        public void SendCallNotificationRequest(string targetId)
        {
            if (!isConnected)
            {
                CustomLogger.LogMessage("SendCallNotificationRequest warning: client is not Connected to PlayFab!");
                return;
            }

            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't SendCallNotificationRequest, cuz instance.GlobalFileLock is {0}!", instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalLockCallNotification(targetId));
                return;
            }

            Debug.Log("SendCallNotification to " + targetId);
            instance.GlobalFileLock += 1;
            Debug.Log("SendCallNotificationRequest set GlobalFileLock to " + instance.GlobalFileLock);
            PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
            {
                FunctionName = "SendCallNotification",
                FunctionParameter = new 
                { 
                    TargetId = targetId, 
                    MessageBody = "called you!", 
                    MessageTitle = "You have a missed call" 
                }
            },
            SendCallNotificationSuccess,
            SendCallNotificationFailed);
        }

        private void SendCallNotificationSuccess(ExecuteCloudScriptResult result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("SendCallNotificationSuccess reset GlobalFileLock to " + instance.GlobalFileLock);
            Debug.Log("SendCallNotification Success!");
            JsonObject jsonResult = (JsonObject)result.FunctionResult;
            if (jsonResult != null)
            {
                object messageValue;
                jsonResult.TryGetValue("ErrorMessage", out messageValue);

                if (messageValue != null)
                {
                    Debug.LogError("SendCallNotificationSuccess due error:\n" + 
                        (string)messageValue);
                }
            }
        }

        private void SendCallNotificationFailed(PlayFabError error)
        {
            instance.GlobalFileLock -= 1;
            Debug.Log("SendCallNotificationFailed reset GlobalFileLock to " + instance.GlobalFileLock);
            Debug.LogError("PlayFabCloudMessenger:SendCallNotificationFailed error:\n" +
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabCloudMessenger");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabCloudMessenger");
        }
        #endregion

        #region Change User Name
        private IEnumerator WaitGlobalLockChangeUsername(string newUserName)
        {
            yield return new WaitUntil(() => instance.GlobalFileLock == 0);
            Debug.Log("WaitGlobalLockChangeUsername complete...");
            ChangeDisplayName(newUserName);
        }
        public void ChangeDisplayName(string newUserName)
        {
            if (!isConnected)
            {
                Debug.Log("ChangeDisplayName warning: client is not Connected to PlayFab!");
                return;
            }

            if (instance.GlobalFileLock != 0)
            {
                Debug.Log(string.Format("Can't ChangeDisplayName, cuz instance.GlobalFileLock is {0}!", instance.GlobalFileLock));
                instance.StartCoroutine(WaitGlobalLockChangeUsername(newUserName));
                return;
            }

            instance.GlobalFileLock += 1;
            Debug.Log("ChangeDisplayName set GlobalFileLock to " + instance.GlobalFileLock);
            Debug.Log("ChangeDisplayName on " + newUserName);
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest()
            {
                DisplayName = newUserName
            },
            ChangeDisplayNameSuccess,
            ChangeDisplayNameFailed);
        }

        private void ChangeDisplayNameSuccess(UpdateUserTitleDisplayNameResult result)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("ChangeDisplayName reset GlobalFileLock to " + instance.GlobalFileLock);
            CustomLogger.LogMessage("ChangeDisplayName Success! New DisplayName is " + result.DisplayName);
        }


        private void ChangeDisplayNameFailed(PlayFabError error)
        {
            if (instance.GlobalFileLock != 0)
                instance.GlobalFileLock -= 1;
            Debug.Log("ChangeDisplayName reset GlobalFileLock to " + instance.GlobalFileLock);
            Debug.LogError("PlayFabCloudMessenger:ChangeDisplayName error:\n" + 
                error.ErrorMessage);

            if (error.Error == PlayFabErrorCode.ServiceUnavailable)
                OnDisconnected("PlayFabCloudMessenger");
            else if (error.Error == PlayFabErrorCode.ExpiredAuthToken ||
    error.Error == PlayFabErrorCode.EntityTokenExpired ||
    error.Error == PlayFabErrorCode.AuthTokenExpired ||
    error.Error == PlayFabErrorCode.ExpiredContinuationToken ||
    error.Error == PlayFabErrorCode.ExpiredGameTicket)
                TokenExpired("PlayFabCloudMessenger");
        }
        #endregion
    }
}
