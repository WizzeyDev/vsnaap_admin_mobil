﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayFabFileLogger : IRemoteFileLogger
{
    #region Private Fields
    private System.Action<string> DownloadRemoteLogFileSuccessEvent;
    private System.Action<bool> UploadRemoteLogFileSuccessEvent;

    private bool isBackupMode;
    #endregion

    #region Private Fields
    public PlayFabFileLogger()
    {
        CustomLogger.ConnectRemoteFileLogger(this);
        GameManager.AddLoginSystemEventHandler(() =>
        {
            string logFileName = CustomLogger.GetLogFileName();
            if (string.IsNullOrEmpty(logFileName))
                return;

            DownloadLogFile(logFileName);
        });
    }
    #endregion

    #region Add Event Listiners
    public void AddDownloadRemoteLogFileSuccessEventListiner(System.Action<string> eventListiner)
    {
        DownloadRemoteLogFileSuccessEvent += eventListiner;
    }
    public void AddUploadRemoteLogFileSuccessEventListiner(System.Action<bool> eventListiner)
    {
        UploadRemoteLogFileSuccessEvent += eventListiner;
    }
    #endregion

    #region Download/Upload Part
    public void DownloadLogFile(string fileName)
    {
        PlayFabManager.DownloadPlayFabFile(fileName, DownloadRemoteLogFileSuccessEvent);
    }

    public void UploadLogFile(string fileName)
    {
        PlayFabManager.UploadPlayFabFile(fileName, UploadRemoteLogFileSuccessEvent);
    }
    #endregion

    #region Setters
    public void SetBackupMode(bool state)
    {
        isBackupMode = state;
    }
    #endregion

    #region Getters
    public bool IsBackupModeEnabled()
    {
        return isBackupMode;
    }

    public bool IsAvailable()
    {
        return PlayFabManager.IsConnectedToPlayFab();
    }
    #endregion
}
