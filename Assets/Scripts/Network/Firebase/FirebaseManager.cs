﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;

public class FirebaseManager : MonoBehaviour
{
    #region Singleton
    private static FirebaseManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("FirebaseManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Private Fields
    private FirebaseCloudStorage cloudStorage;

    private bool isDependenciesResolved;
    #endregion

    #region Setup
    private void Start()
    {
        FirebaseApp.LogLevel = LogLevel.Error;
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => 
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                CustomLogger.LogMessage("Firebase dependencies resolved success!");
                isDependenciesResolved = true;
            }
            else
            {
                CustomLogger.LogErrorMessage("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });

        StartCoroutine(LateInitialize());
    }

    private IEnumerator LateInitialize()
    {
        yield return null;
        cloudStorage = new FirebaseCloudStorage(this);
    }
    #endregion

    #region UploadFile 
    public static void UploadFile(string fileName, System.Action<string> UploadUrlHandler)
    {
        if (isNullInstance)
            return;

        instance.cloudStorage.UploadFile(fileName, UploadUrlHandler);
    }
    #endregion

    #region Getters
    public static int IsFirebaseDependencyResolved()
    {
        if (isNullInstance)
            return 0;

        return instance.isDependenciesResolved ? 1 : -1;
    }
    #endregion
}
