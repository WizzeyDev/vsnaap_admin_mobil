﻿using Firebase;
using Firebase.Storage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class FirebaseCloudStorage
{
    #region Private Fields
    private MonoBehaviour owner;

    private StringBuilder stringBuilder;

    private FirebaseStorage storage;

    private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

    private string MyStorageBucket = "gs://";
    private readonly string UriFileScheme = Uri.UriSchemeFile + "://";

    public bool operationInProgress;
    #endregion

    #region Setup
    public FirebaseCloudStorage(MonoBehaviour owner)
    {
        this.owner = owner;
        stringBuilder = new StringBuilder(64);

        if (!Directory.Exists(GlobalSettings.cloudStorageDownloadPath))
            Directory.CreateDirectory(GlobalSettings.cloudStorageDownloadPath);

        if (!Directory.Exists(GlobalSettings.cloudStorageUploadPath))
            Directory.CreateDirectory(GlobalSettings.cloudStorageUploadPath);

        InitializeCloudStorage();
    }

    private void InitializeCloudStorage()
    {
        string appBucket = FirebaseApp.DefaultInstance.Options.StorageBucket;
        storage = FirebaseStorage.DefaultInstance;
        if (!string.IsNullOrEmpty(appBucket))
        {
            MyStorageBucket = string.Format("gs://{0}/", appBucket);
        }
        storage.LogLevel = LogLevel.Error;
    }
    #endregion

    #region Task Coroutine
    private class WaitForTaskCompletion : CustomYieldInstruction
    {
        private Task task;
        private FirebaseCloudStorage cloudStorage;

        public WaitForTaskCompletion(FirebaseCloudStorage cloudStorage, Task task)
        {
            cloudStorage.operationInProgress = true;
            this.cloudStorage = cloudStorage;
            this.task = task;
        }

        public override bool keepWaiting
        {
            get
            {
                if (task.IsCompleted)
                {
                    cloudStorage.operationInProgress = false;
                    cloudStorage.cancellationTokenSource = new CancellationTokenSource();
                    return false;
                }
                return true;
            }
        }
    }
    #endregion

    #region Upload Part
    private System.Action<string> UploadUrlHandler;
    public void UploadFile(string filePath, System.Action<string> UploadUrlHandler)
    {
        this.UploadUrlHandler = UploadUrlHandler;
        string[] filePathParts = filePath.Split('\\');
        string fileName = filePathParts[filePathParts.Length - 1];
        owner.StartCoroutine(UploadFileCoroutine(filePath, fileName));
    }

    private IEnumerator UploadFileCoroutine(string filePath, string filename)
    {
        yield return null;
        string localFilePath = ConvertPathToSchemePath(filePath);
        StorageReference storageReference = GetStorageReference(filename);

        stringBuilder.AppendFormat("Uploading '{0}' to '{1}'...",
            localFilePath,
            storageReference.Path);
        CustomLogger.LogMessage(stringBuilder.ToString());
        stringBuilder.Clear();

        Task<StorageMetadata> task = storageReference.PutFileAsync(localFilePath, null, null,
            cancellationTokenSource.Token, null);
        yield return new WaitForTaskCompletion(this, task);
        UploadResult(task);
    }

    private void UploadResult(Task<StorageMetadata> task)
    {
        if (!(task.IsFaulted || task.IsCanceled))
        {
            stringBuilder.AppendFormat("Finished uploading {0} file, with size {1} is complete!\n",
                task.Result.Name, task.Result.SizeBytes);
            CustomLogger.LogMessage(stringBuilder.ToString());
            stringBuilder.Clear();

            owner.StartCoroutine(ShowDownloadUrl(task, UploadUrlHandler));
        }
        else
        {
            DisplayStorageException(task.Exception);
            UploadUrlHandler(string.Empty);
        }
    }
    #endregion

    #region Download File Part
    public void DownloadFile(string filename)
    {
        owner.StartCoroutine(DownloadFileCoroutine(filename));
    }
    private IEnumerator DownloadFileCoroutine(string filename)
    {
        string localFilePath = ConvertPathToSchemePath(filename);

        StorageReference storageReference = GetStorageReference(filename);
        stringBuilder.AppendFormat("Start downloading {0} ...", storageReference.Path);
        CustomLogger.LogMessage(stringBuilder.ToString());
        stringBuilder.Clear();

        Task task = storageReference.GetFileAsync(localFilePath, null, cancellationTokenSource.Token);
        yield return new WaitForTaskCompletion(this, task);
        DownloadComplete(filename, task);
    }

    private void DownloadComplete(string filename, Task task)
    {
        if (!(task.IsFaulted || task.IsCanceled))
        {
            stringBuilder.AppendFormat("Finished downloading {0} file is complete!\n",
                filename);
            CustomLogger.LogMessage(stringBuilder.ToString());
            stringBuilder.Clear();
        }
        else
        {
            DisplayStorageException(task.Exception);
        }
    }
    #endregion

    #region Features
    private string ConvertPathToSchemePath(string filename)
    {
        if (filename.StartsWith(UriFileScheme))
            return filename;

        return UriFileScheme + filename;
    }

    private StorageReference GetStorageReference(string filename)
    {
        string storageLocation = MyStorageBucket + filename;
        if (storageLocation.StartsWith("gs://") ||
            storageLocation.StartsWith("http://") ||
            storageLocation.StartsWith("https://"))
        {
            Uri storageUri = new Uri(storageLocation);
            FirebaseStorage firebaseStorage =
                FirebaseStorage.GetInstance(string.Format("{0}://{1}",
                storageUri.Scheme, storageUri.Host));
            return firebaseStorage.GetReferenceFromUrl(storageLocation);
        }

        return FirebaseStorage.DefaultInstance.GetReference(storageLocation);
    }

    private IEnumerator ShowDownloadUrl(Task<StorageMetadata> task, System.Action<string> ShowUrlHandler)
    {
        Task<Uri> uriTask = task.Result.Reference.GetDownloadUrlAsync();
        yield return new WaitForTaskCompletion(this, uriTask);
        if (!(uriTask.IsFaulted || uriTask.IsCanceled))
        {
            stringBuilder.Append("Uploaded file url: ").Append(uriTask.Result);
            CustomLogger.LogMessage(stringBuilder.ToString());
            stringBuilder.Clear();

            ShowUrlHandler(uriTask.Result.AbsoluteUri);
        }
        else
        {
            DisplayStorageException(uriTask.Exception);
            ShowUrlHandler(string.Empty);
        }
    }
    #endregion

    #region Release Debug
    protected void DisplayStorageException(Exception exception)
    {
        StorageException storageException = exception as StorageException;
        if (storageException != null)
        {
            stringBuilder.Append("Error Code: ").
                Append(storageException.ErrorCode).Append('\n').
                Append("HTTP Result Code: ").
                Append(storageException.HttpResultCode).Append('\n').
                Append("HTTP Result Code: ").
                Append(storageException.IsRecoverableException).Append('\n').
                Append(storageException.ToString());
            CustomLogger.LogErrorMessage(stringBuilder.ToString());
            Debug.LogError(stringBuilder.ToString());
            stringBuilder.Clear();
        }
        else
        {
            stringBuilder.Append("StorageException:\n").Append
                (exception.ToString());
            CustomLogger.LogErrorMessage(stringBuilder.ToString());
            Debug.LogError(stringBuilder.ToString());
            stringBuilder.Clear();
        }
    }
    #endregion
}
