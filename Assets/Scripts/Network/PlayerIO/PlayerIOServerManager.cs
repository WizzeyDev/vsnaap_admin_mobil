﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerIOClient;

public class PlayerIOServerManager : MonoBehaviour
{ 
    #region Singleton
    private static PlayerIOServerManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("PlayerIOServerManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Private Properties
    private bool isNullConnection
    {
        get
        {
            if (currentServerConnection == null)
            {
                string scriptName = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                Debug.LogError("PlayerIOClient is not connected to Server!");
                return true;
            }
            return false;
        }
    }
    #endregion

    #region Private Fields
    private Client currentClient;
    private Connection currentServerConnection;

    private System.Action ConnectionSuccessEvent;
    private System.Action UserJoinedEvent;
    private System.Action LostConnectionEvent;
    private System.Action ConnectionFailedEvent;

    private Coroutine connectionWaitingCoroutine;

    private const string gameId = /*"vsn-swy5mpfv6ewmoztvf5b2a";*/ "vsnaap-ibl9rv4skw1zsa1eofma";

    private const string FirstConnectionMessageType = "FirstConnection";
    private const string GetFirstAdminDataMessageType = "GetFirstAdminData";
    private const string CheckingConnectionMessageType = "CheckingConnection";

    private const string GetGarageAndAppraiserDatasMessageType = "GetGarageAndAppraiserDatas";
    private const string GetGarageDatasMessageType = "GetGarageDatas";
    private const string GetAppraiserDatasMessageType = "GetAppraiserDatas";
    private const string GetUpdateCardInfoMessageType = "UpdateCardInfo";

    private const string AddNewEmailMessageType = "AddNewEmail";
    private const string RemoveFromEmailsMessageType = "RemoveFromEmails";
    private const string AddDocumentToAppraiserMessageType = "AddDocumentToAppraiser";
    private const string SendSetPlayFabIDMessageType = "SetPlayFabID";
    private const string SendGetPlayFabIDMessageType = "GetPlayFabID";

    private const string SearchCardsMessageType = "SearchCards";
    private const string AddCardsToAdminBlockMessageType = "AddCardsToAdminBlock";
    private const string GetFullCardDataMessageType = "GetFullCardData";
    private const string ChangeCardDataMessageType = "ChangeCardData";

    private const string CreateAppraiserMessageType = "CreateAppraiser";
    private const string CreateGarageMessageType = "CreateGarage";
    private const string DeleteAppraiserMessageType = "DeleteAppraiser";
    private const string DeleteGarageMessageType = "DeleteGarage";
    private const string ChangeAppraiserDataMessageType = "ChangeAppraiserData";
    private const string ChangeGarageDataMessageType = "ChangeGarageData";

    private const string GetBlocksDataMessageType = "GetBlocksData";
    private const string ConnectAppraiserToCardMessageType = "ConnectAppraiserToCard";
    private const string ChangeAppraiserInCardMessageType = "ChangeAppraiserInCard";
    private const string ChangeGarageInCardMessageType = "ChangeGarageInCard";
    private const string ChangeCardSignalMessageType = "ChangeCardSignal";
    private const string SkipFirstCardCheckMessageType = "SkipFirstCardCheck";
    private const string CloseCardMessageType = "CloseCard";
    private const string CreateCardMessageType = "CreateCard";

    private const string ManualRemoveUnderCardFromBlockMessageType = "ManualRemoveUnderCardFromBlock";
    private const string ManualBusyUnderCardInBlockMessageType = "ManualBusyUnderCardInBlock";

    private const string DebugMessageType = "Debug";

    private string lastAdminPassword;
    #endregion

    #region Add Event Listiners
    public static void AddConnectionSuccessEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.ConnectionSuccessEvent += eventListiner;
    }

    public static void AddUserJoinedEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.UserJoinedEvent += eventListiner;
    }

    public static void AddLostConnectionEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.LostConnectionEvent += eventListiner;
    }

    public static void AddConnectionFailedEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.ConnectionFailedEvent += eventListiner;
    }
    #endregion

    #region Connection
    private IEnumerator WaitForConnection(string adminLogin, string adminPassword)
    {
        yield return null;

        while (true)
        {
            ConnectToServer(adminLogin, adminPassword);

            yield return new WaitForSeconds(7f);

            Debug.Log("Can't connect to Server... Retrying...");
        }
    }

    public static void ConnectToServer(string adminLogin, string adminPassword)
    {
        if (isNullInstance)
            return;

        if (instance.connectionWaitingCoroutine == null)
        {
            CustomLogger.LogMessage(string.Format("Start ConnectToServer with login: {0}, password: {1}",
                adminLogin, adminPassword));

            instance.connectionWaitingCoroutine =
                instance.StartCoroutine(instance.WaitForConnection(adminLogin, adminPassword));
            return;
        }
        else
        {
            CustomLogger.LogMessage(string.Format("Continue ConnectToServer with login: {0}, password: {1}",
                adminLogin, adminPassword));
        }

        if (instance.currentServerConnection != null)
        {
            CustomLogger.LogErrorMessage("Client is already connected to Server!");
            return;
        }
        Dictionary<string, string> authArgs = new Dictionary<string, string>();
        authArgs.Add("userId", Random.Range(int.MinValue, int.MaxValue).ToString());
        instance.lastAdminPassword = adminPassword;

        PlayerIO.Authenticate(gameId, "public", authArgs, null,instance.ConnectionToServerSuccess,instance.ConnectionFailed);
    }
    #endregion

    #region Connection To Game Handler
    private void ConnectionToServerSuccess(Client client) //Fix
    {
        if (instance.currentClient != null)
            return;

        instance.currentClient = client;

        Dictionary<string, string> joinData = new Dictionary<string, string>();
        joinData.Add("password", lastAdminPassword);
        joinData.Add("accountType", "admin");

        client.Multiplayer.CreateJoinRoom("Admin", "RoomAdminMobil", false, null, joinData,ConnectionToServerRoomSuccess,ConnectionFailed);
    }
    private void ConnectionFailed(PlayerIOError error)
    {
        CustomLogger.LogErrorMessage("<color=red>Server ConnectionFailed:/color> \n" + error.Message);
    }
    #endregion

    #region Connection To Room Handler
    private void ConnectionToServerRoomSuccess(Connection connection)
    {
        CustomLogger.LogMessage("<color=green>Connection to Room Success</color>");
        currentServerConnection = connection;
        currentServerConnection.OnMessage += ServerMessageHandler;
        currentServerConnection.OnDisconnect += ServerDisconnectHandler;

        ConnectionSuccessEvent?.Invoke();

        if (connectionWaitingCoroutine != null)
        {
            StopCoroutine(connectionWaitingCoroutine);
            connectionWaitingCoroutine = null;
        }
    }
    #endregion

    #region Server Messages Part
    private void ServerMessageHandler(object sender, Message e)
    {
        switch(e.Type)
        {
            #region First Connection
            case FirstConnectionMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        if (!PlayerPrefs.HasKey("FirstLogin"))
                        {
                            PlayerPrefs.SetInt("FirstLogin", 1);
                            LoginScreen.ShowTermsAndConditions();
                        }

                        string rawAppraiserBlocksData = e.GetString(1);
                        if (!string.IsNullOrEmpty(rawAppraiserBlocksData))
                        { //Getting appraisels from server
                            Debug.Log("<color=yellow> Getting appraisels</color>" + rawAppraiserBlocksData);
                            AdminBlockListScreen.SetupAppraiserBlockList(rawAppraiserBlocksData);
                        }
                        string rawGarageBlocksData = e.GetString(2);
                        if (!string.IsNullOrEmpty(rawGarageBlocksData))
                        {
                            Debug.Log("<color=yellow> Getting rawGarageBlocksData: </color>" + rawGarageBlocksData);
                            AdminBlockListScreen.SetupGarageBlockList(rawGarageBlocksData);
                        }
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage("FirstConnection warning:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("FirstConnection unknown warning...");
                        }
                        ConnectionFailedEvent?.Invoke();
                    }
                }
                break;

            case GetFirstAdminDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawEmailsData = e.GetString(1);
                        if (!string.IsNullOrEmpty(rawEmailsData)) {
                            Debug.Log("<color=yellow> GetFirstAdminDataMessageType: </color>" + rawEmailsData);
                            EmailsListScreen.CreateEmailsList(rawEmailsData);

                        }

                        //CardListManager.SetupCardList(rawEmailsData);//

                        string displayName = e.GetString(2);
                        {
                            Debug.Log("<color=yellow> displayName: </color>" + displayName);
                            GameManager.SetUserDisplayName(displayName);

                        }
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogMessage("GetFirstAdminData warning:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogMessage("GetFirstAdminData unknown warning...");
                        }
                    }
                }
                break;
            #endregion

            #region Get Appraisers/Garages List

            #region Update Names
            //case GetAppraiserNamesMessageType:
            //    {
            //        bool result = e.GetBoolean(0);
            //        if (result)
            //        {
            //            string rawAppraiserNames = e.GetString(1);
            //            AppraiserListScreen.CheckUpdateAppraisersList(rawAppraiserNames);
            //        }
            //        else
            //        {
            //            string errorMessage = e.GetString(1);
            //            if (!string.IsNullOrEmpty(errorMessage))
            //            {
            //                CustomLogger.LogErrorMessage(GetAppraiserNamesMessageType + " error:\n" +
            //                    errorMessage);
            //            }
            //            else
            //            {
            //                CustomLogger.LogErrorMessage(GetAppraiserNamesMessageType + " unknown error...");
            //            }
            //        }
            //    }
            //    break;

            //case GetGarageNamesMessageType:
            //    {
            //        bool result = e.GetBoolean(0);
            //        if (result)
            //        {
            //            string rawAppraiserNames = e.GetString(1);
            //            GaragesListScreen.CheckUpdateGarageList(rawAppraiserNames);
            //        }
            //        else
            //        {
            //            string errorMessage = e.GetString(1);
            //            if (!string.IsNullOrEmpty(errorMessage))
            //            {
            //                Debug.LogError(GetGarageNamesMessageType + " error:\n" +
            //                    errorMessage);
            //            }
            //            else
            //            {
            //                Debug.LogError(GetGarageNamesMessageType + " unknown error...");
            //            }
            //        }
            //    }
            //    break;
            #endregion

            case GetGarageAndAppraiserDatasMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.LogError(GetGarageAndAppraiserDatasMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(GetGarageAndAppraiserDatasMessageType + " unknown error...");
                        }
                    }
                }
                break;

            case GetGarageDatasMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawCardsData = e.GetString(1);
                        Debug.Log("<color=blue> GetGarageDatasMessageType: </color>" + rawCardsData);
                        GaragesListScreen.CreateGarageList(rawCardsData.Split('_'));
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.LogError(GetGarageDatasMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(GetGarageDatasMessageType + " unknown error...");
                        }
                    }
                }
                break;

            case GetAppraiserDatasMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawCardsData = e.GetString(1);
                        Debug.Log("<color=blue> GetAppraiserDatasMessageType: </color>" + rawCardsData);
                        AppraiserListScreen.CreateAppraiserList(rawCardsData.Split('_'));
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.LogError(GetGarageDatasMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(GetGarageDatasMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Add/Remove Email
            #region Add New Email
            case AddNewEmailMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawEmailData = e.GetString(1);
                        Debug.Log("<color=blue> AddNewEmailMessageType: </color>" + rawEmailData);
                        EmailsListScreen.AddNewEmailSuccess(rawEmailData);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(AddNewEmailMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(AddNewEmailMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Remove Email
            case RemoveFromEmailsMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawEmailData = e.GetString(1);
                        Debug.Log("<color=red> RemoveEmail : </color>" + rawEmailData);
                        EmailsListScreen.RemoveEmailFromList(rawEmailData);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(RemoveFromEmailsMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(RemoveFromEmailsMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion
            #endregion

            #region Check Connection
            case CheckingConnectionMessageType:
                {
                    if (isNullConnection)
                        return;

                    currentServerConnection.Send(CheckingConnectionMessageType, true);

                    SendGetBlocksDataMessage();
                }
                break;
            #endregion

            #region Get Blocks Data
            case GetBlocksDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawAppraiserBlocksData = e.GetString(1);
                        if (!string.IsNullOrEmpty(rawAppraiserBlocksData))
                            AdminBlockListScreen.UpdateAppraiserBlockList(rawAppraiserBlocksData.Split('~'));

                        string rawGarageBlocksData = e.GetString(2);
                        if (!string.IsNullOrEmpty(rawGarageBlocksData))
                            AdminBlockListScreen.UpdateGarageBlockList(rawGarageBlocksData.Split('~'));
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.LogError(" Same Error");
                            return;
                            CustomLogger.LogErrorMessage("GetBlocksData error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("GetBlocksData unknown error...");
                        }

                        if (e.Count < 3)
                        {
                            CustomLogger.LogMessage("GetBlocksData can't add 'problemBlockName', cuz 'problemBlockName' is null!");
                            return;
                        }

                        string problemBlockName = e.GetString(2);
                        if (string.IsNullOrEmpty(problemBlockName))
                        {
                            Debug.LogError("GetBlocksData 'problemBlockName' is null!");
                            return;
                        }
                        //AdminBlockListScreen.AddProblemBlock(problemBlockName);
                    }
                }
                break;
            #endregion

            #region Card Control

            #region Change Card Signal
            case ChangeCardSignalMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage("ChangeCardSignal error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("ChangeCardSignal unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Connect Appraiser To Card
            case ConnectAppraiserToCardMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage("ConnectAppraiserToCard error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("ConnectAppraiserToCard unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Change Appraiser In Card
            case ChangeAppraiserInCardMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage("ChangeAppraiserInCard error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("ChangeAppraiserInCard unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Skip First Card Check
            case SkipFirstCardCheckMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage("SkipFirstCardCheck error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage("SkipFirstCardCheck unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Close Card
            case CloseCardMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (!result)
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(CreateCardMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(CreateCardMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Create Card
            case CreateCardMessageType:
                {
                    bool result = e.GetBoolean(0);
                    CardCreationScreen.UnlockBusy();
                    if (result)
                    {
                        CardCreationScreen.CardCreationSuccess();//TODO: Add that it will turn on after InsertNewOrder
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(CreateCardMessageType + " error:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(CreateCardMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Add Cards To Admin Block
            case AddCardsToAdminBlockMessageType:
                {
                    bool result = e.GetBoolean(0);
                    SearchResultScreen.UnlockBusy();
                    if (result)
                    {
                        CardSearchScreen.HideCardsSearchScreen();
                        SearchResultScreen.HideAllScreens();
                        AdminBlockListScreen.ShowBlocksListScreen();
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.Log(AddCardsToAdminBlockMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(AddCardsToAdminBlockMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Get Full Card Data
            case GetFullCardDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string rawCardData = e.GetString(1);
                        if (string.IsNullOrEmpty(rawCardData))
                        {
                            CustomLogger.LogErrorMessage("GetCardDataMessageType result success, but 'rawCardData' is empty!");
                            return;
                        }
                        CardChangerScreen.ShowCardChangerScreen(rawCardData); //FIX
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.Log(GetFullCardDataMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(GetFullCardDataMessageType + " unknown error...");
                        }
                    }
                }
            break;
            #endregion

            #region Change Card Data
            case ChangeCardDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    CardChangerScreen.UnlockBusy();
                    if (result)
                    {
                        CardChangerScreen.ChangeCardDataSuccess();
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            Debug.Log(ChangeCardDataMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            Debug.LogError(ChangeCardDataMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion
            #endregion

            #region Search Cards Result
            case SearchCardsMessageType:
                {
                    bool result = e.GetBoolean(0);
                    CardSearchScreen.UnlockBusy();
                    if (result)
                    {
                        string rawSearchResultData = e.GetString(1);
                        SearchResultScreen.ShowSearchResultScreen(rawSearchResultData);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(SearchCardsMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(SearchCardsMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Garage&Appraiser Control

            #region Create Appraiser
            case CreateAppraiserMessageType:
                {
                    bool result = e.GetBoolean(0);
                    AppraisersSearchScreen.UnlockBusy();
                    if (result)
                    {
                        string rawAppraiserData = e.GetString(1);
                        AppraisersSearchScreen.CreationAppraiserSuccess(rawAppraiserData.Split('_'));
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(CreateAppraiserMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(CreateAppraiserMessageType + " unknown error...");
                        }
                        //if (e.Count > 2)
                        //{
                        //    bool isAppraiserNameExist = e.GetBoolean(2);
                        //    AppraisersSearchScreen.CreationCardFailed(isAppraiserNameExist);
                        //}
                    }
                }
                break;
            #endregion

            #region Create Garage
            case CreateGarageMessageType:
                {
                    bool result = e.GetBoolean(0);
                    GaragesSearchScreen.UnlockBusy();
                    if (result)
                    {
                        string rawGarageData = e.GetString(1);
                        GaragesSearchScreen.CreationGarageSuccess(rawGarageData.Split('_'));
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(CreateGarageMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(CreateGarageMessageType + " unknown error...");
                        }
                        //if (e.Count > 2)
                        //{
                        //    bool isGarageNameExist = e.GetBoolean(2);
                        //    GaragesSearchScreen.CreationCardFailed(isGarageNameExist);
                        //}
                    }
                }
                break;
            #endregion

            #region Delete Appraiser
            case DeleteAppraiserMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string appraiserName = e.GetString(1);
                        AppraiserListScreen.RemoveAppraiserFromList(appraiserName);
                        AppraiserResultScreen.RemoveAppraiserResultFromList(appraiserName);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(DeleteAppraiserMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(DeleteAppraiserMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Delete Garage
            case DeleteGarageMessageType:
                {
                    bool result = e.GetBoolean(0);
                    if (result)
                    {
                        string garageName = e.GetString(1);
                        GaragesListScreen.RemoveGarageFromList(garageName);
                        GarageResultScreen.RemoveGarageResultFromList(garageName);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(DeleteGarageMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(DeleteGarageMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Change Garage Data
            case ChangeGarageDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    GarageResultScreen.UnlockBusy();
                    if (result)
                    {
                        string rawGarageData = e.GetString(1);
                        if (string.IsNullOrEmpty(rawGarageData))
                        {
                            CustomLogger.LogErrorMessage("ChangeGarageData 'rawGarageData' is empty!");
                            return;
                        }
                        string[] garageData = rawGarageData.Split('_');
                        GarageResultScreen.ChangeGarageDataSuccess(garageData);
                        GaragesListScreen.UpdateGarage(garageData);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(ChangeGarageDataMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(ChangeGarageDataMessageType + " unknown error...");
                        }
                        //if (e.Count > 2)
                        //{
                        //    GarageResultScreen.ChangeGarageDataFailed();
                        //}
                    }
                }
                break;
            #endregion

            #region Change Appraiser Data
            case ChangeAppraiserDataMessageType:
                {
                    bool result = e.GetBoolean(0);
                    AppraiserResultScreen.UnlockBusy();
                    if (result)
                    {
                        string rawAppraiserData = e.GetString(1);
                        if (string.IsNullOrEmpty(rawAppraiserData))
                        {
                            CustomLogger.LogErrorMessage("ChangeAppraiserData 'rawAppraiserData' is empty!");
                            return;
                        }
                        string[] appraiserData = rawAppraiserData.Split('_');
                        AppraiserResultScreen.ChangeAppraiserDataSuccess(appraiserData);
                        AppraiserListScreen.UpdateAppraiser(appraiserData);
                    }
                    else
                    {
                        string errorMessage = e.GetString(1);
                        if (!string.IsNullOrEmpty(errorMessage))
                        {
                            CustomLogger.LogErrorMessage(ChangeAppraiserDataMessageType + " message:\n" +
                                errorMessage);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(ChangeAppraiserDataMessageType + " unknown error...");
                        }
                        //if (e.Count > 2)
                        //{
                        //    AppraiserResultScreen.ChangeAppraiserDataFailed();
                        //}
                    }
                }
                break;
            #endregion

            #endregion

            #region Manual Card Commands
            case ManualRemoveUnderCardFromBlockMessageType:
                {
                    bool isAppraiserBlock = e.GetBoolean(0);
                    string blockName = e.GetString(1);
                    string underCardNumber = e.GetString(2);
                    if (isAppraiserBlock)
                        AdminBlockListScreen.RemoveAppraiserUnderCardElement(blockName, underCardNumber);
                    else
                        AdminBlockListScreen.RemoveGarageUnderCardElement(blockName, underCardNumber);
                }
                break;

            case ManualBusyUnderCardInBlockMessageType:
                {
                    bool isAppraiserBlock = e.GetBoolean(0);
                    string blockName = e.GetString(1);
                    string underCardNumber = e.GetString(2);
                    if (isAppraiserBlock)
                        AdminBlockListScreen.MakeBusyAppraiserUnderCardElement(blockName, underCardNumber);
                    else
                        AdminBlockListScreen.MakeBusyGarageUnderCardElement(blockName, underCardNumber);
                }
                break;
            #endregion

            #region Update Under Card Info
            case GetUpdateCardInfoMessageType:
                {
                    bool isAppraiserBlock = e.GetBoolean(0);
                    string blockName = e.GetString(1);
                    string rawUnderCardData = e.GetString(2);
                    if (isAppraiserBlock)
                        AdminBlockListScreen.UpdateAppraiserBlockUnderCard(blockName, rawUnderCardData.Split('_'));
                    else
                        AdminBlockListScreen.UpdateGarageBlockUnderCard(blockName, rawUnderCardData.Split('_'));
                }
                break;
            #endregion

            #region Add Document To Appraiser
            case AddDocumentToAppraiserMessageType:
                {
                    bool result = e.GetBoolean(0);
                    AdminDocumentsScreen.AddDocumentToAppraiserResult(result);
                    if (!result)
                    {
                        string messageData = e.GetString(1);
                        if (!string.IsNullOrEmpty(messageData))
                        {
                            CustomLogger.LogErrorMessage(AddDocumentToAppraiserMessageType + " error:\n" +
                                messageData);
                        }
                        else
                        {
                            CustomLogger.LogErrorMessage(AddDocumentToAppraiserMessageType + " unknown error...");
                        }
                    }
                }
                break;
            #endregion

            #region Debug
            case DebugMessageType:
                {
                    string messageData = e.GetString(0);
                    if (!string.IsNullOrEmpty(messageData))
                    {
                        Debug.Log(DebugMessageType + " warning:\n" +
                            messageData);
                    }
                    else
                    {
                        Debug.Log(DebugMessageType + " unknown warning...");
                    }
                }
                break;
                #endregion
        }
    }

    #region Add/Remove Email
    #region Add Email To List
    public static void SendAddNewEmailMessage(string rawEmailData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(AddNewEmailMessageType, rawEmailData);
    }
    #endregion

    #region Remove Email From List
    public static void SendRemoveFromEmailMessage(string rawEmailData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(RemoveFromEmailsMessageType, rawEmailData);
    }
    #endregion
    #endregion

    #region Get Blocks Data
    public static void SendGetBlocksDataMessage()
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(GetBlocksDataMessageType, true);
    }
    #endregion

    #region Search Cards
    public static void SendSearchCardsMessage(string rawSearchFilter)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(SearchCardsMessageType, rawSearchFilter);
    }
    #endregion

    #region Add Document To Appraiser
    public static void SendDocumentToAppraiserMessage(string appraiserName, string rawDocumentData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(AddDocumentToAppraiserMessageType, appraiserName, rawDocumentData);
    }
    #endregion

    #region Control
    #region Send Confirm Card
    public static void SendConfirmCard(string cardNumber, string appraiserName)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ConnectAppraiserToCardMessageType, cardNumber, appraiserName);
    }
    #endregion

    #region Change Card Appraiser
    public static void SendChangeCardAppraiser(string cardNumber, string oldAppraiser, string newAppraiser, bool isConfirmed)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeAppraiserInCardMessageType,
            cardNumber, oldAppraiser, newAppraiser, isConfirmed);
    }
    public static void SendChangeCardGarage(string cardNumber, string oldGarage, string newGarage)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeGarageInCardMessageType,
            cardNumber, oldGarage, newGarage);
    }
    #endregion

    #region Change Card Signal
    public static void SendChangeCardSignal(string cardNumber, int newSignal)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeCardSignalMessageType, cardNumber, newSignal);
    }
    #endregion

    #region Skip First Card Checking 
    public static void SendSkipFirstCardChecking(string cardNumber, string closeDate)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(SkipFirstCardCheckMessageType, cardNumber, closeDate);
    }
    #endregion

    #region Close Card
    public static void SendCloseCard(string cardNumber, string closeDate) //FIX
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(CloseCardMessageType, cardNumber, closeDate);
    }
    #endregion

    #region Create order Card
    public static void SendCreateCardMessage(string rawCreationData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(CreateCardMessageType, rawCreationData);
    }
    #endregion

    #region Add Cards To Admin Block
    public static void SendAddCardsToAdminBlockMessage(string rawCardNumbers)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(AddCardsToAdminBlockMessageType, rawCardNumbers);
    }
    #endregion

    #region Get Full Card Data
    public static void GetFullCardData(string cardNumber)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(GetFullCardDataMessageType, cardNumber);
    }
    #endregion

    #region Change Card Data
    public static void ChangeCardData(string oldCardNumber, string newCardData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeCardDataMessageType, oldCardNumber, newCardData);
    }
    #endregion
    #endregion

    #region Garage&Appraiser Control

    #region Change Appraiser/Garage Data
    public static void ChangeGarageData(string rawGarageData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeGarageDataMessageType, rawGarageData);
    }
    public static void ChangeAppraiserData(string rawAppraiserData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ChangeAppraiserDataMessageType, rawAppraiserData);
    }
    #endregion

    #region Delete Appraiser/Garage
    public static void DeleteGarage(string garageName)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(DeleteGarageMessageType, garageName);
    }
    public static void DeleteAppraiser(string appraiserName)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(DeleteAppraiserMessageType, appraiserName);
    }
    #endregion

    #region Create Appraiser/Garage
    public static void CreateGarage(string rawGarageCreationData)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(CreateGarageMessageType, rawGarageCreationData);
    }
    public static void CreateAppraiser(string rawAppraiserCreationData) //FIX Wont Upload File
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(CreateAppraiserMessageType, rawAppraiserCreationData);
    }
    #endregion

    #endregion

    #region Manual Card Commands
    public static void SendManualRemoveAppraiserUnderCard(string appraiserBlockName, string cardNumber)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ManualRemoveUnderCardFromBlockMessageType, true,
            appraiserBlockName, cardNumber);
    }
    public static void SendManualRemoveGarageUnderCard(string garageBlockName, string cardNumber)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ManualRemoveUnderCardFromBlockMessageType, false,
            garageBlockName, cardNumber);
    }
    public static void SendManualBusyAppraiserUnderCard(string appraiserBlockName, string cardNumber)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ManualBusyUnderCardInBlockMessageType, true,
            appraiserBlockName, cardNumber);
    }
    public static void SendManualBusyGarageUnderCard(string garageBlockName, string cardNumber)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(ManualBusyUnderCardInBlockMessageType, false,
            garageBlockName, cardNumber);
    }
    #endregion

    #region Set PlayFabID
    public static void SendSetPlayFabID(string playFabID)
    {
        if (isNullInstance || instance.isNullConnection)
            return;

        instance.currentServerConnection.Send(SendSetPlayFabIDMessageType, playFabID);
    }
    #endregion

    #endregion

    #region Disconnect Part
    public static void StopConnectionToServer()
    {
        if (isNullInstance)
            return;

        CustomLogger.LogMessage("StopConnectionToServer");
        if (instance.connectionWaitingCoroutine != null)
        {
            CustomLogger.LogMessage("Stop connectionWaitingCoroutine...");
            instance.StopCoroutine(instance.connectionWaitingCoroutine);
            instance.connectionWaitingCoroutine = null;
        }
        else
        {
            instance.ManualDisconnect();
        }
    }
    private void ManualDisconnect()
    {
        if (isNullInstance)
            return;

        if (currentServerConnection != null)
        {
            CustomLogger.LogMessage("Server ManualDisconnect");

            instance.currentServerConnection.Disconnect();
            currentServerConnection = null;
            currentClient = null;
        }
    }
    private void ServerDisconnectHandler(object sender, string message)
    {
        CustomLogger.LogMessage(string.Format("ServerDisconnectHandler." +
            "Client was disconnected by {0} with reason:\n{1}", sender.ToString(), message));

        if (connectionWaitingCoroutine != null)
        {
            CustomLogger.LogMessage("Stop connectionWaitingCoroutine");

            StopCoroutine(connectionWaitingCoroutine);
            connectionWaitingCoroutine = null;
        }

        if (currentServerConnection != null)
        {
            currentClient = null;
            currentServerConnection = null;
            LostConnectionEvent?.Invoke();
        }

        //if (checkConnectionCoroutine != null)
        //{
        //    CustomLogger.LogMessage("Stop checkConnectionCoroutine");

        //    StopCoroutine(checkConnectionCoroutine);
        //    checkConnectionCoroutine = null;
        //}
    }

    private void OnApplicationQuit()
    {
        if (currentServerConnection != null)
        {
            currentServerConnection.Disconnect();
            currentServerConnection = null;
            currentClient = null;
        }
    }
    #endregion
}
