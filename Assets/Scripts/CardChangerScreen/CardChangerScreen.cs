﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardChangerScreen : MonoBehaviour
{
    #region Singleton
    private static CardChangerScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("CardChangerScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject cardCreationScreen;

    [Space(10)]
    [SerializeField]
    private InputField garageNameInputField; // שם מוסך
    [SerializeField]
    private InputField carNumberInputField; // מס רכב
    [SerializeField]
    private InputField cardNumberInputField; //מספר הזמנה 
    [SerializeField]
    private InputField secondNumberInputField; // מס הרוע
    [SerializeField]
    private InputField secondDateInputField; // תאריך הרוע
    [SerializeField]
    private InputField cardTypeInputField; // סוג בדיקה
    [SerializeField]
    private InputField insurerNameInputField; // חברת ביטוח

    [Space(10)]
    [SerializeField]
    private GameObject creationSuccessImageObj;

    int mId = -1;
    #endregion

    #region Private Fields
    private string oldCardNumber;

    private bool isBusy;
    #endregion

    #region Show/Hide Card Creation Screen
    public static void ShowCardChangerScreen(string rawCardData) //when data Downloaded From PlayerIO
    {
        if (isNullInstance)
            return;

        instance.ShowCardChangerScreen(rawCardData.Split('_'));
    }

    public static void CreateOrderCard(Orders cardData,int id)
    {
        instance.CreateOrdersScreen(cardData,id); //Create OrderCard from data in the right loc
    }

    public void ShowCardChangerScreen(string[] cardData)
    {
        cardCreationScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(cardCreationScreen.transform);

        this.oldCardNumber = cardData[0]; //??  cardNumberInputField
        cardNumberInputField.text = cardData[0];  //מספר הזמנה 
        carNumberInputField.text = cardData[1]; // מס רכב
        garageNameInputField.text = GaragesListScreen.GetDisplayNameByGarage(cardData[2]); // שם מוסך
        secondNumberInputField.text = cardData[3]; // מס ארוע
        secondDateInputField.text = cardData[4]; // תאריך הרוע
        cardTypeInputField.text = cardData[5]; // סוג בדיקה
        insurerNameInputField.text = cardData[6]; // חברת ביטוח
    }

    public void CreateOrdersScreen(Orders cardData, int id)
    {
        cardCreationScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(cardCreationScreen.transform);
        mId = id;
        this.oldCardNumber = cardData.eventID; //??  cardNumberInputField
        cardNumberInputField.text = cardData.eventID;  //מספר הזמנה 
        carNumberInputField.text = cardData.carID; // מס רכב
        garageNameInputField.text = GaragesListScreen.GetDisplayNameByGarage(cardData.garageName); // שם מוסך
        secondNumberInputField.text = cardData.orderID; // מס ארוע
        secondDateInputField.text = cardData.eventDate; // תאריך הרוע
        cardTypeInputField.text = cardData.typeEvent; // סוג בדיקה
        insurerNameInputField.text = cardData.insuranceCompany; // חברת ביטוח
    }


    public static void HideCardsChangerScreen()
    {
        if (isNullInstance)
            return;

        instance.HideCardChangerScreen();
    }
    public void HideCardChangerScreen()
    {
        cardCreationScreen.SetActive(false);
        creationSuccessImageObj.SetActive(false);
    }
    #endregion

    #region Change Garage
    public void ChangeGarage()
    {
        GaragesListScreen.ShowGarageListScreen(ChangeGarageResult);
    }

    private void ChangeGarageResult(string garageName)
    {
        this.garageNameInputField.text = garageName;
    }
    #endregion

    #region OnClick
    public void ConfirmData() //FIX 2  Supposedly this makes Redo Orders??
    {
        if (isBusy)
            return;

        string newCardData = cardNumberInputField.text + "_";
        if (cardNumberInputField.text == oldCardNumber)
        {
            StartCoroutine(HighlighInputField(cardNumberInputField.image));
            return;
        }

        if (carNumberInputField.text.Length < 2 || cardNumberInputField.text.Length < 2)
        {
            if (carNumberInputField.text.Length != 0)
                StartCoroutine(HighlighInputField(carNumberInputField.image));
            else if (cardNumberInputField.text.Length != 0)
                StartCoroutine(HighlighInputField(cardNumberInputField.image));
            else
            {
                StartCoroutine(HighlighInputField(carNumberInputField.image));
                StartCoroutine(HighlighInputField(cardNumberInputField.image));
            }
            return;
        }

        newCardData += carNumberInputField.text + "_";
        newCardData += DateStringConverter.GetDMYHMDate() + "_";

        string garageName = string.Empty;
        if (garageNameInputField.text != string.Empty)
        {
            garageName = GaragesListScreen.GetGarageNameByDisplay(garageNameInputField.text);
            if (garageName == null)
            {
                StartCoroutine(HighlighInputField(garageNameInputField.image));
                return;
            }
        }
        newCardData += garageName + "_";

        newCardData += secondNumberInputField.text + "_";
        newCardData += secondDateInputField.text + "_";
        newCardData += cardTypeInputField.text + "_";
        newCardData += insurerNameInputField.text;
        DataBaseController.UpdateOrders(newCardData.Split('_'), mId); //Update to SQL
        //PlayerIOServerManager.ChangeCardData(oldCardNumber, newCardData);//FIX

        isBusy = true;
        string appraiserBlockName = "Admin" + "\\" + "nimdA";
        AdminBlockListScreen.RemoveAppraiserUnderCardElement(appraiserBlockName, oldCardNumber);
        PlayerIOServerManager.SendManualRemoveAppraiserUnderCard(appraiserBlockName, oldCardNumber);
    }
    #endregion

    #region Creation Success
    public static void ChangeCardDataSuccess()
    {
        if (isNullInstance)
            return;

        instance.creationSuccessImageObj.SetActive(true);
        instance.StartCoroutine(instance.AutoCloseCreationCardScreen());
    }

    private IEnumerator AutoCloseCreationCardScreen()
    {
        yield return new WaitForSeconds(2f);
        HideCardChangerScreen();
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion
}
