﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AppraiserUnderCardInfo : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    private GameObject underCardSignalsPanel;
    [SerializeField]
    private GameObject confirmButtonObj;

    [Space(10)]
    [SerializeField]
    private TextMeshProUGUI carNumberText;
    [SerializeField]
    private TextMeshProUGUI cardNumberText;
    [SerializeField]
    private TextMeshProUGUI statusNameText;

    [Space(10)]
    [SerializeField]
    private GameObject changeDataButtonObj;
    [SerializeField]
    private GameObject changeAppraiserButtonObj;
    [SerializeField]
    private GameObject openPanelButtonObj;
    [SerializeField]
    private GameObject redStatusButtonObj;
    [SerializeField]
    private GameObject greenStatusButtonObj;
    [SerializeField]
    private GameObject skipButtonObj;

    [Space(10)]
    [SerializeField]
    private Image[] signalButtonImages;
    [SerializeField]
    private TextMeshProUGUI[] signalButtonTexts;

    [Space(10)]
    [SerializeField]
    private Image[] controlButtonImages;
    [SerializeField]
    private TextMeshProUGUI[] controlButtonTexts;

    [Space(10)]
    [SerializeField]
    private Image[] confirmButtons;
    #endregion

    #region Properties Fields
    private bool _isBusy;
    private bool isBusy
    {
        set 
        {
            if (value == true)
                PlayerIOServerManager.SendManualBusyAppraiserUnderCard(blockElement.GetAppraiserBlockName(),
                    cardNumberText.text);

            _isBusy = value;
        }
        get {return _isBusy;}
    }
    #endregion

    #region Private Fields
    private RectTransform parentBlockElementRectTransform;
    private AppraiserBlockElement blockElement;

    private int statusIndex = -1;

    private int startSignalIndex = -1;
    private int signalIndex = -1; // 2 = chose  1= assign 0= video
    private int controlIndex = -1; // Abort = 1 

    private bool isOpen = false;
    private bool isConfirmed;
    #endregion

    #region Setup
    private void Start()
    {
        parentBlockElementRectTransform = GetParentBlockElement(transform);
        if (parentBlockElementRectTransform == null)
        {
            CustomLogger.LogErrorMessage("UnserCardInfo element can't find 'parentBlockElementRectTransform'");
        }
        blockElement = parentBlockElementRectTransform.GetComponent<AppraiserBlockElement>();
        blockElement.RebuildEvent();
        transform.SetSiblingIndex(0);

        //if (blockElement.GetAppraiserName() == "dVA")
        //{
        //    foreach (Image img in confirmButtons)
        //    {
        //        img.gameObject.SetActive(false);
        //    }
        //}
    }
    private RectTransform GetParentBlockElement(Transform targetTransform)
    {
        RectTransform parentBlockElementObj = null;
        if (targetTransform.GetComponent<AppraiserBlockElement>() != null)
            parentBlockElementObj = targetTransform.GetComponent<RectTransform>();
        else
        {
            if (targetTransform.parent != null)
                parentBlockElementObj = GetParentBlockElement(targetTransform.parent);
        }
        return parentBlockElementObj;
    }

    public void Setup(string cardNumber, string carNumber, string status, string signal, string confirmed, bool isSpecial)
    {
        cardNumberText.text = cardNumber;
        carNumberText.text = carNumber;

        int cardStatusIndex;
        if (!int.TryParse(status, out cardStatusIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:Setup can't parse 'status' in Card: " +
                cardNumber);
            return;
        }
        ChangeStatus(cardStatusIndex);

        if (isSpecial)
        {
            openPanelButtonObj.SetActive(false);
            if (cardNumber.EndsWith("-0"))
            {
                changeDataButtonObj.SetActive(true);
                changeAppraiserButtonObj.SetActive(false);
            }
            else
            {
                changeDataButtonObj.SetActive(false);
                changeAppraiserButtonObj.SetActive(true);
            }
            return;
        }

        if (cardStatusIndex == 4)
            return;

        int cardSignalIndex;
        if (!int.TryParse(signal, out cardSignalIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:Setup can't parse 'signal' in Card: " +
                cardNumber);
            return;
        }
        //ChangeSignal(cardSignalIndex);
        if (this.signalIndex == cardSignalIndex || cardSignalIndex == -1)
            return;
        this.signalIndex = cardSignalIndex;
        SetSignalButton(signalIndex);

        startSignalIndex = cardSignalIndex;

        bool isConfirmed;
        if (bool.TryParse(confirmed, out isConfirmed))
        {
            this.isConfirmed = isConfirmed;
        }

        if (!this.isConfirmed)
            skipButtonObj.SetActive(false);
        else
        {
            if (cardStatusIndex == -1)
                skipButtonObj.SetActive(true);
            else
                skipButtonObj.SetActive(false);
        }
    }
    #endregion

    #region OnClick
    public void ConfirmChanges() //Used by the Button confirm "ConfirmAppraiser_Button" אשר
    {
        if (_isBusy)
            return;

        AudioManager.PlayButtonSound();
        if (!isConfirmed)
        {
            if (controlIndex == 1) // Abort = 1 
            {
                AdminBlockListScreen.RemoveAppraiserUnderCardElement(blockElement.GetAppraiserBlockName(),
                    cardNumberText.text);
                PlayerIOServerManager.SendManualRemoveAppraiserUnderCard(blockElement.GetAppraiserBlockName(),
                    cardNumberText.text);

                string closeDate = DateStringConverter.GetDMYHMDate();
                PlayerIOServerManager.SendCloseCard(cardNumberText.text, closeDate);

            }
            else
            {
                if (signalIndex != -1)
                {
                    isBusy = true;
                    PlayerIOServerManager.SendConfirmCard(cardNumberText.text, blockElement.GetAppraiserName());
                    PlayerIOServerManager.SendChangeCardSignal(cardNumberText.text, signalIndex);
                    CloseUnderPanel();
                }
            }
            return;
        }

        if (controlIndex != -1)
        {
            isBusy = true;
            string closeDate = DateStringConverter.GetDMYHMDate();
            if (controlIndex == 0)
            {
                PlayerIOServerManager.SendSkipFirstCardChecking(cardNumberText.text, closeDate);
                CloseUnderPanel();
            }
            else
            {
                AdminBlockListScreen.RemoveAppraiserUnderCardElement(blockElement.GetAppraiserBlockName(),cardNumberText.text);
                PlayerIOServerManager.SendManualRemoveAppraiserUnderCard(blockElement.GetAppraiserBlockName(),cardNumberText.text);
                PlayerIOServerManager.SendCloseCard(cardNumberText.text, closeDate);
            }
        }
        else
        {
            if (startSignalIndex != signalIndex)
            {
                isBusy = true;
                PlayerIOServerManager.SendChangeCardSignal(cardNumberText.text, signalIndex);
                CloseUnderPanel();
            }
        }
    }

    public void ChangeAppraiser()
    {
        Debug.Log("ChangeAppraiser");
        AppraiserListScreen.ShowAppraiserListScreen(NewAppraiserChanged, false);
        AudioManager.PlayButtonSound();
    }

    private void NewAppraiserChanged(string newAppraiserName)
    {
        Debug.Log("Is this being sent?");
        PlayerIOServerManager.SendChangeCardAppraiser(cardNumberText.text, blockElement.GetAppraiserName(),
            newAppraiserName, isConfirmed);
        AdminBlockListScreen.RemoveAppraiserUnderCardElement(blockElement.GetAppraiserBlockName(),
            cardNumberText.text);
        PlayerIOServerManager.SendManualRemoveAppraiserUnderCard(blockElement.GetAppraiserBlockName(),
            cardNumberText.text);
    }

    public void ChangeCardData()
    {
        isBusy = true;
        PlayerIOServerManager.GetFullCardData(cardNumberText.text);
        AudioManager.PlayButtonSound();
    }
    #endregion

    #region Change Status
    private void ChangeStatus(int statusIndex)
    {
        if (this.statusIndex == statusIndex || statusIndex == -1)
            return;

        if (this.statusIndex != statusIndex)
        {
            CloseUnderPanel();
            if (statusIndex == 0)
            {
                controlIndex = -1;
            }
        }

        this.statusIndex = statusIndex;
        statusNameText.text = CardStatusManager.GetCardStatusNameByIndex(statusIndex);

        if (statusIndex == 4)
            VisualChangeStatus(true);
    }

    private void VisualChangeStatus(bool isClose)
    {
        if (isClose)
        {
            redStatusButtonObj.SetActive(true);

            greenStatusButtonObj.SetActive(false);
            changeAppraiserButtonObj.SetActive(false);
            openPanelButtonObj.SetActive(false);
            CloseUnderPanel();
        }
        else
        {
            greenStatusButtonObj.SetActive(true);
            redStatusButtonObj.SetActive(false);
        }
        confirmButtonObj.SetActive(false);
    }
    #endregion

    #region Change Signal
    public void ChangeSignal(int signalIndex)
    {
        if (this.signalIndex == signalIndex || signalIndex == -1)
            return;

        this.signalIndex = signalIndex;
        SetSignalButton(signalIndex);
        AudioManager.PlayButtonSound(); //FIX 1
    }

    public void SetSignalButton(int signalIndex)
    {
        for (int i = 0; i < signalButtonImages.Length; ++i)
        {
            signalButtonImages[i].color = GlobalParameters.disabledButtonColor;
            signalButtonTexts[i].color = GlobalParameters.disabledTextColor;
        }

        signalButtonImages[signalIndex].color = GlobalParameters.enabledButtonColor;
        signalButtonTexts[signalIndex].color = GlobalParameters.enabledTextColor;
        this.signalIndex = signalIndex;
    }
    #endregion

    #region Change Control
    public void ChangeControl(int controlIndex) //Cancel = 0? will 
    {
        if (controlIndex == -1)
            return;

        if (this.controlIndex == controlIndex)
            this.controlIndex = -1;
        else
            this.controlIndex = controlIndex;
        SetControlButton(this.controlIndex);
        AudioManager.PlayButtonSound();
    }

    private void SetControlButton(int controlIndex) 
    {
        for (int i = 0; i < controlButtonImages.Length; ++i)
        {
            controlButtonImages[i].color = GlobalParameters.disabledButtonColor;
            controlButtonTexts[i].color = GlobalParameters.disabledTextColor;
        }
        this.controlIndex = controlIndex;
        if (controlIndex == -1)
            return;
        controlButtonImages[controlIndex].color = GlobalParameters.enabledButtonColor;
        controlButtonTexts[controlIndex].color = GlobalParameters.enabledTextColor;
    }
    #endregion

    #region OpenClose Part
    public void OpenCloseUnderCardPanel() // שנה
    {
        if (isOpen)
            CloseUnderPanel();
        else
            OpenUnderPanel();

        isOpen = !isOpen;
        AudioManager.PlayButtonSound();
    }

    private void OpenUnderPanel()
    {
        if (!isOpen && _isBusy)
            return;

        openPanelButtonObj.SetActive(false);

        underCardSignalsPanel.SetActive(true);
        changeAppraiserButtonObj.SetActive(true);
        if (!isConfirmed)
        {
            //if (blockElement.GetAppraiserName() != "dVA")
                confirmButtonObj.SetActive(true);
        }
        else
        {
            VisualChangeStatus(false);
        }

        blockElement.RebuildEvent();
        if (blockElement.isSpecial)
            AdminBlockListScreen.SlideDownAdminUnderPanel(1000f);
        else
            AdminBlockListScreen.SlideDownAppraiserUnderPanel(1000f);
    }

    private void CloseUnderPanel()
    {
        openPanelButtonObj.SetActive(true);

        changeAppraiserButtonObj.SetActive(false);
        underCardSignalsPanel.SetActive(false);
        confirmButtonObj.SetActive(false);
        greenStatusButtonObj.SetActive(false);

        blockElement?.RebuildEvent();
    }
    #endregion

    #region Make Busy Under Card
    public void MakeBusy()
    {
        _isBusy = true;
    }
    #endregion

    #region Update Card Info //Who Updates thisssssssssssss???
    public void UpdateCardInfo(string cardNumber, string carNumber, string status, string signal, string confirmed, bool isSpecial)
    {
        cardNumberText.text = cardNumber;
        carNumberText.text = carNumber;
        //Debug.Log("<color=blue> UpdateCardInfo </color>");
        int cardStatusIndex;
        if (!int.TryParse(status, out cardStatusIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:UpdateCardInfo can't parse 'status' in Card: " +
                cardNumber);
            return;
        }
        ChangeStatus(cardStatusIndex);

        if (isSpecial)
        {
            if (cardNumber.EndsWith("-0"))
            {
                changeDataButtonObj.SetActive(true);
                changeAppraiserButtonObj.SetActive(false);
            }
            else
            {
                changeDataButtonObj.SetActive(false);
                changeAppraiserButtonObj.SetActive(true);
            }
            return;
        }

        if (cardStatusIndex == 4)
            return;

        int cardSignalIndex;
        if (!int.TryParse(signal, out cardSignalIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:UpdateCardInfo can't parse 'signal' in Card: " +
                cardNumber);
            return;
        }
        ChangeSignal(cardSignalIndex);
        startSignalIndex = cardSignalIndex;

        bool isConfirmed;
        if (bool.TryParse(confirmed, out isConfirmed))
        {
            if (this.isConfirmed != isConfirmed)
                CloseUnderPanel();
            this.isConfirmed = isConfirmed;
        }

        if (!this.isConfirmed)
        {
            skipButtonObj.SetActive(false);
        }
        else
        {
            if (cardStatusIndex == -1)
                skipButtonObj.SetActive(true);
            else
                skipButtonObj.SetActive(false);
        }
        
        _isBusy = false;
    }
    #endregion

    #region Getters
    public string GetCardNumber()
    {
        return cardNumberText.text;
    }
    #endregion

    private void OnDestroy()
    {
        blockElement.RebuildEvent();
    }
}
