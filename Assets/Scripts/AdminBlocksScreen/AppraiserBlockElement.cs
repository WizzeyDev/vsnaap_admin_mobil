﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AppraiserBlockElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    private GameObject cardsPanel;
    [SerializeField]
    private GameObject addNewCardButtonObj;
    [SerializeField]
    private RectTransform arrowRectTransform;

    [Space(10)]
    [SerializeField]
    private TextMeshProUGUI displayNameTitleText;
    [SerializeField]
    private TextMeshProUGUI displayNameText;
    [SerializeField]
    private GameObject underCardElementPrefab;
    [SerializeField]
    private Transform underCardListParent;
    #endregion

    #region Private Fields
    private RectTransform rectTransform;
    private RectTransform parentRectTransform;

    private List<AppraiserUnderCardInfo> createdUnderCardElements;
    private List<string> problemUnderCardNumbers;

    private string appraiserName;
    private string appraiserBlockName;

    private const float openedAngle = 0f;
    private const float closedAngle = 180f;

    public bool isSpecial
    {
        get;
        private set;
    }
    private bool isOpen = false;
    #endregion

    #region Setup
    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        parentRectTransform = transform.parent.GetComponent<RectTransform>();
        if (parentRectTransform == null)
        {
            CustomLogger.LogErrorMessage("AppraiserBlockElement element can't find 'parentRectTransform'");
        }
    }

    public void Setup(string[] blockData, bool isSpecial)
    {
        this.appraiserBlockName = blockData[0];
        string[] nameData = appraiserBlockName.Split('\\');
        this.appraiserName = nameData[0];
        this.displayNameText.text = nameData[1];
        if (displayNameText.text == "זקצר")
        {
            displayNameTitleText.text = "משרד השמאות";
        }

        this.isSpecial = isSpecial;

        if (isSpecial)
            addNewCardButtonObj.SetActive(false);

        CreateUnderCards(blockData, isSpecial);
    }
    #endregion

    #region Under Cards
    private void CreateUnderCards(string[] blockData, bool isSpecial)
    {
        createdUnderCardElements = new List<AppraiserUnderCardInfo>();
        problemUnderCardNumbers = new List<string>();
        for (int i = 1; i < blockData.Length; ++i)
        {
            AddUnderCardToList(blockData[i].Split('_'), isSpecial);
        }
    }

    private void AddUnderCardToList(string[] underCardData, bool isSpecial)
    {
        if (createdUnderCardElements == null)
        {
            CustomLogger.LogErrorMessage("AddUnderCards 'createdUnderCardElements' list is null!");
            return;
        }

        AppraiserUnderCardInfo underCardElement = Instantiate(underCardElementPrefab, underCardListParent).
            GetComponent<AppraiserUnderCardInfo>();
        if (underCardElement == null)
        {
            CustomLogger.LogErrorMessage(underCardElement.gameObject.name + " has no attached 'UnderCardInfo'!");
            return;
        }

        underCardElement.Setup(underCardData[0], underCardData[1], underCardData[2], underCardData[3],
            underCardData[4], isSpecial);
        createdUnderCardElements.Add(underCardElement);
    }

    private void RemoveUnderCardFromList(string cardNumber)
    {
        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            string actualCardNumber = createdUnderCardElements[i].GetCardNumber();
            if (actualCardNumber == cardNumber)
            {
                Destroy(createdUnderCardElements[i].gameObject);
                createdUnderCardElements.RemoveAt(i--);
                break;
            }
        }
    }

    public void UpdateBlockInfo(string[] blockData, bool isSpecial)
    {
        List<string> allUnderCardNumbers = new List<string>();
        for (int i = 1; i < blockData.Length; ++i)
        {
            if (string.IsNullOrEmpty(blockData[i]))
                continue;

            string[] underCardData = blockData[i].Split('_');
            string underCardNumber = underCardData[0];
            if (!IsOldListContainsUnderCard(underCardNumber) &&
                !problemUnderCardNumbers.Contains(underCardNumber))
            {
                AddUnderCardToList(underCardData, isSpecial);
            }
            else
            {
                UpdateUnderCardInBlock(underCardData, isSpecial);
            }

            allUnderCardNumbers.Add(underCardNumber);
        }

        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            string cardNumber = createdUnderCardElements[i].GetCardNumber();
            if (!allUnderCardNumbers.Contains(cardNumber))
            {
                Destroy(createdUnderCardElements[i].gameObject);
                createdUnderCardElements.RemoveAt(i--);
            }
        }
    }

    public void UpdateUnderCardInfo(string[] underCardData, bool isSpecial)
    {
        string underCardNumber = underCardData[0];
        if (!IsOldListContainsUnderCard(underCardNumber))
        {
            AddUnderCardToList(underCardData, isSpecial);
        }
        else
        {
            UpdateUnderCardInBlock(underCardData, isSpecial);
        }
    }

    private bool IsOldListContainsUnderCard(string cardNumber)
    {
        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            if (createdUnderCardElements[i].GetCardNumber() == cardNumber)
                return true;
        }
        return false;
    }

    private void UpdateUnderCardInBlock(string[] underCardData, bool isSpecial)
    {
        string cardNumber = underCardData[0];
        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            if (createdUnderCardElements[i].GetCardNumber() == cardNumber)
            {
                createdUnderCardElements[i].UpdateCardInfo(cardNumber,underCardData[1], underCardData[2], underCardData[3], underCardData[4],isSpecial);
                return;
            }
        }
    }
    #endregion

    #region Remove/Busy Commands
    public void RemoveUnderCard(string cardNumber)
    {
        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            if (createdUnderCardElements[i].GetCardNumber() == cardNumber)
            {
                Destroy(createdUnderCardElements[i].gameObject);
                createdUnderCardElements.RemoveAt(i--);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("RemoveUnderCard can't find card {0} 'createdUnderCardElements'",
            cardNumber));
    }

    public void MakeBusyUnderCard(string cardNumber)
    {
        for (int i = 0; i < createdUnderCardElements.Count; ++i)
        {
            if (createdUnderCardElements[i].GetCardNumber() == cardNumber)
            {
                createdUnderCardElements[i].MakeBusy();
                return;
            }
        }
        CustomLogger.LogErrorMessage(string.Format("MakeBusyUnderCard can't find card {0} 'createdUnderCardElements'",
            cardNumber));
    }
    #endregion

    #region OpenClose Panel
    public void OpenCloseUnderCardPanel()
    {
        if (isOpen)
            CloseUnderPanel();
        else
            OpenUnderPanel();

        isOpen = !isOpen;
        AudioManager.PlayButtonSound();
    }

    private void OpenUnderPanel()
    {
        arrowRectTransform.rotation =
            Quaternion.Euler(arrowRectTransform.rotation.eulerAngles.x,
            arrowRectTransform.rotation.eulerAngles.y,
            openedAngle);

        cardsPanel.SetActive(true);

        if (!isSpecial)
            addNewCardButtonObj.SetActive(true);

        RebuildEvent();
        if (isSpecial)
            AdminBlockListScreen.SlideDownAdminUnderPanel(2000f);
        else
            AdminBlockListScreen.SlideDownAppraiserUnderPanel(2000f);
    }

    private void CloseUnderPanel()
    {
        arrowRectTransform.rotation =
            Quaternion.Euler(arrowRectTransform.rotation.eulerAngles.x,
            arrowRectTransform.rotation.eulerAngles.y,
            closedAngle);

        cardsPanel.SetActive(false);
        if (!isSpecial)
            addNewCardButtonObj.SetActive(false);

        RebuildEvent();
    }
    #endregion

    #region On Click
    public void CreateCardInBlock()
    {
        CardCreationScreen.ShowCardsCreationScreen(displayNameText.text);
        AudioManager.PlayButtonSound();
    }
    #endregion

    #region Rebuild Event
    public void RebuildEvent()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
        if (gameObject.activeInHierarchy)
            StartCoroutine(LateRebuild());
    }
    private IEnumerator LateRebuild()
    {
        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(parentRectTransform);
    }
    #endregion

    #region Getters
    public string GetAppraiserBlockName()
    {
        return appraiserBlockName;
    }
    public string GetAppraiserName()
    {
        return appraiserName;
    }
    public string GetDisplayName()
    {
        return displayNameText.text;
    }

    public int GetUnderCardElementCount()
    {
        return createdUnderCardElements.Count;
    }
    #endregion

    #region Problem Card
    public void AddProblemCard(string cardNumber)
    {
        problemUnderCardNumbers.Add(cardNumber);
    }
    #endregion
}
