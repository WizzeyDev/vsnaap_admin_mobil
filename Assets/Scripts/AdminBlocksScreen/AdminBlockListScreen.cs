﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum BlockListType { Appraiser, Garage, Admin };
public class AdminBlockListScreen : MonoBehaviour
{
    #region Singleton
    private static AdminBlockListScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("AppraiserBlockListScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject blocksListScreenObj;
    [SerializeField]
    private GameObject filterWindowObj;

    [Space(10)]
    [SerializeField]
    private Transform appraiserBlocksListParent;
    [SerializeField]
    private Transform adminBlocksListParent;
    [SerializeField]
    private Transform garageBlocksListParent;
    [SerializeField]
    private GameObject appraiserBlockElementPrefab;
    [SerializeField]
    private GameObject garageBlockElementPrefab;

    [Space(10)]
    [SerializeField]
    private ScrollRect appraiserScrollRect;
    [SerializeField]
    private ScrollRect garageScrollRect;
    [SerializeField]
    private ScrollRect adminScrollRect;

    [Space(10)]
    [SerializeField]
    private ChooseBar displayNameFilter;
    [SerializeField]
    private ChooseBar isActiveFilter;
    [SerializeField]
    private ChooseBar areaFilter;
    [SerializeField]
    private ChooseBar roleFilter;

    [Space(10)]
    [SerializeField]
    private Button[] blocksListButtons;
    [SerializeField]
    private TextMeshProUGUI[] blocksListButtonsText;
    [SerializeField]
    private GameObject appraiserBlocksListHeader;
    [SerializeField]
    private GameObject garageBlocksListHeader;
    [SerializeField]
    private GameObject adminBlocksListHeader;
    #endregion

    #region Private Fields
    private List<AppraiserBlockElement> createdAppraiserBlockElements;
    private List<GarageBlockElement> createdGarageBlockElements;

    private List<string> problemAppraiserBlockNames;
    private List<string> problemGarageBlockNames;

    private readonly string[] specialAppraisers = new string[]
    {
        "Admin"
    };
    #endregion

    #region Show/Hide Block List
    public static void ShowBlocksListScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowBlockListScreen();
    }
    public void ShowBlockListScreen()
    {
        blocksListScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(blocksListScreenObj.transform);

        ChangeBlocksListType(0);

        ReturnBlockElementsToParent();

        HideFilterWindow();
        CardChangerScreen.HideCardsChangerScreen();
    }
    public void HideBlockListScreen()
    {
        blocksListScreenObj.SetActive(false);
    }
    private void ReturnBlockElementsToParent()
    {
        if (createdAppraiserBlockElements != null)
        {
            for (int i = 0; i < createdAppraiserBlockElements.Count; ++i)
            {
                if (!createdAppraiserBlockElements[i].isSpecial)
                    createdAppraiserBlockElements[i].transform.SetParent(appraiserBlocksListParent);
                else
                    createdAppraiserBlockElements[i].transform.SetParent(adminBlocksListParent);
            }
        }
        if (createdGarageBlockElements != null)
        {
            for (int i = 0; i < createdGarageBlockElements.Count; ++i)
            {
                createdGarageBlockElements[i].transform.SetParent(garageBlocksListParent);
            }
        }
    }
    #endregion

    #region On Button Click
    public void ChangeBlocksListType(int blockListType)
    {
        for (int i = 0; i < blocksListButtons.Length; i++)
        {
            blocksListButtons[i].interactable = true;
            blocksListButtons[i].image.color = GlobalParameters.enabledButtonColor;
            blocksListButtonsText[i].color = GlobalParameters.enabledTextColor;
        }

        appraiserBlocksListHeader.SetActive(false);
        garageBlocksListHeader.SetActive(false);
        adminBlocksListHeader.SetActive(false);

        appraiserScrollRect.gameObject.SetActive(false);
        garageScrollRect.gameObject.SetActive(false);
        adminScrollRect.gameObject.SetActive(false);

        blocksListButtons[blockListType].interactable = false;
        blocksListButtons[blockListType].image.color = GlobalParameters.disabledButtonColor;
        blocksListButtonsText[blockListType].color = GlobalParameters.disabledTextColor;
        switch((BlockListType)blockListType)
        {
            case BlockListType.Appraiser:
                {
                    appraiserBlocksListHeader.SetActive(true);
                    appraiserScrollRect.gameObject.SetActive(true);
                }
                break;

            case BlockListType.Garage:
                {
                    garageBlocksListHeader.SetActive(true);
                    garageScrollRect.gameObject.SetActive(true);
                }
                break;

            case BlockListType.Admin:
                {
                    adminBlocksListHeader.SetActive(true);
                    adminScrollRect.gameObject.SetActive(true);
                }
                break;
        }
    }
    #endregion

    #region Appraiser Block List 
    public static void SetupAppraiserBlockList(string rawAppraiserBlocksData)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserBlockElements != null)
        {
            CustomLogger.LogErrorMessage("AppraiserBlockListScreen 'createdAppraiserBlockElements' list is already exist!");
            return;
        }

        instance.createdAppraiserBlockElements = new List<AppraiserBlockElement>();
        instance.problemAppraiserBlockNames = new List<string>();

        CreateAppraiserBlockList(rawAppraiserBlocksData.Split('~'));
    }

    private static void CreateAppraiserBlockList(string[] allBlocksData)
    {
        for (int i = 0; i < allBlocksData.Length; ++i)
        {
            if (string.IsNullOrEmpty(allBlocksData[i]))
                continue;

            string[] appraiserBlockData = allBlocksData[i].Split('|');
            AddBlockToAppraiserBlockList(appraiserBlockData);
        }
        instance.SortAppraiserBlockList();
    }
    private void SortAppraiserBlockList()
    {
        for (int i = 0; i < createdAppraiserBlockElements.Count; i++)
        {
            if (createdAppraiserBlockElements[i].GetDisplayName() == "זקצר")
            {
                createdAppraiserBlockElements[i].transform.SetSiblingIndex(0);
                break;
            }
        }
        for (int i = 0; i < createdAppraiserBlockElements.Count; i++)
        {
            if (createdAppraiserBlockElements[i].GetDisplayName() == "nimdA")
            {
                createdAppraiserBlockElements[i].transform.SetSiblingIndex(1);
                break;
            }
        }
    }

    public static void AddBlockToAppraiserBlockList(string[] appraiserBlockData)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserBlockElements == null)
        {
            Debug.LogError("AddBlockToList 'createdBlockElements' list is null!");
            return;
        }

        AppraiserBlockElement blockElement = 
            Instantiate(instance.appraiserBlockElementPrefab, instance.appraiserBlocksListParent).
            GetComponent<AppraiserBlockElement>();
        if (blockElement == null)
        {
            Debug.LogError(blockElement.gameObject.name + " has no attached 'AppraiserBlockElement' !");
            return;
        }

        bool isSpecial = instance.specialAppraisers.Contains(appraiserBlockData[0].Split('\\')[0]);
        if (isSpecial)
            blockElement.transform.SetParent(instance.adminBlocksListParent);

        blockElement.Setup(appraiserBlockData, isSpecial);
        instance.createdAppraiserBlockElements.Add(blockElement);
    }

    public static void UpdateAppraiserBlockList(string[] allBlocksData)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserBlockElements == null)
        {
            CustomLogger.LogMessage("CheckUpdateCardsList 'createdAppraiserBlockElements' is null!");
            instance.createdAppraiserBlockElements = new List<AppraiserBlockElement>();
            instance.problemAppraiserBlockNames = new List<string>();
        }

        List<string> allAppraiserBlockNames = new List<string>();
        for (int i = 0; i < allBlocksData.Length; ++i)
        {
            if (string.IsNullOrEmpty(allBlocksData[i]))
                continue;

            string[] appraiserBlockData = allBlocksData[i].Split('|');
            string appraiserBlockName = appraiserBlockData[0];
            if (!instance.IsOldListContainsAppraiserBlock(appraiserBlockName) &&
                !instance.problemAppraiserBlockNames.Contains(appraiserBlockName))
            {
                AddBlockToAppraiserBlockList(appraiserBlockData);
            }
            else
            {
                UpdateAppraiserBlockInList(appraiserBlockData);
            }

            allAppraiserBlockNames.Add(appraiserBlockName);
        }

        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            string appraiserName =
                instance.createdAppraiserBlockElements[i].GetAppraiserBlockName();
            if (!allAppraiserBlockNames.Contains(appraiserName))
            {
                Destroy(instance.createdAppraiserBlockElements[i].gameObject);
                instance.createdAppraiserBlockElements.RemoveAt(i--);
            }
        }
    }

    private bool IsOldListContainsAppraiserBlock(string appraiserBlockName)
    {
        for (int i = 0; i < createdAppraiserBlockElements.Count; ++i)
        {
            string currentAppraiserBlockName = instance.createdAppraiserBlockElements[i].GetAppraiserBlockName();
            if (currentAppraiserBlockName == appraiserBlockName)
                return true;
        }
        return false;
    }

    private static void UpdateAppraiserBlockInList(string[] blockData)
    {
        string appraiserBlockName = blockData[0];
        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            string currentAppraiserBlockName = instance.createdAppraiserBlockElements[i].GetAppraiserBlockName();
            if (currentAppraiserBlockName == appraiserBlockName)
            {
                instance.createdAppraiserBlockElements[i].UpdateBlockInfo(blockData,
                    instance.specialAppraisers.Contains(instance.createdAppraiserBlockElements[i].GetAppraiserName()));
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("UpdateBlockInList can't AppraiserBlock {0} in 'createdAppraiserBlockElements'",
            appraiserBlockName));
    }

    public static void DestroyAllAppraiserBlockList()
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserBlockElements == null)
            return;

        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            Destroy(instance.createdAppraiserBlockElements[i].gameObject);
        }
        instance.createdAppraiserBlockElements.Clear();
        instance.createdAppraiserBlockElements = null;
    }
    #endregion

    #region Garage Block List
    public static void SetupGarageBlockList(string rawBlocksData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageBlockElements != null)
        {
            CustomLogger.LogErrorMessage("AppraiserBlockListScreen 'createdGarageBlockElements' list is already exist!");
            return;
        }

        instance.createdGarageBlockElements = new List<GarageBlockElement>();
        instance.problemGarageBlockNames = new List<string>();

        CreateGarageBlockList(rawBlocksData.Split('~'));
    }

    private static void CreateGarageBlockList(string[] allGarageBlocksData)
    {
        for (int i = 0; i < allGarageBlocksData.Length; ++i)
        {
            if (string.IsNullOrEmpty(allGarageBlocksData[i]))
                continue;

            string[] garageBlockData = allGarageBlocksData[i].Split('|');
            AddGarageBlockToList(garageBlockData);
        }
    }

    public static void AddGarageBlockToList(string[] garageBlockData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageBlockElements == null)
        {
            Debug.LogError("AddBlockToList 'createdBlockElements' list is null!");
            return;
        }

        GarageBlockElement garageBlockElement = Instantiate(instance.garageBlockElementPrefab, instance.garageBlocksListParent).
            GetComponent<GarageBlockElement>();
        if (garageBlockElement == null)
        {
            Debug.LogError(garageBlockElement.gameObject.name + " has no attached 'GarageBlockElement' !");
            return;
        }

        garageBlockElement.Setup(garageBlockData);
        instance.createdGarageBlockElements.Add(garageBlockElement);
    }

    public static void UpdateGarageBlockList(string[] allBlocksData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageBlockElements == null)
        {
            CustomLogger.LogMessage("CheckUpdateCardsList 'createdGarageBlockElements' is null!");
            instance.createdGarageBlockElements = new List<GarageBlockElement>();
            instance.problemGarageBlockNames = new List<string>();
        }

        List<string> allGarageBlockNames = new List<string>();
        for (int i = 0; i < allBlocksData.Length; ++i)
        {
            if (string.IsNullOrEmpty(allBlocksData[i]))
                continue;

            string[] garageBlockData = allBlocksData[i].Split('|');
            string garageBlockName = garageBlockData[0];
            if (!instance.IsOldGarageListContainsBlock(garageBlockName) &&
                !instance.problemGarageBlockNames.Contains(garageBlockName))
            {
                AddGarageBlockToList(garageBlockData);
            }
            else
            {
                UpdateGarageBlockInList(garageBlockData);
            }

            allGarageBlockNames.Add(garageBlockName);
        }

        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            string appraiserName =
                instance.createdGarageBlockElements[i].GetGarageBlockName();
            if (!allGarageBlockNames.Contains(appraiserName))
            {
                Destroy(instance.createdGarageBlockElements[i].gameObject);
                instance.createdGarageBlockElements.RemoveAt(i--);
            }
        }
    }

    private bool IsOldGarageListContainsBlock(string garageBlockName)
    {
        for (int i = 0; i < createdGarageBlockElements.Count; ++i)
        {
            string currentGarageBlockName = instance.createdGarageBlockElements[i].GetGarageBlockName();
            if (currentGarageBlockName == garageBlockName)
                return true;
        }
        return false;
    }

    private static void UpdateGarageBlockInList(string[] garageBlockData)
    {
        string appraiserBlockName = garageBlockData[0];
        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            string currentAppraiserBlockName = instance.createdGarageBlockElements[i].GetGarageBlockName();
            if (currentAppraiserBlockName == appraiserBlockName)
            {
                instance.createdGarageBlockElements[i].UpdateBlockInfo(garageBlockData);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("UpdateBlockInList can't AppraiserBlock {0} in 'createdGarageBlockElements'",
            appraiserBlockName));
    }

    public static void DestroyAllGarageBlockList()
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageBlockElements == null)
            return;

        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            Destroy(instance.createdGarageBlockElements[i].gameObject);
        }
        instance.createdGarageBlockElements.Clear();
        instance.createdGarageBlockElements = null;
    }
    #endregion

    #region Manual Under Card Commands
    public static void RemoveAppraiserUnderCardElement(string appraiserBlockName, string cardNumber)
    {
        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            if (instance.createdAppraiserBlockElements[i].GetAppraiserBlockName() == appraiserBlockName)
            {
                instance.createdAppraiserBlockElements[i].RemoveUnderCard(cardNumber);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("RemoveUnderCardElement can't find card {0} in AppraiserBlock {1}",
            cardNumber, appraiserBlockName));
    }
    public static void RemoveGarageUnderCardElement(string garageBlockName, string cardNumber)
    {
        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            if (instance.createdGarageBlockElements[i].GetGarageBlockName() == garageBlockName)
            {
                instance.createdGarageBlockElements[i].RemoveUnderCard(cardNumber);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("RemoveUnderCardElement can't find card {0} in GarageBlock {1}",
            cardNumber, garageBlockName));
    }

    public static void MakeBusyAppraiserUnderCardElement(string appraiserBlockName, string cardNumber)
    {
        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            if (instance.createdAppraiserBlockElements[i].GetAppraiserBlockName() == appraiserBlockName)
            {
                instance.createdAppraiserBlockElements[i].MakeBusyUnderCard(cardNumber);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("MakeBusyUnderCardElement can't find card {0} in AppraiserBlock {1}",
            cardNumber, appraiserBlockName));
    }
    public static void MakeBusyGarageUnderCardElement(string garageBlockName, string cardNumber)
    {
        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            if (instance.createdGarageBlockElements[i].GetGarageBlockName() == garageBlockName)
            {
                instance.createdGarageBlockElements[i].MakeBusyUnderCard(cardNumber);
                return;
            }
        }

        CustomLogger.LogErrorMessage(string.Format("MakeBusyUnderCardElement can't find card {0} in GarageBlock {1}",
            cardNumber, garageBlockName));
    }
    #endregion

    #region Open/Close Under Panel
    public static void SlideDownAppraiserUnderPanel(float y)
    {
        if (isNullInstance)
            return;

        instance.appraiserScrollRect.velocity = new Vector2(0f, y);
    }
    public static void SlideDownGarageUnderPanel(float y)
    {
        if (isNullInstance)
            return;

        instance.garageScrollRect.velocity = new Vector2(0f, y);
    }
    public static void SlideDownAdminUnderPanel(float y)
    {
        if (isNullInstance)
            return;

        instance.adminScrollRect.velocity = new Vector2(0f, y);
    }
    #endregion

    #region Update Block/Cards
    public static void UpdateAppraiserBlockUnderCard(string appraiserBlockName, string[] underCardData)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserBlockElements == null)
        {
            CustomLogger.LogErrorMessage("UpdateAppraiserBlockUnderCard 'createdAppraiserBlockElements' list is null");
            return;
        }

        string cardNumber = underCardData[0];
        for (int i = 0; i < instance.createdAppraiserBlockElements.Count; ++i)
        {
            string currentAppraiserBlockName = instance.createdAppraiserBlockElements[i].GetAppraiserBlockName();
            if (currentAppraiserBlockName == appraiserBlockName)
            {
                instance.createdAppraiserBlockElements[i].UpdateUnderCardInfo(underCardData,
                    instance.specialAppraisers.Contains(instance.createdAppraiserBlockElements[i].GetAppraiserName()));
                return;
            }
        }

        CustomLogger.LogMessage(string.Format("UpdateBlockUnderCard can't find card {0} in AppraiserBlock {1}",
            cardNumber, appraiserBlockName));
    }
    public static void UpdateGarageBlockUnderCard(string garageBlockName, string[] underCardData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageBlockElements == null)
        {
            CustomLogger.LogErrorMessage("UpdateGarageBlockUnderCard 'createdGarageBlockElements' list is null");
            return;
        }

        string cardNumber = underCardData[0];
        for (int i = 0; i < instance.createdGarageBlockElements.Count; ++i)
        {
            string currentAppraiserBlockName = instance.createdGarageBlockElements[i].GetGarageBlockName();
            if (currentAppraiserBlockName == garageBlockName)
            {
                instance.createdGarageBlockElements[i].UpdateUnderCardInfo(underCardData);
                return;
            }
        }

        CustomLogger.LogMessage(string.Format("UpdateGarageBlockUnderCard can't find card {0} in GarageBlock {1}",
            cardNumber, garageBlockName));
    }
    #endregion

    #region Problem Card
    public static void AddProblemAppraiserBlock(string cardNumber)
    {
        if (isNullInstance)
            return;

        instance.problemAppraiserBlockNames.Add(cardNumber);
    }
    public static void AddProblemGarageBlock(string cardNumber)
    {
        if (isNullInstance)
            return;

        instance.problemGarageBlockNames.Add(cardNumber);
    }
    #endregion

    #region Filter Blocks
    public void ShowFilterWindow()
    {
        filterWindowObj.SetActive(true);

        displayNameFilter.currentElementName.text = string.Empty;
        isActiveFilter.currentElementName.text = string.Empty;
        areaFilter.currentElementName.text = string.Empty;
        roleFilter.currentElementName.text = string.Empty;
    }
    public void HideFilterWindow()
    {
        filterWindowObj.SetActive(false);
    }

    #region Add Filters
    public static void AddDisplayNameFilterElement(string newDisplayName)
    {
        if (isNullInstance)
            return;

        if (!instance.displayNameFilter.elementNames.Contains(newDisplayName))
        {
            instance.displayNameFilter.elementNames.Add(newDisplayName);
        }
    }

    public static void AddAreaFilterElement(string newArea)
    {
        if (isNullInstance)
            return;

        if (!instance.areaFilter.elementNames.Contains(newArea))
        {
            instance.areaFilter.elementNames.Add(newArea);
        }
    }

    public static void AddRoleFilterElement(string newRole)
    {
        if (isNullInstance)
            return;

        if (!instance.roleFilter.elementNames.Contains(newRole))
        {
            instance.roleFilter.elementNames.Add(newRole);
        }
    }
    #endregion

    public void ConfirmAppraiserFilters()
    {
        if (string.IsNullOrEmpty(displayNameFilter.currentElementName.text) &&
            string.IsNullOrEmpty(isActiveFilter.currentElementName.text) &&
            string.IsNullOrEmpty(areaFilter.currentElementName.text) &&
            string.IsNullOrEmpty(roleFilter.currentElementName.text))
        {

            StartCoroutine(HighlighInputField(displayNameFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(isActiveFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(areaFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(roleFilter.currentElementName.image));
            return;
        }

        string displayName = displayNameFilter.currentElementName.text;
        string isActive = isActiveFilter.currentElementName.text;
        string area = areaFilter.currentElementName.text;
        string role = roleFilter.currentElementName.text;

        List<AppraiserElement> allAppraisers = AppraiserListScreen.GetAllAppraisers();
        for (int i = 0; i < allAppraisers.Count; ++i)
        {
            if (!string.IsNullOrEmpty(displayName))
            {
                if (allAppraisers[i].GetDisplayName() != displayName)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(isActive))
            {
                if (allAppraisers[i].GetIsActive() != isActive)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(area))
            {
                if (allAppraisers[i].GetArea() != area)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(role))
            {
                if (allAppraisers[i].GetRole() != role)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }
        }

        for (int i = 0; i < createdAppraiserBlockElements.Count; ++i)
        {
            createdAppraiserBlockElements[i].transform.parent = null;
        }

        if (allAppraisers.Count > 0)
        {
            for (int i = 0; i < createdAppraiserBlockElements.Count; ++i)
            {
                string appraiserName = createdAppraiserBlockElements[i].GetAppraiserName();
                for (int j = 0; j < allAppraisers.Count; ++j)
                {
                    if (allAppraisers[j].GetAppraiserName() == appraiserName)
                    {
                        createdAppraiserBlockElements[i].transform.SetParent(appraiserBlocksListParent);
                        break;
                    }
                }
            }
        }

        HideFilterWindow();
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
