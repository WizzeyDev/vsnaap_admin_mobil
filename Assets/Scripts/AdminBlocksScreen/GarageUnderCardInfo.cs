﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GarageUnderCardInfo : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    private GameObject underCardButtonsPanel;
    [SerializeField]
    private GameObject confirmButtonObj;

    [Space(10)]
    [SerializeField]
    private TextMeshProUGUI carNumberText;
    [SerializeField]
    private TextMeshProUGUI cardNumberText;
    [SerializeField]
    private TextMeshProUGUI statusNameText;

    [Space(10)]
    [SerializeField]
    private GameObject changeGarageButtonObj;
    [SerializeField]
    private GameObject openPanelButtonObj;
    [SerializeField]
    private GameObject redStatusButtonObj;
    [SerializeField]
    private GameObject greenStatusButtonObj;
    [SerializeField]
    private GameObject skipButtonObj;

    [Space(10)]
    [SerializeField]
    private Image[] controlButtonImages;
    [SerializeField]
    private TextMeshProUGUI[] controlButtonTexts;
    #endregion

    #region Properties Fields
    private bool _isBusy;
    private bool isBusy
    {
        set 
        {
            if (value == true)
                PlayerIOServerManager.SendManualBusyGarageUnderCard(blockElement.GetGarageBlockName(),
                    cardNumberText.text);

            _isBusy = value;
        }
        get {return _isBusy;}
    }
    #endregion

    #region Private Fields
    private RectTransform parentBlockElementRectTransform;
    private GarageBlockElement blockElement;

    private int statusIndex = -1;
    private int controlIndex = -1;

    private bool isOpen = false;
    private bool isConfirmed;
    #endregion

    #region Setup
    private void Start()
    {
        parentBlockElementRectTransform = GetParentBlockElement(transform);
        if (parentBlockElementRectTransform == null)
        {
            CustomLogger.LogErrorMessage("UnserCardInfo element can't find 'parentBlockElementRectTransform'");
        }
        blockElement = parentBlockElementRectTransform.GetComponent<GarageBlockElement>();
        blockElement.RebuildEvent();
        transform.SetSiblingIndex(0);

        //if (blockElement.GetAppraiserName() == "dVA")
        //{
        //    foreach (Image img in confirmButtons)
        //    {
        //        img.gameObject.SetActive(false);
        //    }
        //}
    }
    private RectTransform GetParentBlockElement(Transform targetTransform)
    {
        RectTransform parentBlockElementObj = null;
        if (targetTransform.GetComponent<GarageBlockElement>() != null)
            parentBlockElementObj = targetTransform.GetComponent<RectTransform>();
        else
        {
            if (targetTransform.parent != null)
                parentBlockElementObj = GetParentBlockElement(targetTransform.parent);
        }
        return parentBlockElementObj;
    }

    public void Setup(string cardNumber, string carNumber, string status, string confirmed)
    {
        cardNumberText.text = cardNumber;
        carNumberText.text = carNumber;

        int cardStatusIndex;
        if (!int.TryParse(status, out cardStatusIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:Setup can't parse 'status' in Card: " +
                cardNumber);
            return;
        }
        ChangeStatus(cardStatusIndex);

        if (cardStatusIndex == 4)
            return;

        bool isConfirmed;
        if (bool.TryParse(confirmed, out isConfirmed))
            this.isConfirmed = isConfirmed;

        if (!this.isConfirmed)
            skipButtonObj.SetActive(false);
        else
        {
            if (cardStatusIndex == -1)
                skipButtonObj.SetActive(true);
            else
                skipButtonObj.SetActive(false);
        }
    }
    #endregion

    #region OnClick
    public void ConfirmChanges()
    {
        if (_isBusy)
            return;

        AudioManager.PlayButtonSound();
        if (!isConfirmed)
        {
            if (controlIndex == 1)
            {
                AdminBlockListScreen.RemoveGarageUnderCardElement(blockElement.GetGarageBlockName(),
                    cardNumberText.text);
                PlayerIOServerManager.SendManualRemoveGarageUnderCard(blockElement.GetGarageBlockName(),
                    cardNumberText.text);

                string closeDate = DateStringConverter.GetDMYHMDate();
                PlayerIOServerManager.SendCloseCard(cardNumberText.text, closeDate);

            }
            return;
        }

        if (controlIndex != -1)
        {
            isBusy = true;
            string closeDate = DateStringConverter.GetDMYHMDate();
            if (controlIndex == 0)
            {
                PlayerIOServerManager.SendSkipFirstCardChecking(cardNumberText.text, closeDate);
                CloseUnderPanel();
            }
            else
            {
                AdminBlockListScreen.RemoveGarageUnderCardElement(blockElement.GetGarageBlockName(),
                    cardNumberText.text);
                PlayerIOServerManager.SendManualRemoveGarageUnderCard(blockElement.GetGarageBlockName(),
                    cardNumberText.text);
                PlayerIOServerManager.SendCloseCard(cardNumberText.text, closeDate);
            }
        }
    }

    public void ChangeGarage()
    {
        GaragesListScreen.ShowGarageListScreen(NewGarageChanged);
        AudioManager.PlayButtonSound();
    }

    private void NewGarageChanged(string newGarageName)
    {
        PlayerIOServerManager.SendChangeCardGarage(cardNumberText.text, blockElement.GetGarageName(),
            newGarageName);
        AdminBlockListScreen.RemoveGarageUnderCardElement(blockElement.GetGarageName(),
            cardNumberText.text);
        PlayerIOServerManager.SendManualRemoveGarageUnderCard(blockElement.GetGarageBlockName(),
            cardNumberText.text);
    }

    public void ChangeCardData()
    {
        isBusy = true;
        PlayerIOServerManager.GetFullCardData(cardNumberText.text);
        AudioManager.PlayButtonSound();
    }
    #endregion

    #region Change Status
    private void ChangeStatus(int statusIndex)
    {
        if (this.statusIndex == statusIndex || statusIndex == -1)
            return;

        if (this.statusIndex != statusIndex)
        {
            CloseUnderPanel();
            if (statusIndex == 0)
            {
                controlIndex = -1;
            }
        }

        this.statusIndex = statusIndex;
        statusNameText.text = CardStatusManager.GetCardStatusNameByIndex(statusIndex);

        if (statusIndex == 4)
            VisualChangeStatus(true);
    }

    private void VisualChangeStatus(bool isClose)
    {
        if (isClose)
        {
            redStatusButtonObj.SetActive(true);

            greenStatusButtonObj.SetActive(false);
            changeGarageButtonObj.SetActive(false);
            openPanelButtonObj.SetActive(false);
            CloseUnderPanel();
        }
        else
        {
            greenStatusButtonObj.SetActive(true);
            redStatusButtonObj.SetActive(false);
        }
        confirmButtonObj.SetActive(false);
    }
    #endregion

    #region Change Control
    public void ChangeControl(int controlIndex)
    {
        if (controlIndex == -1)
            return;

        if (this.controlIndex == controlIndex)
            this.controlIndex = -1;
        else
            this.controlIndex = controlIndex;
        SetControlButton(this.controlIndex);
        AudioManager.PlayButtonSound();
    }

    private void SetControlButton(int controlIndex)
    {
        for (int i = 0; i < controlButtonImages.Length; ++i)
        {
            controlButtonImages[i].color = GlobalParameters.disabledButtonColor;
            controlButtonTexts[i].color = GlobalParameters.disabledTextColor;
        }
        this.controlIndex = controlIndex;
        if (controlIndex == -1)
            return;
        controlButtonImages[controlIndex].color = GlobalParameters.enabledButtonColor;
        controlButtonTexts[controlIndex].color = GlobalParameters.enabledTextColor;
    }
    #endregion

    #region OpenClose Part
    public void OpenCloseUnderCardPanel()
    {
        if (isOpen)
            CloseUnderPanel();
        else
            OpenUnderPanel();

        isOpen = !isOpen;
        AudioManager.PlayButtonSound();
    }

    private void OpenUnderPanel()
    {
        if (!isOpen && _isBusy)
            return;

        openPanelButtonObj.SetActive(false);

        underCardButtonsPanel.SetActive(true);
        changeGarageButtonObj.SetActive(true);
        if (!isConfirmed)
        {
            //if (blockElement.GetAppraiserName() != "dVA")
                confirmButtonObj.SetActive(true);
        }
        else
        {
            VisualChangeStatus(false);
        }

        blockElement.RebuildEvent();
        AdminBlockListScreen.SlideDownGarageUnderPanel(1000f);
    }

    private void CloseUnderPanel()
    {
        openPanelButtonObj.SetActive(true);

        changeGarageButtonObj.SetActive(false);
        underCardButtonsPanel.SetActive(false);
        confirmButtonObj.SetActive(false);
        greenStatusButtonObj.SetActive(false);

        blockElement?.RebuildEvent();
    }
    #endregion

    #region Make Busy Under Card
    public void MakeBusy()
    {
        _isBusy = true;
    }
    #endregion

    #region Update Card Info
    public void UpdateCardInfo(string cardNumber, string carNumber, string status, string confirmed)
    {
        cardNumberText.text = cardNumber;
        carNumberText.text = carNumber;

        int cardStatusIndex;
        if (!int.TryParse(status, out cardStatusIndex))
        {
            CustomLogger.LogMessage("UnderCardInfo:UpdateCardInfo can't parse 'status' in Card: " +
                cardNumber);
            return;
        }
        ChangeStatus(cardStatusIndex);

        if (cardStatusIndex == 4)
            return;

        bool isConfirmed;
        if (bool.TryParse(confirmed, out isConfirmed))
        {
            if (this.isConfirmed != isConfirmed)
                CloseUnderPanel();
            this.isConfirmed = isConfirmed;
        }

        if (!this.isConfirmed)
        {
            skipButtonObj.SetActive(false);
        }
        else
        {
            if (cardStatusIndex == -1)
                skipButtonObj.SetActive(true);
            else
                skipButtonObj.SetActive(false);
        }
        
        _isBusy = false;
    }
    #endregion

    #region Getters
    public string GetCardNumber()
    {
        return cardNumberText.text;
    }
    #endregion

    private void OnDestroy()
    {
        blockElement.RebuildEvent();
    }
}
