﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFileBrowser;

public class FileBrowserScreen : MonoBehaviour
{
    #region Singleton
    private static FileBrowserScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("FileBrowserScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    [SerializeField]
    private GameObject screenObj;

    public static void SelectFile(FileBrowser.OnSuccess OnSuccess)
    {
        if (isNullInstance)
            return;

        instance.screenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.screenObj.transform);
        FileBrowser.ShowLoadDialog((filePath) =>
        {
            instance.screenObj.SetActive(false);
            OnSuccess(filePath);
        }, 
        instance.OnCancel);
    }

    private void OnCancel()
    {
        Debug.Log("FileBrowser was canceled...");
        instance.screenObj.SetActive(false);
    }
}
