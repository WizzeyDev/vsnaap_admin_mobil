﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardStatusManager : MonoBehaviour
{
    #region Singleton
    private static CardStatusManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("CardStatusManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Private Fields
    private static int statusIndex;

    private string[] statusNames = new string[]
    {
        "ראשונית", "חוזרת", "מסמכים", "משלימה", "בוטל"
    };
    #endregion


    public static void ChangeCurrentStatusIndex(int newStatusIndex)
    {
        if (isNullInstance)
            return;

        statusIndex = newStatusIndex;
    }

    public static string GetCardStatusNameByIndex(int statusIndex)
    {
        if (isNullInstance)
            return string.Empty;

        return instance.statusNames[statusIndex];
    }

    public static int GetCurrentStatusIndex()
    {
        if (isNullInstance)
            return -1;

        return statusIndex;
    }
}
