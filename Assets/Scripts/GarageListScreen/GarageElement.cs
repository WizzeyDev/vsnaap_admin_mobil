﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GarageElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    protected TextMeshProUGUI displayNameText;
    [SerializeField]
    protected TextMeshProUGUI IDText;
    [SerializeField]
    protected TextMeshProUGUI officePhoneText;
    [SerializeField]
    protected TextMeshProUGUI mobilePhoneText;
    [SerializeField]
    protected TextMeshProUGUI faxText;
    [SerializeField]
    protected TextMeshProUGUI emailText;
    [SerializeField]
    protected InputField adressText;
    [SerializeField]
    protected TextMeshProUGUI areaText;
    [SerializeField]
    protected TextMeshProUGUI checkTypeText;
    [SerializeField]
    protected TextMeshProUGUI isActiveText;
    [SerializeField]
    protected TextMeshProUGUI insurerNameText;

    [Space(10)]
    [SerializeField]
    private Button selectButton;
    [SerializeField]
    private Image selectButtonImg;
    #endregion

    #region Private Fields
    protected string garageName;
    protected string password;

    private Color selectedColor = new Color32(232, 88, 88, 255);
    private Color unselectedColor = Color.white;

    private bool isSelected;
    #endregion

    public void Setup(string garageName, string password, string displayName, string ID, string officePhone, string mobilePhone, string fax,
        string email, string adress, string area, string checkType, string isActive, string insurerName)
    {
        this.garageName = garageName;
        this.password = password;
        displayNameText.text = displayName;
        IDText.text = ID;
        officePhoneText.text = officePhone;
        mobilePhoneText.text = mobilePhone;
        faxText.text = fax;
        emailText.text = email;
        adressText.text = adress;
        areaText.text = area;
        isActiveText.text = isActive;
        checkTypeText.text = checkType;
        insurerNameText.text = insurerName;


        selectButton.onClick.AddListener(() =>
        {
            GaragesListScreen.OnClickGarageElement(this);
            AudioManager.PlayButtonSound();
        });
    }

    #region Setters
    public void UpdateData(string password, string ID, string officePhone, string mobilePhone, string fax,
        string email, string adress, string area, string checkType, string isActive, string insurerName)
    {
        this.password = password;
        IDText.text = ID;
        officePhoneText.text = officePhone;
        mobilePhoneText.text = mobilePhone;
        faxText.text = fax;
        emailText.text = email;
        adressText.text = adress;
        areaText.text = area;
        isActiveText.text = isActive;
        checkTypeText.text = checkType;
        insurerNameText.text = insurerName;
    }
    #endregion

    #region On Click
    public void SelectCurrentElement()
    {
        if (!isSelected)
            selectButtonImg.color = selectedColor;
        else
            selectButtonImg.color = unselectedColor;

        isSelected = !isSelected;
        AudioManager.PlayButtonSound();
    }
    #endregion

    #region Getters
    public string GetGarageName()
    {
        return garageName;
    }

    public string GetPassword()
    {
        return password;
    }

    public string GetDisplayName()
    {
        return displayNameText.text;
    }

    public string GetID()
    {
        return IDText.text;
    }

    public string GetOfficePhone()
    {
        return officePhoneText.text;
    }

    public string GetMobilePhone()
    {
        return mobilePhoneText.text;
    }

    public string GetFax()
    {
        return faxText.text;
    }
    public string GetEmail()
    {
        return emailText.text;
    }
    public string GetAdress()
    {
        return adressText.text;
    }
    public string GetArea()
    {
        return areaText.text;
    }
    public string GetCheckType()
    {
        return checkTypeText.text;
    }
    public string GetIsActive()
    {
        return isActiveText.text;
    }
    public string GetInsurerName()
    {
        return insurerNameText.text;
    }

    public bool IsSelected()
    {
        return isSelected;
    }
    #endregion
}
