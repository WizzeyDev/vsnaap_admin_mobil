﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GaragesListScreen : MonoBehaviour
{
    #region Singleton
    private static GaragesListScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("GarageListScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            //Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject screenObj;
    [SerializeField]
    private Transform garageListParent;

    [Space(10)]
    [SerializeField]
    private GameObject garageElementPrefab;

    [Space(10)]
    [SerializeField]
    private GameObject sendDocumentButtonObj;

    [Space(10)]
    [SerializeField]
    private GameObject addDocumentSuccessScreen;
    #endregion

    #region Private Fields
    private System.Action<string> OnSelectedGarage;

    private List<GarageElement> selectedGarageElements = new List<GarageElement>();

    private List<GarageElement> createdGarageElements;
    #endregion

    #region Garages List Part

    #region Update Garage Names
    //public static void CheckUpdateGarageList(string rawGarageNames)
    //{
    //    if (isNullInstance)
    //        return;

    //    if(instance.createdGarageElements == null)
    //        instance.createdGarageElements = new List<GarageElement>();

    //    string[] allGarageNames = rawGarageNames.Split('_');

    //    string rawNewGarageNames = string.Empty;
    //    for (int i = 0; i < allGarageNames.Length; ++i)
    //    {
    //        if (string.IsNullOrEmpty(allGarageNames[i]))
    //            continue;

    //        if (!instance.IsOldListContainsGarage(allGarageNames[i]))
    //        {
    //            rawNewGarageNames += allGarageNames[i] + "_";
    //        }
    //    }

    //    if (rawNewGarageNames.EndsWith("_"))
    //        rawNewGarageNames = rawNewGarageNames.Remove(rawNewGarageNames.Length - 1, 1);

    //    if (!string.IsNullOrEmpty(rawNewGarageNames))
    //    {
    //        PlayerIOServerManager.SendGetGarageDatasMessage(rawNewGarageNames);
    //    }
    //    else
    //    {
    //        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
    //        {
    //            if (!allGarageNames.Contains(instance.createdGarageElements[i].GetGarageName()))
    //            {
    //                Destroy(instance.createdGarageElements[i].gameObject);
    //                instance.createdGarageElements.RemoveAt(i--);
    //            }
    //        }
    //    }
    //}

    //private bool IsOldListContainsGarage(string targetGarageName)
    //{
    //    for (int i = 0; i < createdGarageElements.Count; ++i)
    //    {
    //        if (createdGarageElements[i].GetGarageName() == targetGarageName)
    //            return true;
    //    }
    //    return false;
    //}
    #endregion

    public static void CreateGarageList(string[] allGaragesData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements != null)
        {
            CustomLogger.LogErrorMessage("GarageListScreen 'createdGarageElements' list is already exist!");
            return;
        }

        instance.createdGarageElements = new List<GarageElement>();

        for (int i = 0; i < allGaragesData.Length; ++i)
        {
            string rawGarageData = allGaragesData[i];
            if (string.IsNullOrEmpty(rawGarageData))
            {
                Debug.LogError("UpdateCardsList at " + i + " index has null cardData");
                continue;
            }

            string[] garageData = rawGarageData.Split('\\');
            string garageName = garageData[0];
            string password = garageData[1];
            string displayName = garageData[2];
            string ID = garageData[3];
            string officePhone = garageData[4];
            string mobilePhone = garageData[5];
            string fax = garageData[6];
            string email = garageData[7];
            string area = garageData[8];
            string adress = garageData[9];
            string insurerName = garageData[10];
            string isActive = garageData[11];
            string checkType = garageData[12];
            AddGarageToList(garageName, password, displayName, ID, officePhone, mobilePhone,
                fax, email, adress, area, checkType, isActive,
                insurerName);
        }
    }

    public static void AddGarageToList(string garageName, string password, string displayName, string ID, string officePhone, string mobilePhone, string fax,
        string email, string adress, string area, string checkType, string isActive, string insurerName)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
            instance.createdGarageElements = new List<GarageElement>();

        GarageElement garageElement = Instantiate(instance.garageElementPrefab, instance.garageListParent).GetComponent<GarageElement>();
        garageElement.Setup(garageName, password, displayName, ID, officePhone, mobilePhone, fax, email, adress, area,
             checkType, isActive, insurerName);

        instance.createdGarageElements.Add(garageElement);

        GaragesSearchScreen.AddDisplayNameFilterElement(displayName);
        GaragesSearchScreen.AddAreaFilterElement(area);
        GaragesSearchScreen.AddCityFilterElement(adress);
    }

    public static void UpdateGarage(string[] appraiserData)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
        {
            CustomLogger.LogErrorMessage("UpdateAppraiser 'createdGarageElements' list is null!");
            return;
        }

        string garageName = appraiserData[0];
        string password = appraiserData[1];
        string ID = appraiserData[2];
        string officePhone = appraiserData[3];
        string mobilePhone = appraiserData[4];
        string fax = appraiserData[5];
        string email = appraiserData[6];
        string area = appraiserData[7];
        string adress = appraiserData[8];
        string insurerName = appraiserData[9];
        string isActive = appraiserData[10];
        string checkType = appraiserData[11];
        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetGarageName() == garageName)
            {
                instance.createdGarageElements[i].UpdateData(password, ID, officePhone, mobilePhone, fax, email,
                    adress, area, checkType, isActive, insurerName);

                //GaragesSearchScreen.AddDisplayNameFilterElement(displayName);
                GaragesSearchScreen.AddAreaFilterElement(area);
                GaragesSearchScreen.AddCityFilterElement(adress);
                return;
            }
        }
    }

    public static void RemoveGarageFromList(string garageName)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
            return;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetGarageName() == garageName)
            {
                //Destroy(instance.createdGarageElements[i].gameObject);
                //instance.createdGarageElements.RemoveAt(i--);
                //return;
            }
        }
    }

    public static void DestroyAllGaragesList()
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
            return;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            //Destroy(instance.createdGarageElements[i].gameObject);
        }
        instance.createdGarageElements.Clear();
        instance.createdGarageElements = null;
    }
    #endregion

    #region Show/Hide Garage Elements
    public void ShowGaragesListScreen()
    {
        selectedGarageElements = new List<GarageElement>();

        screenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(screenObj.transform);
        sendDocumentButtonObj.SetActive(true);

        OnClickGarageEvent = instance.SelectMultiGarage;
    }

    public static void ShowGarageListScreen(System.Action<string> eventListiner)
    {
        if (isNullInstance)
            return;

        instance.screenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.screenObj.transform);
        instance.sendDocumentButtonObj.SetActive(false);

        instance.OnClickGarageEvent = instance.SelectSingleGarage;
        instance.OnSelectedGarage = eventListiner;
    }

    public void HideGaragesListScreen()
    {
        screenObj.SetActive(false);
        foreach(GarageElement garageElement in selectedGarageElements)
        {
            if(garageElement.IsSelected())
                garageElement.SelectCurrentElement();
        }
    }
    #endregion

    #region OnClick Garage Element Part
    private System.Action<GarageElement> OnClickGarageEvent;
    public static void OnClickGarageElement(GarageElement appraiserElement)
    {
        if (isNullInstance)
            return;

        instance.OnClickGarageEvent(appraiserElement);
    }

    private void SelectSingleGarage(GarageElement garageElement)
    {
        HideGaragesListScreen();
        OnSelectedGarage(garageElement.GetDisplayName());
    }

    private void SelectMultiGarage(GarageElement garageElement)
    {
        garageElement.SelectCurrentElement();
        if(!selectedGarageElements.Contains(garageElement))
            selectedGarageElements.Add(garageElement);
        else
            selectedGarageElements.Remove(garageElement);
    }
    #endregion

    #region Send Document Part
    private bool isBusy;

    public static void AddDocumentToGaragesResult(bool result)
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
        instance.HideGaragesListScreen();
        if (result)
            ShowAddDocumentSuccess();
    }
    #endregion

    #region Add Document To Garages Success
    private static void ShowAddDocumentSuccess()
    {
        if (isNullInstance)
            return;

        if (!instance.screenObj.activeSelf)
            return;

        instance.addDocumentSuccessScreen.SetActive(true);
        instance.StartCoroutine(instance.AutoCloseSuccessCardScreen());
    }

    private IEnumerator AutoCloseSuccessCardScreen()
    {
        yield return new WaitForSeconds(2f);
        addDocumentSuccessScreen.SetActive(false);
        HideGaragesListScreen();
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Getters
    public static string GetGarageNameByDisplay(string displayName)
    {
        if (isNullInstance || instance.createdGarageElements == null)
            return null;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetDisplayName() == displayName)
                return instance.createdGarageElements[i].GetGarageName();
        }
        return null;
    }

    public static string GetDisplayNameByGarage(string garageName)
    {
        if (isNullInstance || instance.createdGarageElements == null)
            return null;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetGarageName() == garageName)
                return instance.createdGarageElements[i].GetDisplayName();
        }
        return null;
    }

    public static List<GarageElement> GetAllAppraisers()
    {
        return new List<GarageElement>(instance.createdGarageElements);
    }
    #endregion
}
