﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalParameters
{
    public static readonly Color disabledButtonColor = Color.white;
    public static readonly Color enabledButtonColor = new Color32(63, 84, 152, 255);
    public static readonly Color unActiveButtonColor = new Color32(200, 200, 200, 255);

    public static readonly Color disabledTextColor = Color.black;
    public static readonly Color enabledTextColor = Color.white;
    public static readonly Color unActiveTextColor = new Color32(150, 150, 150, 255);

    public static readonly Color alphaColor = new Color(1f, 1f, 1f, 0f);

    public static readonly string[] forbiddenSymbols =
    {
        "_", "\\", "-"
    };
}
