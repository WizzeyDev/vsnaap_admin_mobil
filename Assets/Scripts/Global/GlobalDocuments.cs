﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalDocuments : MonoBehaviour
{
    #region Singleton
    private static GlobalDocuments instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("GlobalDocuments instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    [SerializeField]
    private Sprite pdfImage;
    [SerializeField]
    private Sprite wordImage;
    [SerializeField]
    private Sprite excelImage;
    [SerializeField]
    private Sprite powerPoint;

    public enum DocumentType { Unknown, Photo, PDF, Word, PowerPoint, Excel };

    public const string pdfImageUrl = "http://icons.iconarchive.com/icons/icons8/ios7/128/Files-Pdf-icon.png";
    public const string wordImageUrl = "http://icons.iconarchive.com/icons/carlosjj/microsoft-office-2013/128/Word-icon.png";
    public const string excelImageUrl = "http://icons.iconarchive.com/icons/carlosjj/microsoft-office-2013/128/Excel-icon.png";
    public const string powerPointImageUrl = "http://icons.iconarchive.com/icons/ziggy19/microsoft-office-mac-tilt/128/PowerPoint-icon.png";

    public static DocumentType GetDocumentType(string filePath)
    {
        string[] splitFilePath = filePath.Split('\\');
        string fileName = splitFilePath[splitFilePath.Length - 1];
        string[] alias = fileName.Split('/');
        if (alias.Length > 1)
            fileName = alias[alias.Length - 1];

        if (fileName.EndsWith(".png") || fileName.EndsWith(".jpg") || fileName.EndsWith(".jpeg"))
        {
            return DocumentType.Photo;
        }
        else if (fileName.EndsWith(".pdf"))
        {
            return DocumentType.PDF;
        }
        else if (fileName.EndsWith(".doc") || fileName.EndsWith(".docx"))
        {
            return DocumentType.Word;
        }
        else if (fileName.EndsWith(".xlsx") || fileName.EndsWith(".xlsm"))
        {
            return DocumentType.Excel;
        }
        else if (fileName.EndsWith(".ppt") || fileName.EndsWith(".pptx") ||
            fileName.EndsWith(".pps") || fileName.EndsWith(".ppsx"))
        {
            return DocumentType.PowerPoint;
        }
        else
        {
            Debug.Log(string.Format("Document {0} has an unknown format...", fileName));
            return DocumentType.Unknown;
        }
    }

    public static Sprite GetDocumentSprite(string filePath)
    {
        if (isNullInstance)
            return null;

        if (filePath.EndsWith(".png") || filePath.EndsWith(".jpg") || filePath.EndsWith(".jpeg"))
        {
            return SpriteLoader.GetSpriteFromFile(filePath);
        }
        else if (filePath.EndsWith(".pdf"))
        {
            return instance.pdfImage;
        }
        else if (filePath.EndsWith(".doc") || filePath.EndsWith(".docx"))
        {
            return instance.wordImage;
        }
        else if (filePath.EndsWith(".xlsx") || filePath.EndsWith(".xlsm"))
        {
            return instance.excelImage;
        }
        else if (filePath.EndsWith(".ppt") || filePath.EndsWith(".pptx") ||
            filePath.EndsWith(".pps") || filePath.EndsWith(".ppsx"))
        {
            return instance.powerPoint;
        }
        else
        {
            Debug.Log(string.Format("Document {0} has an unknown format...", filePath));
            return null;
        }
    }

    public static bool IsDocumentValid(string filePath)
    {
        if (isNullInstance)
            return false;

        return filePath.EndsWith(".png") ||
            filePath.EndsWith(".jpg") ||
            filePath.EndsWith(".jpeg") ||
            filePath.EndsWith(".pdf") ||
            filePath.EndsWith(".doc") ||
            filePath.EndsWith(".docx") ||
            filePath.EndsWith(".xlsx") ||
            filePath.EndsWith(".xlsm") ||
            filePath.EndsWith(".ppt") ||
            filePath.EndsWith(".pptx") ||
            filePath.EndsWith(".pps") ||
            filePath.EndsWith(".ppsx");
    }

    public static Sprite GetDocumentSpriteByType(DocumentType documentType)
    {
        if (isNullInstance)
            return null;

        switch(documentType)
        {
            case DocumentType.PDF:
                {
                    return instance.pdfImage;
                }
                break;

            case DocumentType.Word:
                {
                    return instance.wordImage;
                }
                break;

            case DocumentType.Excel:
                {
                    return instance.excelImage;
                }
                break;

            case DocumentType.PowerPoint:
                {
                    return instance.powerPoint;
                }
                break;

            case DocumentType.Unknown:
                {
                    Debug.LogError("DocumentType is uknown...");
                    return null;
                }
                break;

            case DocumentType.Photo:
                {
                    Debug.Log("DocumentType is Photo...");
                    return null;
                }
                break;
        }
        return null;
    }
}
