﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class DropBoxTest : MonoBehaviour
{
    private static string dropbox_access_token = "iYV2Udg15VAAAAAAAAAAOCz9cQdvubfLprVIh1TmrdPkFBYGU881u9n0g3fiGeBQ";

    public static string dropboxFolderPath = "/Vsnapp/DB/";
    //public static string dropBoxKey = "iYV2Udg15VAAAAAAAAAAOCz9cQdvubfLprVIh1TmrdPkFBYGU881u9n0g3fiGeBQ";  //"HijZltjcDaAAAAAAAAAAQie6rsuHJpmzk-QFlJzVBrsrDQalQYys3MLvjij6G0dA";
    // Start is called before the first frame update


    //public static UnityWebRequest UploadFileDropbox(string fileTargetPath)
    //{
    //    byte[] data = File.ReadAllBytes(fileTargetPath);
    //    string requestPath = "/upload";
    //    string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
    //    string contentType = "application/octet-stream";
    //    // if your texture2d has RGb24 type, don't need to redraw new texture2d
    //    DropboxData dropData = new DropboxData();
    //    dropData.path = "/Mazzee_Servers/" + Path.GetFileName(fileTargetPath);
    //    dropData.autorename = true;
    //    dropData.mode = "overwrite";
    //    UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
    //    request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
    //    request.SetRequestHeader("Content-Type", contentType);
    //    request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));//"+'"'+"/*{"path":"/uploadUnity.jpg"}' */
    //    try
    //    {
    //        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
    //    }
    //    catch { }
    //    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
    //    return request;
    //}


    #region ----------------- Download -----------------------------
    public static UnityWebRequest DownloadFromDropboxAndSave(string fileName)
    {
        string requestPath = "/download";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;//postRequest.setURI(new URI(url + "/targets"));
        string contentType = "application/octet-stream";
        DropTest2 dropData = new DropTest2();
        dropData.path = fileName;
        var request = new UnityWebRequest(serviceURI, "POST");
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        Debug.Log("DownloadFromDropboxAndSave Dropbox-API-Arg:" + request.GetRequestHeader("Dropbox-API-Arg"));
        return request;
    }

    #endregion   ----------------- Download -----------------------------

    public static UnityWebRequest UploadCapturedToDropbox(string fileTargetPath)
    {
        byte[] data = System.IO.File.ReadAllBytes(fileTargetPath);

        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;
        string contentType = "application/octet-stream";

        DropboxUploadParm dropData = new DropboxUploadParm();
        dropData.path = /*"/CaptureVideos/"*/ dropboxFolderPath + Path.GetFileName(fileTargetPath);
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        Debug.Log("KOKACHO " + request.GetRequestHeader("Dropbox-API-Arg"));
        try
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
            Debug.Log("Uploaded complated");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    
}

public class DropTest2
{
    public string path;
}


    public class DropboxUploadParm
{
    public string path;
    public bool autorename;
    public string mode;
}

//public class saveFile
//{
//    public string name;
//    public string format;
//}