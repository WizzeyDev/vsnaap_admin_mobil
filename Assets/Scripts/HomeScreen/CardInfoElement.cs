﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardInfoElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    private TextMeshProUGUI garageNameText;
    [SerializeField]
    private TextMeshProUGUI appraiserNameText;
    [SerializeField]
    private TextMeshProUGUI carNumberText;
    [SerializeField]
    private TextMeshProUGUI creationDateText;
    [SerializeField]
    private TextMeshProUGUI cardNumberText;

    [SerializeField]
    private Image confirmIndicatorImage;
    #endregion

    //    #region Setters
    //    public void SetGarageName(string garageName)
    //    {
    //        garageNameText.text = garageName;
    //    }
    //    public void SetGarageAppraiser(string appraiserName)
    //    {
    //        appraiserNameText.text = appraiserName;
    //    }
    //    public void SetCarNumber(string carNumber)
    //    {
    //        carNumberText.text = carNumber;
    //    }
    //    public void SetCreationDate(string creationDate)
    //    {
    //        creationDateText.text = creationDate;
    //    }
    //    public void SetCardNumber(string cardNumber)
    //    {
    //        cardNumberText.text = cardNumber;
    //    }

    //    public void SetConfirmedIndicatorState(bool isConfirmed)
    //    {
    //        if (isConfirmed)
    //            confirmIndicatorImage.color = Color.green;
    //        else
    //            confirmIndicatorImage.color = Color.red;
    //    }
    //    #endregion

    //    public void SelectCard()
    //    {
    //        if (string.IsNullOrEmpty(cardNumberText.text))
    //        {
    //            Debug.LogError("CardNumber name is null!");
    //            return;
    //        }

    //        CardManager.SelectCard(cardNumberText.text);
    //    }

    //    #region Getters
    //    public string GetCardNumber()
    //    {
    //        return cardNumberText.text;
    //    }
    //    #endregion
}
