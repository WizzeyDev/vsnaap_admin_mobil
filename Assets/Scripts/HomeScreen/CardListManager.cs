﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CardListManager : MonoBehaviour
{
    #region Singleton
    private static CardListManager instance;
    private static bool isNullInstance {
        get {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("InviewUICanvasManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private Transform cardListParent;

    [Space(10)]
    [SerializeField]
    private GameObject cardElementPrefab;
    #endregion

    //    #region Private Fields
    //    private List<CardInfoElement> createdCardInfoElements;
    //    #endregion

    //    #region Cards List Part
    //    public static void SetupCardList(string rawCardNames)
    //    {
    //        if (isNullInstance)
    //            return;

    //        if (instance.createdCardInfoElements != null)
    //        {
    //            Debug.LogError("createdCardInfoElements list is already exist!");
    //            return;
    //        }

    //        instance.createdCardInfoElements = new List<CardInfoElement>();

    //        if (!string.IsNullOrEmpty(rawCardNames))
    //            PlayerIOManager.SendGetCardsDataMessage(rawCardNames);
    //    }
    //    public static void CreateCardsList(string[] allCardsData)
    //    {
    //        if (isNullInstance)
    //            return;

    //        for (int i = 0; i < allCardsData.Length; ++i)
    //        {
    //            if (string.IsNullOrEmpty(allCardsData[i]))
    //                continue;

    //            string[] cardData = allCardsData[i].Split('\\');
    //            AddCardToList(cardData);
    //        }
    //    }

    //    private static void AddCardToList(string[] cardData)
    //    {
    //        if (isNullInstance)
    //            return;

    //        if (instance.createdCardInfoElements == null)
    //        {
    //            Debug.LogError("AddCardToList cannot be done, cuz createdCardInfoElements list is null!");
    //            return;
    //        }

    //        CardInfoElement cardElement = Instantiate(instance.cardElementPrefab, instance.cardListParent).
    //            GetComponent<CardInfoElement>();
    //        if (cardElement == null)
    //        {
    //            Debug.LogError(cardElement.gameObject.name + " has no attached 'CardInfoElement' !");
    //            return;
    //        }

    //        cardElement.SetGarageName(cardData[0]);
    //        cardElement.SetGarageAppraiser(cardData[1]);
    //        cardElement.SetCarNumber(cardData[2]);
    //        cardElement.SetCreationDate(cardData[3]);
    //        bool isConfirmed;
    //        bool.TryParse(cardData[4], out isConfirmed);
    //        cardElement.SetConfirmedIndicatorState(isConfirmed);
    //        cardElement.SetCardNumber(cardData[cardData.Length - 1]);

    //        instance.createdCardInfoElements.Add(cardElement);
    //    }

    //    public static void CheckUpdateCardsList(string[] allCardNames)
    //    {
    //        if (isNullInstance)
    //            return;

    //        string rawNewCardNames = string.Empty;
    //        for (int i = 0; i < allCardNames.Length; ++i)
    //        {
    //            if (string.IsNullOrEmpty(allCardNames[i]))
    //                continue;

    //            if (!instance.IsOldListContainsCard(allCardNames[i]))
    //            {
    //                rawNewCardNames += allCardNames[i] + "_";
    //            }
    //        }
    //        if (rawNewCardNames.EndsWith("_"))
    //            rawNewCardNames = rawNewCardNames.Remove(rawNewCardNames.Length - 1, 1);

    //        if (!string.IsNullOrEmpty(rawNewCardNames))
    //        {
    //            PlayerIOServerManager.SendGetCardsDataMessage(rawNewCardNames);
    //        }

    //        for (int i = 0; i < instance.createdCardInfoElements.Count; ++i)
    //        {
    //            string rawCardData = instance.createdCardInfoElements[i].GetCardNumber();
    //            if (!allCardNames.Contains(rawCardData))
    //            {
    //                Destroy(instance.createdCardInfoElements[i].gameObject);
    //                instance.createdCardInfoElements.RemoveAt(i--);
    //            }
    //        }
    //    }

    //    private bool IsOldListContainsCard(string targetName)
    //    {
    //        for(int i = 0; i < createdCardInfoElements.Count; ++i)
    //        {
    //            if (createdCardInfoElements[i].GetCardNumber() == targetName)
    //                return true;
    //        }
    //        return false;
    //    }

    //    public static void UpdateCardsList(string[] allCardsData)
    //    {
    //        for (int i = 0; i < allCardsData.Length; ++i)
    //        {
    //            if (string.IsNullOrEmpty(allCardsData[i]))
    //            {
    //                Debug.LogError("UpdateCardsList at " + i + " index has null cardData");
    //                continue;
    //            }

    //            string[] cardData = allCardsData[i].Split('\\');
    //            if (cardData.Length == 0)
    //            {
    //                Debug.LogError("UpdateCardsList at " + i + " index has empty cardData");
    //                continue;
    //            }

    //            AddCardToList(cardData);
    //        }
    //    }

    //    public static void RemoveCardFromList(string targetCardName)
    //    {
    //        if (isNullInstance)
    //            return;

    //        for (int i = 0; i < instance.createdCardInfoElements.Count; ++i)
    //        {
    //            string currentCardName = instance.createdCardInfoElements[i].GetCardNumber();
    //            if(currentCardName == targetCardName)
    //            {
    //                CardInfoElement cardElement = instance.createdCardInfoElements[i];
    //                instance.createdCardInfoElements.RemoveAt(i);
    //                Destroy(cardElement.gameObject);
    //                break;
    //            }
    //        }
    //    }

    //    public static void ConfirmCardData(string targetCardName, string newAppraiser)
    //    {
    //        if (isNullInstance)
    //            return;

    //        for (int i = 0; i < instance.createdCardInfoElements.Count; ++i)
    //        {
    //            string currentCardName = instance.createdCardInfoElements[i].GetCardNumber();
    //            if (currentCardName == targetCardName)
    //            {
    //                CardInfoElement cardElement = instance.createdCardInfoElements[i];
    //                cardElement.SetConfirmedIndicatorState(true);
    //                cardElement.SetGarageAppraiser(newAppraiser);
    //                break;
    //            }
    //        }
    //    }
    //    #endregion
}