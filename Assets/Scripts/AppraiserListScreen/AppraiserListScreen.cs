﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AppraiserListScreen : MonoBehaviour
{
    #region Singleton
    private static AppraiserListScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("AppraiserListScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject screenObj;
    [SerializeField]
    private Transform appraisersListParent;

    [Space(10)]
    [SerializeField]
    private GameObject appraiserElementPrefab;
    #endregion

    #region Private Fields
    private System.Action<string> OnSelectedAppraiser;

    public List<AppraiserElement> createdAppraiserElements;
    #endregion

    #region Show/Hide Appraiser List Screen
    private bool isDisplayName;
    public static void ShowAppraiserListScreen(System.Action<string> eventListiner, bool isDisplayName)
    {
        if (isNullInstance)
            return;

        instance.isDisplayName = isDisplayName;

        instance.screenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.screenObj.transform);
        instance.OnSelectedAppraiser = eventListiner;
    }

    public void HideAppraiserListScreen()
    {
        screenObj.SetActive(false);
    }
    #endregion

    #region Appraisers List Part
    public static void CreateAppraiserList(string[] allAppraiserData)
    {
    
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements != null)
        {
            //CustomLogger.LogErrorMessage("AppraiserListScreen 'createdAppraiserElements' list is already exist!");
            //return;
        }

        instance.createdAppraiserElements = new List<AppraiserElement>();

        for (int i = 0; i < allAppraiserData.Length; ++i)
        {
            string rawAppraiserData = allAppraiserData[i];
            if (string.IsNullOrEmpty(rawAppraiserData))
            {
                Debug.LogError("UpdateCardsList at " + i + " index has null cardData");
                continue;
            }
            Debug.Log("<color=red> CreateAppraiserList </color>" + string.Join(" ", rawAppraiserData));
           string[] appraiserData = rawAppraiserData.Split('\\');
            string appraiserName = appraiserData[0];
            string password = appraiserData[1];
            string displayName = appraiserData[2];
            string ID = appraiserData[3];
            string officePhone = appraiserData[4];
            string carTax = appraiserData[5];
            string role = appraiserData[6];
            string area = appraiserData[7];
            string adress = appraiserData[8];
            string email = appraiserData[9];
            string isActive = appraiserData[10];
            AddAppraiserToList(appraiserName, password, displayName, ID, officePhone, carTax,
                role, adress, area, isActive, email);
        }
    }

    public static void AddAppraiserToList(string appraiserName, string password, string displayName, string ID, string officePhone, string carTax,
        string role, string adress, string area, string isActive, string email)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements == null)
            instance.createdAppraiserElements = new List<AppraiserElement>();
        //Creating Appraiser Object
        Debug.Log("<color=red> AddAppraiserToList </color>");
        AppraiserElement appraiserElement = Instantiate(instance.appraiserElementPrefab, instance.appraisersListParent).GetComponent<AppraiserElement>(); 
        appraiserElement.Setup(appraiserName, password, displayName, ID, officePhone, carTax, role, adress, area, isActive, email);

        instance.createdAppraiserElements.Add(appraiserElement);

        AppraisersSearchScreen.AddDisplayNameFilterElement(displayName);
        AppraisersSearchScreen.AddAreaFilterElement(area);
        AppraisersSearchScreen.AddCityFilterElement(adress);

        AdminBlockListScreen.AddDisplayNameFilterElement(displayName);
        AdminBlockListScreen.AddAreaFilterElement(area);
        AdminBlockListScreen.AddRoleFilterElement(role);
    }

    public static void UpdateAppraiser(string[] appraiserData)
    {
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements == null)
        {
            CustomLogger.LogErrorMessage("UpdateAppraiser 'createdAppraiserElements' list is null!");
            return;
        }

        string appraiserName = appraiserData[0];
        string password = appraiserData[1];
        string ID = appraiserData[2];
        string officePhone = appraiserData[3];
        string carTax = appraiserData[4];
        string role = appraiserData[5];
        string area = appraiserData[6];
        string adress = appraiserData[7];
        string email = appraiserData[8];
        string isActive = appraiserData[9];
        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetAppraiserName() == appraiserName)
            {
                instance.createdAppraiserElements[i].UpdateData(password, ID, officePhone, carTax, role, adress,
                    area, isActive, email);

                //AppraisersSearchScreen.AddDisplayNameFilterElement(displayName);
                AppraisersSearchScreen.AddAreaFilterElement(area);
                AppraisersSearchScreen.AddCityFilterElement(adress);

                //AppraiserBlockListScreen.AddDisplayNameFilterElement(displayName);
                AdminBlockListScreen.AddAreaFilterElement(area);
                AdminBlockListScreen.AddRoleFilterElement(role);
                return;
            }
        }
    }

    public static void DestroyAllAppraisersList()
    {
        Debug.Log("<color=red> DestroyAllAppraisersList </color>");
        if (isNullInstance)
            return;

        if (instance.createdAppraiserElements == null)
            return;
        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            Destroy(instance.createdAppraiserElements[i].gameObject);
        }
        instance.createdAppraiserElements.Clear();
        instance.createdAppraiserElements = null;
    }

    public static void RemoveAppraiserFromList(string appraiserName)
    {
        Debug.Log("<color=red> RemoveAppraiserFromList </color>");
        if (isNullInstance)
            return;
        if (instance.createdAppraiserElements == null)
            return;

        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetAppraiserName() == appraiserName)
            {
                Destroy(instance.createdAppraiserElements[i].gameObject);
                instance.createdAppraiserElements.RemoveAt(i--);
                return;
            }
        }
    }
    #endregion

    #region Appraiser On Click
    public static void SelectAppraiser(AppraiserElement appraiserElement)
    {
        if (isNullInstance)
            return;

        instance.HideAppraiserListScreen();
        if (instance.isDisplayName)
            instance.OnSelectedAppraiser(appraiserElement.GetDisplayName());
        else
            instance.OnSelectedAppraiser(appraiserElement.GetAppraiserName());
    }
    #endregion

    #region Getters
    public static string GetAppraiserNameByDisplay(string displayName)
    {
        if (isNullInstance || instance.createdAppraiserElements == null)
            return null;

        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetDisplayName() == displayName)
                return instance.createdAppraiserElements[i].GetAppraiserName();
        }
        return null;
    }

    public static string GetDisplayNameByAppraiser(string appraiserName)
    {
        if (isNullInstance || instance.createdAppraiserElements == null)
            return null;

        for (int i = 0; i < instance.createdAppraiserElements.Count; ++i)
        {
            if (instance.createdAppraiserElements[i].GetAppraiserName() == appraiserName)
                return instance.createdAppraiserElements[i].GetDisplayName();
        }
        return null;
    }

    public static List<AppraiserElement> GetAllAppraisers()
    {
        return new List<AppraiserElement>(instance.createdAppraiserElements);
    }
    #endregion
}
