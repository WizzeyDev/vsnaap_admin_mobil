﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AppraiserElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    protected TextMeshProUGUI displayNameText;
    [SerializeField]
    protected TextMeshProUGUI IDText;
    [SerializeField]
    protected TextMeshProUGUI officePhoneText;
    [SerializeField]
    protected TextMeshProUGUI carTaxText;
    [SerializeField]
    protected TextMeshProUGUI roleText;
    [SerializeField]
    protected InputField adressText;
    [SerializeField]
    protected TextMeshProUGUI areaText;
    [SerializeField]
    protected TextMeshProUGUI isActiveText;
    [SerializeField]
    protected TextMeshProUGUI emailText;

    [Space(10)]
    [SerializeField]
    private Button selectButton;
    #endregion

    #region Private Fields
    protected string appraiserName;
    protected string password;
    #endregion

    public void Setup(string appraiserName, string password, string displayName, string ID, string officePhone, string carTax, string role, 
        string adress, string area, string isActive, string email)
    {
        Debug.Log("<color=green> AppraiserElement Setup");
        this.appraiserName = appraiserName;
        this.password = password;
        displayNameText.text = displayName;
        IDText.text = ID;
        officePhoneText.text = officePhone;
        carTaxText.text = carTax;
        roleText.text = role;
        adressText.text = adress;
        areaText.text = area;
        isActiveText.text = isActive;
        emailText.text = email;

        selectButton.onClick.AddListener(() =>
        {
            AppraiserListScreen.SelectAppraiser(this);
            AudioManager.PlayButtonSound();
        });
    }

    #region Setters
    public void UpdateData(string password, string ID, string officePhone, string carTax, string role,
    string adress, string area, string isActive, string email)
    {
        this.password = password;
        IDText.text = ID;
        officePhoneText.text = officePhone;
        carTaxText.text = carTax;
        roleText.text = role;
        adressText.text = adress;
        areaText.text = area;
        isActiveText.text = isActive;
        emailText.text = email;
    }
    #endregion

    #region Getters
    public string GetAppraiserName()
    {
        return appraiserName;
    }

    public string GetPassword()
    {
        return password;
    }

    public string GetDisplayName()
    {
        return displayNameText.text;
    }

    public string GetID()
    {
        return IDText.text;
    }

    public string GetOfficePhone()
    {
        return officePhoneText.text;
    }
    public string GetCarTax()
    {
        return carTaxText.text;
    }
    public string GetRole()
    {
        return roleText.text;
    }
    public string GetAdress()
    {
        return adressText.text;
    }
    public string GetArea()
    {
        return areaText.text;
    }
    public string GetIsActive()
    {
        return isActiveText.text;
    }
    public string GetEmail()
    {
        return emailText.text;
    }
    #endregion
}
