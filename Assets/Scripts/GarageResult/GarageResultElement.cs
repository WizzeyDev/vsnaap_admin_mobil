﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageResultElement : GarageElement
{
    #region Serializable Fields
    [SerializeField]
    private RectTransform arrowRectTransform;
    [SerializeField]
    private GameObject underPanelObj;
    #endregion

    #region Private Fields
    private RectTransform rectTransform;
    private RectTransform parentRectTransform;

    private const float openedAngle = 0f;
    private const float closedAngle = 180f;

    private bool isOpen;
    #endregion

    #region Setup       
    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        parentRectTransform = rectTransform.parent.GetComponent<RectTransform>();
    }
    public void Setup(GarageElement refElement)
    {
        garageName = refElement.GetGarageName();
        password = refElement.GetPassword();
        displayNameText.text = refElement.GetDisplayName();
        IDText.text = refElement.GetID();
        officePhoneText.text = refElement.GetOfficePhone();
        mobilePhoneText.text = refElement.GetMobilePhone();
        faxText.text = refElement.GetFax();
        emailText.text = refElement.GetEmail();
        adressText.text = refElement.GetAdress();
        areaText.text = refElement.GetArea();
        checkTypeText.text = refElement.GetCheckType();
        isActiveText.text = refElement.GetIsActive();
        insurerNameText.text = refElement.GetInsurerName();
    }
    #endregion

    #region OpenClose Panel
    public void OpenCloseUnderCardPanel()
    {
        if (isOpen)
            CloseUnderPanel();
        else
            OpenUnderPanel();

        RebuildEvent();
        isOpen = !isOpen;
        AudioManager.PlayButtonSound();
    }

    private void OpenUnderPanel()
    {
        arrowRectTransform.rotation =
            Quaternion.Euler(arrowRectTransform.rotation.eulerAngles.x,
            arrowRectTransform.rotation.eulerAngles.y,
            openedAngle);

        underPanelObj.SetActive(true);
        GarageResultScreen.SlideDownUnderPanel();
    }

    private void CloseUnderPanel()
    {
        arrowRectTransform.rotation =
            Quaternion.Euler(arrowRectTransform.rotation.eulerAngles.x,
            arrowRectTransform.rotation.eulerAngles.y,
            closedAngle);

        underPanelObj.SetActive(false);
    }
    #endregion

    #region On Click
    public void OpenGarageChangeWindow()
    {
        GarageResultScreen.ShowChangeGarageDataWindow(this);
        AudioManager.PlayButtonSound();
    }
    public void DeleteGarage()
    {
        PlayerIOServerManager.DeleteGarage(garageName);
        AudioManager.PlayButtonSound();
    }
    #endregion

    #region Rebuild Event
    public void RebuildEvent()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
        if (gameObject.activeInHierarchy)
            StartCoroutine(LateRebuild());
    }
    private IEnumerator LateRebuild()
    {
        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(parentRectTransform);
    }
    #endregion
}
