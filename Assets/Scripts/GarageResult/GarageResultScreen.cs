﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageResultScreen : MonoBehaviour
{
    #region Singleton
    private static GarageResultScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("GarageResultScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject garagesResultScreen;
    [SerializeField]
    private GameObject changeGarageWindowObj;

    [Space(10)]
    [SerializeField]
    private Transform garagesListParent;
    [SerializeField]
    private GameObject garageElementPrefab;

    [Space(10)]
    [SerializeField]
    private ScrollRect scrollRect;

    [Space(10)]
    [SerializeField]
    private InputField garageName;
    [SerializeField]
    private InputField displayName;
    [SerializeField]
    private InputField passwordInputField;
    [SerializeField]
    private InputField idInputField;
    [SerializeField]
    private InputField officePhoneInputField;
    [SerializeField]
    private InputField mobilePhoneInputField;
    [SerializeField]
    private InputField faxInputField;
    [SerializeField]
    private InputField emailInputField;
    [SerializeField]
    private InputField areaInputField;
    [SerializeField]
    private InputField adressInputField;
    [SerializeField]
    private InputField insurerNameInputField;
    [SerializeField]
    private InputField isActiveInputField;
    [SerializeField]
    private InputField checkTypeInputField;
    #endregion

    #region Private Fields

    private List<GarageResultElement> createdGarageElements;

    private bool isBusy;
    #endregion

    #region Show/Hide Garage Result Screen
    public static void ShowGaragesResultScreen(List<GarageElement> allGarages)
    {
        if (isNullInstance)
            return;

        instance.ShowGarageResultScreen(allGarages);
    }
    public void ShowGarageResultScreen(List<GarageElement> allGarages)
    {
        garagesResultScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(garagesResultScreen.transform);

        if (instance.createdGarageElements != null)
        {
            CustomLogger.LogErrorMessage("ShowGarageResultScreen 'createdGarageElements' list is already exist!");
            return;
        }

        createdGarageElements = new List<GarageResultElement>();

        for (int i = 0; i < allGarages.Count; ++i)
        {
            AddGarageResultElementToList(allGarages[i]);
        }
    }

    public static void HideGaragesResultScreen()
    {
        if (isNullInstance)
            return;

        instance.HideGarageResultScreen();
    }
    public void HideGarageResultScreen()
    {
        DestroyAllGaragesList();
        garagesResultScreen.SetActive(false);
        HideChangeGarageDataWindow();
    }
    public static void DestroyAllGaragesList()
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
            return;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            Destroy(instance.createdGarageElements[i].gameObject);
        }
        instance.createdGarageElements.Clear();
        instance.createdGarageElements = null;
    }

    public static void RemoveGarageResultFromList(string garageName)
    {
        if (isNullInstance)
            return;

        if (instance.createdGarageElements == null)
            return;

        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetGarageName() == garageName)
            {
                Destroy(instance.createdGarageElements[i].gameObject);
                instance.createdGarageElements.RemoveAt(i--);
                return;
            }
        }
    }
    #endregion

    #region Garage Result Elements List
    private void AddGarageResultElementToList(GarageElement refElement)
    {
        GarageResultElement garageElement =
            Instantiate(instance.garageElementPrefab, instance.garagesListParent).
            GetComponent<GarageResultElement>();
        garageElement.Setup(refElement);

        instance.createdGarageElements.Add(garageElement);
    }
    #endregion

    #region Open/Close Under Panel
    public static void SlideDownUnderPanel()
    {
        if (isNullInstance)
            return;

        instance.scrollRect.velocity = new Vector2(0f, 1000f);
    }
    #endregion

    #region Change Garage Data
    public static void ShowChangeGarageDataWindow(GarageResultElement refElement)
    {
        instance.changeGarageWindowObj.SetActive(true);

        instance.garageName.text = refElement.GetGarageName();
        instance.displayName.text = refElement.GetDisplayName();
        instance.passwordInputField.text = refElement.GetPassword();
        instance.idInputField.text = refElement.GetID();
        instance.officePhoneInputField.text = refElement.GetOfficePhone();
        instance.mobilePhoneInputField.text = refElement.GetMobilePhone();
        instance.faxInputField.text = refElement.GetFax();
        instance.emailInputField.text = refElement.GetEmail();
        instance.areaInputField.text = refElement.GetArea();
        instance.adressInputField.text = refElement.GetAdress();
        instance.insurerNameInputField.text = refElement.GetInsurerName();
        instance.isActiveInputField.text = refElement.GetIsActive();
        instance.checkTypeInputField.text = refElement.GetCheckType();
    }
    public void HideChangeGarageDataWindow()
    {
        instance.changeGarageWindowObj.SetActive(false);
    }

    public void ConfirmChangeGarage()
    {
        if (isBusy)
            return;

        string[] joinStrings = new string[]
        {
            garageName.text,
            passwordInputField.text,
            idInputField.text,
            officePhoneInputField.text,
            mobilePhoneInputField.text,
            faxInputField.text,
            emailInputField.text,
            areaInputField.text,
            adressInputField.text,
            insurerNameInputField.text,
            isActiveInputField.text,
            checkTypeInputField.text
        };
        string rawGarageData = string.Join("_", joinStrings);

        isBusy = true;

        PlayerIOServerManager.ChangeGarageData(rawGarageData);
    }

    public static void ChangeGarageDataSuccess(string[] appraiserData)
    {
        if (isNullInstance)
            return;

        instance.HideChangeGarageDataWindow();

        if (instance.createdGarageElements == null)
        {
            CustomLogger.LogErrorMessage("ChangeGarageDataSuccess 'createdGarageElements' list is null!");
            return;
        }

        string garageName = appraiserData[0];
        string password = appraiserData[1];
        string ID = appraiserData[2];
        string officePhone = appraiserData[3];
        string mobilePhone = appraiserData[4];
        string fax = appraiserData[5];
        string email = appraiserData[6];
        string area = appraiserData[7];
        string adress = appraiserData[8];
        string insurerName = appraiserData[9];
        string isActive = appraiserData[10];
        string checkType = appraiserData[11];
        for (int i = 0; i < instance.createdGarageElements.Count; ++i)
        {
            if (instance.createdGarageElements[i].GetGarageName() == garageName)
            {
                instance.createdGarageElements[i].UpdateData(password, ID, officePhone, mobilePhone, fax, email,
                    adress, area, checkType, isActive, insurerName);
                return;
            }
        }
       
    }
    //public static void ChangeGarageDataFailed()
    //{
    //    if (isNullInstance)
    //        return;

    //    instance.StartCoroutine(instance.HighlighInputField(instance.displayNameInputField.image));
    //}
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
