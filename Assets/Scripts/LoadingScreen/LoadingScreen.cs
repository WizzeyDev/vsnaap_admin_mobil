﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreen : MonoBehaviour
{
    #region Singleton
    private static LoadingScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("LoadingScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject loadingScreenObj;
    [SerializeField]
    private GameObject cancelButtonObj;
    #endregion

    #region Private Fields
    private System.Action cancelButtonEventHandler;
    #endregion

    #region Show/Hide Loading Screen
    public static void ShowLoadingScreen(System.Action cancelButtonEventHandler)
    {
        if (isNullInstance)
            return;

        instance.loadingScreenObj.SetActive(true);
        instance.cancelButtonObj.SetActive(false);

        instance.cancelButtonEventHandler = cancelButtonEventHandler;

        instance.StartCoroutine(instance.LateShowCancelButton());
    }

    public static void HideLoadingScreen()
    {
        if (isNullInstance)
            return;

        instance.loadingScreenObj.SetActive(false);
    }
    #endregion

    #region Cancel Button
    private IEnumerator LateShowCancelButton()
    {
        yield return new WaitForSeconds(3f);
        cancelButtonObj.SetActive(true);
    }

    public void CancelButtonEventHandler()
    {
        cancelButtonEventHandler?.Invoke();
        HideLoadingScreen();
        LoginScreen.ShowLoginScreen();
    }
    #endregion
}
