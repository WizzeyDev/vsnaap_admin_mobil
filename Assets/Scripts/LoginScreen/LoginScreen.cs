﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginScreen : MonoBehaviour
{
    #region Singleton
    private static LoginScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("LoginScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject loginScreenObj;
    [SerializeField]
    private Image loginCoverImg;
    [SerializeField]
    private Image blackLogoImg;
    [SerializeField]
    private RectTransform logoRectTransform;

    [Space(10)]
    [SerializeField]
    private GameObject termsAndConditionsCheckmarkObj;
    [SerializeField]
    private Image logInButtonImg;
    [SerializeField]
    private TextMeshProUGUI logInButtonText;
    [SerializeField]
    private Button logInButton;
    [SerializeField]
    private GameObject termsAndConditionsScreenObj;
    #endregion

    #region Private Fields
    private System.Action<string, string> EnterInSystemEvent;

    private Vector2 blackLogoPosition = new Vector2(0f, 655f);

    private const float alphaSpeed = 1f;
    private const float logoSpeed = 1000f;
    #endregion

    #region Setup
    private void Start()
    {
        StartCoroutine(WaitForDependencyChecking());
    }

    private IEnumerator WaitForDependencyChecking()
    {
        //while (true)
        //{
        //    int isDependencyResolved = FirebaseManager.IsFirebaseDependencyResolved();
        //    if (isDependencyResolved == 1)
        //    {
        //        CustomLogger.LogMessage("Dependency resolved success!");
        //        break;
        //    }
        //    else if (isDependencyResolved == 0)
        //    {
        //        CustomLogger.LogErrorMessage("Dependency can't be resolved!");
        //        yield break;
        //    }

        //    yield return null;
        //}

        CustomLogger.LogMessage("Wait for permissions granted...");
        while (true)
        {
            int isPermissionGranted = GameManager.IsPermissionsGranted();
            if (isPermissionGranted == 1)
            {
                CustomLogger.LogMessage("Permissions granted success!");
                break;
            }
            else if (isPermissionGranted == 0)
            {
                CustomLogger.LogErrorMessage("Permissions can't be granted!");
                yield break;
            }

            yield return null;
        }

        yield return null;
        StartCoroutine(HideLoginCover());
        StartCoroutine(HideWhiteLogo());
        StartCoroutine(MoveLogoToBlackPosition());

        if (PlayerPrefs.HasKey("AdminAutoLogin"))
            LogIn();
    }

    #region Animations
    private IEnumerator HideLoginCover()
    {
        while (loginCoverImg.color.a > 0f)
        {
            loginCoverImg.color = new Color(loginCoverImg.color.r,
                 loginCoverImg.color.g, loginCoverImg.color.b,
                 Mathf.MoveTowards(loginCoverImg.color.a, 0f, alphaSpeed * Time.deltaTime));
            yield return null;
        }
        loginCoverImg.gameObject.SetActive(false);
    }

    private IEnumerator HideWhiteLogo()
    {
        while (blackLogoImg.color.a < 1f)
        {
            blackLogoImg.color = new Color(blackLogoImg.color.r,
                 blackLogoImg.color.g, blackLogoImg.color.b,
                 Mathf.MoveTowards(blackLogoImg.color.a, 1f, alphaSpeed * Time.deltaTime));
            yield return null;
        }
    }

    private IEnumerator MoveLogoToBlackPosition()
    {
        while (logoRectTransform.anchoredPosition.y != blackLogoPosition.y)
        {
            logoRectTransform.anchoredPosition = Vector2.MoveTowards(logoRectTransform.anchoredPosition,
                blackLogoPosition, logoSpeed * Time.deltaTime);
            yield return null;
        }
    }
    #endregion

    #endregion

    #region Add Event Listiners
    public static void AddEnterInSystemEventListiner(System.Action<string, string> eventListiner)
    {
        if (isNullInstance)
            return;

        instance.EnterInSystemEvent += eventListiner;
    }
    #endregion

    #region Show/Hide Login Screen
    public static void ShowLoginScreen()
    {
        if (isNullInstance)
            return;

        instance.loginScreenObj.SetActive(true);
    }
    public static void HideLoginScreen()
    {
        if (isNullInstance)
            return;

        instance.loginScreenObj.SetActive(false);
    }
    #endregion

    #region Checkmark OnClick
    public void TermsAndConditionsCheckmarkOnClick()
    {
        if (termsAndConditionsCheckmarkObj.activeSelf)
        {
            termsAndConditionsCheckmarkObj.SetActive(false);
            logInButtonImg.color = GlobalParameters.disabledButtonColor;
            logInButtonText.color = GlobalParameters.disabledTextColor;
            logInButton.interactable = false;
        }
        else
        {
            termsAndConditionsCheckmarkObj.SetActive(true);
            logInButtonImg.color = GlobalParameters.enabledButtonColor;
            logInButtonText.color = GlobalParameters.enabledTextColor;
            logInButton.interactable = true;
        }
    }

    #endregion

    #region Show/Hide Login Screen
    public static void ShowTermsAndConditions()
    {
        if (isNullInstance)
            return;

        instance.termsAndConditionsScreenObj.SetActive(true);
    }
    public void ShowTermsAndConditionsScreen()
    {
        termsAndConditionsScreenObj.SetActive(true);
    }

    public void HideTermsAndConditionsScreen()
    {
        termsAndConditionsScreenObj.SetActive(false);
    }
    #endregion

    #region LogIn
    public void LogIn()
    {
        EnterInSystemEvent?.Invoke("Admin", "12345");
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
