﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCamManager : MonoBehaviour
{
    #region Singleton
    private static DisplayCamManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("DisplayCamManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private RawImage displayCamRawImg;
    #endregion

    #region Private Properties
    private RectTransform _displayCamImgRect;
    private RectTransform displayCamImgRect
    {
        get
        {
            if (_displayCamImgRect == null)
                _displayCamImgRect = displayCamRawImg.GetComponent<RectTransform>();

            return _displayCamImgRect;
        }
    }
    #endregion

    #region Private Fields

    private Vector2 _standartSizeDelta;
    private Vector2 standartSizeDelta
    {
        get
        {
            if (_standartSizeDelta == Vector2.zero)
            {
                _standartSizeDelta = GameResolution.GetCurrentMainCanvasResolution();
                if (_standartSizeDelta.x < _standartSizeDelta.y)
                    _standartSizeDelta = new Vector2(_standartSizeDelta.y, _standartSizeDelta.x);
            }

            return _standartSizeDelta;
        }
    }

    private Vector2 horizontalRotation;
    private Vector2 horizontalSize;

    private Vector2 verticalRotation;
    private Vector2 verticalSize;

    private bool displayCamOrientationMode;
    #endregion

    #region Setup
    public static void SetupDisplaySize(DeviceOrientation currentDeviceOrientation)
    {
        instance.displayCamImgRect.anchorMin = new Vector2(0.5f, 0.5f);
        instance.displayCamImgRect.anchorMax = new Vector2(0.5f, 0.5f);

#if UNITY_STANDALONE
        instance.displayCamImgRect.sizeDelta = new Vector2(instance.standartSizeDelta.x / 2f, instance.standartSizeDelta.y);
#else
        if (currentDeviceOrientation == DeviceOrientation.Portrait || currentDeviceOrientation == DeviceOrientation.PortraitUpsideDown)
            instance.displayCamImgRect.sizeDelta = new Vector2(instance.standartSizeDelta.y, instance.standartSizeDelta.x);
        else
            instance.displayCamImgRect.sizeDelta = instance.standartSizeDelta;
#endif
    }

    #endregion

    #region Display Camera Orientation
    public static void SetCameraDisplayOrientation(DeviceOrientation oponentDeviceOrientation)
    {
        if (isNullInstance)
            return;

#if UNITY_STANDALONE || UNITY_EDITOR
        instance.SetStandaloneCameraDisplayOrientation(oponentDeviceOrientation);
#else
        instance.SetMobileCameraDisplayOrientation(oponentDeviceOrientation);
#endif
    }

    private void SetStandaloneCameraDisplayOrientation(DeviceOrientation oponentDeviceOrientation)
    {
        if (oponentDeviceOrientation == DeviceOrientation.Portrait)
        {
            displayCamImgRect.sizeDelta = new Vector2(instance.standartSizeDelta.x / 2f, instance.standartSizeDelta.y);
        }
        else
        {
            displayCamImgRect.sizeDelta = new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
        }
    }

    private void SetMobileCameraDisplayOrientation(DeviceOrientation oponentDeviceOrientation)
    { // метод для переворота RawImage дисплей камеры в соответствении с ориентацией опонента, при учёте нынешней ориентации
        if (Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            Debug.Log("Current device orientation is Portret");
            switch (oponentDeviceOrientation)
            {
                case DeviceOrientation.LandscapeLeft:
                    {
                        Debug.Log("Oponent device orientation is HorizontalLeft");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, -90f);
                    }
                    break;

                case DeviceOrientation.LandscapeRight:
                    {
                        Debug.Log("Oponent device orientation is HorizontalRight");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 90f);
                    }
                    break;

                default:
                    {
                        Debug.Log("Oponent device orientation is Vertical");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.y, instance.standartSizeDelta.x);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 180f);
                    }
                    break;
            }
        }
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        {
            Debug.Log("Current device orientation is LandscapeLeft");
            switch (oponentDeviceOrientation)
            {
                case DeviceOrientation.LandscapeLeft:
                    {
                        Debug.Log("Oponent device orientation is HorizontalLeft");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 180f);
                    }
                    break;

                case DeviceOrientation.LandscapeRight:
                    {
                        Debug.Log("Oponent device orientation is HorizontalRight");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 0f);
                    }
                    break;

                default:
                    {
                        Debug.Log("Oponent device orientation is Vertical");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.y, instance.standartSizeDelta.x);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 90f);
                    }
                    break;
            }
        }
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            Debug.Log("Current device orientation is LandscapeRight");
            switch (oponentDeviceOrientation)
            {
                case DeviceOrientation.LandscapeLeft:
                    {
                        Debug.Log("Oponent device orientation is HorizontalLeft");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 0f);
                    }
                    break;

                case DeviceOrientation.LandscapeRight:
                    {
                        Debug.Log("Oponent device orientation is HorizontalRight");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.x, instance.standartSizeDelta.y);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, 180f);
                    }
                    break;

                default:
                    {
                        Debug.Log("Oponent device orientation is Vertical");
                        displayCamImgRect.sizeDelta =
                            new Vector2(instance.standartSizeDelta.y, instance.standartSizeDelta.x);
                        displayCamImgRect.localEulerAngles =
                            new Vector3(instance.displayCamImgRect.localEulerAngles.x, instance.displayCamImgRect.localEulerAngles.y, -90f);
                    }
                    break;
            }
        }
    }
    #endregion

    #region Agora VideoSurface Part
    public static void EnableDisplayCam(uint id)
    {
        if (isNullInstance || !AgoraAdapter.IsJoinedToChannel())
            return;

        Debug.Log("ANOTHER USER CONNECTED");
        agora_gaming_rtc.VideoSurface oldVideoSurface = instance.displayCamRawImg.gameObject.GetComponent<agora_gaming_rtc.VideoSurface>();
        if (oldVideoSurface != null)
        {
            oldVideoSurface.SetEnable(false);
            instance.displayCamRawImg.gameObject.SetActive(false);
            instance.displayCamRawImg.texture = null;
            Destroy(oldVideoSurface);
        }

        instance.StartCoroutine(instance.LateDisplayEnable(id));
    }

    private IEnumerator LateDisplayEnable(uint id)
    {
        yield return null;
        displayCamRawImg.gameObject.SetActive(true);
        agora_gaming_rtc.VideoSurface newVideoSurface = displayCamRawImg.gameObject.AddComponent<agora_gaming_rtc.VideoSurface>();
        newVideoSurface.SetEnable(true);
        newVideoSurface.SetVideoSurfaceType(agora_gaming_rtc.AgoraVideoSurfaceType.RawImage);
        newVideoSurface.SetForUser(id);
    }

    private void DisableDisplayCam()
    {
        Debug.Log("ANOTHER USER DISCONNECTED");
        agora_gaming_rtc.VideoSurface videoSurface = displayCamRawImg.gameObject.GetComponent<agora_gaming_rtc.VideoSurface>();
    }

    private void ManualDisalbeDisplayCam()
    {
        DisableDisplayCam();
    }
    #endregion

    private void OnApplicationQuit()
    {
        ManualDisalbeDisplayCam();
    }
}
