﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Singleton
    private static AudioManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("AudioManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private AudioSource screenshotSource;
    [SerializeField]
    private AudioSource callingSource;
    [SerializeField]
    private AudioSource buttonSource;
    #endregion

    public static void PlayScreenshotSound()
    {
        instance.screenshotSource.Play();
    }

    public static void PlayCallingSound()
    {
        instance.callingSource.Play();
    }

    public static void StopCallingSound()
    {
        instance.callingSource.Stop();
    }

    public void PlayButtonSound()
    {
        buttonSource.Play();
    }

    public static void PlayButtonSound(bool fake = false)
    {
        Debug.Log("PlayButtonSound");
        instance.buttonSource.Play();
    }
}
