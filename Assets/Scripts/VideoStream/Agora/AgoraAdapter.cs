﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using agora_gaming_rtc;

/// <summary>
/// Скрипт, синглтон, отвечает за удобное взаимодействие с Agora API
/// </summary>
public class AgoraAdapter : MonoBehaviour
{
    #region Singleton
    private static AgoraAdapter instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("AgoraAdapter instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Private Properties
    private bool isNullRtcEngine
    {
        get
        {
            if (iRtcEngine == null)
            {
                string scriptName = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                Debug.LogError("Agora Engine not initialized!");
                return true;
            }
            return false;
        }
    }
    #endregion

    #region Private Fields
    private IRtcEngine iRtcEngine;

    private System.Action<uint> JoinChannelSuccessEvent;

    private System.Action LeaveChannelEvent;
    private System.Action<uint> UserJoinedEvent;
    private System.Action UserLeftEvent;

    private const string agoraAppID = "5452e9f7376b4afe92cae1b6f0389bbb";

    private bool isConnectedToChannel;
    #endregion

    #region Agora API Part
    public static bool InitializeEngine()
    {
        if (isNullInstance)
            return false;

        instance.iRtcEngine = IRtcEngine.GetEngine(agoraAppID);
        instance.iRtcEngine.SetLogFilter(LOG_FILTER.ERROR);

        VideoEncoderConfiguration config = new VideoEncoderConfiguration();
        config.minBitrate = 10000;
        config.bitrate = 100000;
        config.degradationPreference = DEGRADATION_PREFERENCE.MAINTAIN_QUALITY;
        config.dimensions.width = 1920;
        config.dimensions.height = 1080;
        config.orientationMode = ORIENTATION_MODE.ORIENTATION_MODE_ADAPTIVE;
        instance.iRtcEngine.SetVideoEncoderConfiguration(config);

        instance.iRtcEngine.OnJoinChannelSuccess += instance.OnJoinChannelSuccessHandler;
        instance.iRtcEngine.OnUserJoined += instance.OnUserJoinedHandler;
        instance.iRtcEngine.OnUserOffline += instance.OnUserOfflineHandler;
        instance.iRtcEngine.OnLeaveChannel += instance.OnLeaveChannelHandler;
        instance.iRtcEngine.OnError += instance.OnErrorHandler;

        return true;
    }

    public static bool JoinToChannel(string channelName)
    {
        if (isNullInstance)
            return false;

        if (instance.isNullRtcEngine)
            return false;

        Debug.Log(string.Format("Trying JoinToChannel {0} ...", channelName));

        instance.iRtcEngine.EnableVideo();
        instance.iRtcEngine.EnableVideoObserver();
        uint userID = (uint)Random.Range(0, int.MaxValue / 2);
        int indexResult = instance.iRtcEngine.JoinChannel(channelName, null, userID);

        if (indexResult < 0)
        {
            switch (indexResult)
            {
                case -2:
                    Debug.Log("JoinChannel failed due INVALID_ARGUMENT");
                    break;
                case -3:
                    Debug.Log("JoinChannel failed due NOT_READY");
                    break;
                case -5:
                    Debug.Log("JoinChannel failed due REFUSED");
                    break;
                default:
                    Debug.Log("JoinChannel failed due UNKNOWN");
                    break;
            }
            return false;
        }

        return true;
    }

    public static bool LeaveFromCurrentChannel()
    {
        if (isNullInstance)
            return false;

        if (instance.isNullRtcEngine)
            return false;

        if (!IsJoinedToChannel())
        {
            CustomLogger.LogMessage("You are is not connected to the channel, so you cant leave!");
            return false;
        }

#if !UNITY_EDITOR
        CustomLogger.LogMessage("You are leaving from this channel...");
        instance.iRtcEngine.LeaveChannel();
#endif
        return true;
    }

    public void TerminateEngine()
    {
        if (isNullRtcEngine)
            return;

        iRtcEngine.LeaveChannel();
        iRtcEngine.DisableVideo();
        iRtcEngine.DisableVideoObserver();

        IRtcEngine.Destroy();
        iRtcEngine = null;
    }

    public void SwitchCamera()
    {
        if (isNullRtcEngine)
            return;

        Debug.Log("Try to switch camera");
        instance.iRtcEngine.SwitchCamera();
    }
    #endregion

    #region Add Event Listiners
    public static void AddJoinChannelSuccessEventListiner(System.Action<uint> eventListiner)
    {
        if (isNullInstance)
            return;

        instance.JoinChannelSuccessEvent += eventListiner;
    }
    public static void AddJoinChannelSuccessEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.JoinChannelSuccessEvent += (temp) => eventListiner();
    }

    public static void AddLeaveChannelEventListiner(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.LeaveChannelEvent += eventListiner;
    }

    public static void AddUserJoinedEventListiner(System.Action<uint> eventListiner)
    {
        if (instance == null)
        {
            Debug.LogError("AgoraAdapter instance is null!");
            return;
        }
        instance.UserJoinedEvent += eventListiner;
    }
    #endregion

    #region Event Listiners
    private void OnJoinChannelSuccessHandler(string channelName, uint uid, int elapsed)
    {
        Debug.Log("You are " + uid + " is Joined to channel: " + channelName);
        JoinChannelSuccessEvent?.Invoke(uid);

        instance.isConnectedToChannel = true;
    }

    private void OnUserJoinedHandler(uint uid, int elapsed)
    {
        Debug.Log("User " + uid + " is Joined to this channel.");

        UserJoinedEvent?.Invoke(uid);
    }

    private void OnUserOfflineHandler(uint uid, USER_OFFLINE_REASON reason)
    {
        Debug.Log("User " + uid + " has left this channel by reason: " + reason.ToString());

        UserLeftEvent?.Invoke();
    }

    private void OnLeaveChannelHandler(RtcStats stats)
    {
        Debug.Log("You has left this channel");

        LeaveChannelEvent?.Invoke();

        instance.isConnectedToChannel = false;
    }

    private void OnErrorHandler(int error, string msg)
    {
        Debug.LogError("Agora error number: " + error + " with message:\n" +
            msg);
    }
#endregion
    
    #region Check Connection
    public static bool IsJoinedToChannel()
    {
        if (isNullInstance)
            return false;

        bool isConnected = instance.iRtcEngine.GetConnectionState() ==
             CONNECTION_STATE_TYPE.CONNECTION_STATE_CONNECTED;

        if (!isConnected)
            return false;

        return instance.isConnectedToChannel;
    }
#endregion

    private void OnApplicationQuit()
    {
        TerminateEngine();
    }
}
