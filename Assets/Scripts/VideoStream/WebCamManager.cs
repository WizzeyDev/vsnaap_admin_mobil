﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WebCamManager : MonoBehaviour
{
    #region Singleton
    private static WebCamManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
                string scriptName = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                Debug.LogError(scriptName + " instance not found!");
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            DestroyImmediate(this);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private RawImage webCamRawImg;
    #endregion

    #region Private Fields
    private RectTransform webCamImageRect;

    private Vector2 verticalWebcamSize = new Vector2(250f, 350f);
    private Vector2 horizontalWebcamSize = new Vector2(400f, 250f);

    private Vector2 verticalWebcamPosition = new Vector2(-85f, -440f);
    private Vector2 horizontalWebcamPosition = new Vector2(-75f, -320f);
    #endregion

    #region Setup
    private void Start()
    {
        webCamImageRect = webCamRawImg.GetComponent<RectTransform>();

        AgoraAdapter.AddJoinChannelSuccessEventListiner(EnableWebCam);
        AgoraAdapter.AddLeaveChannelEventListiner(DisableWebCam);
    }
    #endregion

    #region Update Orientation Part
    public static void ChangeCameraOrientationImage(DeviceOrientation newDeviceOrientation)
    { // единственный метод для переворота RawImage веб камеры
        if (newDeviceOrientation == DeviceOrientation.Portrait || newDeviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            instance.webCamImageRect.sizeDelta = instance.verticalWebcamSize;
            instance.webCamImageRect.anchoredPosition = instance.verticalWebcamPosition;
        }
        else // не else if, по скольку DeviceOrientationManager уже отмел ненужные ориентации смартфона
        {
            instance.webCamImageRect.sizeDelta = instance.horizontalWebcamSize;
            instance.webCamImageRect.anchoredPosition = instance.horizontalWebcamPosition;
        }
    }
    #endregion

    #region Agora VideoSurface Part
    private void EnableWebCam(uint id)
    {
        Debug.Log("I IS CONNECTED");
        webCamRawImg.gameObject.SetActive(true);
        agora_gaming_rtc.VideoSurface videoSurface = webCamRawImg.gameObject.GetComponent<agora_gaming_rtc.VideoSurface>();
        videoSurface.SetEnable(true);
        videoSurface.SetVideoSurfaceType(agora_gaming_rtc.AgoraVideoSurfaceType.RawImage);
        videoSurface.SetForUser(0);
    }

    private void DisableWebCam()
    {
        Debug.Log("ANOTHER USER DISCONNECTED");
        agora_gaming_rtc.VideoSurface videoSurface = webCamRawImg.gameObject.GetComponent<agora_gaming_rtc.VideoSurface>();
        videoSurface.SetEnable(false);
        webCamRawImg.gameObject.SetActive(false);
    }

    private void ManualDisalbeWebCam()
    {
        DisableWebCam();
    }
    #endregion

    private void OnApplicationQuit()
    {
        ManualDisalbeWebCam();
    }
}
