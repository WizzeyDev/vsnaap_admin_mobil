﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOrientationManager : MonoBehaviour
{
    #region Singleton
    private static DeviceOrientationManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
                string scriptName = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
                Debug.LogError(scriptName + " instance not found!");
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            DestroyImmediate(this);
            return;
        }
    }
    #endregion

    #region Private Fields
    private DeviceOrientation lastDeviceOrientation;
    private DeviceOrientation lastOponentDeviceOrientation;
    #endregion

    #region Setup 
    private void Start()
    {
#if UNITY_STANDALONE || UNITY_EDITOR
        SetupStandaloneDisplay();
#else
        SetupMobileDisplay();
#endif
    }

    private void SetupMobileDisplay()
    {
        DisplayCamManager.SetupDisplaySize(Input.deviceOrientation);
    }
    private void SetupStandaloneDisplay()
    {
        DisplayCamManager.SetupDisplaySize(DeviceOrientation.LandscapeLeft);
    }
    #endregion

#region Monitor Device Rotation
#if !UNITY_STANDALONE
    void Update() // отслеживает изменения в ориентации смартфона,
    { // отправляю эти данные на рассылку через бэкенд и изменяя RawImage для камеры опонента, а так же меняя RawImage для отображения веб камеры
        DeviceOrientation currentDeviceOrientation = Input.deviceOrientation;
        if (currentDeviceOrientation == DeviceOrientation.Unknown ||
            currentDeviceOrientation == DeviceOrientation.FaceUp ||
            currentDeviceOrientation == DeviceOrientation.FaceDown)
            return; // не реагирует на перечисленные смены ориентации смартфона

        if(currentDeviceOrientation != lastDeviceOrientation)
        {
            //WebCamManager.ChangeCameraOrientationImage(currentDeviceOrientation);
            //PlayerIOManager.SendOrientationStatus(currentDeviceOrientation);
            CustomLogger.LogMessage("Update Caller...");
            DisplayCamManager.SetCameraDisplayOrientation(lastOponentDeviceOrientation);
        }
        lastDeviceOrientation = currentDeviceOrientation;
    }
#endif

//    private void BroadcastCurrentOrientationStatus()
//    {
//#if UNITY_STANDALONE
//        PlayerIOManager.SendOrientationStatus(DeviceOrientation.LandscapeLeft);
//#else
//        PlayerIOManager.SendOrientationStatus(Input.deviceOrientation);
//#endif
//    }

    public static void SetCameraDisplayOrientation(DeviceOrientation oponentDeviceOrientation)
    {
        if (isNullInstance)
            return;

        CustomLogger.LogMessage("SetCameraDisplayOrientation Caller...");
        DisplayCamManager.SetCameraDisplayOrientation(oponentDeviceOrientation);
        instance.lastOponentDeviceOrientation = oponentDeviceOrientation;
    }
#endregion
}
