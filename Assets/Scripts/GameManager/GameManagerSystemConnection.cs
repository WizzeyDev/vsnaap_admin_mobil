﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour
{
    private class GameManagerSystemConnection
    {
        private Coroutine reconnectCoroutine;

        #region Connection Failed
        public void StopConnection(string reason)
        {
            CustomLogger.LogMessage(string.Format("StopConnection by reason: {0}!", reason));

            if (instance.waitForInitializeUserCoroutine != null)
            {
                instance.StopCoroutine(instance.waitForInitializeUserCoroutine);
                instance.waitForInitializeUserCoroutine = null;
            }
            if (reconnectCoroutine != null)
            {
                instance.StopCoroutine(reconnectCoroutine);
                reconnectCoroutine = null;
            }
            PlayerIOServerManager.StopConnectionToServer();
            PlayFabManager.ManualDisconnect();
        }
        #endregion

        #region Reconnection
        public void LostConnection()
        {
            if (isNullInstance)
                return;

            CustomLogger.LogMessage("LostConnection(0): Trying to reconnect...");

            StopConnection("LostConnection");

            AdminBlockListScreen.DestroyAllAppraiserBlockList();
            AdminBlockListScreen.DestroyAllGarageBlockList();
            GaragesListScreen.DestroyAllGaragesList();
            AppraiserListScreen.DestroyAllAppraisersList();
            EmailsListScreen.DestroyAllEmailsList();

            CardCreationScreen.UnlockBusy();
            CardSearchScreen.UnlockBusy();
            CardChangerScreen.UnlockBusy();
            GaragesListScreen.UnlockBusy();
            SearchResultScreen.UnlockBusy();
            AppraiserResultScreen.UnlockBusy();
            GarageResultScreen.UnlockBusy();
            AppraisersSearchScreen.UnlockBusy();
            GaragesSearchScreen.UnlockBusy();

            instance.isInSystem = false;

            if (reconnectCoroutine != null)
            {
                CustomLogger.LogMessage("reconnectCoroutine was restarted!");
                instance.StopCoroutine(reconnectCoroutine);
            }
            reconnectCoroutine = instance.StartCoroutine(ReconnectionCoroutine());
        }

        private IEnumerator ReconnectionCoroutine()
        {
            CustomLogger.LogMessage("LostConnection(1): Start ReconnectionCoroutine...");

            LoginScreen.ShowLoginScreen();
            instance.systemEnter.EnterInSystem(instance.lastAppraiserLogin, instance.lastAppraiserPassword);
            LoadingScreen.ShowLoadingScreen(instance.systemExit.ExitFromSystem);

            yield return new WaitUntil(() => instance.isInSystem);
            CustomLogger.LogMessage("LostConnection(2-E): Reconnection Success!");

            yield return null;
            LoadingScreen.HideLoadingScreen();
        }
        #endregion
    }
}
