﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public partial class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("GameManager instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    #endregion

    #region Private Fields
    private GameManagerSystemConnection systemConnection;
    private GameManagerSystemEnter systemEnter;
    private GameManagerSystemExit systemExit;

    private Coroutine waitForInitializeUserCoroutine;

    private System.Action LoginSystemEvent;

    private string lastAppraiserLogin, lastAppraiserPassword;
    private string userDisplayName;

    private bool permissionsGranted;
    private bool isInSystem;
    #endregion

    #region Setup
    private void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
#if !UNITY_EDITOR
        StartCoroutine(WaitForAllPermissions());
#endif
        systemConnection = new GameManagerSystemConnection();
        systemEnter = new GameManagerSystemEnter();
        systemExit = new GameManagerSystemExit();

        PlayerIOServerManager.AddConnectionSuccessEventListiner(systemEnter.ConnectionToServerSuccess);
        PlayFabManager.AddAuthenticateEventListiner(systemEnter.ConnectionToPlayFabSuccess);

        LoginScreen.AddEnterInSystemEventListiner(systemEnter.EnterInSystem);
        MenuPanelManager.AddExitFromSystemEventListiner(systemExit.ExitFromSystem);

        PlayFabManager.AddLostConnectionEventListiner(systemConnection.LostConnection);
        PlayFabManager.AddConnectionFailedEventListiner(systemExit.ConnectionToSystemFailed);
        PlayFabManager.AddDownloadLogFileSuccess(systemEnter.DownloadLogFileSuccess);

        PlayerIOServerManager.AddLostConnectionEventListiner(systemConnection.LostConnection);
        PlayerIOServerManager.AddConnectionFailedEventListiner(systemExit.ConnectionToSystemFailed);
    }

    private IEnumerator WaitForAllPermissions()
    {
        yield return null;

        var versionClazz = new AndroidJavaClass("android.os.Build$VERSION");
        var apiLevel = versionClazz.GetStatic<int>("SDK_INT");
        CustomLogger.LogMessage("Current API version is " + apiLevel);
        if (apiLevel > 23)
        {
            CustomLogger.LogMessage("Try to check Permissions");
            while (true)
            {
                if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
                    Permission.RequestUserPermission(Permission.Microphone);
                else
                    break;

                yield return new WaitForSeconds(1f);
            }

            while (true)
            {
                if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
                    Permission.RequestUserPermission(Permission.Camera);
                else
                    break;

                yield return new WaitForSeconds(1f);
            }
        }

        if (!AgoraAdapter.InitializeEngine())
        {
            CustomLogger.LogErrorMessage("Can't initialize Agora Engine");
            yield break;
        }
        else
        {
            permissionsGranted = true;
            CustomLogger.LogMessage("Initialize Agora Engine Success!");
        }
    }
    #endregion

    #region Add Event Handlers
    public static void AddLoginSystemEventHandler(System.Action eventListiner)
    {
        if (isNullInstance)
            return;

        instance.LoginSystemEvent += eventListiner;
    }
    #endregion

    #region Setters
    public static void SetUserDisplayName(string garageDisplayName)
    {
        if (isNullInstance)
            return;

        instance.userDisplayName = garageDisplayName;
        MenuPanelManager.SetAppraiserDisplayName(garageDisplayName);
    }
    #endregion

    #region Getters
    public static string GetUserDisplayName()
    {
        if (isNullInstance)
            return string.Empty;

        return instance.userDisplayName;
    }
    public static int IsPermissionsGranted()
    {
        if (isNullInstance)
            return 0;

#if UNITY_EDITOR
        return 1;
#else
        return instance.permissionsGranted ? 1 : -1;
#endif
    }
#endregion
    
    #region Close Application Part
    public void Quit()
    {
        Application.Quit();
    }
#endregion
}
