﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour
{
    private class GameManagerSystemExit
    {
        public void ExitFromSystem()
        {
            if (isNullInstance)
                return;

            LoadingScreen.HideLoadingScreen();
            LoginScreen.ShowLoginScreen();

            //AdminBlockListScreen.DestroyAllAppraiserBlockList(); // очищает и удаляет список блоков, ЕСЛИ он есть
            AdminBlockListScreen.DestroyAllGarageBlockList(); // очищает и удаляет список блоков, ЕСЛИ он есть
            GaragesListScreen.DestroyAllGaragesList(); // очищает и удаляет список СТО, ЕСЛИ он есть
            //AppraiserListScreen.DestroyAllAppraisersList(); // очищает и удаляет список Оценщиков, ЕСЛИ он есть
            EmailsListScreen.DestroyAllEmailsList(); // очищает и удаляет список имейлов, ЕСЛИ он есть

            CardCreationScreen.UnlockBusy();
            CardSearchScreen.UnlockBusy();
            CardChangerScreen.UnlockBusy();
            GaragesListScreen.UnlockBusy();
            SearchResultScreen.UnlockBusy();
            AppraiserResultScreen.UnlockBusy();
            GarageResultScreen.UnlockBusy();
            AppraisersSearchScreen.UnlockBusy();
            GaragesSearchScreen.UnlockBusy();

            instance.isInSystem = false;
            CustomLogger.LogMessage("isInSystem false");
            //instance.systemConnection.StopConnection("ExitFromSystem");
        }

        public void ConnectionToSystemFailed()
        {
            if (isNullInstance)
                return;

            CustomLogger.LogMessage("EnterInSystem(5-E): ConnectionToSystemFailed!");

            ExitFromSystem();
            LoadingScreen.HideLoadingScreen();
        }
    }
}
