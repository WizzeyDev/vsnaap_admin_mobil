﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour
{
    private class GameManagerSystemEnter
    {
        #region Private Fields
        private bool isLogFileLoaded;
        #endregion
        public void EnterInSystem(string appraiserLogin, string appraiserPassword)
        {
            if (isNullInstance)
                return;

            CustomLogger.LogMessage("EnterInSystem(0): start connecting...");

            isLogFileLoaded = false;

            instance.lastAppraiserLogin = appraiserLogin;
            instance.lastAppraiserPassword = appraiserPassword;

            LoadingScreen.ShowLoadingScreen(() =>
            {
                instance.systemConnection.StopConnection("Manual");
            });
            PlayerIOServerManager.ConnectToServer(appraiserLogin, appraiserPassword);
        }

        public void ConnectionToServerSuccess()
        {
            CustomLogger.LogMessage("EnterInSystem(1): ConntetionToServerSuccess!");
            if (instance.waitForInitializeUserCoroutine != null)
            {
                CustomLogger.LogMessage("waitForInitializeUserCoroutine was restarted!");
                instance.StopCoroutine(instance.waitForInitializeUserCoroutine);
            }
            instance.waitForInitializeUserCoroutine = instance.StartCoroutine(WaitForInitializeUser());
        }

        private IEnumerator WaitForInitializeUser()
        {
            CustomLogger.LogMessage("EnterInSystem(2): Trying to get 'userDisplayName'from server...");
            yield return new WaitUntil(() => !string.IsNullOrEmpty(instance.userDisplayName));
            instance.waitForInitializeUserCoroutine = null;

            CustomLogger.LogMessage(string.Format("EnterInSystem(3): Get 'userDisplayName' {0} success!",
                      instance.userDisplayName));
            CustomLogger.LogMessage(string.Format("EnterInSystem(4): Trying to Login in PlayFab with customID: {0}...",
                      instance.lastAppraiserLogin));
            PlayFabManager.PlayFabLogin(instance.lastAppraiserLogin);
        }

        public void ConnectionToPlayFabSuccess()
        {
            instance.StartCoroutine(LoginFinishing());
        }

        private IEnumerator LoginFinishing()
        {
            if (string.IsNullOrEmpty(instance.userDisplayName))
            {
                CustomLogger.LogErrorMessage("ConnectionToPlayFabSuccess 'userDisplayName' is empty!");
                yield break;
            }
            PlayFabManager.ChangeDisplayName(instance.userDisplayName);

            CustomLogger.LogMessage("EnterInSystem(5): PlayerIO Send SetPlayFabID...");
            PlayerIOServerManager.SendSetPlayFabID(PlayFabManager.GetPlayFabID());

            CustomLogger.LogMessage("EnterInSystem(6): Handle LoginSystemEvent...");
            instance.LoginSystemEvent?.Invoke();

            yield return new WaitUntil(() => isLogFileLoaded);
            CustomLogger.LogMessage("EnterInSystem(7-E): Success!");
            LoadingScreen.HideLoadingScreen();
            LoginScreen.HideLoginScreen();

            PlayerPrefs.SetString("AdminAutoLogin", "Admin\\" + "12345");

            instance.isInSystem = true;
        }

        public void DownloadLogFileSuccess(string filePath)
        {
            isLogFileLoaded = true;
        }
    }
}
