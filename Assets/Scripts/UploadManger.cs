﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UploadManger : MonoBehaviour
{

    int interval = 5, lastTimeChecked;

    bool isDownloading;

    static UploadManger instance;

    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup - interval > lastTimeChecked)
        {
            CheckDropBoxUpdate();
            lastTimeChecked = (int)Time.realtimeSinceStartup;
        }
    }

    void CheckDropBoxUpdate()
    {
       
        StartCoroutine(DownloadFromDB(new DbDownload(DropBoxTest.dropboxFolderPath + "info.txt","")));
   
    }

    void UpdateOrderList()
    {
        //TODO
        //DataBaseController.DownloadDB();
        DataBaseController.ToggleAllDBConn(false);
        for (int i = 0; i < DataBaseController.fileName.Length; i++)
        {
            StartCoroutine(DownloadFromDB(new DbDownload(DropBoxTest.dropboxFolderPath + DataBaseController.fileName[i], DataBaseController.mDbPaths[i])));
        }       
    }

    IEnumerator DownloadFromDB(DbDownload dbDownload)
    {
        Debug.Log("Started Download static?");
        instance.isDownloading = true;
        var request = DropBoxTest.DownloadFromDropboxAndSave(dbDownload.filePath);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                //TODO: Create File and Check who changed it last
                if (dbDownload.localPath == "")
                {

                Debug.Log("Downloaded Data: " + System.Text.Encoding.UTF8.GetString(request.downloadHandler.data));
                }
                else
                {
                    System.IO.File.WriteAllBytes(dbDownload.localPath, request.downloadHandler.data);
                    Debug.Log("Downloaded Data:  Writeing to file " + dbDownload.localPath);
                }
                //System.IO.File.WriteAllBytes(local_target_path + fuckingSlash + metadata_name_format, request.downloadHandler.data);
                break;
            default:

                Debug.LogError("DownloadFromDBToString Error " + request.downloadHandler.text);


                break;
        }
        instance.isDownloading = false;
    }
}

public class DbDownload
{
    public string filePath, localPath;

    public DbDownload(string path, string Path2)
    {
        filePath = path;
            localPath = Path2;
    }
}
