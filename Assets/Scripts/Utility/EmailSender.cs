﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public static class EmailSender
{
    public static string e_mail = "vsnaapservice@gmail.com";
    public static string password = "vsnaap12345";

    public static void SendDocumentUrls(string email, string subject, Dictionary<string, GlobalDocuments.DocumentType> documentUrls)
    {
        try
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(e_mail);
            mail.To.Add(email);
            mail.Subject = subject;
            mail.IsBodyHtml = true;

            foreach(var documentData in documentUrls)
            {
                switch (documentData.Value)
                {
                    case GlobalDocuments.DocumentType.Excel:
                        {
                            mail.Body += string.Format(@"<a href=""{0}""><img src=""{1}""/></a>",
                                documentData.Key, GlobalDocuments.excelImageUrl);
                        }
                        break;

                    case GlobalDocuments.DocumentType.Word:
                        {
                            mail.Body += string.Format(@"<a href=""{0}""><img src=""{1}""/></a>",
                                documentData.Key, GlobalDocuments.wordImageUrl);
                        }
                        break;

                    case GlobalDocuments.DocumentType.PowerPoint:
                        {
                            mail.Body += string.Format(@"<a href=""{0}""><img src=""{1}""/></a>",
                                documentData.Key, GlobalDocuments.powerPointImageUrl);
                        }
                        break;

                    case GlobalDocuments.DocumentType.PDF:
                        {
                            mail.Body += string.Format(@"<a href=""{0}""><img src=""{1}""/></a>",
                                documentData.Key, GlobalDocuments.pdfImageUrl);
                        }
                        break;

                    case GlobalDocuments.DocumentType.Photo:
                        {
                            mail.Body += string.Format(@"<a href=""{0}""><img src=""{0}""/></a>",
                                documentData.Key);
                        }
                        break;
                }

                mail.Body += "\n";
            }



            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential(e_mail, password) as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            smtpServer.Send(mail);
        }
        catch(System.Exception ex)
        {
            Debug.LogError("Error while sending email:\n" +
                ex.Message);
        }
    }
}
