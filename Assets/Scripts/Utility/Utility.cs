﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class Utility 
{

    public static string GeneratePhoneNumber() {
        string r = "05";
        int i;
        for (i = 1; i < 9; i++) {
            r += Random.Range(0, 9);
        }       
        return r;
    }


   public static string AssingFilePath(string fileName) {
        string fileDestinastion = "";
#if UNITY_ANDROID
         fileDestinastion = Application.persistentDataPath + fileName;

#endif
#if UNITY_EDITOR || UNITY_STANDALONE
        fileDestinastion = Application.dataPath + fileName;
#endif
        return fileDestinastion;
    }


    public static string CreateFilePath(string fileName) {
        //string fileDestinastion = AssingFilePath(fileName);
        fileName = AssingFilePath(fileName);
        if (!File.Exists(fileName)) {
           
            FileStream d = File.Create(fileName);
            d.Close();
            Debug.Log("CreateFilePath " + fileName);
        }
        else
        {

        Debug.Log("File exsists " + fileName);
        }
        return fileName;
    }

    public static string MoveFile(string filePath,string newFileName) {
        //if (File.Exists(newFileName)) {
        //    File.Delete(newFileName);
        //}
        if (File.Exists(filePath)) {
            File.Move(filePath, newFileName);
            return newFileName;
        }
        return null;
    }

    public static bool CheckIfFileExist(string filePath) {
        return File.Exists(filePath);
    }
}
