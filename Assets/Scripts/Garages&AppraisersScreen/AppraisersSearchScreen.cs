﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AppraisersSearchScreen : MonoBehaviour
{
    #region Singleton
    private static AppraisersSearchScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("AppraisersSearchScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject appraisersSearchScreen;
    [SerializeField]
    private GameObject createAppraiserWindowObj;

    [Space(10)]
    [SerializeField]
    private ChooseBar displayNameFilter;
    [SerializeField]
    private ChooseBar areaFilter;
    [SerializeField]
    private ChooseBar cityFilter;

    [Space(10)]
    [SerializeField]
    private InputField loginInputField;
    [SerializeField]
    private InputField displayNameInputField;

    [SerializeField]
    private InputField[] allAppraiserCreationInputFields;
    #endregion

    #region Private Fields
    private bool isBusy;
    #endregion

    #region Show/Hide Appraisers Search Screen
    public static void ShowAppraisersSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowAppraiserSearchScreen();
    }
    public void ShowAppraiserSearchScreen()
    {
        appraisersSearchScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(appraisersSearchScreen.transform);

        displayNameFilter.currentElementName.text = string.Empty;
        areaFilter.currentElementName.text = string.Empty;
        cityFilter.currentElementName.text = string.Empty;
    }

    public static void HideAppraisersSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.HideAppraiserSearchScreen();
    }
    public void HideAppraiserSearchScreen()
    {
        appraisersSearchScreen.SetActive(false);
        HideCreateAppraiserWindow();
    }
    #endregion

    #region Filter
    public void ConfirmFilters()
    {
        if (string.IsNullOrEmpty(displayNameFilter.currentElementName.text) &&
            string.IsNullOrEmpty(areaFilter.currentElementName.text) &&
            string.IsNullOrEmpty(cityFilter.currentElementName.text))
        {

            StartCoroutine(HighlighInputField(displayNameFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(areaFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(cityFilter.currentElementName.image));
            return;
        }

        string displayName = displayNameFilter.currentElementName.text;
        string area = areaFilter.currentElementName.text;
        string city = cityFilter.currentElementName.text;

        List<AppraiserElement> allAppraisers = AppraiserListScreen.GetAllAppraisers();
        for (int i = 0; i < allAppraisers.Count; ++i)
        {
            if (allAppraisers[i].GetAppraiserName() == "dVA")
            {
                allAppraisers.RemoveAt(i--);
                continue;
            }

            if (!string.IsNullOrEmpty(displayName))
            {
                if (allAppraisers[i].GetDisplayName() != displayName)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(area))
            {
                if (allAppraisers[i].GetArea() != area)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(city))
            {
                if (allAppraisers[i].GetAdress() != city)
                {
                    allAppraisers.RemoveAt(i--);
                    continue;
                }
            }
        }

        AppraiserResultScreen.ShowAppraisersResultScreen(allAppraisers);
    }
    public static void AddDisplayNameFilterElement(string newDisplayName)
    {
        if (isNullInstance)
            return;

       if (!instance.displayNameFilter.elementNames.Contains(newDisplayName))
       {
            instance.displayNameFilter.elementNames.Add(newDisplayName);
       }
    }

    public static void AddAreaFilterElement(string newArea)
    {
        if (isNullInstance)
            return;

        if (!instance.areaFilter.elementNames.Contains(newArea))
        {
            instance.areaFilter.elementNames.Add(newArea);
        }
    }

    public static void AddCityFilterElement(string newCity)
    {
        if (isNullInstance)
            return;

        if (!instance.cityFilter.elementNames.Contains(newCity))
        {
            instance.cityFilter.elementNames.Add(newCity);
        }
    }
    #endregion

    #region Create New Appraiser
    public void ShowCreateAppraiserWindow()
    {
        if (isBusy)
            return;

        createAppraiserWindowObj.SetActive(true);

        foreach (InputField field in allAppraiserCreationInputFields)
        {
            field.text = string.Empty;
        }
    }
    public void HideCreateAppraiserWindow()
    {
        createAppraiserWindowObj.SetActive(false);
    }

    public void ConfirmCreateAppraiser() //FIX 1 
    {
        if (isBusy)
            return;

        if (displayNameInputField.text.Length < 3)
        {
            StartCoroutine(HighlighInputField(displayNameInputField.image));
            return;
        }

        if (loginInputField.text.Length < 3)
        {
            StartCoroutine(HighlighInputField(loginInputField.image));
            return;
        }

        foreach (InputField field in allAppraiserCreationInputFields)
        {
            if (string.IsNullOrEmpty(field.text))
            {
                StartCoroutine(HighlighInputField(field.image));
                return;
            }
        }

        isBusy = true;

        PlayFabManager.AddRegistrationSuccessEventListiner(instance.RegistrationPlayFabSuccess);
        PlayFabManager.AddRegistrationFailedEventListiner(instance.RegistrationPlayFabFailed);
        PlayFabManager.RegistrationPlayFabUsername(loginInputField.text,
            displayNameInputField.text);
    }

    private void RegistrationPlayFabSuccess()
    {
        string rawAppraiserCreationData = string.Empty;
        foreach (InputField field in allAppraiserCreationInputFields)
        {
            rawAppraiserCreationData += field.text + "_";
        }

        if (rawAppraiserCreationData.EndsWith("_"))
            rawAppraiserCreationData = rawAppraiserCreationData.Remove(rawAppraiserCreationData.Length - 1, 1);

        PlayerIOServerManager.CreateAppraiser(rawAppraiserCreationData);

        instance.HideCreateAppraiserWindow();
    }

    private void RegistrationPlayFabFailed(string reason)
    {
        isBusy = false;

        PlayerIOServerManager.DeleteAppraiser(instance.allAppraiserCreationInputFields[0].text);
        if (reason == "Username not available")
            StartCoroutine(HighlighInputField(instance.loginInputField.image));
        else if (reason == "The DisplayName entered is not available" || reason == "The display name entered is not available.")
            StartCoroutine(HighlighInputField(instance.displayNameInputField.image));
    }

    public static void CreationAppraiserSuccess(string[] appraiserData)
    {
        if (isNullInstance)
            return;

        string appraiserName = appraiserData[0];
        string password = appraiserData[1];

        string displayName = appraiserData[2];
        string ID = appraiserData[3];
        string officePhone = appraiserData[4];
        string carTax = appraiserData[5];
        string role = appraiserData[6];
        string area = appraiserData[7];
        string adress = appraiserData[8];
        string email = appraiserData[9];
        string isActive = appraiserData[10];
        AppraiserListScreen.AddAppraiserToList(appraiserName, password, displayName, ID, officePhone, carTax,
            role, adress, area, isActive, email);
    }
    public static void CreationCardFailed(bool isAppraiserNameExist)
    {
        if (isNullInstance)
            return;

        //if (isAppraiserNameExist)
        //{
        //    instance.StartCoroutine(instance.HighlighInputField(instance.loginInputField.image));
        //}
        //else
        //{
        //    instance.StartCoroutine(instance.HighlighInputField(instance.displayNameInputField.image));
        //}
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
