﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GaragesAndAppraisersScreen : MonoBehaviour
{
    #region Singleton
    private static GaragesAndAppraisersScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("GaragesAndAppraisersScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject garagesAndAppraisersScreen;
    #endregion

    #region Show/Hide GaragesAndAppraisers Screen
    public static void ShowGaragesAndAppraisersScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowGarageAndAppraiserScreen();
    }
    public void ShowGarageAndAppraiserScreen()
    {
        garagesAndAppraisersScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(garagesAndAppraisersScreen.transform);

        AppraisersSearchScreen.HideAppraisersSearchScreen();
        GaragesSearchScreen.HideGaragesSearchScreen();

        GarageResultScreen.HideGaragesResultScreen();
        AppraiserResultScreen.HideAppraisersResultScreen();
    }

    public void HideGaragesAndAppraisersScreen()
    {
        garagesAndAppraisersScreen.SetActive(false);
    }
    #endregion
}
