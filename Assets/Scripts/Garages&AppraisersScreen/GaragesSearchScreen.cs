﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaragesSearchScreen : MonoBehaviour
{
    #region Singleton
    private static GaragesSearchScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("GaragesSearchScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject garagesSearchScreen;
    [SerializeField]
    private GameObject createGarageWindowObj;
    [SerializeField]
    private ChooseBar displayNameFilter;
    [SerializeField]
    private ChooseBar areaFilter;
    [SerializeField]
    private ChooseBar cityFilter;

    [Space(10)]
    [SerializeField]
    private InputField loginInputField;
    [SerializeField]
    private InputField displayNameInputField;

    [SerializeField]
    private InputField[] allGarageCreationInputFields;
    #endregion

    #region Private Fields
    private bool isBusy;
    #endregion

    #region Show/Hide Garages Search Screen
    public static void ShowGaragesSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowGarageSearchScreen();
    }
    public void ShowGarageSearchScreen()
    {
        garagesSearchScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(garagesSearchScreen.transform);

        displayNameFilter.currentElementName.text = string.Empty;
        areaFilter.currentElementName.text = string.Empty;
        cityFilter.currentElementName.text = string.Empty;
    }

    public static void HideGaragesSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.HideGarageSearchScreen();
    }
    public void HideGarageSearchScreen()
    {
        garagesSearchScreen.SetActive(false);
        HideCreateGarageWindow();
    }
    #endregion

    #region Filter
    public void ConfirmFilters()
    {
        if (string.IsNullOrEmpty(displayNameFilter.currentElementName.text) &&
            string.IsNullOrEmpty(areaFilter.currentElementName.text) &&
            string.IsNullOrEmpty(cityFilter.currentElementName.text))
        {

            StartCoroutine(HighlighInputField(displayNameFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(areaFilter.currentElementName.image));
            StartCoroutine(HighlighInputField(cityFilter.currentElementName.image));
            return;
        }

        string displayName = displayNameFilter.currentElementName.text;
        string area = areaFilter.currentElementName.text;
        string city = cityFilter.currentElementName.text;

        List<GarageElement> allGarages = GaragesListScreen.GetAllAppraisers();
        for (int i = 0; i < allGarages.Count; ++i)
        {
            if (!string.IsNullOrEmpty(displayName))
            {
                if (allGarages[i].GetDisplayName() != displayName)
                {
                    allGarages.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(area))
            {
                if (allGarages[i].GetArea() != area)
                {
                    allGarages.RemoveAt(i--);
                    continue;
                }
            }

            if (!string.IsNullOrEmpty(city))
            {
                if (allGarages[i].GetAdress() != city)
                {
                    allGarages.RemoveAt(i--);
                    continue;
                }
            }
        }

        GarageResultScreen.ShowGaragesResultScreen(allGarages);
    }
    public static void AddDisplayNameFilterElement(string newDisplayName)
    {
        if (isNullInstance)
            return;

        if (!instance.displayNameFilter.elementNames.Contains(newDisplayName))
        {
            instance.displayNameFilter.elementNames.Add(newDisplayName);
        }
    }
    public static void AddAreaFilterElement(string newArea)
    {
        if (isNullInstance)
            return;

        if (!instance.areaFilter.elementNames.Contains(newArea))
        {
            instance.areaFilter.elementNames.Add(newArea);
        }
    }
    public static void AddCityFilterElement(string newCity)
    {
        if (isNullInstance)
            return;

        if (!instance.cityFilter.elementNames.Contains(newCity))
        {
            instance.cityFilter.elementNames.Add(newCity);
        }
    }
    #endregion

    #region Create New Garage
    public void ShowCreateGarageWindow()
    {
        if (isBusy)
            return;

        createGarageWindowObj.SetActive(true);

        foreach (InputField field in allGarageCreationInputFields)
        {
            field.text = string.Empty;
        }
    }
    public void HideCreateGarageWindow()
    {
        createGarageWindowObj.SetActive(false);
    }

    public void ConfirmCreateGarage() //Fix 1
    {
        if (isBusy)
            return;

        if (displayNameInputField.text.Length < 3)
        {
            StartCoroutine(HighlighInputField(displayNameInputField.image));
            return;
        }

        if (loginInputField.text.Length < 3)
        {
            StartCoroutine(HighlighInputField(loginInputField.image));
            return;
        }

        foreach (InputField field in allGarageCreationInputFields)
        {
            if (string.IsNullOrEmpty(field.text))
            {
                StartCoroutine(HighlighInputField(field.image));
                return;
            }
        }

        isBusy = true;

        PlayFabManager.AddRegistrationSuccessEventListiner(instance.RegistrationPlayFabSuccess);
        PlayFabManager.AddRegistrationFailedEventListiner(instance.RegistrationPlayFabFailed);
        PlayFabManager.RegistrationPlayFabUsername(loginInputField.text,
            displayNameInputField.text);
    }

    private void RegistrationPlayFabSuccess()
    {
        string rawGarageCreationData = string.Empty;
        foreach (InputField field in allGarageCreationInputFields)
        {
            rawGarageCreationData += field.text + "_";
        }

        if (rawGarageCreationData.EndsWith("_"))
            rawGarageCreationData = rawGarageCreationData.Remove(rawGarageCreationData.Length - 1, 1);

        PlayerIOServerManager.CreateGarage(rawGarageCreationData);

        instance.HideCreateGarageWindow();
    }
    private void RegistrationPlayFabFailed(string reason)
    {
        isBusy = false;

        PlayerIOServerManager.DeleteGarage(instance.allGarageCreationInputFields[0].text);
        if (reason == "Username not available")
            StartCoroutine(HighlighInputField(instance.loginInputField.image));
        else if (reason == "The DisplayName entered is not available" || reason == "The display name entered is not available.")
            StartCoroutine(HighlighInputField(instance.displayNameInputField.image));
    }

    public static void CreationGarageSuccess(string[] garageData)
    {
        if (isNullInstance)
            return;

        string garageName = garageData[0];
        string password = garageData[1];

        string displayName = garageData[2];
        string ID = garageData[3];
        string officePhone = garageData[4];
        string mobilePhone = garageData[5];
        string fax = garageData[6];
        string email = garageData[7];
        string area = garageData[8];
        string adress = garageData[9];
        string insurerName = garageData[10];
        string isActive = garageData[11];
        string checkType = garageData[12];
        GaragesListScreen.AddGarageToList(garageName, password, displayName, ID, officePhone, mobilePhone,
                fax, email, adress, area, checkType, isActive,
                insurerName);
    }
    public static void CreationCardFailed(bool isGarageNameExist)
    {
        if (isNullInstance)
            return;

        //if (isGarageNameExist)
        //{
        //    instance.StartCoroutine(instance.HighlighInputField(instance.loginInputField.image));
        //}
        //else
        //{
        //    instance.StartCoroutine(instance.HighlighInputField(instance.displayNameInputField.image));
        //}
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion
}
