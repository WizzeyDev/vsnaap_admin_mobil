﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

#pragma warning disable 0168, 0219 // unused variables
#pragma warning disable 0649

public class CalendarScreen : MonoBehaviour
{
    #region Singleton
    private static CalendarScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                CustomLogger.LogErrorMessage(scriptName + " instance not found at line " + lineNumber + " !");
#else
                CustomLogger.LogErrorMessage("CalendarScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject calendarScreenObj;
    [SerializeField]
    private ChooseBar yearBar;
    [SerializeField]
    private ChooseBar monthBar;
    [SerializeField]
    private InputField yearInputField;
    [SerializeField]
    private InputField monthInputField;
    [SerializeField]
    private GameObject[] calendarCells;
    #endregion

    #region Private Fields
    private System.Action<string> OnDateSelected;

    private string[] years = new string[]
    {
        "2020",
        "2019",
        "2018",
        "2017",
        "2016",
        "2015",
        "2014",
        "2013",
        "2012",
        "2011",
        "2010",
        "2009",
        "2008",
        "2007",
        "2006",
        "2005",
        "2004",
        "2003",
        "2002",
        "2001",
        "2000"
    };
    private string[] months = new string[]
    {
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    };

    private int currentYearIndex = 0;
    private int currentMonthIndex = 0;
    private int currentDayIndex = -1;
    #endregion

    private void Start()
    {
        yearInputField.onEndEdit.AddListener(YearEdit);
        monthInputField.onEndEdit.AddListener(MonthEdit);
        monthBar.elementNames = months.ToList();
        yearBar.elementNames = years.ToList();
    }

    public static void ShowCalendarScreen(System.Action<string> eventListiner)
    {
        instance.calendarScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenZero(instance.calendarScreenObj.transform);

        instance.currentYearIndex = 0;
        instance.yearBar.Select(instance.years[instance.currentYearIndex]);
        instance.SetCurrentDayIndex(DateTime.Now.Day - 1);
        instance.monthBar.Select(instance.months[DateTime.Now.Month - 1]);
        instance.OnDateSelected = eventListiner;
    }

    private void HideCalendarScreen()
    {
        instance.calendarScreenObj.SetActive(false);
    }

    public void ConfirmDate()
    {
        HideCalendarScreen();

        string selectedDate = (currentDayIndex + 1) + "." +
            (currentMonthIndex + 1) + "." + (years[currentYearIndex]);
        OnDateSelected(selectedDate);
    }

    #region Next/Previous Month
    public void NextMonth()
    {
        currentMonthIndex = (currentMonthIndex + 1) % months.Length;
        instance.monthBar.Select(months[currentMonthIndex]);
    }
    public void PreviousMonth()
    {
        if (currentMonthIndex == 0)
            currentMonthIndex = months.Length - 1;
        else
            currentMonthIndex = currentMonthIndex - 1;
        instance.monthBar.Select(months[currentMonthIndex]);
    }
    #endregion

    #region Next/Previous Year
    public void NextYear()
    {
        if (currentYearIndex == 0)
            currentYearIndex = years.Length - 1;
        else
            currentYearIndex = currentYearIndex - 1;
        yearBar.Select(years[currentYearIndex]);
    }
    public void PreviousYear()
    {
        currentYearIndex = (currentYearIndex + 1) % years.Length;
        yearBar.Select(years[currentYearIndex]);
    }
    #endregion

    #region On Edit
    private void MonthEdit(string edit)
    {
        currentMonthIndex = Array.FindIndex(months, str => str == monthInputField.text);

        int year = int.Parse(years[currentYearIndex]);
        int month = currentMonthIndex + 1;
        int days = DateTime.DaysInMonth(year, month);
        for (int i = 0; i < calendarCells.Length; ++i)
        {
            calendarCells[i].SetActive(false);
        }
        for (int i = 0; i < days; ++i)
        {
            calendarCells[i].SetActive(true);
        }

        if (currentDayIndex >= days)
        {
            SetCurrentDayIndex(0);
        }
        //DateTime dateValue = new DateTime(year, month, currentDayIndex + 1);
        //SetCurrentDayIndex(currentDayIndex);
    }
    private void YearEdit(string edit)
    {
        currentYearIndex = Array.FindIndex(years, str => str == yearInputField.text);
        MonthEdit(null);
    }
    #endregion

    public void SetCurrentDayIndex(int dayIndex)
    {
        if (dayIndex == -1)
            return;

        if (currentDayIndex != -1)
        {
            calendarCells[currentDayIndex].GetComponent<Image>().color = GlobalParameters.disabledButtonColor;
             calendarCells[currentDayIndex].GetComponentInChildren<Text>().color = GlobalParameters.enabledButtonColor;
        }

        currentDayIndex = dayIndex;
        calendarCells[currentDayIndex].GetComponent<Image>().color = GlobalParameters.enabledButtonColor;
        calendarCells[currentDayIndex].GetComponentInChildren<Text>().color = GlobalParameters.enabledTextColor;
    }
}
