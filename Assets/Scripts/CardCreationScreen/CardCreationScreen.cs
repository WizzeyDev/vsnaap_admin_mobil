﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardCreationScreen : MonoBehaviour
{
    #region Singleton
    private static CardCreationScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("CardCreationScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject cardCreationScreen;

    [Space(10)]
    [SerializeField]
    private InputField garageNameInputField;
    [SerializeField]
    private InputField appraiserNameInputField;
    [SerializeField]
    private InputField carNumberInputField; //מס הזמנה
    [SerializeField]
    private InputField cardNumberInputField;
    [SerializeField]
    private InputField secondNumberInputField; //מספר הארוע
    [SerializeField]
    private InputField secondDateInputField;
    [SerializeField]
    private InputField cardTypeInputField;
    [SerializeField]
    private InputField insurerNameInputField;

    [Space(10)]
    [SerializeField]
    private Image createCardButtonImage;
    [SerializeField]
    private Image confirmCardButtonImage;

    [Space(10)]
    [SerializeField]
    private TextMeshProUGUI createCardButtonText;
    [SerializeField]
    private TextMeshProUGUI confirmCardButtonText;

    [Space(10)]
    [SerializeField]
    private Button createCardButton;
    [SerializeField]
    private Button confirmCardButton;

    [Space(10)]
    [SerializeField]
    private GameObject warningTextObject;

    [Space(10)]
    [SerializeField]
    private GameObject creationSuccessImageObj;
    #endregion

    #region Private Fields
    private InputField[] importantInputFields;
    private InputField[] notImportantInputFields;

    private System.Text.StringBuilder stringBuilder;

    private bool isBusy;
    private bool isButtonEnabled;
    #endregion

    #region Setup
    private void Start()
    {
        stringBuilder = new System.Text.StringBuilder(128);

        importantInputFields = new InputField[5];
        importantInputFields[0] = garageNameInputField;
        importantInputFields[1] = appraiserNameInputField;
        importantInputFields[2] = carNumberInputField;
        importantInputFields[3] = cardNumberInputField;
        importantInputFields[4] = secondDateInputField;

        notImportantInputFields = new InputField[6];
        notImportantInputFields[0] = carNumberInputField;
        notImportantInputFields[1] = cardNumberInputField;
        notImportantInputFields[2] = secondNumberInputField;
        notImportantInputFields[3] = secondDateInputField;
        notImportantInputFields[4] = cardTypeInputField;
        notImportantInputFields[5] = insurerNameInputField;
    }
    #endregion

    #region Show/Hide Card Creation Screen
    public static void ShowCardsCreationScreen(string appraiserName = "")
    {
        if (isNullInstance)
            return;

        instance.ShowCardCreationScreen(appraiserName);
    }
    public void ShowCardCreationScreen(string appraiserName)
    {
        cardCreationScreen.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(cardCreationScreen.transform);

        appraiserNameInputField.text = appraiserName;
        garageNameInputField.text = string.Empty;
        carNumberInputField.text = string.Empty;
        cardNumberInputField.text = string.Empty;

        secondNumberInputField.text = string.Empty;
        secondDateInputField.text = string.Empty;
        cardTypeInputField.text = string.Empty;
        insurerNameInputField.text = string.Empty;
    }

    public void HideCardCreationScreen()
    {
        cardCreationScreen.SetActive(false);
        creationSuccessImageObj.SetActive(false);

        foreach (var inputField in notImportantInputFields)
        {
            inputField.text = string.Empty;
        }

        if (isButtonEnabled)
            ChangeButtonsState();
    }
    #endregion

    private void ChangeButtonsState()
    {
        if (!isButtonEnabled)
        {
            createCardButtonImage.color = GlobalParameters.disabledButtonColor;
            confirmCardButtonImage.color = GlobalParameters.enabledButtonColor;

            createCardButtonText.color = GlobalParameters.unActiveTextColor;
            confirmCardButtonText.color = GlobalParameters.enabledTextColor;
        }
        else
        {
            createCardButtonImage.color = GlobalParameters.enabledButtonColor;
            confirmCardButtonImage.color = GlobalParameters.disabledButtonColor;

            createCardButtonText.color = GlobalParameters.enabledTextColor;
            confirmCardButtonText.color = GlobalParameters.unActiveTextColor;
        }

        isButtonEnabled = !isButtonEnabled;

        createCardButton.enabled = !createCardButton.enabled;
        confirmCardButton.enabled = !confirmCardButton.enabled;

        warningTextObject.SetActive(warningTextObject.activeSelf);
    }

    #region Change Garage
    public void ChangeGarage()
    {
        GaragesListScreen.ShowGarageListScreen(ChangeGarageResult);
    }

    private void ChangeGarageResult(string garageName)
    {
        this.garageNameInputField.text = garageName;
    }
    #endregion

    #region Change Appraiser
    public void ChangeAppraiser()
    {
        AppraiserListScreen.ShowAppraiserListScreen(ChangeAppraiserResult, true);
    }

    private void ChangeAppraiserResult(string garageName)
    {
        this.appraiserNameInputField.text = garageName;
    }
    #endregion

    #region Create Card
    public void ConfirmEdit() 
    {
        Debug.Log("<color=red>ConfirmEdit </color>");
        foreach (InputField field in importantInputFields)
        {
            if (string.IsNullOrEmpty(field.text))
            {
                StartCoroutine(HighlighInputField(field.image));
                //return;
            }
        }

        foreach (InputField field in notImportantInputFields)
        {
            int forbiddenCount = GlobalParameters.forbiddenSymbols.Where(sym => field.text.Contains(sym)).Count();
            if (forbiddenCount > 0)
            {
                StartCoroutine(HighlighInputField(field.image));
                //return;
            }
        }

        if (carNumberInputField.text.Length < 2 || cardNumberInputField.text.Length < 2)
        {
            if (carNumberInputField.text.Length != 0)
                StartCoroutine(HighlighInputField(carNumberInputField.image));
            else if (cardNumberInputField.text.Length != 0)
                StartCoroutine(HighlighInputField(cardNumberInputField.image));
            else
            {
                StartCoroutine(HighlighInputField(carNumberInputField.image));
                StartCoroutine(HighlighInputField(cardNumberInputField.image));
            }
            //return;
        }
        Debug.Log("<color=red>Gonna create? </color>");
        CreateCard();
    }

    private void CreateCard()
    {
        Debug.Log("<color=red>CreatCard </color>" + isBusy);
        if (isBusy)
            return;

        isBusy = true;
        if (Settings.isNewDataBase)
        {

            return;
        }
        string  apending = "( '";
        apending += cardNumberInputField.text + "', '";
        apending += carNumberInputField.text + "', '";
        apending += DateStringConverter.GetDMYHMDate() + "', '";
        stringBuilder.Append(cardNumberInputField.text).Append("\\");
        stringBuilder.Append(carNumberInputField.text).Append("\\");
        stringBuilder.Append(DateStringConverter.GetDMYHMDate()).Append("\\");

        string garageName = string.Empty;
        if (garageNameInputField.text != string.Empty)
        {
            garageName = GaragesListScreen.GetGarageNameByDisplay(garageNameInputField.text);
            if (garageName == null)
            {
                StartCoroutine(HighlighInputField(garageNameInputField.image));
                Debug.Log("No Garage?");
                //return;
            }
        }
        stringBuilder.Append(garageName).Append("\\");

        string appraiserName = string.Empty;
        if (appraiserNameInputField.text != string.Empty)
        {
            appraiserName = AppraiserListScreen.GetAppraiserNameByDisplay(appraiserNameInputField.text);
            if (appraiserName == null)
            {
                StartCoroutine(HighlighInputField(appraiserNameInputField.image));
                Debug.Log("No Appraisel?");
                //return;
            }
        }
        stringBuilder.Append(appraiserName).Append("\\");
        stringBuilder.Append(secondNumberInputField.text).Append("\\");
        stringBuilder.Append(secondDateInputField.text).Append("\\");
        stringBuilder.Append(cardTypeInputField.text).Append("\\");
        stringBuilder.Append(insurerNameInputField.text);

        apending += appraiserName + "', '";
        apending += secondNumberInputField.text + "', '";
        apending += secondDateInputField.text + "', '";
        apending += cardTypeInputField.text+"' ";
        //apending += insurerNameInputField.text + ")";

        Debug.Log("<color=red>New Order stringBuilder</color>" + apending);
        DataBaseController.InsertNewOrder(0,apending);
        //PlayerIOServerManager.SendCreateCardMessage(stringBuilder.ToString()); //FIX 1 OrderCard
        stringBuilder.Clear();
    }

    public static void CardCreationSuccess()
    {
        instance.ChangeButtonsState();
    }
    #endregion

    public void SetSecondDate()
    {
        CalendarScreen.ShowCalendarScreen((selectedDate) =>
        {
            secondDateInputField.text = selectedDate;
        });
    }

    #region Save Input
    public void SaveGarageName()
    {
        PlayerPrefs.SetString("GarageName", garageNameInputField.text);
    }
    public void SaveCarNumber()
    {
        PlayerPrefs.SetString("CarNumber", carNumberInputField.text);
    }
    public void SaveCardNumber()
    {
        PlayerPrefs.SetString("CardNumber", cardNumberInputField.text);
    }
    public void SaveSecondNumber()
    {
        PlayerPrefs.SetString("SecondNumber", secondNumberInputField.text);
    }
    public void SaveSecondDate()
    {
        PlayerPrefs.SetString("SecondDate", secondDateInputField.text);
    }
    public void SaveInsurerName()
    {
        PlayerPrefs.SetString("InsurerName", insurerNameInputField.text);
    }
    public void SaveCardType()
    {
        PlayerPrefs.SetString("CardType", cardTypeInputField.text);
    }
    private void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteKey("GarageName");
        garageNameInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("CarNumber");
        carNumberInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("CardNumber");
        cardNumberInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("SecondNumber");//What is it for?
        secondNumberInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("SecondDate"); //What is it for?
        secondDateInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("CardType");
        cardTypeInputField.text = string.Empty;

        PlayerPrefs.DeleteKey("InsurerName");
        insurerNameInputField.text = string.Empty;
    }
    #endregion

    #region Creation Success
    public void CreationCardSuccess()
    {
        ClearPlayerPrefs();
        creationSuccessImageObj.SetActive(true);
        StartCoroutine(AutoCloseCreationCardScreen());
    }

    private IEnumerator AutoCloseCreationCardScreen()
    {
        yield return new WaitForSeconds(2f);
        HideCardCreationScreen();
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while(inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion
}
