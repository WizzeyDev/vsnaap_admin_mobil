﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class ChooseBar : MonoBehaviour
{
    #region Getters Fields
    public GameObject elementBtnPrefab;
    public GameObject specialElementBtnPrefab;
    public GameObject viewPortObj;

    public RectTransform arrowRectTransform;
    public Transform parent;

    public Button chooseButton;

    public InputField currentElementName;

    public List<string> elementNames;
    public List<string> specialElementNames;
    #endregion

    #region Private Fields
    private List<GameObject> elements = new List<GameObject>();

    private bool isShown;
    #endregion
    void Start()
    {
        chooseButton.onClick.AddListener(() => ShowHidePanel());
        Button arrowBtn = arrowRectTransform.GetComponent<Button>();
        if (arrowBtn != null)
            arrowBtn.onClick.AddListener(() => ShowHidePanel());
    }

    private void ShowHidePanel()
    {
        if (isShown)
        {
            HideButtons();
            isShown = false;
        }
        else
        {
            ShowButtons();
            isShown = true;
        }
    }

    private void ShowButtons()
    {
        viewPortObj.SetActive(true);
        for (int i = 0; i < elementNames.Count; ++i)
        {
            string elementText = elementNames[i];
            if (currentElementName.text != elementText)
            {
                var obj = Instantiate(elementBtnPrefab, parent);
                elements.Add(obj);

                TextMeshProUGUI textComponent = obj.GetComponentInChildren<TextMeshProUGUI>();
                if (textComponent != null)
                {
                    textComponent.text = elementText;
                }
                else
                {
                    textComponent = obj.GetComponent<TextMeshProUGUI>();
                    if (textComponent != null)
                    {
                        textComponent.text = elementText;
                    }
                    else
                    {
                        Text text = obj.GetComponent<Text>();
                        if (text != null)
                        {
                            text.text = elementText;
                        }
                        else
                        {
                            InputField inputComponent = obj.GetComponentInChildren<InputField>();
                            inputComponent.text = elementText;
                        }
                    }
                }
                                
                obj.GetComponent<Button>().onClick.AddListener(() => Select(elementText));
            }
        }

        for (int i = 0; i < specialElementNames.Count; ++i)
        {
            string elementText = specialElementNames[i];
            var obj = Instantiate(specialElementBtnPrefab, parent);
            elements.Add(obj);

            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = elementText;
            obj.GetComponent<Button>().onClick.AddListener(() => SpecialSelect());
        }

        arrowRectTransform.localScale = new Vector3(arrowRectTransform.localScale.x, 1);
    }

    private void HideButtons()
    {
        AudioManager.PlayButtonSound();
        viewPortObj.SetActive(false);
        foreach (var btn in elements)
        {
            Destroy(btn);
        }
        elements.Clear();

        arrowRectTransform.localScale = new Vector3(arrowRectTransform.localScale.x, -1);
    }

    public void Select(string Name)
    {
        currentElementName.text = Name;
        currentElementName.onEndEdit.Invoke(string.Empty);
        HideButtons();
        isShown = false;
    }
    public void SpecialSelect()
    {
        EventSystem m_EventSystem = EventSystem.current;
        currentElementName.OnPointerClick(new PointerEventData(m_EventSystem));
        m_EventSystem.SetSelectedGameObject(currentElementName.gameObject, new BaseEventData(m_EventSystem));

        currentElementName.text = string.Empty;
        ActivateBar(false);

        HideButtons();
        isShown = false;
    }

    public void ActivateBar(bool state)
    {
        if (!state)
        {
            chooseButton.gameObject.SetActive(false);
            arrowRectTransform.gameObject.SetActive(false);
        }
        else
        {
            chooseButton.gameObject.SetActive(true);
            arrowRectTransform.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        //currentElementName.interactable = false;
        //currentElementName.text = string.Empty;

        chooseButton.gameObject.SetActive(true);
        arrowRectTransform.gameObject.SetActive(true);
    }
}
