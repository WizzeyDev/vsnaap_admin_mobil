﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SearchResultElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    private TextMeshProUGUI appraiserDisplayName;
    [SerializeField]
    private TextMeshProUGUI garageDisplayName;
    [SerializeField]
    private TextMeshProUGUI carNumber;
    [SerializeField]
    private TextMeshProUGUI createdDate;
    [SerializeField]
    private TextMeshProUGUI cardNumber;

    [Space(10)]
    [SerializeField]
    private Image selectButtonImg;
    #endregion

    #region Private Fields
    private string appraiserName;
    private string garageName;

    private Color selectedColor = new Color32(232, 88, 88, 255);
    private Color unselectedColor = Color.white;

    private bool isSelected;
    #endregion

    public void Setup(string[] searchResultElementData)
    {
        this.appraiserName = searchResultElementData[0];
        this.appraiserDisplayName.text = AppraiserListScreen.GetDisplayNameByAppraiser(appraiserName);
        garageName = searchResultElementData[1];
        this.garageDisplayName.text = GaragesListScreen.GetDisplayNameByGarage(garageName);

        this.carNumber.text = searchResultElementData[2];
        this.createdDate.text = DateStringConverter.FromRawDateToParsedDate(searchResultElementData[3]); 
        this.cardNumber.text = searchResultElementData[4];
    }

    public void SelectElement()
    {
        if (!isSelected)
            selectButtonImg.color = selectedColor;
        else
            selectButtonImg.color = unselectedColor;

        isSelected = !isSelected;
        AudioManager.PlayButtonSound();
    }

    #region Getters
    public bool IsSelected()
    {
        return isSelected;
    }
    public string GetCardNumber()
    {
        return cardNumber.text;
    }
    #endregion
}
