﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchResultScreen : MonoBehaviour
{
    #region Singleton
    private static SearchResultScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("SearchResultScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject searchResultScreenObj;

    [Space(10)]
    [SerializeField]
    private GameObject searchResultElementPrefab;

    [Space(10)]
    [SerializeField]
    private Transform searchResultElementsParent;
    #endregion

    #region Private Fields
    private List<SearchResultElement> searchResultElements = new List<SearchResultElement>();

    private List<GameObject> searchResultObjList = new List<GameObject>();

    private bool isBusy;
    #endregion

    #region Show/Hide Result Screen
    public static void ShowSearchResultScreen(string rawSearchResultData)
    {
        instance.searchResultScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(instance.searchResultScreenObj.transform);

        if (!string.IsNullOrEmpty(rawSearchResultData))
            instance.CreateSearchResultElementList(rawSearchResultData.Split('~'));
    }

    public static void HideAllScreens()
    {
        if (isNullInstance)
            return;

        instance.HideSearchResultScreen();
    }

    public void HideSearchResultScreen()
    {
        foreach (var resultObj in searchResultObjList)
        {
            Destroy(resultObj);
        }

        searchResultObjList.Clear();
        searchResultElements.Clear();

        searchResultScreenObj.SetActive(false);
    }
    #endregion

    #region Search Result List
    private void CreateSearchResultElementList(string[] searchResultData)
    {
        for (int i = 0; i < searchResultData.Length; ++i)
        {
            string rawSearchResultElementData = searchResultData[i];
            instance.CreateSearchResultElement(rawSearchResultElementData.Split('|'));
        }
    }
    private void CreateSearchResultElement(string[] searchResultElementData)
    {
        GameObject searchResultObj =
          Instantiate(searchResultElementPrefab, searchResultElementsParent);
        searchResultObjList.Add(searchResultObj);

        SearchResultElement searchResultElement =
            searchResultObj.GetComponent<SearchResultElement>();
        searchResultElement.Setup(searchResultElementData);
        searchResultElements.Add(searchResultElement);
    }

    public void SelectSearchedElements()
    {
        if (isBusy)
            return;

        isBusy = true;

        string rawSelectedCardNumbers = string.Empty;

        for (int i = 0; i < searchResultElements.Count; ++i)
        {
            if (searchResultElements[i].IsSelected())
            {
                rawSelectedCardNumbers += searchResultElements[i].GetCardNumber() + "_";
            }
        }

        rawSelectedCardNumbers = rawSelectedCardNumbers.Trim('_');

        if (!string.IsNullOrEmpty(rawSelectedCardNumbers))
            PlayerIOServerManager.SendAddCardsToAdminBlockMessage(rawSelectedCardNumbers);
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion
}
