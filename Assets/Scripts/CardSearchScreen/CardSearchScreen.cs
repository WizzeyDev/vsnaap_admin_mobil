﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardSearchScreen : MonoBehaviour
{
    #region Singleton
    private static CardSearchScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("CardSearchScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject cardSearchScreenObj;

    [Space(10)]
    [SerializeField]
    private InputField garageName;
    [SerializeField]
    private InputField appraiserName;
    [SerializeField]
    private InputField carInputField;
    [SerializeField]
    private InputField cardInputField;
    [SerializeField]
    private InputField firstCreatedDateInputField;
    [SerializeField]
    private InputField lastCreatedDateInputField;

    [Space(10)]
    [SerializeField]
    private InputField secondNumberInputField;
    [SerializeField]
    private InputField secondDateInputField;
    [SerializeField]
    private InputField cardTypeInputField;
    [SerializeField]
    private InputField insurerNameInputField;

    [Space(10)]
    [SerializeField]
    private Image[] statusButtonImages;
    [SerializeField]
    private TextMeshProUGUI[] statusButtonTexts;
    #endregion

    #region Private Fields
    private List<int> statusFilter = new List<int>();

    private bool isBusy;
    #endregion

    #region Show/Hide Search Screen
    public static void ShowCardsSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowCardSearchScreen();
    }
    public void ShowCardSearchScreen()
    {
        cardSearchScreenObj.SetActive(true);
        MenuPanelManager.SetCurrentScreenFirst(cardSearchScreenObj.transform);

        garageName.text = string.Empty;
        appraiserName.text = string.Empty;
        carInputField.text = string.Empty;
        cardInputField.text = string.Empty;
        firstCreatedDateInputField.text = string.Empty;
        lastCreatedDateInputField.text = string.Empty;

        ResetStatusFilter();

        secondNumberInputField.text = string.Empty;
        secondDateInputField.text = string.Empty;
        cardTypeInputField.text = string.Empty;
        insurerNameInputField.text = string.Empty;
    }

    public void HideCardSearchScreen()
    {
        cardSearchScreenObj.SetActive(false);
    }

    public static void HideCardsSearchScreen()
    {
        if (isNullInstance)
            return;

        instance.cardSearchScreenObj.SetActive(false);
    }
    #endregion

    public void ConfirmSearchEdit()
    {
        if (isBusy)
            return;

        isBusy = true;

        string garageNameParameter = string.Empty;
        if (garageName.text != string.Empty)
        {
            garageNameParameter = GaragesListScreen.GetGarageNameByDisplay(garageName.text);
            if (garageNameParameter == null)
            {
                StartCoroutine(HighlighInputField(garageName.image));
                return;
            }
        }

        string appraiserNameParameter = string.Empty;
        if (appraiserName.text != string.Empty)
        {
            appraiserNameParameter = AppraiserListScreen.GetAppraiserNameByDisplay(appraiserName.text);
            if (appraiserNameParameter == null)
            {
                StartCoroutine(HighlighInputField(appraiserName.image));
                return;
            }
        }

        string carNumberParameter = carInputField.text;
        string cardNumberParameter = cardInputField.text;
        string firstCreatedDate = firstCreatedDateInputField.text;
        string lastCreatedDate = lastCreatedDateInputField.text;

        string rawStatusFilter = string.Empty;
        foreach (int statusIndex in statusFilter)
        {
            rawStatusFilter += statusIndex + "\\";
        }
        if (rawStatusFilter.EndsWith("\\"))
            rawStatusFilter = rawStatusFilter.Remove(rawStatusFilter.Length - 1, 1);

        string secondNumberParameter = secondNumberInputField.text;
        string secondDateParameter = secondDateInputField.text;
        string cardTypeParameter = cardTypeInputField.text;
        string insurerNameParameter = insurerNameInputField.text;

        string rawSearchFilter = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}_{8}_{9}_{10}",
            garageNameParameter, appraiserNameParameter, carNumberParameter, cardNumberParameter,
            firstCreatedDate, lastCreatedDate,
            rawStatusFilter,
            secondNumberParameter, secondDateParameter, cardTypeParameter, insurerNameParameter);

        PlayerIOServerManager.SendSearchCardsMessage(rawSearchFilter);
    }

    public void AddStatusFilter(int newStatusIndex)
    {
        if (!statusFilter.Contains(newStatusIndex))
        {
            statusButtonImages[newStatusIndex].color = GlobalParameters.enabledButtonColor;
            statusButtonTexts[newStatusIndex].color = GlobalParameters.enabledTextColor;
            statusFilter.Add(newStatusIndex);
        }
        else
        {
            statusButtonImages[newStatusIndex].color = GlobalParameters.disabledButtonColor;
            statusButtonTexts[newStatusIndex].color = GlobalParameters.disabledTextColor;
            statusFilter.Remove(newStatusIndex);
        }
    }

    public void ResetStatusFilter()
    {
        statusFilter.Clear();
        for (int i = 0; i < statusButtonImages.Length; ++i)
        {
            statusButtonImages[i].color = GlobalParameters.disabledButtonColor;
            statusButtonTexts[i].color = GlobalParameters.disabledTextColor;
        }
    }

    #region Set Date
    public void SetFirstCreatedDate()
    {
        CalendarScreen.ShowCalendarScreen((selectedDate) =>
        {
            firstCreatedDateInputField.text = selectedDate;
        });
    }
    public void SetLastCreatedDate()
    {
        CalendarScreen.ShowCalendarScreen((selectedDate) =>
        {
            lastCreatedDateInputField.text = selectedDate;
        });
    }
    public void SetSecondDate()
    {
        CalendarScreen.ShowCalendarScreen((selectedDate) =>
        {
            secondDateInputField.text = selectedDate;
        });
    }
    #endregion

    #region Change Garage
    public void ChangeGarage()
    {
        GaragesListScreen.ShowGarageListScreen(ChangeGarageResult);
    }

    private void ChangeGarageResult(string garageName)
    {
        this.garageName.text = garageName;
    }

    public void ChangeAppraiser()
    {
        AppraiserListScreen.ShowAppraiserListScreen(ChangeAppraiserResult, true);
    }

    private void ChangeAppraiserResult(string appraiserName)
    {
        this.appraiserName.text = appraiserName;
    }
    #endregion

    #region Highlight InputField
    private const float alphaHighlighSpeed = 10f;
    private IEnumerator HighlighInputField(Image inputFieldImg)
    {
        while (inputFieldImg.color.a < 1f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 1f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
        yield return new WaitForSeconds(0.1f);

        while (inputFieldImg.color.a > 0f)
        {
            inputFieldImg.color = new Color(inputFieldImg.color.r,
                 inputFieldImg.color.g, inputFieldImg.color.b,
                 Mathf.MoveTowards(inputFieldImg.color.a, 0f, alphaHighlighSpeed * Time.deltaTime));
            yield return null;
        }
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion
}
