﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AdminDocumentsScreen : MonoBehaviour
{
    #region Singleton
    private static AdminDocumentsScreen instance;
    private static bool isNullInstance
    {
        get
        {
            if (instance == null)
            {
#if UNITY_EDITOR
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
                string scriptName = stackFrame.GetFileName();
                int lineNumber = stackFrame.GetFileLineNumber();
                Debug.LogError(scriptName + " instance not found at line " + lineNumber + " !");
#else
                Debug.LogError("AdminDocumentsScreen instance not found!");
#endif
                return true;
            }
            return false;
        }
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    #endregion

    #region Serializable Fields
    [SerializeField]
    private GameObject adminDocumentsScreenObj;

    [Space(10)]
    [SerializeField]
    private RectTransform snapScrollContentRect;
    [SerializeField]
    private ScrollRect scrollView;

    [Space(10)]
    [SerializeField]
    private GameObject adminDocumentPrefab;
    [SerializeField]
    private Transform adminDocumentsParent;
    [SerializeField]
    private GameObject radioButtonPrefab;
    [SerializeField]
    private Transform radioButtonsParent;

    [Space(10)]
    [SerializeField]
    private Sprite hollowSprite;
    [SerializeField]
    private Sprite filledSprite;

    [SerializeField]
    private GameObject addDocumentSuccessScreen;

    [Space(20)]
    [SerializeField]
    private Image uploadingProgressImg;
    [SerializeField]
    private Image sendEmailButtonImg;
    [SerializeField]
    private Image sendToAppraiserButtonImg;
    [SerializeField]
    private Image uploadButtonImg;

    [SerializeField]
    private TextMeshProUGUI sendEmailButtonText;
    [SerializeField]
    private TextMeshProUGUI sendToAppraiserButtonText;
    [SerializeField]
    private TextMeshProUGUI uploadButtonText;

    [SerializeField]
    private Button sendEmailButton;
    [SerializeField]
    private Button sendToAppraiserButton;
    [SerializeField]
    private Button uploadButton;
    #endregion

    #region Private Fields
    private List<RectTransform> scrollElementRects = new List<RectTransform>();

    private List<Image> radioButtonImages = new List<Image>();

    private List<AdminDocumentElement> documentElements =
    new List<AdminDocumentElement>();
    private List<AdminDocumentElement> uploadedDocumentsList =
        new List<AdminDocumentElement>();
    private List<AdminDocumentElement> localDocumentsList =
        new List<AdminDocumentElement>();

    private SnapScroll snapScroll;

    private int currentDocumentIndex = -1;
    private bool isBusy;
    #endregion

    #region Setup
    private void Start()
    {
        if(snapScrollContentRect == null)
        {
            CustomLogger.LogErrorMessage("AdminDocumentsScreen Start error: snapScrollRect is null!");
            return;
        }
        if (scrollView == null)
        {
            CustomLogger.LogErrorMessage("AdminDocumentsScreen Start error: scrollView is null!");
            return;
        }

        snapScroll = new SnapScroll(this, snapScrollContentRect, scrollView, 
            scrollElementRects);
    }
    #endregion

    #region Show/Hide Admin Documents Screen
    public static void ShowAdminDocumentsScreen()
    {
        if (isNullInstance)
            return;

        instance.ShowAdminDocumentScreen();
    }
    public void ShowAdminDocumentScreen()
    {
        MenuPanelManager.SetCurrentScreenFirst(instance.adminDocumentsScreenObj.transform);
        if (instance.adminDocumentsScreenObj.activeSelf)
            return;
        else
        {
            instance.adminDocumentsScreenObj.SetActive(true);
        }

        instance.snapScroll.ActivateSnapScroll();
    }

    public static void HideAdminDocumentsScreen()
    {
        if (isNullInstance)
            return;

        instance.HideAdminDocumentScreen();
    }
    public void HideAdminDocumentScreen()
    {
        adminDocumentsScreenObj.SetActive(false);

        if (scrollElementRects == null)
        {
            CustomLogger.LogErrorMessage("AdminDocumentsScreen HideAdminDocumentScreen error:\n" +
                "scrollElementRects is null!");
            return;
        }

        if (scrollElementRects.Count == 0)
            return;

        snapScroll.Reset();
    }
    #endregion

    #region Documents Scrolling Part
    private void FixedUpdate()
    {
        snapScroll.FixedUpdate();
        SetRadioButtonActive(snapScroll.GetClosestElementIndex());
    }

    #region SnapScroll Input
    public void BeginScroll()
    {
        snapScroll.Scrolling(true);
    }

    public void EndScroll()
    {
        snapScroll.Scrolling(false);
    }
    #endregion

    #endregion

    #region Radio Buttons
    private void SetRadioButtonActive(int index)
    {
        if (index == currentDocumentIndex || index == -1 || radioButtonImages.Count == 0)
            return;

        if (currentDocumentIndex != -1)
            radioButtonImages[currentDocumentIndex].sprite = hollowSprite;

        currentDocumentIndex = index;
        radioButtonImages[currentDocumentIndex].sprite = filledSprite;
    }

    private void AddRadioButton()
    {
        radioButtonImages.Add(Instantiate(radioButtonPrefab, radioButtonsParent).
            GetComponent<Image>());
    }

    private void DeleteRadioButtons()
    {
        if (radioButtonImages == null)
            return;

        currentDocumentIndex = -1;

        for (int i = 0; i < radioButtonImages.Count; ++i)
        {
            Destroy(radioButtonImages[i].gameObject);
        }
        radioButtonImages.Clear();
    }
    private void RemoveRadioButton()
    {
        Destroy(instance.radioButtonImages[currentDocumentIndex].gameObject);
        radioButtonImages.RemoveAt(currentDocumentIndex);
    }
    #endregion

    #region Add Documents

    #region From Drive
    public void AddDocumentFromDrive()
    {
        FileBrowserScreen.SelectFile(DocumentSelectedSuccess);
    }
    private void DocumentSelectedSuccess(string documentPath)
    {
        if (!GlobalDocuments.IsDocumentValid(documentPath))
        {
            Debug.Log(string.Format("Document {0} has an unknown format...", documentPath));
            return;
        }

        CustomLogger.LogMessage("DocumentSelectedSuccess from path: " + documentPath);
        byte[] bytes = File.ReadAllBytes(documentPath);

        string[] splitedPath = documentPath.Split('.');
        string documentName = "Vsm-AdminMobil-document-" +
               DateStringConverter.GetMDHMSMDate() + "." + splitedPath[splitedPath.Length - 1];
        documentPath = Path.Combine(GlobalSettings.cloudStorageUploadPath, documentName);
        File.WriteAllBytes(documentPath, bytes);
        CustomLogger.LogMessage("DocumentSelectedSuccess copied to new path " + documentPath);

        Sprite documentSprite = GlobalDocuments.GetDocumentSprite(documentPath);
        if (documentSprite == null)
        {
            CustomLogger.LogErrorMessage("DocumentSelectedSuccess 'documentSprite' is null!");
            return;
        }

        AddDocumentToList(documentPath, documentSprite);
    }
    #endregion

    #region From Camera
    public void AddDocumentFromCamera()
    {
        CustomLogger.LogMessage("Trying to open device camera:\n" +
    "HasCamera: " + NativeCamera.DeviceHasCamera() + "\n" +
    "IsBusy: " + NativeCamera.IsCameraBusy());
        if (NativeCamera.DeviceHasCamera() && !NativeCamera.IsCameraBusy())
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            NativeCamera.TakePicture(CaptureDeviceCamHandle, -1,
                    true, NativeCamera.PreferredCamera.Rear);
#endif
        }
    }
    private void CaptureDeviceCamHandle(string pathToPhoto)
    {
        if (string.IsNullOrEmpty(pathToPhoto))
        {
            CustomLogger.LogMessage("CaptureDeviceCamHandle has empty path!");
            return;
        }

        StartCoroutine(LateSaveCapturedPhoto(pathToPhoto));
    }
    private IEnumerator LateSaveCapturedPhoto(string photoPath)
    {
        yield return null;

        CustomLogger.LogMessage("CaptureDeviceCamHandle on path: " + photoPath);
        byte[] bytes = File.ReadAllBytes(photoPath);
        File.Delete(photoPath);

        Sprite photoSprite = SpriteLoader.GetSpriteFromBytes(bytes);
        if (photoSprite == null)
        {
            CustomLogger.LogErrorMessage("CaptureDeviceCam complete, but sprite is null in path:\n" +
                photoPath);
            yield break;
        }

        Sprite rotatedSprite = TextureRotator.RotateSprite(photoSprite);
        bytes = rotatedSprite.texture.EncodeToJPG();
        string photoName = "Vsm-AdminMobil-devicePhoto-" + DateStringConverter.GetMDHMSMDate() + ".jpg";
        photoPath = Path.Combine(GlobalSettings.cloudStorageUploadPath, photoName);
        File.WriteAllBytes(photoPath, bytes);
        CustomLogger.LogMessage("CaptureDeviceCamHandle replaced by new path " + photoPath);

        AddDocumentToList(photoPath, rotatedSprite);
    }
    #endregion

    private void AddDocumentToList(string documentLocalPath, Sprite documentSprite)
    {
        AdminDocumentElement documentElement = Instantiate(adminDocumentPrefab, adminDocumentsParent).
            GetComponent<AdminDocumentElement>();
        documentElement.transform.SetSiblingIndex(0);
        documentElement.SetupDocumentElement(documentLocalPath, documentSprite,
            GlobalDocuments.GetDocumentType(documentLocalPath));

        localDocumentsList.Add(documentElement);
        documentElements.Add(documentElement);
        scrollElementRects.Insert(0, documentElement.GetComponent<RectTransform>());

        DeactivateUploadButtons();
        AddRadioButton();
    }
    #endregion

    #region Change Buttons
    private void DeactivateUploadButtons()
    {
        sendEmailButtonImg.color = GlobalParameters.disabledButtonColor;
        sendEmailButtonText.color = GlobalParameters.disabledTextColor;
        sendEmailButton.interactable = false;

        sendToAppraiserButtonImg.color = GlobalParameters.disabledButtonColor;
        sendToAppraiserButtonText.color = GlobalParameters.disabledTextColor;
        sendToAppraiserButton.interactable = false;

        uploadButton.gameObject.SetActive(true);
        uploadButtonImg.color = GlobalParameters.enabledButtonColor;
        uploadButtonText.color = GlobalParameters.enabledTextColor;
        uploadButton.interactable = true;
    }

    private void ActivateUploadButtons()
    {
        sendEmailButtonImg.color = GlobalParameters.enabledButtonColor;
        sendEmailButtonText.color = GlobalParameters.enabledTextColor;
        sendEmailButton.interactable = true;

        sendToAppraiserButtonImg.color = GlobalParameters.enabledButtonColor;
        sendToAppraiserButtonText.color = GlobalParameters.enabledTextColor;
        sendToAppraiserButton.interactable = true;

        uploadButton.gameObject.SetActive(false);
    }
    #endregion

    #region Upload Documents
    public void UploadSelectedDocuments()
    {
        if (isBusy || localDocumentsList.Count == 0)
            return;

        isBusy = true;

        uploadingProgressImg.fillAmount = 0f;

        FirebaseManager.UploadFile(localDocumentsList[localDocumentsList.Count - 1].GetDocumentLocalPath(),
            UploadDocumentHanlder);
    }

    private void UploadDocumentHanlder(string documentURL)
    {
        if (string.IsNullOrEmpty(documentURL))
        {
            UploadErrorHandle();
            return;
        }

        AdminDocumentElement documentElement = localDocumentsList[localDocumentsList.Count - 1];
        documentElement.SetDocumentUrl(documentURL);
        uploadedDocumentsList.Add(documentElement);
        localDocumentsList.RemoveAt(localDocumentsList.Count - 1);

        uploadingProgressImg.fillAmount = (float)uploadedDocumentsList.Count / (documentElements.Count);

        if (localDocumentsList.Count > 0)
        {
            FirebaseManager.UploadFile(localDocumentsList[localDocumentsList.Count - 1].GetDocumentLocalPath(),
              UploadDocumentHanlder);
        }
        else
            AllDocumentsUploaded();
    }

    private void AllDocumentsUploaded()
    {
        ActivateUploadButtons();

        isBusy = false;
    }

    private void UploadErrorHandle()
    {
        instance.uploadingProgressImg.color = Color.red;
        instance.uploadingProgressImg.fillAmount = 1f;

        isBusy = false;
    }
    #endregion

    #region Remove Document
    public void RemoveCurrentDocumentElement()
    {
        Destroy(instance.scrollElementRects[currentDocumentIndex].gameObject);

        instance.scrollElementRects.RemoveAt(currentDocumentIndex);

        AdminDocumentElement documentElement = instance.documentElements[currentDocumentIndex];
        if (localDocumentsList.Contains(documentElement))
            localDocumentsList.Remove(documentElement);
        if (uploadedDocumentsList.Contains(documentElement))
            uploadedDocumentsList.Remove(documentElement);
        documentElements.RemoveAt(currentDocumentIndex);

        RemoveRadioButton();

        instance.currentDocumentIndex = -1;
    }
    #endregion

    #region Send Documents To Appraiser
    public void SendDocumentsToAppraiser()
    {
        AppraiserListScreen.ShowAppraiserListScreen(AppraiserSelected, false);
    }
    private void AppraiserSelected(string appraiserName)
    {
        if (uploadedDocumentsList.Count == 0)
        {
            CustomLogger.LogErrorMessage("AppraiserSelected 'uploadedDocumentsList' is empty!");
            return;
        }

        string rawDocumentsData = string.Empty;
        for (int i = 0; i < uploadedDocumentsList.Count; ++i)
        {
            if (rawDocumentsData != string.Empty)
                rawDocumentsData += "_";

            rawDocumentsData += uploadedDocumentsList[i].GetDocumentUrl() + "\\"
                + (int)uploadedDocumentsList[i].GetDocumentType();
        }

        PlayerIOServerManager.SendDocumentToAppraiserMessage(appraiserName, rawDocumentsData);
    }

    #region Send Documents To Appraiser Success
    public static bool AddDocumentToAppraiserResult(bool result)
    {
        if (isNullInstance)
            return false;

        if (!instance.adminDocumentsScreenObj.activeSelf)
            return false;

        if (result)
            ShowAddDocumentSuccess();
        return true;
    }
    private static void ShowAddDocumentSuccess()
    {
        instance.addDocumentSuccessScreen.SetActive(true);
        instance.StartCoroutine(instance.AutoCloseSuccessCardScreen());
    }
    private IEnumerator AutoCloseSuccessCardScreen()
    {
        yield return new WaitForSeconds(2f);
        addDocumentSuccessScreen.SetActive(false);
        HideAdminDocumentsScreen();
    }
    #endregion

    #endregion

    #region Send Documents To Emails
    public void SendDocumentsToEmails()
    {
        EmailsListScreen.ShowEmailsListScreen(SendDocumentsToEmails);
    }

    public void SendDocumentsToEmails(string[] emails)
    {
        Dictionary<string, GlobalDocuments.DocumentType> documentUrls = 
            new Dictionary<string, GlobalDocuments.DocumentType>();

        for (int i = 0; i < uploadedDocumentsList.Count; ++i)
        {
            AdminDocumentElement closestElement = uploadedDocumentsList[i];
            documentUrls.Add(closestElement.GetDocumentUrl(), closestElement.GetDocumentType());
        }
        
        foreach (string email in emails)
        {
            EmailSender.SendDocumentUrls(email, "Documents", documentUrls);
        }
    }
    #endregion

    #region Print Document
    public void PrintCurrentDocument()
    {
        Texture2D printTexture = documentElements[currentDocumentIndex].GetDocumentSprite().texture;
        PrntPlatformClass.ColorModeEnum colorMode = 
            PrntPlatformClass.ColorModeEnum.COLOR_MODE_MONOCHROME;
        PrntPlatformClass.OrientationEnum orientationMode = 
            PrntPlatformClass.OrientationEnum.ORIENTATION_LANDSCAPE;
        PrntPlatformClass.ScaleModeEnum scaleMode =
            PrntPlatformClass.ScaleModeEnum.SCALE_MODE_FILL;

        PrntPlatformClass.Print("UNITY TEST",
            printTexture, colorMode, orientationMode, scaleMode);
    }
    #endregion

    #region Unlock Busy
    public static void UnlockBusy()
    {
        if (isNullInstance)
            return;

        instance.isBusy = false;
    }
    #endregion

    #region Getters
    public static Sprite GetCurrentDocumentSprite()
    {
        if (isNullInstance)
            return null;

        return instance.documentElements[instance.currentDocumentIndex].GetDocumentSprite();
    }

    public static string GetCurrentDocumentUrl()
    {
        if (isNullInstance)
            return string.Empty;

        return instance.documentElements[instance.currentDocumentIndex].GetDocumentUrl();
    }

    public static GlobalDocuments.DocumentType GetCurrentDocumentType()
    {
        if (isNullInstance)
            return GlobalDocuments.DocumentType.Unknown;

        return instance.documentElements[instance.currentDocumentIndex].GetDocumentType();
    }
    #endregion 
}
