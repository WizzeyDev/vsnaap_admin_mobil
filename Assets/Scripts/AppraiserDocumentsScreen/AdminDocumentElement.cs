﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GlobalDocuments;

public class AdminDocumentElement : MonoBehaviour
{
    #region Serializable Fields
    [SerializeField]
    protected Image photoImg;
    [SerializeField]
    private GameObject fullViewPhotoBtn;
    #endregion

    #region Private Fields
    protected DocumentType documentType;

    protected string documentLocalPath;
    protected string documentUrl;
    protected string rawDocumentData;
    #endregion

    #region Setup
    public void SetupDocumentElement(string documentLocalPath, Sprite photoSprite, DocumentType documentType)
    {
        this.documentLocalPath = documentLocalPath;
        this.documentType = documentType;

        if (this.documentType == DocumentType.Photo)
            SetDocumentImage(photoSprite);
        else
        {
            Sprite sprite = GetDocumentSpriteByType(this.documentType);
            if(sprite == null)
            {
                Debug.LogError("SetupPhotoElement sprite is null!");
                return;
            }
            SetDocumentImage(sprite);
        }

        if (documentType == DocumentType.Photo)
            fullViewPhotoBtn.SetActive(true);

        rawDocumentData = documentLocalPath + "\\" + (int)documentType;
    }
    #endregion

    public void FullViewPhoto()
    {
        FullViewImageScreen.ShowFullViewImageScreen(photoImg.sprite);
    }

    #region Setters
    public void SetDocumentUrl(string documentUrl)
    {
        this.documentUrl = documentUrl;
    }
    private void SetDocumentImage(Sprite sprite)
    {
        photoImg.sprite = sprite;
    }
    #endregion

    #region Getters
    public string GetDocumentUrl()
    {
        return documentUrl;
    }
    public string GetDocumentLocalPath()
    {
        return documentLocalPath;
    }

    public Sprite GetDocumentSprite()
    {
        return photoImg.sprite;
    }

    public DocumentType GetDocumentType()
    {
        return documentType;
    }

    public string GetRawData()
    {
        return rawDocumentData;
    }
    #endregion
}
