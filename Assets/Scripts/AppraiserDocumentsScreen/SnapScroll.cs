﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapScroll
{
    #region Private Fields
    private MonoBehaviour owner;

    private RectTransform contentRect;

    private ScrollRect scrollView;

    private List<RectTransform> scrollElementRects;

    private const float scrollSpeed = 1500;
    private const float magnetizeVelocityBorder = 400f;

    private float closestElementDist;
    private int closestElementIndex = -1;

    private bool isScrolling, isEnable;
    #endregion

    #region Setup
    public SnapScroll(MonoBehaviour owner, RectTransform contentRect, ScrollRect scrollView,
        List<RectTransform> scrollElementRects)
    {
        this.owner = owner;
        this.contentRect = contentRect;
        this.scrollView = scrollView;
        this.scrollElementRects = scrollElementRects;
    }

    public void ActivateSnapScroll()
    {
        isEnable = true;
    }

    #endregion

    #region Scrolling Input
    public void Scrolling(bool state)
    {
        isScrolling = state;
        if (state)
            scrollView.inertia = true;
    }

    public void Reset()
    {
        isEnable = false;
        isScrolling = false;
    }
    #endregion

    #region Scrolling Update
    public void FixedUpdate()
    {
        if (!isEnable || scrollElementRects.Count == 0)
            return;

        FindClosestScrollElementIndex();
        if (closestElementIndex == -1)
        {
            return;
        }

        if (!isScrolling)
        {
            float scrollVelocity = Mathf.Abs(scrollView.velocity.x);

            if (scrollVelocity < magnetizeVelocityBorder && closestElementDist > 0f)
            {
                AfterScrollingMagnetize(scrollVelocity, scrollElementRects[closestElementIndex]);
            }
        }
    }

    private void FindClosestScrollElementIndex()
    { // при УСЛОВИИ что Content имеет привязку к MiddleLeft, anchoredPosition у Content'а при
      // визуальном наведении на самый ЛЕВЫЙ элемент должена иметь 0 по x, при условии что RectTransform ЭЛЕМЕНТА
      // имеет pivot == Vector2(0f, 1f), НО очень важно, что бы размер Content'а с 1 элементом был равен
      // размеру самого этого элемента, иначе будет погрешность, из-за которой будет дёрганье
        closestElementDist = float.MaxValue;
        closestElementIndex = -1;
        for (int i = 0; i < scrollElementRects.Count; ++i)
        {
            var coordDist = Mathf.Abs( (-contentRect.anchoredPosition.x) - scrollElementRects[i].anchoredPosition.x); // -x, так как прокрутка всегда противоположна
            if (coordDist < closestElementDist)
            {
                closestElementDist = coordDist;
                closestElementIndex = i;
            }
        }
    }
    private void AfterScrollingMagnetize(float scrollVelocity, RectTransform targetScrollElement)
    {
        if (scrollVelocity < magnetizeVelocityBorder)
            scrollView.inertia = false;

        float targetPositionX = scrollElementRects[closestElementIndex].anchoredPosition.x;
        Vector2 targetPosition = new Vector2(-targetPositionX, contentRect.anchoredPosition.y);
        contentRect.anchoredPosition = Vector2.MoveTowards(contentRect.anchoredPosition, targetPosition,
            scrollSpeed * Time.deltaTime);

        scrollView.velocity = Vector2.zero;
    }
    #endregion

    #region Getters
    public int GetClosestElementIndex()
    {
        return closestElementIndex;
    }
    #endregion
}
